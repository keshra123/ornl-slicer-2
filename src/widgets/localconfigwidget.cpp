#include "widgets/localconfigwidget.h"

#include "ui_local_config_widget.h"

namespace ORNL
{
    LocalConfigWidget::LocalConfigWidget(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::LocalConfigWidget)
        , m_preferences_manager(PreferencesManager::getInstance())
    {
        ui->setupUi(this);
    }

    LocalConfigWidget::~LocalConfigWidget()
    {
        delete ui;
    }

    void LocalConfigWidget::setConfigType(ConfigTypes config_type)
    {
        /*
        m_config_type = config_type;

        ui->categoriesList->clear();
        switch(config_type)
        {
            case kMachine:
                ui->categoriesList->addItems(Constants::MachineSettings::kCategories);
                m_category_widgets[Constants::MachineSettings::kDimensionsCategory]
        = new MachineDimensionsCategoryWidget();
                m_category_widgets[Constants::MachineSettings::kSyntaxCategory]
        = new MachineSyntaxCategoryWidget(); break; case kMaterial:
                ui->categoriesList->addItems(Constants::MaterialSettings::kCategories);
                m_category_widgets[Constants::MaterialSettings::kPropertiesCategory]
        = new MaterialPropertiesCategoryWidget(); break; case kPrint:
                ui->categoriesList->addItems(Constants::PrintSettings::kCategories);
                m_category_widgets[Constants::PrintSettings::kPerimetersCategory]
        = new PrintPerimeterCategoryWidget();
                m_category_widgets[Constants::PrintSettings::kInsetsCategory] =
        new PrintInsetsCategoryWidget();
                m_category_widgets[Constants::PrintSettings::kInfillCategory] =
        new PrintInfillCategoryWidget();
                m_category_widgets[Constants::PrintSettings::kSkinsCategory] =
        new PrintSkinCategoryWidget(); break;
        }

        foreach (SettingsCategoryWidgetBase* scwb, m_category_widgets.values())
        { ui->rightVerticalLayout->addWidget(scwb); scwb->hide();
        }

        ui->categoriesList->setCurrentRow(0);
        handleCategorySelectionChanged();
        connect(ui->categoriesList,
        SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this,
        SLOT(handleCategorySelectionChanged()));
        */
    }

    void LocalConfigWidget::setCurrentConfig(SettingsBase* temp,
                                             SettingsBase* undo_temp)
    {
        /*
        m_temp_settings = temp;
        m_undo_temp_settings = undo_temp;

        if(!m_current_category.isEmpty())
        {
            m_category_widgets[m_current_category]->setSettings(m_temp_settings,
        m_undo_temp_settings);
        }
        */
    }

    void LocalConfigWidget::handleCategorySelectionChanged()
    {
        /*
        // Hide previous category
        if(!m_current_category.isEmpty())
        {
            m_category_widgets[m_current_category]->hide();
        }

        m_current_category = ui->categoriesList->currentItem()->text();

        if(m_current_category.isEmpty())
        {
            return;
        }

        m_category_widgets[m_current_category]->show();
        */
    }
}  // namespace ORNL
