#ifndef MESHVIEWWIDGET_H
#define MESHVIEWWIDGET_H

#include <QObject>
#include <QSharedPointer>

class QWidget;

namespace Qt3DCore
{
    class QEntity;
    class QTransform;
}  // namespace Qt3DCore

namespace Qt3DExtras
{
    class Qt3DWindow;
}
namespace Qt3DRender
{
    class QPointLight;
    class QCamera;
}  // namespace Qt3DRender

namespace ORNL
{
    class MeshGraphics;
    class CameraController;

    class MeshViewController : public QObject
    {
        Q_OBJECT
    public:
        MeshViewController();

        ~MeshViewController();

        QWidget* widget();

    public slots:
        //! \brief Set the parent of each mesh entity to the root entity
        void addToWindow(MeshGraphics* mesh_graphic);

    private:
        Qt3DExtras::Qt3DWindow* m_window;
        QWidget* m_widget;
        Qt3DCore::QEntity* m_root_entity;

        // Light
        Qt3DCore::QEntity* m_light_entity;
        Qt3DRender::QPointLight* m_point_light;
        Qt3DCore::QTransform* m_light_transform;

        Qt3DRender::QCamera* m_camera;
        // QSharedPointer<CameraController> m_camera_controller;
    };
}  // namespace ORNL


#endif  // MESHVIEWWIDGET_H
