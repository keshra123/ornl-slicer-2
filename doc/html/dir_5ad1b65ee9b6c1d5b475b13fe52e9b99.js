var dir_5ad1b65ee9b6c1d5b475b13fe52e9b99 =
[
    [ "closepolygonresult.cpp", "closepolygonresult_8cpp.html", null ],
    [ "closepolygonresult.h", "closepolygonresult_8h.html", [
      [ "ClosePolygonResult", "class_o_r_n_l_1_1_close_polygon_result.html", "class_o_r_n_l_1_1_close_polygon_result" ]
    ] ],
    [ "gapcloserresult.cpp", "gapcloserresult_8cpp.html", null ],
    [ "gapcloserresult.h", "gapcloserresult_8h.html", [
      [ "GapCloserResult", "class_o_r_n_l_1_1_gap_closer_result.html", "class_o_r_n_l_1_1_gap_closer_result" ]
    ] ],
    [ "meshface.cpp", "meshface_8cpp.html", null ],
    [ "meshface.h", "meshface_8h.html", [
      [ "MeshFace", "class_o_r_n_l_1_1_mesh_face.html", "class_o_r_n_l_1_1_mesh_face" ]
    ] ],
    [ "meshvertex.cpp", "meshvertex_8cpp.html", null ],
    [ "meshvertex.h", "meshvertex_8h.html", [
      [ "MeshVertex", "class_o_r_n_l_1_1_mesh_vertex.html", "class_o_r_n_l_1_1_mesh_vertex" ]
    ] ],
    [ "possiblestitch.cpp", "possiblestitch_8cpp.html", null ],
    [ "possiblestitch.h", "possiblestitch_8h.html", [
      [ "PossibleStitch", "class_o_r_n_l_1_1_possible_stitch.html", "class_o_r_n_l_1_1_possible_stitch" ]
    ] ],
    [ "regiongenerator.cpp", "regiongenerator_8cpp.html", null ],
    [ "regiongenerator.h", "regiongenerator_8h.html", [
      [ "RegionGenerator", "class_o_r_n_l_1_1_region_generator.html", null ]
    ] ],
    [ "slicer.cpp", "slicer_8cpp.html", null ],
    [ "slicer.h", "slicer_8h.html", [
      [ "Slicer", "class_o_r_n_l_1_1_slicer.html", "class_o_r_n_l_1_1_slicer" ]
    ] ],
    [ "slicerlayer.cpp", "slicerlayer_8cpp.html", "slicerlayer_8cpp" ],
    [ "slicerlayer.h", "slicerlayer_8h.html", [
      [ "SlicerLayer", "class_o_r_n_l_1_1_slicer_layer.html", "class_o_r_n_l_1_1_slicer_layer" ]
    ] ],
    [ "slicersegment.cpp", "slicersegment_8cpp.html", null ],
    [ "slicersegment.h", "slicersegment_8h.html", [
      [ "SlicerSegment", "class_o_r_n_l_1_1_slicer_segment.html", "class_o_r_n_l_1_1_slicer_segment" ]
    ] ],
    [ "sparsegrid.cpp", "sparsegrid_8cpp.html", null ],
    [ "sparsegrid.h", "sparsegrid_8h.html", [
      [ "SparseGrid", "class_o_r_n_l_1_1_sparse_grid.html", "class_o_r_n_l_1_1_sparse_grid" ]
    ] ],
    [ "sparsepointgrid.cpp", "sparsepointgrid_8cpp.html", null ],
    [ "sparsepointgrid.h", "sparsepointgrid_8h.html", [
      [ "SparsePointGrid", "class_o_r_n_l_1_1_sparse_point_grid.html", "class_o_r_n_l_1_1_sparse_point_grid" ]
    ] ],
    [ "terminus.cpp", "terminus_8cpp.html", null ],
    [ "terminus.h", "terminus_8h.html", [
      [ "Terminus", "class_o_r_n_l_1_1_terminus.html", "class_o_r_n_l_1_1_terminus" ]
    ] ],
    [ "terminustrackingmap.cpp", "terminustrackingmap_8cpp.html", null ],
    [ "terminustrackingmap.h", "terminustrackingmap_8h.html", [
      [ "TerminusTrackingMap", "class_o_r_n_l_1_1_terminus_tracking_map.html", "class_o_r_n_l_1_1_terminus_tracking_map" ]
    ] ]
];