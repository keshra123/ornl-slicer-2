#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H

//! \file preferenceswindow.h

#include <QCloseEvent>
#include <QColor>
#include <QColorDialog>
#include <QFileDialog>
#include <QMainWindow>
#include <QStandardPaths>

#include "managers/preferences_manager.h"
#include "managers/window_manager.h"
#include "utilities/constants.h"
#include "utilities/richtextitemdelegate.h"

namespace Ui
{
    class PreferencesWindow;
}

namespace ORNL
{
    class WindowManager;

    /*!
     * \class PreferencesWindow
     * \brief Window that allows the user to change his/her preferences
     */
    class PreferencesWindow : public QMainWindow
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit PreferencesWindow(QWidget* parent = 0);

        //! \brief Destructor
        ~PreferencesWindow();

    private slots:
        //! \brief Tell the preferences manager to import preferences from file
        void importPreferences();

        //! \brief Tell the preferences manager to export preferences to file
        void exportPreferences();

    private:
        //! \brief Handles closing of the window and clearing the pointer in the
        //! window manager
        void closeEvent(QCloseEvent* event);

        Ui::PreferencesWindow* ui;

        QSharedPointer< WindowManager > m_window_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;
    };  // class PreferencesWindow
}  // namespace ORNL
#endif  // PREFERENCESWINDOW_H
