#include "windows/local_settings_window.h"

#include <QCloseEvent>

#include "ui_local_settings_window.h"

namespace ORNL
{
    LocalSettingsWindow::LocalSettingsWindow(QString mesh_name,
                                             int layer_number)
        : QMainWindow()
        , ui(new Ui::LocalSettingsWindow)
        , m_project_manager(ProjectManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
    {
        ui->setupUi(this);
        /*

        ui->machine->setConfigType(ConfigTypes::kMachine);
        ui->material->setConfigType(ConfigTypes::kMaterial);
        ui->print->setConfigType(ConfigTypes::kPrint);

        ui->meshComboBox->addItems(m_project_manager->meshNameList());

        if(!mesh_name.isEmpty())
        {
            Mesh* mesh = m_project_manager->mesh(mesh_name);

            int mesh_index = ui->meshComboBox->findText(mesh_name);
            ui->meshComboBox->setCurrentIndex(mesh_index);



                QStringList layer_list;
                for(int i = 1; i <= mesh->size(); i++)
                {
                    layer_list << QString::number(i);
                }
                ui->layerComboBox->addItems(layer_list);

                ui->meshAddButton->hide();


                if(layer_number != -1)
                {
                    Layer* layer = mesh->getLayer(layer_number);
                    ui->layerComboBox->setCurrentIndex(layer_number - 1);

                    m_layer_temp_settings[model_name] = QMap<int, QMap<int,
        SettingsBase*> >(); m_layer_temp_settings[model_name][mesh_number] =
        QMap<int, SettingsBase*>();
                    m_layer_temp_settings[model_name][mesh_number][layer_number]
        = new SettingsBase(layer);

                    m_layer_undo_settings[model_name] = QMap<int, QMap<int,
        SettingsBase*> >(); m_layer_undo_settings[model_name][mesh_number] =
        QMap<int, SettingsBase*>();
                    m_layer_undo_settings[model_name][mesh_number][layer_number]
        = new SettingsBase(layer);

                    ui->machine->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);
                    ui->material->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);
                    ui->print->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);

                    ui->layerAddButton->hide();
                }
                // No Layer number, so this is a mesh specific setting
                else
                {
                    ui->layerComboBox->setCurrentIndex(-1);

                    m_mesh_temp_settings[mesh_name] = = new SettingsBase(mesh);

                    m_mesh_undo_settings[model_name] = QMap<int,
        SettingsBase*>(); m_mesh_undo_settings[model_name][mesh_number] = new
        SettingsBase();

                    ui->machine->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);
                    ui->material->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);
                    ui->print->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);

                    ui->layerComboBox->hide();
                    ui->layerDeleteButton->hide();
                }
            }
            // No mesh number, so this is a model specific setting
            else
            {
                ui->meshComboBox->setCurrentIndex(-1);
                ui->layerComboBox->setCurrentIndex(-1);

                m_model_temp_settings[model_name] = new SettingsBase(model);
                m_model_undo_settings[model_name] = new SettingsBase();

                ui->machine->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);
                ui->material->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);
                ui->print->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);

                ui->meshComboBox->hide();
                ui->meshDeleteButton->hide();

                ui->layerAddButton->hide();
                ui->layerComboBox->hide();
                ui->layerLabel->hide();
                ui->layerDeleteButton->hide();
            }
        }
        // No model name (shouldn't happen...)
        else
        {
            //emit error("No model name);
        }

        connect(ui->meshAddButton, SIGNAL(clicked(bool)), this,
        SLOT(addMeshSettings())); connect(ui->meshDeleteButton,
        SIGNAL(clicked(bool)), this, SLOT(removeMeshSettings()));
        connect(ui->layerAddButton, SIGNAL(clicked(bool)), this,
        SLOT(addLayerSettings())); connect(ui->layerDeleteButton,
        SIGNAL(clicked(bool)), this, SLOT(removeLayerSettings()));

        connect(ui->modelComboBox, SIGNAL(currentIndexChanged(QString)), this,
        SLOT(changeModel(QString))); connect(ui->meshComboBox,
        SIGNAL(currentIndexChanged(QString)), this, SLOT(changeMesh()));
        connect(ui->layerComboBox, SIGNAL(currentIndexChanged(QString)), this,
        SLOT(changeLayer()));

        connect(ui->saveButton, SIGNAL(clicked(bool)), this, SLOT(save()));
        */
    }

    LocalSettingsWindow::~LocalSettingsWindow()
    {
        delete ui;
    }

    void LocalSettingsWindow::closeEvent(QCloseEvent* event)
    {
        m_window_manager->removeLocalSettingsWindow();
        event->accept();
    }

    void LocalSettingsWindow::addMeshSettings()
    {
        /*
        ui->meshComboBox->clear();
        ui->meshComboBox->addItems(m_project_manager->meshNumberList(ui->modelComboBox->currentText()));
        ui->meshComboBox->show();
        ui->meshAddButton->hide();
        ui->meshDeleteButton->show();

        ui->layerLabel->show();
        ui->layerAddButton->show();
        */
    }

    void LocalSettingsWindow::removeMeshSettings()
    {
        /*
        ui->meshComboBox->setCurrentIndex(-1);
        ui->meshComboBox->hide();
        ui->meshComboBox->clear();
        ui->meshDeleteButton->hide();
        ui->meshAddButton->show();

        ui->layerComboBox->setCurrentIndex(-1);
        ui->layerLabel->hide();
        ui->layerComboBox->hide();
        ui->layerAddButton->hide();
        ui->layerDeleteButton->hide();
        */
    }

    void LocalSettingsWindow::addLayerSettings()
    {
        /*
        ui->layerComboBox->clear();
        uint number_of_layers =
        m_project_manager->mesh(ui->modelComboBox->currentText(),
        ui->meshComboBox->currentText().toInt())->numberOfLayers(); QStringList
        layer_list; for(uint i = 1; i <= number_of_layers; i++)
        {
            layer_list << QString::number(i);
        }
        ui->layerComboBox->addItems(layer_list);
        ui->layerComboBox->show();
        ui->layerAddButton->hide();
        ui->layerDeleteButton->show();
        */
    }

    void LocalSettingsWindow::removeLayerSettings()
    {
        /*
        ui->layerComboBox->setCurrentIndex(-1);
        ui->layerComboBox->hide();
        ui->layerComboBox->clear();
        ui->layerAddButton->show();
        ui->layerDeleteButton->hide();
        */
    }

    void LocalSettingsWindow::changeModel(QString model_name)
    {
        /*
        if(!m_model_temp_settings.contains(model_name))
        {
            Model* model = m_project_manager->model(model_name);
            m_model_temp_settings[model_name] = new SettingsBase(model);
            m_model_undo_settings[model_name] = new SettingsBase();
        }

        ui->machine->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);
        ui->material->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);
        ui->print->setCurrentConfig(m_model_temp_settings[model_name],
        m_model_undo_settings[model_name]);

        removeMeshSettings();
        */
    }

    void LocalSettingsWindow::changeMesh()
    {
        /*
        QString model_name = ui->modelComboBox->currentText();

        // Create map for model if no mesh settings have been previously created
        if(!m_mesh_temp_settings.contains(model_name))
        {
            m_mesh_temp_settings[model_name] = QMap<int, SettingsBase*>();
            m_mesh_undo_settings[model_name] = QMap<int, SettingsBase*>();
        }

        int mesh_number = ui->meshComboBox->currentText().toInt();

        // Create map for mesh if not previously created
        if(!m_mesh_temp_settings[model_name].contains(mesh_number))
        {
            Mesh* mesh = m_project_manager->mesh(model_name, mesh_number);
            m_mesh_temp_settings[model_name][mesh_number] = new
        SettingsBase(mesh); m_mesh_undo_settings[model_name][mesh_number] = new
        SettingsBase();
        }

        ui->machine->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);
        ui->material->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);
        ui->print->setCurrentConfig(m_mesh_temp_settings[model_name][mesh_number],
        m_mesh_undo_settings[model_name][mesh_number]);

        removeLayerSettings();
        */
    }

    void LocalSettingsWindow::changeLayer()
    {
        /*
        QString model_name = ui->modelComboBox->currentText();

        // Create map for model if no mesh settings have been previously created
        if(!m_layer_temp_settings.contains(model_name))
        {
            m_layer_temp_settings[model_name] = QMap< int, QMap<int,
        SettingsBase*> >(); m_layer_undo_settings[model_name] = QMap< int,
        QMap<int, SettingsBase*> >();
        }

        int mesh_number = ui->meshComboBox->currentText().toInt();

        // Create map for mesh if not previously created
        if(!m_layer_temp_settings[model_name].contains(mesh_number))
        {
            m_layer_temp_settings[model_name][mesh_number] = QMap<int,
        SettingsBase*>(); m_layer_undo_settings[model_name][mesh_number] =
        QMap<int, SettingsBase*>();
        }

        int layer_number = ui->layerComboBox->currentText().toInt();
        if(!m_layer_temp_settings[model_name][mesh_number].contains(layer_number))
        {
            Layer* layer = m_project_manager->mesh(model_name,
        mesh_number)->getLayer(layer_number);
            m_layer_temp_settings[model_name][mesh_number][layer_number] = new
        SettingsBase(layer);
            m_layer_undo_settings[model_name][mesh_number][layer_number] = new
        SettingsBase();
        }

        ui->machine->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);
        ui->material->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);
        ui->print->setCurrentConfig(m_layer_temp_settings[model_name][mesh_number][layer_number],
        m_layer_undo_settings[model_name][mesh_number][layer_number]);
        */
    }

    void LocalSettingsWindow::save()
    {
        /*
        QString model_name = ui->modelComboBox->currentText();
        Model* model = m_project_manager->model(model_name);

        int mesh_number =ui->meshComboBox->currentIndex();
        if(mesh_number > -1)
        {
            Mesh* mesh = model->getMesh(mesh_number);
            int layer_number = ui->layerComboBox->currentIndex();
            // Layer specific setting
            if(layer_number > -1)
            {
                Layer* layer = mesh->getLayer(layer_number);
                layer->update(m_layer_temp_settings[model_name][mesh_number][layer_number]);
                delete
        m_layer_temp_settings[model_name][mesh_number][layer_number];
                m_layer_temp_settings[model_name][mesh_number][layer_number] =
        new SettingsBase(layer); delete
        m_layer_undo_settings[model_name][mesh_number][layer_number];
                m_layer_undo_settings[model_name][mesh_number][layer_number] =
        new SettingsBase();

                //m_window_manager->createLayerRangeDialog(model_name,
        mesh_number, layer_number);
            }
            // Mesh specific setting
            else
            {
                mesh->update(m_mesh_temp_settings[model_name][mesh_number]);
                delete m_mesh_temp_settings[model_name][mesh_number];
                m_mesh_temp_settings[model_name][mesh_number] = new
        SettingsBase(mesh); delete
        m_mesh_undo_settings[model_name][mesh_number];
                m_mesh_undo_settings[model_name][mesh_number] = new
        SettingsBase();
            }
        }
        // Model specific setting
        else
        {
            model->update(m_model_temp_settings[model_name]);
            delete m_model_temp_settings[model_name];
            m_model_temp_settings[model_name] = new SettingsBase(model);
            delete m_model_undo_settings[model_name];
            m_model_undo_settings[model_name] = new SettingsBase();
        }
        */
    }
}  // namespace ORNL
