#include "widgets/settings_category_widgets/print/printperimetercategorywidget.h"

#include "ui_print_perimeter_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    PrintPerimeterCategoryWidget::PrintPerimeterCategoryWidget()
        : ui(new Ui::PrintPerimeterCategoryWidget)
    {
        ui->setupUi(this);

        // Setup the use perimeters checkbox
        ui->enable->label(
            Constants::PrintSettings::Perimeters::Labels::kEnable);
        ui->enable->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kEnable);
        m_checkbox[Constants::PrintSettings::Perimeters::kEnable] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->enable);

        // Setup perimeter extruder id
        ui->extruder_id->label(
            Constants::PrintSettings::Perimeters::Labels::kExtruderId);
        ui->extruder_id->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kExtruderId);
        m_count[Constants::PrintSettings::Perimeters::kExtruderId] =
            QSharedPointer< TemplateCountSettingItem >(ui->extruder_id);

        // Setup the number of perimeter rings textbox
        ui->number->label(
            Constants::PrintSettings::Perimeters::Labels::kNumber);
        ui->number->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kNumber);
        m_count[Constants::PrintSettings::Perimeters::kNumber] =
            QSharedPointer< TemplateCountSettingItem >(ui->number);

        // Setup enable external first
        ui->external_first->label(
            Constants::PrintSettings::Perimeters::Labels::kExternalFirst);
        ui->external_first->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kExternalFirst);
        m_checkbox[Constants::PrintSettings::Perimeters::kExternalFirst] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->external_first);

        // Setup enable skeletons
        ui->enable_skeletons->label(
            Constants::PrintSettings::Perimeters::Labels::kEnableSkeletons);
        ui->enable_skeletons->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kEnableSkeletons);
        m_checkbox[Constants::PrintSettings::Perimeters::kEnableSkeletons] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->enable_skeletons);

        // Setup enable variable width skeletons
        ui->enable_variable_width_skeletons->label(
            Constants::PrintSettings::Perimeters::Labels::
                kEnableVariableWidthSkeletons);
        ui->enable_variable_width_skeletons->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::
                kEnableVariableWidthSkeletons);
        m_checkbox[Constants::PrintSettings::Perimeters::
                       kEnableVariableWidthSkeletons] =
            QSharedPointer< TemplateCheckboxSettingItem >(
                ui->enable_variable_width_skeletons);

        // Setup only retract when crossing perimeters
        ui->only_retract_when_crossing->label(
            Constants::PrintSettings::Perimeters::Labels::
                kOnlyRetractWhenCrossing);
        ui->only_retract_when_crossing->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::
                kOnlyRetractWhenCrossing);
        m_checkbox
            [Constants::PrintSettings::Perimeters::kOnlyRetractWhenCrossing] =
                QSharedPointer< TemplateCheckboxSettingItem >(
                    ui->only_retract_when_crossing);

        // Setup small length
        ui->small_length->label(
            Constants::PrintSettings::Perimeters::Labels::kSmallLength);
        ui->small_length->tooltip(
            Constants::PrintSettings::Perimeters::Tooltips::kSmallLength);
        m_distance[Constants::PrintSettings::Perimeters::kSmallLength] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->small_length);

        initializeSettings();
    }

    PrintPerimeterCategoryWidget::~PrintPerimeterCategoryWidget()
    {
        delete ui;
    }

    void PrintPerimeterCategoryWidget::handleConditions()
    {
        // TODO: add conditional
    }
}  // namespace ORNL
