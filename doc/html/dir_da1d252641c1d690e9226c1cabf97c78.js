var dir_da1d252641c1d690e9226c1cabf97c78 =
[
    [ "cameramanager.cpp", "cameramanager_8cpp.html", "cameramanager_8cpp" ],
    [ "cameramanager.h", "cameramanager_8h.html", "cameramanager_8h" ],
    [ "globalsettingsmanager.cpp", "globalsettingsmanager_8cpp.html", null ],
    [ "globalsettingsmanager.h", "globalsettingsmanager_8h.html", "globalsettingsmanager_8h" ],
    [ "inputmanager.cpp", "inputmanager_8cpp.html", "inputmanager_8cpp" ],
    [ "inputmanager.h", "inputmanager_8h.html", [
      [ "InputManager", "class_o_r_n_l_1_1_input_manager.html", "class_o_r_n_l_1_1_input_manager" ]
    ] ],
    [ "preferencesmanager.cpp", "preferencesmanager_8cpp.html", null ],
    [ "preferencesmanager.h", "preferencesmanager_8h.html", [
      [ "PreferencesManager", "class_o_r_n_l_1_1_preferences_manager.html", "class_o_r_n_l_1_1_preferences_manager" ]
    ] ],
    [ "projectmanager.cpp", "projectmanager_8cpp.html", null ],
    [ "projectmanager.h", "projectmanager_8h.html", [
      [ "ProjectManager", "class_o_r_n_l_1_1_project_manager.html", "class_o_r_n_l_1_1_project_manager" ]
    ] ],
    [ "windowmanager.cpp", "windowmanager_8cpp.html", null ],
    [ "windowmanager.h", "windowmanager_8h.html", [
      [ "WindowManager", "class_o_r_n_l_1_1_window_manager.html", "class_o_r_n_l_1_1_window_manager" ]
    ] ]
];