#ifndef POLYLINE_H
#define POLYLINE_H

#include <QVector>

#include "geometry/point.h"
#include "geometry/polygon.h"
#include "geometry/polygon_list.h"

namespace ORNL
{
    class Polygon;
    class PolygonList;

    /*!
     * \class Polyline
     *
     * \brief List of line segments that form an open path
     */
    class Polyline : public QVector< Point >
    {
    public:
        /*!
         * \brief Constructor
         */
        Polyline() = default;

        /*!
         * \brief Conversion Constructor
         */
        Polyline(const QVector< Point >& path);

        /*!
         * \brief Conversion Constructor
         */
        Polyline(const QVector< ClipperLib::IntPoint >& path);

        /*!
         * \brief Conversion Constructor
         */
        Polyline(const ClipperLib::Path& path);

        /*!
         * \brief Returns whether the polyline is shorter than \p check_length
         *
         * \param check_length The threshold length
         */
        bool shorterThan(Distance check_length) const;

        /*!
         * \brief Returns the closest point to \p rhs
         *
         * \param rhs The point
         * \returns The point on this polyline closest to \p rhs
         */
        Point closestPointTo(const Point& rhs) const;

        /*!
         * \brief Returns a smoothed polyline
         *
         * \param remove_length
         * \returns The smoothed polyline
         */
        Polyline smooth(Distance removeLength);

        /*!
         * \brief Removes consecutive line segments with the same orientation
         *
         * Removes vertices which are connected to line segments which are both
         * too small. Removes vertices which detour from a direct line from the
         * previous and next vertice by a too small amount.
         *
         * \param smallest_line_segment_squared
         * \param allowed_error_distance_squared
         * \returns The simplified polyline
         */
        Polyline simplify(Area smallest_line_segment_squared,
                          Area allowed_error_distance_squared);

        /*!
         * \brief Close this polyline into a polygon
         * \returns The closed polygon
         */
        Polygon close();

        /*!
         * \brief Rotates the polyline around the origin
         *
         * \param rotation_angle Angle to rotate
         * \param axis The axis to rotate around
         * \returns The rotated polyline
         */
        Polyline rotate(Angle rotation_angle, QVector3D axis = {0, 0, 1});

        /*!
         * \brief Rotates the polyline around a point
         *
         * \param center The point to rotate around
         * \param rotation_angle Angle to rotate
         * \param axis The axis to rotate around
         * \returns The rotated polyline
         */
        Polyline rotateAround(Point center,
                              Angle rotation_angle,
                              QVector3D axis = {0, 0, 1});

        /*!
         * \brief joins the two polylines together
         *
         * \param rhs The other polyline to join
         * \returns The two polylines joined together
         */
        Polyline operator+(Polyline rhs);

        /*!
         * \brief Creates a polyline that is the current one with \p rhs add on
         *
         * \param rhs The point to add to the polyline
         * \returns A polyline that is the point added to the current polyline
         */
        Polyline operator+(const Point& rhs);

        /*!
         * \brief Joins another polyline with this one
         *
         * \param rhs The other polyline to join
         * \returns A reference to this polyline
         */
        Polyline& operator+=(Polyline rhs);

        /*!
         * \brief Add \p rhs to this polyline
         * \param rhs Point to add
         * \return A reference to this polyline
         */
        Polyline& operator+=(const Point& rhs);

        /*!
         * \brief clips the polyline using the polygon
         *
         * \param rhs The polygon to clip this polyline with
         * \returns A list of polylines
         */
        QVector< Polyline > operator-(const Polygon& rhs);

        /*!
         * \brief clips the polyline using the list of polygons (holes are used
         * correctly)
         *
         * \param rhs The polygon list to clip this polyline with
         * \returns A list of polylines
         */
        QVector< Polyline > operator-(const PolygonList& rhs);

        /*!
         * \brief Returns the intersection of this polyline with another
         *        polyline as a list of points
         *
         * \note: when an entire line segment overlaps, still needs to be
         * determined...
         */
        QVector< Point > operator&(const Polyline& rhs);

        friend class Polygon;
        friend class PolygonList;

    private:
        //! \brief Internal function used for Clipper to get a Clipper::Path
        //! from the current polyline
        ClipperLib::Path operator()() const;
    };
}  // namespace ORNL
#endif  // POLYLINE_H
