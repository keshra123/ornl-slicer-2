#include "controllers/gcodecontroller.h"

#include <QDebug>
#include <QObject>
#include <iostream>

#include "widgets/gcodetextboxwidget.h"


namespace ORNL
{
    GcodeController::GcodeController(QObject* parent,
                                     GcodeTextBoxWidget* textbox)
        : parent(parent)
        , m_textbox(textbox)
        , m_textbox_blockcount(1)
    {
        // TODO: Add connections to the textbox and controller.
        connect(m_textbox,
                SIGNAL(blockCountChanged(int)),
                this,
                SLOT(textboxBlockCountChanged(int)));
        connect(
            m_textbox, SIGNAL(textChanged()), this, SLOT(textboxTextChanged()));
        //        connect(m_textbox, SIGNAL(cursorPositionChanged()), this,
        //        SLOT(textboxCursorPositionChanged()));
        m_gcode_strings.push_back("");
    }

    GcodeController::~GcodeController()
    {}

    void GcodeController::selectionMade(int line_number)
    {}

    void GcodeController::setGcode(QStringList gcode, GcodeSyntax gcode_syntax)
    {
        m_gcode_strings      = gcode;
        m_textbox_blockcount = gcode.size();
        m_parser.selectParser(gcode_syntax);
        m_gcode_commands = m_parser.parse(gcode.begin(), gcode.end());
        m_textbox->setPlainText(gcode.join('\n'));
    }

    void GcodeController::textboxTextChanged()
    {
        if (m_textbox->blockCount() != m_textbox_blockcount)
        {
            return;
        }

        qDebug() << "REPLACED:"
                 << m_gcode_strings.at(m_textbox->getCursorBlockNumber())
                 << "ON LINE" << m_textbox->getCursorBlockNumber() + 1 << "WITH"
                 << m_textbox->document()
                        ->findBlockByNumber(m_textbox->getCursorBlockNumber())
                        .text();

        m_gcode_strings.replace(
            m_textbox->getCursorBlockNumber(),
            m_textbox->document()
                ->findBlockByNumber(m_textbox->getCursorBlockNumber())
                .text());
    }

    void GcodeController::textboxBlockCountChanged(int newBlockCount)
    {
        int difference;
        // Used only for inital setup
        if (newBlockCount == m_textbox_blockcount)
        {
            return;
        }

        difference = std::abs(newBlockCount - m_textbox_blockcount);

        qDebug() << "CURSOR BLOCK NUMBER:" << m_textbox->getCursorBlockNumber();
        qDebug() << "DIFFERENCE:" << difference;


        // Since the new block count is less than the previous, then some data
        // was deleted.
        if (newBlockCount < m_textbox_blockcount)
        {
            qDebug() << "REPLACED:"
                     << m_gcode_strings.at(m_textbox->getCursorBlockNumber())
                     << "ON LINE" << m_textbox->getCursorBlockNumber() + 1
                     << "WITH"
                     << m_textbox->document()
                            ->findBlockByNumber(
                                m_textbox->getCursorBlockNumber())
                            .text();

            m_gcode_strings.replace(
                m_textbox->getCursorBlockNumber(),
                m_textbox->document()
                    ->findBlockByNumber(m_textbox->getCursorBlockNumber())
                    .text());

            for (int i = 0; i < difference; i++)
            {
                qDebug() << "REMOVED:"
                         << m_gcode_strings.at(
                                m_textbox->getCursorBlockNumber() + 1)
                         << "ON LINE"
                         << m_textbox->getCursorBlockNumber() + 2 + i;

                m_gcode_strings.removeAt(m_textbox->getCursorBlockNumber() + 1);
            }
        }
        // Only other option is that the new block count is greater than the old
        // one, thus new data was inserted.
        else
        {
            // Replaces the previous line that the cursor was editing.
            if (m_textbox->getCursorBlockNumber() > difference - 1)
            {
                qDebug() << "REPLACED:"
                         << m_gcode_strings.at(
                                m_textbox->getCursorBlockNumber() - difference)
                         << "ON LINE"
                         << m_textbox->getCursorBlockNumber() - difference + 1
                         << "WITH"
                         << m_textbox->document()
                                ->findBlockByNumber(
                                    m_textbox->getCursorBlockNumber() -
                                    difference)
                                .text();

                m_gcode_strings.replace(
                    m_textbox->getCursorBlockNumber() - difference,
                    m_textbox->document()
                        ->findBlockByNumber(m_textbox->getCursorBlockNumber() -
                                            difference)
                        .text());
            }

            // TODO: Optimizie with iterators.
            for (int i = 0; i < difference; i++)
            {
                qDebug() << "ADDED:"
                         << m_textbox->document()
                                ->findBlockByNumber(
                                    m_textbox->getCursorBlockNumber() - i)
                                .text()
                                .trimmed()
                         << "ON LINE"
                         << m_textbox->getCursorBlockNumber() - i + 1;

                m_gcode_strings.insert(
                    m_textbox->getCursorBlockNumber() - difference + 1,
                    m_textbox->document()
                        ->findBlockByNumber(m_textbox->getCursorBlockNumber() -
                                            i)
                        .text());
            }

            // Replaces the string after the new position of the text cursor.
            if (m_textbox->getCursorBlockNumber() < newBlockCount - 1)
            {
                qDebug() << "REPLACED:"
                         << m_gcode_strings.at(
                                m_textbox->getCursorBlockNumber() + 1)
                         << "ON LINE" << m_textbox->getCursorBlockNumber() + 2
                         << "WITH"
                         << m_textbox->document()
                                ->findBlockByNumber(
                                    m_textbox->getCursorBlockNumber() + 1)
                                .text()
                                .trimmed();

                m_gcode_strings.replace(
                    m_textbox->getCursorBlockNumber() + 1,
                    m_textbox->document()
                        ->findBlockByNumber(m_textbox->getCursorBlockNumber() +
                                            1)
                        .text());
            }
        }

        // Comment out if too large.
        qDebug() << "GCODE STRINGS:" << m_gcode_strings;
        qDebug() << endl;

        m_textbox_blockcount = newBlockCount;
    }
}  // namespace ORNL
