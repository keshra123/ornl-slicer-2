
export DEBIAN_FRONTEND=noninteractive

set -ev

sudo add-apt-repository -y ppa:beineri/opt-qt591-trusty
sudo apt-get update

sudo apt-get --yes install qt59-meta-full \
 qt59base \
 qt59declarative \
 qt59xmlpatterns \
 qt59script \
 qt59tools \
 qt59multimedia \
 qt59svg \
 qt59graphicaleffects \
 qt59imageformats \
 qt59translations \
 qt59quickcontrols \
 qt59quickcontrols2 \
 qt59sensors \
 qt59serialbus \
 qt59serialport \
 qt59x11extras \
 qt59connectivity \
 qt59location \
 qt59websockets \
 qt59webchannel \
 qt593d \
 qt59canvas3d \
 qt59scxml \
 qt59gamepad \
 qt59webengine \
 qt59speech \
 qt59remoteobjects

set +e
