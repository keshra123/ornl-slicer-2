#ifndef TST_DISPLAYCONFIGBASE_H
#define TST_DISPLAYCONFIGBASE_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

#if 0 // TODO fix first

class TestDisplayConfigBase : public QObject
{
    Q_OBJECT
private slots:
    void testSaveAndLoad();
};

DECLARE_TEST(TestDisplayConfigBase)

#endif // TST_DISPLAYCONFIGBASE_H

#endif
