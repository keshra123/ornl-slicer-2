var dir_68267d1309a1af8e8297ef4c3efbcdba =
[
    [ "configs", "dir_5e44f741ef78961d5b384b3270a82363.html", "dir_5e44f741ef78961d5b384b3270a82363" ],
    [ "dialogs", "dir_9870864803860a03def48f6710b1f137.html", "dir_9870864803860a03def48f6710b1f137" ],
    [ "exceptions", "dir_6e33d6500a76933db4361f663e54ab12.html", "dir_6e33d6500a76933db4361f663e54ab12" ],
    [ "gcode", "dir_d02d051f37f6b5eefc66864861ad91b6.html", "dir_d02d051f37f6b5eefc66864861ad91b6" ],
    [ "geometry", "dir_405fd32de3649961a5f009c7a3fe84df.html", "dir_405fd32de3649961a5f009c7a3fe84df" ],
    [ "layer_regions", "dir_b8376f1315b4783328c948b5516ff022.html", "dir_b8376f1315b4783328c948b5516ff022" ],
    [ "loaders", "dir_89d1d1577d9ec970dcea336ea8223af4.html", "dir_89d1d1577d9ec970dcea336ea8223af4" ],
    [ "managers", "dir_da1d252641c1d690e9226c1cabf97c78.html", "dir_da1d252641c1d690e9226c1cabf97c78" ],
    [ "optimizers", "dir_a4ee97eb2dae28df5964868678c00e5b.html", "dir_a4ee97eb2dae28df5964868678c00e5b" ],
    [ "slicer", "dir_57fb0c464b47a148cf5cf624d5a43fe9.html", "dir_57fb0c464b47a148cf5cf624d5a43fe9" ],
    [ "threading", "dir_7e55306ba01a4f64bc8db2780859eb09.html", "dir_7e55306ba01a4f64bc8db2780859eb09" ],
    [ "units", "dir_f4739f0eb21cb15a404f3cea1464656d.html", "dir_f4739f0eb21cb15a404f3cea1464656d" ],
    [ "utilities", "dir_7b5d38f1875f1b693f62ca6a108a1129.html", "dir_7b5d38f1875f1b693f62ca6a108a1129" ],
    [ "widgets", "dir_0abdee562046be6f5823d1ca8c3fd13c.html", "dir_0abdee562046be6f5823d1ca8c3fd13c" ],
    [ "windows", "dir_f584182df4c69fab0b14563b4d535158.html", "dir_f584182df4c69fab0b14563b4d535158" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ]
];