#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

//! \brief cameramanager.h

#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>
#include <Qt3DCore/QTransform>

namespace ORNL
{
    /*!
     * \class CameraManager
     * \brief Manages the camera for an OpenGL widget
     */
    class CameraManager
    {
    public:
        // Constants
        static const QVector3D LocalForward;
        static const QVector3D LocalUp;
        static const QVector3D LocalRight;

        //! \brief Constructor
        CameraManager();

        // Transform By (Add/Scale)
        //! \brief Translate the camera
        void translate(const QVector3D& dt);

        //! \brief Translate the camera
        void translate(float dx, float dy, float dz);

        //! \brief Rotate the camera
        void rotate(const QQuaternion& dr);

        //! \brief Rotate the camera
        void rotate(float angle, const QVector3D& axis);

        //! \brief Rotate the camera
        void rotate(float angle, float ax, float ay, float az);

        /*!
         * \brief Camera orbits around the `point`
         * \param point center of orbit
         * \param angle amount to orbit
         * \param axis axis to rotate around
         */
        void rotateAround(const QVector3D& point,
                          float angle,
                          const QVector3D& axis);

        // Transform To (Setters)
        //! \brief Sets the translation of the camera
        void setTranslation(const QVector3D& t);

        //! \brief Sets the translation of the camera
        void setTranslation(float x, float y, float z);

        //! \brief Sets the rotation of the camera
        void setRotation(const QQuaternion& r);

        //! \brief Sets the rotation of the camera
        void setRotation(float angle, const QVector3D& axis);

        //! \brief Sets the rotation of the camera
        void setRotation(float angle, float ax, float ay, float az);

        /*!
         * \brief lookAt
         * \param eye
         * \param center
         * \param up
         */
        void lookAt(const QVector3D& eye,
                    const QVector3D& center,
                    const QVector3D& up);

        // Accessors
        //! \brief returns the translation
        const QVector3D& translation() const;

        //! \brief returns the rotation
        const QQuaternion& rotation() const;

        //! \brief return the matrix of the world from the camera's perspective
        const QMatrix4x4& toMatrix();

        // Queries
        //! \brief forward from the perspective of the camera
        QVector3D forward() const;

        //! \brief up from the perspective of the camera
        QVector3D up() const;

        //! \brief right from the perspective of the camera
        QVector3D right() const;

    private:
        bool mDirty;
        QVector3D mTranslation;
        QQuaternion mRotation;
        QMatrix4x4 mWorld;

#ifndef QT_NO_DATASTREAM
        friend QDataStream& operator<<(QDataStream& out,
                                       const CameraManager& transform);
        friend QDataStream& operator>>(QDataStream& in,
                                       CameraManager& transform);
#endif
    };  // class CameraManager

// Qt Streams
#ifndef QT_NO_DEBUG_STREAM
    QDebug operator<<(QDebug dbg, const CameraManager& transform);
#endif

#ifndef QT_NO_DATASTREAM
    QDataStream& operator<<(QDataStream& out, const CameraManager& transform);
    QDataStream& operator>>(QDataStream& in, CameraManager& transform);
#endif

    // Constructors
    inline CameraManager::CameraManager()
        : mDirty(true)
    {}

    inline void CameraManager::translate(float dx, float dy, float dz)
    {
        translate(QVector3D(dx, dy, dz));
    }

    inline void CameraManager::rotate(float angle, const QVector3D& axis)
    {
        rotate(QQuaternion::fromAxisAndAngle(axis, angle));
    }

    inline void CameraManager::rotate(float angle, float ax, float ay, float az)
    {
        rotate(QQuaternion::fromAxisAndAngle(ax, ay, az, angle));
    }

    inline void CameraManager::setTranslation(float x, float y, float z)
    {
        setTranslation(QVector3D(x, y, z));
    }

    inline void CameraManager::setRotation(float angle, const QVector3D& axis)
    {
        setRotation(QQuaternion::fromAxisAndAngle(axis, angle));
    }

    inline void CameraManager::setRotation(float angle,
                                           float ax,
                                           float ay,
                                           float az)
    {
        setRotation(QQuaternion::fromAxisAndAngle(ax, ay, az, angle));
    }

    inline const QVector3D& CameraManager::translation() const
    {
        return mTranslation;
    }

    inline const QQuaternion& CameraManager::rotation() const
    {
        return mRotation;
    }
}  // namespace ORNL

Q_DECLARE_TYPEINFO(ORNL::CameraManager, Q_MOVABLE_TYPE);

#endif  // CAMERAMANAGER_H
