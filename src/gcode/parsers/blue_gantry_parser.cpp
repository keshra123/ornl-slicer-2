
#include "gcode/parsers/blue_gantry_parser.h"

#include <QString>

namespace ORNL
{
    BluegantryParser::BluegantryParser()
    {}

    void BluegantryParser::config()
    {}

    void BluegantryParser::G1Handler(QStringList& commands)
    {}

    void BluegantryParser::G21Handler()
    {}

    void BluegantryParser::G90Handler()
    {}

    void BluegantryParser::M67Handler(QStringList& commands)
    {}

    void BluegantryParser::M72Handler(QStringList& commands)
    {}

    void BluegantryParser::M83Handler()
    {}

    void BluegantryParser::M84Handler()
    {}

    void BluegantryParser::M104Handler(QStringList& commands)
    {}

    void BluegantryParser::M109Handler(QStringList& commands)
    {}

    void BluegantryParser::ToolChangeHandler(QStringList& params)
    {}
}  // namespace ORNL
