#ifndef REGION_H
#define REGION_H

#include <QSharedPointer>
#include <QVector>

#ifdef QT_DEBUG
#    include <QPen>
#endif

#include "configs/settings_base.h"
#include "geometry/polygon_list.h"
#include "json.hpp"

class QReadWriteLock;

namespace ORNL
{
    class Island;
    class Path;
    class WriterBase;

    /*!
     * \class Region
     *
     * \brief Partially Abstract Base class for Island regions.
     *        Also handles all the locking for multithreading.
     */
    class Region : public SettingsBase
    {
    public:
        //! \brief Constructor
        Region(Island* parent);

        //! \brief Destructor
        virtual ~Region() = default;

        //! \brief Returns the outline for this region
        PolygonList outline();

        //! \brief Returns a list of paths for this region
        QVector< Path > paths();

        //! \brief Locks the region and runs the child's computeInternal
        //! function
        void compute();

        //! \brief Each region will have its own computeInternal function
        virtual void computeInternal() = 0;

        //! \brief Locks the region and runs the child's orderInternal function
        void order(Point& previous_location);

        //! \brief Each region will have its own orderInternal function
        virtual void orderInternal(Point& previous_location) = 0;

        //! \brief Returns the gcode for this region using the specified
        //! WriterBase
        QString gcode(WriterBase* syntax);

        //! \brief Returns the number of paths for this region
        uint numberPaths();

        //! \brief Returns the path for the specified index
        Path& operator[](int path_nr);

#ifdef DEBUG
        bool isDebugOption(QString option);

        QStringList debugOptions();

        virtual QStringList debugOptionsInternal() = 0;

        void paintDebug(QString option,
                        QPainter* painter,
                        Distance& ratio,
                        int vertical_offset);

        virtual void paintDebugInternal(QString option,
                                        QPainter* painter,
                                        Distance& ratio,
                                        int vertical_offset) = 0;
#endif

    protected:
        template < typename ValueType >
        void initialCache(QString key, ValueType value)
        {
            m_cache_settings[key.toStdString()] = value;
        }

        template < typename KeyType, typename ValueType >
        bool tryUpdateCache(KeyType key, ValueType value)
        {
            if (m_cache_settings.operator[](key)
                    .template get< decltype(value) >() != value)
            {
                m_cache_settings[key] = value;
                return true;
            }
            return false;
        }

        template < typename ValueType >
        bool tryUpdateCache(QString key, ValueType value)
        {
            if (m_cache_settings[key.toStdString()].get< decltype(value) >() !=
                value)
            {
                m_cache_settings[key.toStdString()] = value;
                return true;
            }
            return false;
        }

        template < typename T >
        T cacheValue(QString key)
        {
            return m_cache_settings[key.toStdString()].get< T >();
        }

        //! \brief Order the region based on time
        void orderTime(Point& previous_location, QVector< Path >& paths);

        //! \brief Order the region based on distance
        void orderDistance(Point& previous_location, QVector< Path >& paths);

        //! \brief Order the region based on time with a genetic algorithm
        void orderGenetic(Point& previous_location, QVector< Path >& paths);

        Island* m_parent;
        PolygonList m_outline;
        QVector< Path > m_paths;

        nlohmann::json m_cache_settings;

#ifdef DEBUG
        bool m_debug_dirty;
        QStringList m_debug_options;
#endif
        QSharedPointer< QReadWriteLock > m_lock;

#ifdef DEBUG
        // Painting
        QPen m_polygon_pen;
        QPen m_outline_pen;  //!< Outlines the drawing
        QPen m_centerline_pen;
#endif
    };
}  // namespace ORNL


#endif  // REGION_H
