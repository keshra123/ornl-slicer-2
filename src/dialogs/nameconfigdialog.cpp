#include "dialogs/nameconfigdialog.h"

#include "ui_name_config_dialog.h"
namespace ORNL
{
    NameConfigDialog::NameConfigDialog(QWidget* parent)
        : QDialog(parent)
        , ui(new Ui::NameConfigDialog)
        , m_settings_manager(GlobalSettingsManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
    {
        ui->setupUi(this);

        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

        connect(ui->configNameEdit,
                SIGNAL(textChanged(QString)),
                this,
                SLOT(errorCheck()));
    }

    NameConfigDialog::~NameConfigDialog()
    {
        delete ui;
    }

    void NameConfigDialog::setConfigType(ConfigTypes ct)
    {
        m_config_type = ct;
        switch (ct)
        {
            case ConfigTypes::kMachine:
                ui->configNameLabel->setText("Machine:");
                break;
            case ConfigTypes::kMaterial:
                ui->configNameLabel->setText("Material:");
                break;
            case ConfigTypes::kPrint:
                ui->configNameLabel->setText("Print:");
                break;
            case ConfigTypes::kNozzle:
                ui->configNameLabel->setText("Nozzle:");
                break;
        }
    }

    void NameConfigDialog::setMaterial(QString material)
    {
        m_material = material;
    }

    void NameConfigDialog::accept()
    {
        QString config_base = ui->baseConfigSelector->currentText();
        QString config_name = ui->configNameEdit->text();
        if (m_config_type == ConfigTypes::kNozzle)
        {
            config_name = m_material + ":" + config_name;
        }
        m_settings_manager->addConfig(m_config_type, config_name, config_base);
        m_window_manager->createGlobalSettingsWindow(m_config_type,
                                                     config_name);

        m_window_manager->removeNameConfigDialog();
    }

    void NameConfigDialog::reject()
    {
        m_window_manager->removeNameConfigDialog();
    }

    void NameConfigDialog::errorCheck()
    {
        // If empty, already exists, or contains non alpha numeric character
        // (except spaces, underscores, and periods)
        if (ui->configNameEdit->text().isEmpty() ||
            m_settings_manager->getConfigsNames(m_config_type)
                .contains(ui->configNameEdit->text()) ||
            ui->configNameEdit->text().contains(
                QRegularExpression("[^a-zA-Z0-9\s \\.\\_]")))
        {
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
        else
        {
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        }
    }
}  // namespace ORNL
