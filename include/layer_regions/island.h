#ifndef ISLAND_H
#define ISLAND_H
//! \file island.h

#include "configs/settings_base.h"
#include "geometry/polygon_list.h"
#include "threading/island_thread.h"


namespace ORNL
{
    class Region;
    class WriterBase;
    class Layer;
    class Perimeters;
    class Infill;
    class Skin;

    typedef Perimeters Insets;

    /*!
     * \class Island
     * \brief An individual island on an individual layer
     */
    class Island : public SettingsBase
    {
    public:
        //! \brief Constructor
        Island(Layer* parent);

        //! \brief Constructor
        Island(Layer* parent, PolygonList outlines);

        //! \brief Returns the parent layer for this island
        Layer* parent();

        //! \brief Sets the outlines for the island
        void outlines(PolygonList outlines);

        //! \brief Returns the outlines for the island
        PolygonList outlines();

        //! \brief Returns the perimeter region for this island
        QSharedPointer< Perimeters > perimeters();

        //! \brief Returns the inset region for this island
        QSharedPointer< Insets > insets();

        //! \brief Returns the skin region for this island
        QSharedPointer< Skin > skin();

        //! \brief Returns the infill region for this island
        QSharedPointer< Infill > infill();

        //! \brief order the paths. previous location gets updated to the last
        //! point
        void order(Point& previous_location,
                   RegionType region_type = static_cast< RegionType >(
                       RegionType::kPerimeter | RegionType::kInset |
                       RegionType::kInfill | RegionType::kTopSkin |
                       RegionType::kBottomSkin));

        //! \brief Returns a string with the gcode representing this island
        QString gcode(WriterBase* syntax);

        //! \brief Returns the region specified. Returns nullptr if region that
        //! is not one of the ones the island contains
        QSharedPointer< Region > operator[](RegionType region_type);

    private:
        PolygonList m_outlines;
        QSharedPointer< Perimeters > m_perimeters;
        QSharedPointer< Insets > m_insets;
        QSharedPointer< Infill > m_infill;
        QSharedPointer< Skin > m_skin;

        Layer* m_parent;

        friend class IslandThread;
    };  // class Island
}  // namespace ORNL
#endif  // ISLAND_H
