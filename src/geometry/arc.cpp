#include "geometry/arc.h"

namespace ORNL
{
    Arc::Arc(Point start, Point end, Point center, Angle angle, bool clockwise)
        : m_start(start)
        , m_end(end)
        , m_center(center)
        , m_angle(angle)
        , m_clockwise(clockwise)
    {}

    Point Arc::start() const
    {
        return m_start;
    }

    Point Arc::end() const
    {
        return m_end;
    }

    Point Arc::center() const
    {
        return m_center;
    }

    Angle Arc::angle() const
    {
        return m_angle;
    }

    bool Arc::clockwise() const
    {
        return m_clockwise;
    }
}  // namespace ORNL
