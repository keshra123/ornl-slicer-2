#ifndef GCODETEXTBOXWIDGET_H
#define GCODETEXTBOXWIDGET_H

#include <QObject>
#include <QPlainTextEdit>

#include "gcode/gcode_command.h"
#include "gcodehighlighter.h"

class QPaintEvent;
class QResizeEvent;
class QWidget;

namespace ORNL
{
    class LineNumberDisplay;

    //! \class GcodeTextBoxWidget
    //! \brief This class implements the Gcode textbox seen on the gcodetab
    //! within
    //!        the GUI.
    class GcodeTextBoxWidget : public QPlainTextEdit
    {
        Q_OBJECT

    signals:

    public:
        //! \brief Constructor for the Gcode Textbox.
        //! \param parent Parent class for the textbox.
        GcodeTextBoxWidget(QWidget* parent = nullptr);

        //! \brief This function handles the painting of the line numbers to be
        //! seen
        //!        on the side of the textbox display.
        //! \param event The paint event that lays out the rectangle for the
        //! line numbers.
        void lineNumbersPaintEvent(QPaintEvent* event);

        //! \brief This functiounm, calculates the width of the line numbers
        //! display box based
        //!        on the number of lines within the text document.
        int calculateLineNumbersDisplayWidth();

        //! \brief This function is a convenience funciton that returns the
        //! block number in which
        //!        the cursor is currently highlighted on.
        //! \note The block numbers are 0 indexed, so line number 1 corresponds
        //! to block number 0. \note This function takes into account that
        //! QPlainTextEdit has each block as its
        //!       own line.
        //! \return Block number that cursor is currently highlighted on.
        int getCursorBlockNumber();

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private slots:
        void updateLineNumberDisplayAreaWidth(int blockCount);
        void updateLineNumberDisplayArea(const QRect& rect, int height);

    private:
        QWidget* m_LineNumberDisplayArea;
        GcodeHighlighter m_highlighter;
    };
}  // namespace ORNL
#endif  // GCODETEXTBOXWIDGET_H
