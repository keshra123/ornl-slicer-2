#include "windows/preferences_window.h"

#include "ui_preferences_window.h"
namespace ORNL
{
    PreferencesWindow::PreferencesWindow(QWidget* parent)
        : QMainWindow(parent)
        , ui(new Ui::PreferencesWindow)
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
    {
        ui->setupUi(this);

        // ui->distance_unit_combobox->setItemDelegate(new
        // RichTextItemDelegate());
        ui->distance_unit_combobox->addItems(Constants::Units::kDistanceUnits);
        ui->distance_unit_combobox->setCurrentText(
            m_preferences_manager->getDistanceUnitText());
        connect(ui->distance_unit_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_preferences_manager.data(),
                SLOT(setDistanceUnit(QString)));

        ui->velocity_unit_combobox->addItems(Constants::Units::kVelocityUnits);
        ui->velocity_unit_combobox->setCurrentText(
            m_preferences_manager->getVelocityUnitText());
        // ui->velocity_unit_combobox->setItemDelegate(new
        // RichTextItemDelegate());
        connect(ui->velocity_unit_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_preferences_manager.data(),
                SLOT(setVelocityUnit(QString)));

        ui->acceleration_unit_combobox->addItems(
            Constants::Units::kAccelerationUnits);
        ui->acceleration_unit_combobox->setCurrentText(
            m_preferences_manager->getAccelerationUnitText());
        // ui->acceleration_unit_combobox->setItemDelegate(new
        // RichTextItemDelegate());
        connect(ui->acceleration_unit_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_preferences_manager.data(),
                SLOT(setAccelerationUnit(QString)));

        ui->angle_unit_combobox->addItems(Constants::Units::kAngleUnits);
        ui->angle_unit_combobox->setCurrentText(
            m_preferences_manager->getAngleUnitText());
        // ui->angle_unit_combobox->setItemDelegate(new RichTextItemDelegate());
        connect(ui->angle_unit_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_preferences_manager.data(),
                SLOT(setAngleUnit(QString)));

        ui->time_unit_combobox->addItems(Constants::Units::kTimeUnits);
        ui->time_unit_combobox->setCurrentText(
            m_preferences_manager->getTimeUnitText());
        // ui->time_unit_combobox->setItemDelegate(new RichTextItemDelegate());
        connect(ui->time_unit_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_preferences_manager.data(),
                SLOT(setTimeUnit(QString)));

        connect(ui->actionImport_Preferences,
                SIGNAL(triggered(bool)),
                this,
                SLOT(importPreferences()));
        connect(ui->actionExport_Preferences,
                SIGNAL(triggered(bool)),
                this,
                SLOT(exportPreferences()));
    }

    PreferencesWindow::~PreferencesWindow()
    {
        delete ui;
    }

    void PreferencesWindow::closeEvent(QCloseEvent* event)
    {
        m_window_manager->removePreferencesWindow();
        event->accept();
    }

    void PreferencesWindow::importPreferences()
    {
        QString filepath = QFileDialog::getOpenFileName(
            this,
            "Load .preferences file",
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation),
            "*.preferences");
        if (!filepath.isNull())
        {
            m_preferences_manager->importPreferences(filepath);
            ui->distance_unit_combobox->setCurrentText(
                m_preferences_manager->getDistanceUnitText());
            ui->velocity_unit_combobox->setCurrentText(
                m_preferences_manager->getVelocityUnitText());
            ui->acceleration_unit_combobox->setCurrentText(
                m_preferences_manager->getAccelerationUnitText());
            ui->angle_unit_combobox->setCurrentText(
                m_preferences_manager->getAngleUnitText());
            ui->time_unit_combobox->setCurrentText(
                m_preferences_manager->getTimeUnitText());
        }
    }

    void PreferencesWindow::exportPreferences()
    {
        QString filepath = QFileDialog::getSaveFileName(
            this,
            "Save .preferences file",
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation),
            "*.preferences");
        if (!filepath.isNull())
        {
            m_preferences_manager->exportPreferences(filepath);
        }
    }
}  // namespace ORNL
