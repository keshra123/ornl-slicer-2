#ifndef GCODE_LOADER_H
#define GCODE_LOADER_H

#include <QFile>
#include <QObject>

#include "utilities/enums.h"

class QObject;

namespace ORNL
{
    // TODO: Line number ordering. If present.

    class GcodeLoader : public QObject
    {
        Q_OBJECT
    public:
        //! \brief Contructor that takes the filename or path to a file for
        //! opening and reading
        //!        and a pointer to the parent for connection of signals.
        //! \param filename Filename to open for reading.
        //! \param parent Parent pointer used to connect signals to alert when
        //! finished reading and processing.
        GcodeLoader(QString filename, QObject* parent = nullptr);

        //! \brief Destructor
        ~GcodeLoader();

        //! \brief Function that is to be run once all signals are connected to
        //! allow for,
        //!        the Gcode data and parser to be selected.
        //! \note The signals needed are:
        //!         - finishedGcode once the gcode has completely loaded.
        //!         - finished if the loader loads the file but encounters a
        //!         problem.
        //!         - errorLoadingFile, the loader has enocuntered an error when
        //!         loading the specified gcode file.
        void run();

    signals:

        //! \brief sends the gcode and syntax to the project manager
        void finishedGcode(QStringList gcode, GcodeSyntax syntax);

        //! \brief signal that the file has been loaded
        void finished();

        //! \brief emits error signal
        void errorLoadingFile(QString filename, QString error);

    private:
        //! \brief Returns the filename specified for the object reading.
        QString filename();

        //! \brief Reads a single line from the file, maintaining the file
        //! position state. \return QString The line read from the specified
        //! file, with the newline included.
        QString readLine();

        //! \brief Reads the whole contents of the file from the beginning and
        //! separates thiem into lines,
        //!        This function retains the previous file position.
        //! \return QStringList A newline separated list without the newlines
        //! included.
        QStringList readAll();

        //! \brief This funciton detects the parser to be used from the file
        //! directly.
        //!        The syntax used to find this is through lookign for the
        //!        string "GCODE-Syntax:" and comparing it to all known machine
        //!        strings.
        //! \return GcodeSyntax Syntax enum for choosing a specified parser.
        //! \sa selectParserFromString(QString)
        GcodeSyntax detectParser();


        //! \brief Helper function to throw an IOException.
        //! \param exceptionString Message to be sent with the exception.
        //! \throws IOException Occurs when the function is called
        void throwIOException(QString exceptionString);

        //! \brief Helper function to throw an InvalidParserException.
        //! \throws InvalidParserException Occurs when the function is called
        void throwInvalidParserException();

        //! \brief Helper funciton to select a parser based on its constant
        //! string name. \param parserID String representation of the Gcode
        //! parser. \return The gcode syntax enum that correspondes to that
        //! specific string. \throws InvalidParserException Throws when the
        //! string passed does not match any
        //!         preset parser constants from the
        //!         Constants::MachineSettings:Syntax::<Machine-Name> strings.
        GcodeSyntax selectParserFromString(QString parserID);

        QFile m_gcode_file;
    };
}  // namespace ORNL

#endif  // GCODE_LOADER_H
