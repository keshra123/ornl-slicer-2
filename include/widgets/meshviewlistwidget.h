#ifndef MESHVIEWTREEWIDGET_H
#define MESHVIEWTREEWIDGET_H

#include <QListWidget>

#include "managers/project_manager.h"
#include "managers/window_manager.h"

namespace ORNL
{
    /*!
     * \class MeshViewTreeWidget
     *
     * \brief List Widget that displays the names of the currently loaded in
     * meshes. Allows copy, paste, and removal
     */
    class MeshViewListWidget : public QListWidget
    {
        Q_OBJECT
    public:
        //! \brief constructor
        MeshViewListWidget(QWidget* parent = nullptr);

    public slots:
        //! \brief Update the displayed list with any new meshes
        void updateList(QStringList mesh_name_list);

        //! \brief Display the context menu for the mesh that is right clicked
        void showContextMenu(const QPoint& point);

        //! \brief handles selection of a mesh with the left click
        void select(QListWidgetItem* lwi);

        //! \brief Deselect any selected items
        void deselect();

    private:
        //! \brief Handler for when the mouse is clicked
        void mousePressEvent(QMouseEvent* event);

        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< WindowManager > m_window_manager;
    };  // class MeshViewTreeWidget
}  // namespace ORNL
#endif  // MESHVIEWTREEWIDGET_H
