#ifndef TEMPLATECHECKBOXSETTINGITEM_H
#define TEMPLATECHECKBOXSETTINGITEM_H

#include <QWidget>

namespace Ui
{
    class TemplateCheckboxSettingItem;
}

namespace ORNL
{
    class TemplateCheckboxSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        explicit TemplateCheckboxSettingItem(QWidget* parent = 0);

        //! \brief Destructor
        ~TemplateCheckboxSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        //! \brief Sets the value for the checkbox
        void checked(bool c);

        //! \brief Returns the value for the checkbox
        bool value();

        //! \brief Sets the stylesheet for the checkbox
        void setStyleSheet(const QString& stylesheet);

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        //! \brief Signals that the value has changed
        void valueChanged(bool);

    public slots:
        //! \brief Sets the value for the checkbox
        void value(bool c);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    private slots:
        void handleUndo();
        void handleRedo();
        void handleValueChanged();

    private:
        Ui::TemplateCheckboxSettingItem* ui;
    };
}  // namespace ORNL


#endif  // TEMPLATECHECKBOXSETTINGITEM_H
