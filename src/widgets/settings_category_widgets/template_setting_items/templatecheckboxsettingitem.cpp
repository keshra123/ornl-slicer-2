#include "widgets/settings_category_widgets/template_setting_items/templatecheckboxsettingitem.h"

#include "ui_template_checkbox_setting_item.h"

namespace ORNL
{
    TemplateCheckboxSettingItem::TemplateCheckboxSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplateCheckboxSettingItem)
    {
        ui->setupUi(this);

        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->check_box,
                SIGNAL(stateChanged(int)),
                this,
                SLOT(handleValueChanged()));
    }

    TemplateCheckboxSettingItem::~TemplateCheckboxSettingItem()
    {
        delete ui;
    }

    void TemplateCheckboxSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }


    void TemplateCheckboxSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }


    void TemplateCheckboxSettingItem::value(bool c)
    {
        ui->check_box->setChecked(c);
    }

    bool TemplateCheckboxSettingItem::value()
    {
        return ui->check_box->isChecked();
    }

    void TemplateCheckboxSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->check_box->setStyleSheet(stylesheet);
    }

    void TemplateCheckboxSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplateCheckboxSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplateCheckboxSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplateCheckboxSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplateCheckboxSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplateCheckboxSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplateCheckboxSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
