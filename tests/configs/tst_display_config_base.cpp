#include "configs/tst_display_config_base.h"
#include "configs/display_config_base.h"

using namespace ORNL;

#if 0 // needs to be fixed

void TestDisplayConfigBase::testSaveAndLoad()
{
    DisplayConfigBase* dcb = new DisplayConfigBase("test", "test", nullptr);

    dcb->setSetting<Distance>("distance", 2.0);
    dcb->setSetting<int>("integer", 12);
    dcb->setSetting<double>("double", 2.34);
    dcb->setSetting<bool>("boolean", true);
    dcb->setSetting<QString>("string", "value");

    QString current_directory = QDir::currentPath();

    dcb->saveToFile(current_directory);

    DisplayConfigBase* other_dcb = new DisplayConfigBase("test", "test", nullptr);
    other_dcb->loadFromFile(current_directory);
    QCOMPARE(dcb->json(), other_dcb->json());
}

#endif
