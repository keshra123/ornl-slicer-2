#ifndef SLICERSEGMENT_H
#define SLICERSEGMENT_H

#include "geometry/point.h"

namespace ORNL
{
    class MeshVertex;

    /*!
     * \class CrossSectionSegment
     * \brief
     *
     * Based on the Cura Engine by Ultimaker
     */
    class CrossSectionSegment
    {
    public:
        CrossSectionSegment();

        Point start, end;
        int face_index;
        int end_other_face_idx;  //!< The index of the other face connected via
                                 //!< the edge that created end
        const MeshVertex* end_vertex;
        bool added_to_polygon;
    };
}  // namespace ORNL

#endif  // SLICERSEGMENT_H
