#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

//! \file windowmanager.h

#include <QMultiMap>
#include <QObject>
#include <QSharedPointer>

namespace ORNL
{
    class MainWindow;
    class GlobalSettingsWindow;
    class LocalSettingsWindow;
    class PreferencesWindow;
    class NameConfigDialog;
    class LoadingDialog;
    class SavingDialog;
    class GlobalSettingsManager;
    class PreferencesManager;
    enum class ConfigTypes : uint8_t;

    /*!
     * \class WindowManager
     * \brief The WindowManager class
     */
    class WindowManager : public QObject
    {
        Q_OBJECT
    public:
        //! \brief returns the singleton
        static QSharedPointer< WindowManager > getInstance();

    public slots:
        //! \brief Creates the main window
        void createMainWindow();

        /*!
         * \brief Creates the global settings window that start with the
         * specified config type and config
         */
        void createGlobalSettingsWindow(ConfigTypes configTypes,
                                        QString config);

        /*!
         * \brief Creates the global settings window that start with the
         * specified config type and config
         */
        void createGlobalSettingsWindow(QString config);

        //! \brief Creates the local settings window for a specific mesh
        void createLocalSettingsWindow(QString mesh_name);

        //! \brief Creates the local settings window for a specific layer on a
        //! specific mesh
        void createLocalSettingsWindow(QString mesh_name, int layer_number);

        //! \brief Creates the preferences window
        void createPreferencesWindow();

        //! \brief Creates the dialog for creating configs for nozzle
        void createNameConfigDialog(QString material);

        //! \brief Creates the dialog for creating configs
        void createNameConfigDialog(ConfigTypes ct);

        /*!
         * \brief Creates the dialog for creating configs
         */
        void createNameConfigDialog(int ct);

        //! \brief Creates the dialog for loading models/projects
        void createLoadingDialog(QString filename);

        //! \brief Creates the dialog for saving models/projects
        void createSavingDialog(QString filename);

        //! \brief Creates the dialog for displaying errors
        void createErrorDialog(QString error);

        //! \brief Creates the dialog for displaying errors
        void createErrorDialog(QString filename, QString error);

        //! \brief Remove the global settings window
        void removeGlobalSettingsWindow();

        //! \brief Remove the local settings window
        void removeLocalSettingsWindow();

        //! \brief Remove the preferences window
        void removePreferencesWindow();

        //! \brief Remove the dialog for creating configs
        void removeNameConfigDialog();

        //! \brief Remove the dialog for loading models/projects
        void removeLoadingDialog();

        //! \brief Remove the dialog for saving models/projects
        void removeSavingDialog();

    private:
        WindowManager();

        static QSharedPointer< WindowManager > m_singleton;

        // Managers
        QSharedPointer< GlobalSettingsManager > m_global_settings_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;

        // Windows
        MainWindow* m_main_window;
        GlobalSettingsWindow* m_global_settings_window;
        LocalSettingsWindow* m_local_settings_window;
        PreferencesWindow* m_preferences_window;

        // Dialogs
        NameConfigDialog* m_name_config_dialog;
        LoadingDialog* m_loading_dialog;
        SavingDialog* m_saving_dialog;
        QMultiMap< void*, QMetaObject::Connection > m_connections;
    };  // class WindowManager
}  // namespace ORNL
#endif  // WINDOWMANAGER_H
