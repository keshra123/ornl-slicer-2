#ifndef NAMECONFIGDIALOG_H
#define NAMECONFIGDIALOG_H

//! \file nameconfigdialog.h

#include <QDialog>
#include <QPushButton>

#include "managers/global_settings_manager.h"
#include "managers/window_manager.h"
#include "utilities/enums.h"

namespace Ui
{
    class NameConfigDialog;
}

namespace ORNL
{
    class GlobalSettingsManager;
    class WindowManager;

    /*!
     * \class NameConfigDialog
     * \brief Dialog Window for creating a new config
     */
    class NameConfigDialog : public QDialog
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        explicit NameConfigDialog(QWidget* parent = 0);

        //! \brief Destructor
        ~NameConfigDialog();

        //! \brief Sets which type of config this will create if accepted
        void setConfigType(ConfigTypes ct);

        /*!
         * \brief Sets the material for creating a nozzle config
         *
         * \warning Should only be used for creating a nozzle config
         */
        void setMaterial(QString material);

        //! \brief runs if ok is clicked
        void accept();

        //! \brief runs if closed or cancelled
        void reject();

    public slots:
        //! \brief Checks that the name doesn't already exist
        void errorCheck();

    private:
        Ui::NameConfigDialog* ui;

        QSharedPointer< WindowManager > m_window_manager;
        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        ConfigTypes m_config_type;

        QString m_material;  //!< Only ever used for creating a nozzle config
    };                       // class NameConfigDialog
}  // namespace ORNL
#endif  // NAMECONFIGDIALOG_H
