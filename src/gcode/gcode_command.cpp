#include "gcode/gcode_command.h"

#include <QMap>
#include <QString>
#include <iostream>

namespace ORNL
{
    GcodeCommand::GcodeCommand()
    {
        m_line_number = -1;
        m_command     = 0;
        m_command_id  = -1;
    }

    //! \brief Retrieves the line number of the command
    const int& GcodeCommand::getLineNumber() const
    {
        return m_line_number;
    }

    //! \brief Retrieves the Command
    const char& GcodeCommand::getCommand() const
    {
        return m_command;
    }

    //! \brief Retrieves the Command ID
    const int& GcodeCommand::getCommandID() const
    {
        return m_command_id;
    }

    //! \brief Retrieves all parameters
    const QMap< char, float >& GcodeCommand::getParameters() const
    {
        return m_parameters;
    }

    //! \brief Retrieves the Comment
    const QString& GcodeCommand::getComment() const
    {
        return m_comment;
    }

    void GcodeCommand::setLineNumber(const int line_number)
    {
        m_line_number = line_number;
    }

    void GcodeCommand::setCommand(const char command)
    {
        m_command = command;
    }

    void GcodeCommand::setCommandID(const int commandID)
    {
        m_command_id = commandID;
    }

    void GcodeCommand::setComment(const QString comment)
    {
        m_comment = comment;
    }

    void GcodeCommand::addParameter(const char param_key,
                                    const float param_value)
    {
        m_parameters.insert(param_key, param_value);
    }

    bool GcodeCommand::removeParameter(const char param_key)
    {
        if (m_parameters.remove(param_key) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void GcodeCommand::clearParameters()
    {
        m_parameters.clear();
    }

    void GcodeCommand::clearComment()
    {
        m_comment.clear();
    }

    bool GcodeCommand::operator==(const GcodeCommand& r)
    {
        bool ret = this->getLineNumber() == r.getLineNumber() &&
            this->getCommand() == r.getCommand() &&
            this->getCommandID() == r.getCommandID() &&
            this->getComment() == r.getComment();


        QMap< char, float >::const_iterator temp;
        for (QMap< char, float >::const_iterator i = m_parameters.cbegin();
             i != m_parameters.cend();
             ++i)
        {
            ret &=
                (temp = r.m_parameters.find(i.key())) != r.m_parameters.end();
            ret &= temp.value() == i.value();
        }

        return ret;
    }

    bool GcodeCommand::operator!=(const GcodeCommand& r)
    {
        return !(*this == r);
    }

}  // namespace ORNL
