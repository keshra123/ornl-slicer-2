#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

#include <QSignalMapper>

#include "utilities/mathutils.h"

namespace ORNL
{
    SettingsCategoryWidgetBase::SettingsCategoryWidgetBase()
        : m_preferences_manager(PreferencesManager::getInstance())
        , m_distance_signal_mapper(new QSignalMapper(this))
        , m_velocity_signal_mapper(new QSignalMapper(this))
        , m_acceleration_signal_mapper(new QSignalMapper(this))
        , m_time_signal_mapper(new QSignalMapper(this))
        , m_angle_signal_mapper(new QSignalMapper(this))
        , m_count_signal_mapper(new QSignalMapper(this))
        , m_percentage_signal_mapper(new QSignalMapper(this))
        , m_bool_signal_mapper(new QSignalMapper(this))
        , m_string_signal_mapper(new QSignalMapper(this))
        , m_string_enum_signal_mapper(new QSignalMapper(this))
        , m_undo_signal_mapper(new QSignalMapper(this))
        , m_redo_signal_mapper(new QSignalMapper(this))
        , m_undo_history(nullptr)
        , m_redo_history(nullptr)
        , m_active_text("background: white; color: black")
        , m_inactive_text("background: black; color: gray")
        , m_error_text("background: red; color: black")
        , m_global(false)
    {
        setFocusPolicy(Qt::ClickFocus);
    }

    void SettingsCategoryWidgetBase::initializeSettings()
    {
        if (m_distance.size())
        {
            for (auto it = m_distance.begin(); it != m_distance.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_distance_signal_mapper.data(),
                        SLOT(map()));
                m_distance_signal_mapper->setMapping(it.value().data(),
                                                     it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_distance_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateDistanceSetting(QString)));
        }

        if (m_velocity.size())
        {
            for (auto it = m_velocity.begin(); it != m_velocity.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_velocity_signal_mapper.data(),
                        SLOT(map()));
                m_velocity_signal_mapper->setMapping(it.value().data(),
                                                     it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_velocity_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateVelocitySetting(QString)));
        }

        if (m_acceleration.size())
        {
            for (auto it = m_acceleration.begin(); it != m_acceleration.end();
                 it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_acceleration_signal_mapper.data(),
                        SLOT(map()));
                m_acceleration_signal_mapper->setMapping(it.value().data(),
                                                         it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_acceleration_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateAccelerationSetting(QString)));
        }


        if (m_time.size())
        {
            for (auto it = m_time.begin(); it != m_time.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_time_signal_mapper.data(),
                        SLOT(map()));
                m_time_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_time_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateTimeSetting(QString)));
        }

        if (m_angle.size())
        {
            for (auto it = m_angle.begin(); it != m_angle.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_angle_signal_mapper.data(),
                        SLOT(map()));
                m_angle_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_angle_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateAngleSetting(QString)));
        }

        if (m_count.size())
        {
            for (auto it = m_count.begin(); it != m_count.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(int)),
                        m_count_signal_mapper.data(),
                        SLOT(map()));
                m_count_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_count_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateCountSetting(QString)));
        }

        if (m_percent.size())
        {
            for (auto it = m_percent.begin(); it != m_percent.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(double)),
                        m_percentage_signal_mapper.data(),
                        SLOT(map()));
                m_percentage_signal_mapper->setMapping(it.value().data(),
                                                       it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_percentage_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updatePercentageSetting(QString)));
        }

        if (m_checkbox.size())
        {
            for (auto it = m_checkbox.begin(); it != m_checkbox.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(bool)),
                        m_bool_signal_mapper.data(),
                        SLOT(map()));
                m_bool_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_bool_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateBoolSetting(QString)));
        }

        if (m_string.size())
        {
            for (auto it = m_string.begin(); it != m_string.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(QString)),
                        m_string_signal_mapper.data(),
                        SLOT(map()));
                m_string_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_string_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateStringSetting(QString)));
        }

        if (m_enum.size())
        {
            for (auto it = m_enum.begin(); it != m_enum.end(); it++)
            {
                connect(it.value().data(),
                        SIGNAL(valueChanged(QString)),
                        m_string_enum_signal_mapper.data(),
                        SLOT(map()));
                m_string_enum_signal_mapper->setMapping(it.value().data(),
                                                        it.key());
                connect(it.value().data(),
                        SIGNAL(undo()),
                        m_undo_signal_mapper.data(),
                        SLOT(map()));
                m_undo_signal_mapper->setMapping(it.value().data(), it.key());
                connect(it.value().data(),
                        SIGNAL(redo()),
                        m_redo_signal_mapper.data(),
                        SLOT(map()));
                m_redo_signal_mapper->setMapping(it.value().data(), it.key());
            }

            connect(m_string_enum_signal_mapper.data(),
                    SIGNAL(mapped(const QString&)),
                    this,
                    SLOT(updateStringEnumSetting(QString)));
        }

        connect(m_undo_signal_mapper.data(),
                SIGNAL(mapped(const QString&)),
                this,
                SLOT(undoSetting(QString)));
        connect(m_redo_signal_mapper.data(),
                SIGNAL(mapped(const QString&)),
                this,
                SLOT(redoSetting(QString)));

        setUnits();
    }

    void SettingsCategoryWidgetBase::setSettings(
        QSharedPointer< SettingsBase > temp_settings,
        QSharedPointer< json > undo_history,
        QSharedPointer< json > redo_history)
    {
        m_temp_settings = temp_settings;
        m_undo_history  = undo_history;
        m_redo_history  = redo_history;
        load();
    }

    void SettingsCategoryWidgetBase::load()
    {
        handleConditions();

        // update distance textboxes
        loadUnitSetting(m_distance, m_distance_unit);

        // update velocity textboxes
        loadUnitSetting(m_velocity, m_velocity_unit);

        // update acceleration textboxes
        loadUnitSetting(m_acceleration, m_acceleration_unit);

        // update time textboxes
        loadUnitSetting(m_time, m_time_unit);

        // update angle textboxes
        loadUnitSetting(m_angle, m_angle_unit);

        // update count textboxes
        for (auto it = m_count.begin(); it != m_count.end(); it++)
        {
            // unsaved value for the setting
            if (m_temp_settings->contains(it.key(), false))
            {
                if (m_global ||
                    (!m_active.contains(it.key()) || !m_active[it.key()]))
                {
                    m_count[it.key()]->setStyleSheet(m_active_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = true;
            }
            // saved value for the setting or no value for setting
            else
            {
                // If parent owned mark the row as not active
                if (!m_global &&
                    (!m_active.contains(it.key()) || m_active[it.key()]))
                {
                    m_count[it.key()]->setStyleSheet(m_inactive_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = false;
            }

            int setting = m_temp_settings->setting< int >(it.key());
            // If value changed then update everything
            if (it.value()->value() != setting)
            {
                // If the setting doesn't exist the value is 0
                it.value()->value(setting);
            }

            // Show or hide undo button based on whether it is possible
            if ((*m_undo_history)[it.key().toStdString()].size() ||
                m_temp_settings->contains(it.key(), false))
            {
                it.value()->showUndo();
            }
            else
            {
                it.value()->hideUndo();
            }

            // Show or hide redo button based on whether it is possible
            if ((*m_redo_history)[it.key().toStdString()].size())
            {
                it.value()->showRedo();
            }
            else
            {
                it.value()->hideRedo();
            }
        }

        // update percentage textboxes
        for (auto it = m_percent.begin(); it != m_percent.end(); it++)
        {
            // unsaved value for the setting
            if (m_temp_settings->contains(it.key(), false))
            {
                if (m_global ||
                    (!m_active.contains(it.key()) || !m_active[it.key()]))
                {
                    m_percent[it.key()]->setStyleSheet(m_active_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = true;
            }
            // saved value for the setting or no value for setting
            else
            {
                // If parent owned mark the row as not active
                if (!m_global &&
                    (!m_active.contains(it.key()) || m_active[it.key()]))
                {
                    m_percent[it.key()]->setStyleSheet(m_inactive_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = false;
            }

            double setting = m_temp_settings->setting< double >(it.key());
            // If value changed then update everything
            if (MathUtils::notEquals(it.value()->value(), setting))
            {
                // If the setting doesn't exist the value is 0
                it.value()->value(setting);
            }

            // Show or hide undo button based on whether it is possible
            if ((*m_undo_history)[it.key().toStdString()].size() ||
                m_temp_settings->contains(it.key(), false))
            {
                it.value()->showUndo();
            }
            else
            {
                it.value()->hideUndo();
            }

            // Show or hide redo button based on whether it is possible
            if ((*m_redo_history)[it.key().toStdString()].size())
            {
                it.value()->showRedo();
            }
            else
            {
                it.value()->hideRedo();
            }
        }

        // update bool checkboxes
        for (auto it = m_checkbox.begin(); it != m_checkbox.end(); it++)
        {
            // unsaved value for the setting
            if (m_temp_settings->contains(it.key(), false))
            {
                if (m_global ||
                    (!m_active.contains(it.key()) || !m_active[it.key()]))
                {
                    m_checkbox[it.key()]->setStyleSheet(m_active_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = true;
            }
            // saved value for the setting or no value for setting
            else
            {
                // If parent owned mark the row as not active
                if (!m_global &&
                    (!m_active.contains(it.key()) || m_active[it.key()]))
                {
                    m_checkbox[it.key()]->setStyleSheet(m_inactive_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = false;
            }

            bool setting = m_temp_settings->setting< bool >(it.key());
            // If value changed then update everything
            if (it.value()->value() != setting)
            {
                // If the setting doesn't exist the value is 0
                it.value()->value(setting);
            }

            // Show or hide undo button based on whether it is possible
            if ((*m_undo_history)[it.key().toStdString()].size() ||
                m_temp_settings->contains(it.key(), false))
            {
                it.value()->showUndo();
            }
            else
            {
                it.value()->hideUndo();
            }

            // Show or hide redo button based on whether it is possible
            if ((*m_redo_history)[it.key().toStdString()].size())
            {
                it.value()->showRedo();
            }
            else
            {
                it.value()->hideRedo();
            }
        }

        // update string textboxes
        for (auto it = m_string.begin(); it != m_string.end(); it++)
        {
            // unsaved value for the setting
            if (m_temp_settings->contains(it.key(), false))
            {
                if (m_global ||
                    (!m_active.contains(it.key()) || !m_active[it.key()]))
                {
                    m_string[it.key()]->setStyleSheet(m_active_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = true;
            }
            // saved value for the setting or no value for setting
            else
            {
                // If parent owned mark the row as not active
                if (!m_global &&
                    (!m_active.contains(it.key()) || m_active[it.key()]))
                {
                    m_string[it.key()]->setStyleSheet(m_inactive_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = false;
            }

            QString setting = m_temp_settings->setting< QString >(it.key());
            // If value changed then update everything
            if (it.value()->value() != setting)
            {
                // If the setting doesn't exist the value is 0
                it.value()->value(setting);
            }

            // Show or hide undo button based on whether it is possible
            if ((*m_undo_history)[it.key().toStdString()].size() ||
                m_temp_settings->contains(it.key(), false))
            {
                it.value()->showUndo();
            }
            else
            {
                it.value()->hideUndo();
            }

            // Show or hide redo button based on whether it is possible
            if ((*m_redo_history)[it.key().toStdString()].size())
            {
                it.value()->showRedo();
            }
            else
            {
                it.value()->hideRedo();
            }
        }

        // update dropdowns
        for (auto it = m_enum.begin(); it != m_enum.end(); it++)
        {
            // unsaved value for the setting
            if (m_temp_settings->contains(it.key(), false))
            {
                if (m_global ||
                    (!m_active.contains(it.key()) || !m_active[it.key()]))
                {
                    m_enum[it.key()]->setStyleSheet(m_active_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = true;
            }
            // saved value for the setting or no value for setting
            else
            {
                // If parent owned mark the row as not active
                if (!m_global &&
                    (!m_active.contains(it.key()) || m_active[it.key()]))
                {
                    m_enum[it.key()]->setStyleSheet(m_inactive_text);
                }

                // Active only if the setting comes from the current
                // SettingsBase and not one of its ancestors
                m_active[it.key()] = false;
            }

            QString setting = m_temp_settings->setting< QString >(it.key());
            // If value changed then update everything
            if (it.value()->value() != setting)
            {
                // If the setting doesn't exist the value is 0
                it.value()->value(setting);
            }

            // Show or hide undo button based on whether it is possible
            if ((*m_undo_history)[it.key().toStdString()].size() ||
                m_temp_settings->contains(it.key(), false))
            {
                it.value()->showUndo();
            }
            else
            {
                it.value()->hideUndo();
            }

            // Show or hide redo button based on whether it is possible
            if ((*m_redo_history)[it.key().toStdString()].size())
            {
                it.value()->showRedo();
            }
            else
            {
                it.value()->hideRedo();
            }
        }
    }

    void SettingsCategoryWidgetBase::setUnits()
    {
        // Set the initial values for the units
        updateDistanceUnit();
        updateVelocityUnit();
        updateAccelerationUnit();
        updateAngleUnit();
        updateTimeUnit();

        connect(m_preferences_manager.data(),
                SIGNAL(distanceUnitChanged(Distance, Distance)),
                this,
                SLOT(updateDistanceUnit(Distance)));
        connect(m_preferences_manager.data(),
                SIGNAL(velocityUnitChanged(Velocity, Velocity)),
                this,
                SLOT(updateVelocityUnit(Velocity)));
        connect(m_preferences_manager.data(),
                SIGNAL(accelerationUnitChanged(Acceleration, Acceleration)),
                this,
                SLOT(updateAccelerationUnit(Acceleration)));
        connect(m_preferences_manager.data(),
                SIGNAL(angleUnitChanged(Angle, Angle)),
                this,
                SLOT(updateAngleUnit(Angle)));
        connect(m_preferences_manager.data(),
                SIGNAL(timeUnitChanged(Time, Time)),
                this,
                SLOT(updateTimeUnit(Time)));
    }

    void SettingsCategoryWidgetBase::updateDistanceUnit()
    {
        Distance new_unit = m_preferences_manager->getDistanceUnit();
        updateDistanceUnit(new_unit);
    }

    void SettingsCategoryWidgetBase::updateDistanceUnit(Distance new_unit)
    {
        if (new_unit != m_distance_unit)
        {
            QString unit_text = m_preferences_manager->getDistanceUnitText();
            for (QSharedPointer< TemplateUnitsSettingItem > tusi : m_distance)
            {
                try
                {
                    Distance old = tusi->value() * m_distance_unit;
                    tusi->value(old.to(new_unit));
                }
                // Happens the first time
                catch (SettingValueException e)
                {
                    qDebug() << e.what();
                }

                tusi->unit(unit_text);
            }
            m_distance_unit = new_unit;
        }
    }

    void SettingsCategoryWidgetBase::updateVelocityUnit()
    {
        Velocity new_unit = m_preferences_manager->getVelocityUnit();
        updateVelocityUnit(new_unit);
    }

    void SettingsCategoryWidgetBase::updateVelocityUnit(Velocity new_unit)
    {
        if (new_unit != m_velocity_unit)
        {
            QString unit_text = m_preferences_manager->getVelocityUnitText();
            for (QSharedPointer< TemplateUnitsSettingItem > tusi : m_velocity)
            {
                try
                {
                    Velocity old = tusi->value() * m_velocity_unit;
                    tusi->value(old.to(new_unit));
                }

                // Happens the first time
                catch (SettingValueException e)
                {
                    qDebug() << e.what();
                }
                tusi->unit(unit_text);
            }
            m_velocity_unit = m_preferences_manager->getVelocityUnit();
        }
    }

    void SettingsCategoryWidgetBase::updateAccelerationUnit()
    {
        Acceleration new_unit = m_preferences_manager->getAccelerationUnit();
        updateAccelerationUnit(new_unit);
    }

    void SettingsCategoryWidgetBase::updateAccelerationUnit(
        Acceleration new_unit)
    {
        if (new_unit != m_acceleration_unit)
        {
            QString unit_text =
                m_preferences_manager->getAccelerationUnitText();
            for (QSharedPointer< TemplateUnitsSettingItem > tusi :
                 m_acceleration)
            {
                try
                {
                    Acceleration old = tusi->value() * m_acceleration_unit;
                    tusi->value(old.to(new_unit));
                }
                // Happens the first time
                catch (SettingValueException e)
                {
                    qDebug() << e.what();
                }
                tusi->unit(unit_text);
            }
            m_acceleration_unit = new_unit;
        }
    }

    void SettingsCategoryWidgetBase::updateAngleUnit()
    {
        Angle new_unit = m_preferences_manager->getAngleUnit();
        updateAngleUnit(new_unit);
    }

    void SettingsCategoryWidgetBase::updateAngleUnit(Angle new_unit)
    {
        if (new_unit != m_angle_unit)
        {
            QString unit_text = m_preferences_manager->getAngleUnitText();
            for (QSharedPointer< TemplateUnitsSettingItem > tusi : m_angle)
            {
                try
                {
                    Angle old = tusi->value() * m_angle_unit;
                    tusi->value(old.to(new_unit));
                }
                // Happens the first time
                catch (SettingValueException e)
                {
                    qDebug() << e.what();
                }
                tusi->unit(unit_text);
            }
            m_angle_unit = new_unit;
        }
    }

    void SettingsCategoryWidgetBase::updateTimeUnit()
    {
        Time new_unit = m_preferences_manager->getTimeUnit();
        updateTimeUnit(new_unit);
    }

    void SettingsCategoryWidgetBase::updateTimeUnit(Time new_unit)
    {
        if (new_unit != m_time_unit)
        {
            QString unit_text = m_preferences_manager->getTimeUnitText();
            for (QSharedPointer< TemplateUnitsSettingItem > tusi : m_time)
            {
                try
                {
                    Time old = tusi->value() * m_time_unit;
                    tusi->value(old.to(new_unit));
                }
                // Happens the first time
                catch (SettingValueException e)
                {
                    qDebug() << e.what();
                }
                tusi->unit(unit_text);
            }
            m_time_unit = new_unit;
        }
    }

    void SettingsCategoryWidgetBase::setGlobal(bool global)
    {
        m_global = global;
    }

    void SettingsCategoryWidgetBase::updateDistanceSetting(QString key)
    {
        updateUnitSetting< Distance >(key, m_distance[key], m_distance_unit);
    }

    void SettingsCategoryWidgetBase::updateVelocitySetting(QString key)
    {
        updateUnitSetting< Velocity >(key, m_velocity[key], m_velocity_unit);
    }

    void SettingsCategoryWidgetBase::updateAccelerationSetting(QString key)
    {
        updateUnitSetting< Acceleration >(
            key, m_acceleration[key], m_acceleration_unit);
    }

    void SettingsCategoryWidgetBase::updateTimeSetting(QString key)
    {
        updateUnitSetting< Time >(key, m_time[key], m_time_unit);
    }

    void SettingsCategoryWidgetBase::updateAngleSetting(QString key)
    {
        updateUnitSetting< Angle >(key, m_angle[key], m_angle_unit);
    }

    void SettingsCategoryWidgetBase::updateCountSetting(QString key)
    {
        QSharedPointer< TemplateCountSettingItem > tusi = m_count[key];

        try
        {
            int current  = tusi->value();
            int previous = m_temp_settings->setting< int >(key);
            if (previous == current)
            {
                return;
            }

            if (m_temp_settings->contains(key, false))
            {
                if (m_undo_history->find(key.toStdString()) ==
                    m_undo_history->end())
                {
                    (*m_undo_history)[key.toStdString()] = json::array();
                }

                if (m_redo_history->find(key.toStdString()) ==
                    m_redo_history->end())
                {
                    (*m_redo_history)[key.toStdString()] = json::array();
                }

                (*m_undo_history)[key.toStdString()].push_back(previous);
                (*m_redo_history)[key.toStdString()].clear();
            }
            m_temp_settings->setSetting< int >(key, current);
            m_active[key] = true;
            tusi->setStyleSheet(m_active_text);

            tusi->showUndo();
            tusi->hideRedo();

            // Run conditionals
            handleConditions();

            emit modified();
        }
        catch (SettingValueException e)
        {
            tusi->setStyleSheet(m_error_text);
        }
    }

    void SettingsCategoryWidgetBase::updatePercentageSetting(QString key)
    {
        QSharedPointer< TemplatePercentSettingItem > tusi = m_percent[key];

        try
        {
            double current  = tusi->value();
            double previous = m_temp_settings->setting< double >(key);
            if (MathUtils::equals(previous, current))
            {
                return;
            }

            if (m_temp_settings->contains(key, false))
            {
                if (m_undo_history->find(key.toStdString()) ==
                    m_undo_history->end())
                {
                    (*m_undo_history)[key.toStdString()] = json::array();
                }

                if (m_redo_history->find(key.toStdString()) ==
                    m_redo_history->end())
                {
                    (*m_redo_history)[key.toStdString()] = json::array();
                }

                (*m_undo_history)[key.toStdString()].push_back(previous);
                (*m_redo_history)[key.toStdString()].clear();
            }
            m_temp_settings->setSetting< double >(key, current);
            m_active[key] = true;
            tusi->setStyleSheet(m_active_text);

            tusi->showUndo();
            tusi->hideRedo();

            // Run conditionals
            handleConditions();

            emit modified();
        }
        catch (SettingValueException e)
        {
            tusi->setStyleSheet(m_error_text);
        }
    }

    void SettingsCategoryWidgetBase::updateBoolSetting(QString key)
    {
        QSharedPointer< TemplateCheckboxSettingItem > tcsi = m_checkbox[key];

        bool current  = tcsi->value();
        bool previous = m_temp_settings->setting< bool >(key);

        if (previous == current)
        {
            return;
        }

        if (m_temp_settings->contains(key, false))
        {
            if (m_undo_history->find(key.toStdString()) ==
                m_undo_history->end())
            {
                (*m_undo_history)[key.toStdString()] = json::array();
            }

            if (m_redo_history->find(key.toStdString()) ==
                m_redo_history->end())
            {
                (*m_redo_history)[key.toStdString()] = json::array();
            }

            (*m_undo_history)[key.toStdString()].push_back(previous);
            (*m_redo_history)[key.toStdString()].clear();
        }
        m_temp_settings->setSetting< bool >(key, current);

        tcsi->showUndo();
        tcsi->hideRedo();


        // Run conditionals
        handleConditions();

        emit modified();
    }

    void SettingsCategoryWidgetBase::updateStringSetting(QString key)
    {
        QSharedPointer< TemplateStringSettingItem > tssi = m_string[key];

        QString s        = tssi->value();
        QString previous = m_temp_settings->setting< QString >(key);

        if (previous == s)
        {
            return;
        }

        if (m_temp_settings->contains(key, false))
        {
            if (m_undo_history->find(key.toStdString()) ==
                m_undo_history->end())
            {
                (*m_undo_history)[key.toStdString()] = json::array();
            }

            if (m_redo_history->find(key.toStdString()) ==
                m_redo_history->end())
            {
                (*m_redo_history)[key.toStdString()] = json::array();
            }

            (*m_undo_history)[key.toStdString()].push_back(previous);
            (*m_redo_history)[key.toStdString()].clear();
        }
        m_temp_settings->setSetting< QString >(key, s);
        m_active[key] = true;
        m_string[key]->setStyleSheet(m_active_text);

        tssi->showUndo();
        tssi->hideRedo();

        // Run conditionals
        handleConditions();

        emit modified();
    }

    void SettingsCategoryWidgetBase::updateStringEnumSetting(QString key)
    {
        QSharedPointer< TemplateDropdownSettingItem > tdsi = m_enum[key];

        QString s        = tdsi->value();
        QString previous = m_temp_settings->setting< QString >(key);

        if (previous == s)
        {
            return;
        }

        if (m_temp_settings->contains(key, false))
        {
            if (m_undo_history->find(key.toStdString()) ==
                m_undo_history->end())
            {
                (*m_undo_history)[key.toStdString()] = json::array();
            }

            if (m_redo_history->find(key.toStdString()) ==
                m_redo_history->end())
            {
                (*m_redo_history)[key.toStdString()] = json::array();
            }

            (*m_undo_history)[key.toStdString()].push_back(previous);
            (*m_redo_history)[key.toStdString()].clear();
        }
        m_temp_settings->setSetting< QString >(key, s);

        tdsi->showUndo();
        tdsi->hideRedo();

        // Run conditionals
        handleConditions();

        emit modified();
    }

    void SettingsCategoryWidgetBase::undoSetting(QString key)
    {
        if (m_redo_history->find(key.toStdString()) == m_redo_history->end())
        {
            (*m_redo_history)[key.toStdString()] = json::array();
        }

        json& settings_json = m_temp_settings->json();
        (*m_redo_history)[key.toStdString()].push_back(
            settings_json[key.toStdString()]);

        if (m_undo_history->find(key.toStdString()) == m_undo_history->end() ||
            !(*m_undo_history)[key.toStdString()].size())
        {
            m_temp_settings->remove(key);
        }
        else
        {
            // pop_back
            auto undo_value = (*m_undo_history)[key.toStdString()].back();
            m_temp_settings->setSetting< decltype(undo_value) >(key,
                                                                undo_value);
            (*m_undo_history)[key.toStdString()].erase(
                (*m_undo_history)[key.toStdString()].size() - 1);
        }

        load();

        emit modified();
    }

    void SettingsCategoryWidgetBase::redoSetting(QString key)
    {
        if (m_undo_history->find(key.toStdString()) == m_undo_history->end())
        {
            (*m_undo_history)[key.toStdString()] = json::array();
        }

        if (m_temp_settings->contains(key, false))
        {
            json& settings_json = m_temp_settings->json();
            (*m_undo_history)[key.toStdString()].push_back(
                settings_json[key.toStdString()]);
        }

        if (m_redo_history->find(key.toStdString()) == m_redo_history->end() ||
            !(*m_redo_history)[key.toStdString()].size())
        {
            m_temp_settings->remove(key);
        }
        else
        {
            // pop_back
            auto redo_value = (*m_redo_history)[key.toStdString()].back();
            m_temp_settings->setSetting< decltype(redo_value) >(key,
                                                                redo_value);
            (*m_redo_history)[key.toStdString()].erase(
                (*m_redo_history)[key.toStdString()].size() - 1);
        }

        load();

        emit modified();
    }
}  // namespace ORNL
