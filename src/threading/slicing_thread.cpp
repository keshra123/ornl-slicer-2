#include "threading/slicing_thread.h"

#include <QMutex>
#include <QReadWriteLock>
#include <QWaitCondition>
#include <QWriteLocker>

#include "geometry/mesh.h"
#include "layer_regions/island.h"
#include "layer_regions/layer.h"
#include "managers/project_manager.h"
#include "managers/window_manager.h"
#include "threading/island_thread.h"

namespace ORNL
{
    QSharedPointer< SlicingThread > SlicingThread::m_singleton =
        QSharedPointer< SlicingThread >();

    QSharedPointer< SlicingThread > SlicingThread::getInstance()
    {
        if (m_singleton.isNull())
        {
            m_singleton.reset(new SlicingThread());
        }
        return m_singleton;
    }

    SlicingThread::SlicingThread(QObject* parent)
        : QThread(parent)
        , m_project_manager(ProjectManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
        , m_restart(false)
        , m_abort(false)
        , m_complete(0)
        , m_total(0)
        , m_condition(new QWaitCondition)
        , m_mutex(new QMutex)
    {
        m_mesh_lock = m_project_manager->meshLock();
    }

    SlicingThread::~SlicingThread()
    {
        // Makes sure threads aren't getting started
        m_mutex->lock();
        m_abort = true;
        m_condition->wakeOne();
        m_mutex->unlock();
        wait();

        // Speed up the island threads destruction
        for (QSharedPointer< Island > island : m_island_threads_running.keys())
        {
            if (m_island_threads_running[island])
            {
                m_island_threads[island]->requestInterruption();
            }
        }
    }

    void SlicingThread::settingsUpdated()
    {
        m_mutex->lock();
        m_mesh_lock->lockForRead();
        for (QSharedPointer< Mesh > mesh : m_project_manager->meshList())
        {
            for (QSharedPointer< Layer > layer : *mesh)
            {
                for (QSharedPointer< Island > island : *layer)
                {
                    m_slice_islands_queue.insert(island);

                    // If queued island is currently running. Request stopping,
                    // so it can restart faster
                    if (m_island_threads_running[island])
                    {
                        m_island_threads[island]->requestInterruption();
                        m_request_interrupt_queue.insert(island);
                    }
                }
            }
        }
        m_mesh_lock->unlock();

        if (!isRunning())
        {
            start(HighestPriority);
        }
        else
        {
            m_restart = true;
            m_condition->wakeOne();
        }
        m_mutex->unlock();
    }

    void SlicingThread::settingsUpdated(QSharedPointer< Mesh > mesh)
    {
        m_mutex->lock();
        m_mesh_lock->lockForRead();

        // Add mesh to queue
        for (QSharedPointer< Layer > layer : *mesh)
        {
            for (QSharedPointer< Island > island : *layer)
            {
                m_slice_islands_queue.insert(island);
                // If queued island is currently running. Request stopping, so
                // it can restart faster
                if (m_island_threads_running[island])
                {
                    m_island_threads[island]->requestInterruption();
                    m_request_interrupt_queue.insert(island);
                }
            }
        }
        m_mesh_lock->unlock();

        if (!isRunning())
        {
            start(HighestPriority);
        }
        else
        {
            m_restart = true;
            m_condition->wakeOne();
        }
        m_mutex->unlock();
    }

    void SlicingThread::settingsUpdated(QSharedPointer< Layer > layer)
    {
        m_mutex->lock();
        m_mesh_lock->lockForRead();

        for (QSharedPointer< Island > island : *layer)
        {
            m_slice_islands_queue.insert(island);
            // If queued island is currently running. Request it to stop, so it
            // can restart faster
            if (m_island_threads_running[island])
            {
                m_island_threads[island]->requestInterruption();
                m_request_interrupt_queue.insert(island);
            }
        }

        m_mesh_lock->unlock();
        if (!isRunning())
        {
            start(HighestPriority);
        }
        else
        {
            m_restart = true;
            m_condition->wakeOne();
        }
        m_mutex->unlock();
    }

    void SlicingThread::meshAdded(QSharedPointer< Mesh > mesh)
    {
        m_mutex->lock();
        m_mesh_lock->lockForRead();

        // Add mesh to queue and create IslandThread objects for it
        for (QSharedPointer< Layer > layer : *mesh)
        {
            for (QSharedPointer< Island > island : *layer)
            {
                m_island_threads.insert(island,
                                        QSharedPointer< IslandThread >(
                                            new IslandThread(this, island)));
                m_slice_islands_queue.insert(island);
            }
        }

        m_mesh_lock->unlock();

        if (!isRunning())
        {
            start(HighestPriority);
        }
        else
        {
            m_restart = true;
            m_condition->wakeOne();
        }
        m_mutex->unlock();
    }

    void SlicingThread::meshRemoved(QSharedPointer< Mesh > mesh)
    {
        // The mesh has been removed from project manager, so this parameter is
        // the final copy of it
        m_mutex->lock();
        for (QSharedPointer< Layer > layer : *mesh)
        {
            for (QSharedPointer< Island > island : *layer)
            {
                if (m_slice_islands_queue.contains(island))
                {
                    m_slice_islands_queue.remove(island);
                }

                /*
                 *  If the thread is running, request interruption and put it
                 *  in the remove queue so that when it finishes or is
                 * interrupted it will be removed.
                 */
                if (m_island_threads_running[island])
                {
                    m_island_threads[island]->requestInterruption();
                    m_request_interrupt_queue.insert(island);
                    m_remove_islands_queue.insert(island);
                }
                // Otherwise remove it now.
                else
                {
                    m_island_threads_running.remove(island);
                    m_island_threads.remove(island);
                }
            }
        }
        m_mutex->unlock();
    }

    void SlicingThread::islandThreadFinished(QSharedPointer< Island > island)
    {
        m_mutex->lock();
        m_island_threads_running[island] = false;
        m_complete++;
        if (m_total == m_complete)
        {
            m_complete = 0;
            m_total    = 0;
            emit sliceFinished();
        }
        else
        {
            emit progress(m_complete, m_total);
        }
        m_mutex->unlock();
    }

    void SlicingThread::islandThreadInterrupted(QSharedPointer< Island > island)
    {
        m_mutex->lock();
        m_request_interrupt_queue.remove(island);
        // If interrupted island is suppored to be removed then remove it
        if (m_remove_islands_queue.contains(island))
        {
            m_island_threads_running.remove(island);
            m_island_threads.remove(island);
        }
        // Otherwise mark it as not running
        else
        {
            m_island_threads_running[island] = false;
        }

        // If there are no threads left waiting to be interrupted then wake the
        // condition
        if (m_request_interrupt_queue.empty())
        {
            m_condition->wakeOne();
        }

        // If all the island have been sliced or interrupted then the slice is
        // considered finished
        if (m_total == m_complete)
        {
            m_complete = 0;
            m_total    = 0;
            emit sliceFinished();
        }
        // Otherwise update the progress
        else
        {
            emit progress(m_complete, m_total);
        }
        m_mutex->unlock();
    }


    void SlicingThread::run()
    {
        // Mutex is unlocked during sleep and when waiting for threads to be
        // interrupted
        m_mutex->lock();
        forever
        {
            // Start all the islands that are queued
            if (m_slice_islands_queue.size())
            {
                bool new_run = false;
                // If this is the start of a new slicing run alert the main
                // window
                if (m_total == 0)
                {
                    new_run = true;
                    emit sliceStarting();
                }

                for (QSharedPointer< Island > island : m_slice_islands_queue)
                {
                    m_island_threads[island]->start();
                    m_island_threads_running[island] = true;
                    m_total++;
                }
                if (new_run)
                {
                    emit total(m_total);
                }
            }

            // Put the thread to sleep until more computation is needed
            if (!m_restart)
            {
                m_condition->wait(m_mutex.data());
            }
            m_restart = false;

            if (m_abort)
            {
                break;
            }

            // If threads need to be interrupted wait for them to be interrupted
            if (m_request_interrupt_queue.size())
            {
                m_condition->wait(m_mutex.data());
            }

            if (m_abort)
            {
                break;
            }
        }
        m_mutex->unlock();
    }


}  // namespace ORNL
