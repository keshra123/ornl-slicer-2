var dir_e270597328dbf693fc6810a4b7904fa1 =
[
    [ "printinfillcategorywidget.cpp", "printinfillcategorywidget_8cpp.html", null ],
    [ "printinfillcategorywidget.h", "printinfillcategorywidget_8h.html", [
      [ "PrintInfillCategoryWidget", "class_o_r_n_l_1_1_print_infill_category_widget.html", "class_o_r_n_l_1_1_print_infill_category_widget" ]
    ] ],
    [ "printinsetscategorywidget.cpp", "printinsetscategorywidget_8cpp.html", null ],
    [ "printinsetscategorywidget.h", "printinsetscategorywidget_8h.html", [
      [ "PrintInsetsCategoryWidget", "class_o_r_n_l_1_1_print_insets_category_widget.html", "class_o_r_n_l_1_1_print_insets_category_widget" ]
    ] ],
    [ "printperimetercategorywidget.cpp", "printperimetercategorywidget_8cpp.html", null ],
    [ "printperimetercategorywidget.h", "printperimetercategorywidget_8h.html", [
      [ "PrintPerimeterCategoryWidget", "class_o_r_n_l_1_1_print_perimeter_category_widget.html", "class_o_r_n_l_1_1_print_perimeter_category_widget" ]
    ] ],
    [ "printsettingcategorywidgets.h", "printsettingcategorywidgets_8h.html", null ],
    [ "printskincategorywidget.cpp", "printskincategorywidget_8cpp.html", null ],
    [ "printskincategorywidget.h", "printskincategorywidget_8h.html", [
      [ "PrintSkinCategoryWidget", "class_o_r_n_l_1_1_print_skin_category_widget.html", "class_o_r_n_l_1_1_print_skin_category_widget" ]
    ] ]
];