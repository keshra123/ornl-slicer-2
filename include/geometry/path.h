#ifndef PATH_H
#define PATH_H

#include <QVector>

#include "layer_regions/region.h"
#include "path_segment.h"
#include "polyline.h"

namespace ORNL
{
    class Region;
    class PathSegment;

    /*!
     * \class Path
     *
     * A list of path segments
     */
    class Path
        : public QVector< PathSegment >
        , SettingsBase
    {
    public:
        //! \brief default constructor
        Path() = default;

        //! \brief constructor
        Path(Region* region);

        //! \brief Copy Constructor
        Path(const Path& path);

        //! \brief adds a path segment to the path
        void add(PathSegment ps);
        Path operator+(const PathSegment& ps);
        Path operator+(const PathSegment& ps) const;
        Path operator+=(const PathSegment& ps);

        RegionType type();

        bool closed();

        QString gcode(WriterBase* syntax);

        //! \brief Creates a single path from a polygon
        static Path createPath(Region* region,
                               RegionType path_type,
                               Polygon& polygon);

        //! \brief Creates a single path from a polyline
        static Path createPath(Region* region,
                               RegionType path_type,
                               Polyline& polyline);

        //! \brief Creates a list of paths based on a list of polygons
        static QVector< Path > createPaths(Region* region,
                                           RegionType path_type,
                                           PolygonList& polygons);

        //! \brief Creates a list of paths based on a list of polygons
        static QVector< Path > createPaths(Region* region,
                                           RegionType path_type,
                                           QVector< PolygonList >& polygons);

        static QVector< Path > createPaths(Region* region,
                                           RegionType path_type,
                                           QVector< Polyline >& polylines);

#ifdef QT_DEBUG
        void paintDebug(QPainter* painter,
                        Distance& ratio,
                        int vertical_offset);
#endif
    private:
        Region* m_parent;
        RegionType m_type;

#ifdef QT_DEBUG
        QPen m_outline_pen;
        QPen m_centerline_pen;
        QPen m_path_pen;
        QMap< PathModifiers, QPen > m_modifier_pens;
#endif
    };
}  // namespace ORNL

#endif  // PATH_H
