#include "gcode/gcode_layer.h"

// #include <omp.h>

#include "gcode/writers/writer_base.h"
#include "geometry/mesh.h"
#include "layer_regions/island.h"
#include "layer_regions/layer.h"
#include "managers/project_manager.h"

namespace ORNL
{
    GcodeLayer::GcodeLayer(uint layer_nr)
        : m_layer_nr(layer_nr)
        , m_project_manager(ProjectManager::getInstance())
    {}

    void GcodeLayer::updateMesh(QSharedPointer< Mesh > mesh)
    {
        QSharedPointer< Layer > layer = (*mesh)[static_cast< int >(m_layer_nr)];

        QSharedPointer< QVector< QSharedPointer< Island > > > copy = layer;

        // TODO: remove mesh islands that are no longer in mesh

        for (int i = 0; i < size(); i++)
        {
            QSharedPointer< Island > island = operator[](i);
            if (island->parent()->parent() == mesh)
            {
                // If found remove from the copy
                if (layer.staticCast< QVector< QSharedPointer< Island > > >()
                        ->contains(island))
                {
                    copy->removeOne(island);
                }
                // else remove from this
                else
                {
                    remove(i);
                    i--;
                }
            }
        }

        for (QSharedPointer< Island > island : (*copy))
        {
            append(island);
        }
    }


    void GcodeLayer::removeMesh(QString mesh_name)
    {
        removeMesh(m_project_manager->mesh(mesh_name));
    }

    void GcodeLayer::removeMesh(QSharedPointer< Mesh > mesh)
    {
        QSharedPointer< Layer > layer = (*mesh)[static_cast< int >(m_layer_nr)];

        for (int i = 0; i < size(); i++)
        {
            QSharedPointer< Island > island = operator[](i);
            if (island->parent()->parent() == mesh &&
                !layer.staticCast< QVector< QSharedPointer< Island > > >()
                     ->contains(island))
            {
                remove(i);
                i--;
            }
        }
    }

    uint GcodeLayer::layerNumber()
    {
        return m_layer_nr;
    }

    void GcodeLayer::order()
    {}

    QString GcodeLayer::gcode(WriterBase* syntax)
    {
        QString gcode;
        int num_islands = size();
        QVector< QString > gcode_island_text;
        gcode_island_text.resize(num_islands);
        // QVector<omp_lock_t> locks;
        // locks.resize(num_islands);

        // Initialize all the locks and set them (to be unset after text is
        // collected from their respective layer)
        // #pragma omp parallel for
        for (int island_nr = 0; island_nr < num_islands; island_nr++)
        {
            // omp_init_lock(&locks[island_nr]);
            // omp_set_lock(&locks[island_nr]);
        }

        // Collect the gcode for the layers at the same time as writing it
        // (writing is done sequentially)
        // #pragma omp parallel sections
        {
            // #pragma omp section
            {
                // #pragma omp parallel for
                for (int island_nr = 0; island_nr < num_islands; island_nr++)
                {
                    gcode_island_text[island_nr] = operator[](island_nr)->gcode(
                        syntax);
                    // omp_unset_lock(&locks[island_nr]);
                }
            }

            // #pragma omp section
            {
                for (int island_nr = 0; island_nr < num_islands; island_nr++)
                {
                    // Forces waiting until the layer is finished creating the
                    // string omp_set_lock(&locks[island_nr]);
                    gcode += gcode_island_text[island_nr];
                    // omp_unset_lock(&locks[island_nr]);
                }

                gcode += syntax->onLayerChange(m_layer_nr);
            }
        }

        // Destroy all the locks
        // #pragma omp parallel for
        for (int island_nr = 0; island_nr < num_islands; island_nr++)
        {
            // omp_destroy_lock(&locks[island_nr]);
        }

        return gcode;
    }
}  // namespace ORNL
