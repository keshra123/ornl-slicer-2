#ifndef TEMPLATESTRINGSETTINGITEM_H
#define TEMPLATESTRINGSETTINGITEM_H

#include <QWidget>

namespace Ui
{
    class TemplateStringSettingItem;
}

namespace ORNL
{
    class TemplateStringSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit TemplateStringSettingItem(QWidget* parent = 0);
        ~TemplateStringSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        QString value();
        void setStyleSheet(const QString& stylesheet);

    public slots:
        void value(QString t);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        void valueChanged(QString text);

    private slots:
        void handleUndo();
        void handleRedo();
        void handleValueChanged();

    private:
        Ui::TemplateStringSettingItem* ui;
    };
}  // namespace ORNL


#endif  // TEMPLATESTRINGSETTINGITEM_H
