#include "managers/global_settings_manager.h"

#include <QDir>
#include <QStandardPaths>

#include "configs/display_config_base.h"
#include "utilities/enums.h"
namespace ORNL
{
    QSharedPointer< GlobalSettingsManager > GlobalSettingsManager::m_singleton =
        QSharedPointer< GlobalSettingsManager >();

    QSharedPointer< GlobalSettingsManager > GlobalSettingsManager::getInstance()
    {
        if (m_singleton.isNull())
        {
            m_singleton.reset(new GlobalSettingsManager());
        }
        return m_singleton;
    }

    GlobalSettingsManager::GlobalSettingsManager()
        : m_global(new SettingsBase())
    {
        QString app_data_path =
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        QDir app_data(app_data_path);
        if (!app_data.exists())
        {
            app_data.mkpath(".");
        }

        // Check for settings folders and create them if they do not exist
        for (int i = 0; i < NUM_SETTING_TYPES; i++)
        {
            QString config_type;
            switch (static_cast< ConfigTypes >(i))
            {
                case ConfigTypes::kMachine:
                    config_type = "machine";
                    break;
                case ConfigTypes::kPrint:
                    config_type = "print";
                    break;
                case ConfigTypes::kMaterial:
                    config_type = "material";
                    break;
                case ConfigTypes::kNozzle:
                    config_type = "nozzle";
                    break;
            }
            QString configs_path = app_data_path + QDir::separator() +
                config_type + QString("_settings");
            QDir configs_dir(configs_path);

            m_configs.append(
                QMap< QString, QSharedPointer< DisplayConfigBase > >());
            m_project_configs.append(
                QMap< QString, QSharedPointer< DisplayConfigBase > >());

            if (!configs_dir.exists())
            {
                configs_dir.mkpath(".");
            }
            // Nozzle structure is a little different
            else if (i == static_cast< uint8_t >(ConfigTypes::kNozzle))
            {
                for (QString material_name :
                     m_configs[static_cast< uint8_t >(ConfigTypes::kMaterial)]
                         .keys())
                {
                    QString nozzle_configs_path =
                        configs_path + QDir::separator() + material_name;
                    QDir nozzle_configs_dir(nozzle_configs_path);
                    if (!nozzle_configs_dir.exists())
                    {
                        nozzle_configs_dir.mkpath(".");
                    }
                    else
                    {
                        for (QString config_name :
                             nozzle_configs_dir.entryList(QDir::Filter::Files))
                        {
                            config_name =
                                config_name.left(config_name.lastIndexOf("."));
                            config_name = material_name + ":" + config_name;
                            QSharedPointer< DisplayConfigBase > dcb(
                                new DisplayConfigBase(
                                    config_name, config_type, m_global.data()));
                            dcb->loadFromFile();
                            m_configs[i][config_name] = dcb;
                        }
                    }
                }
            }
            else
            {
                for (QString config_name :
                     configs_dir.entryList(QDir::Filter::Files))
                {
                    config_name =
                        config_name.left(config_name.lastIndexOf("."));
                    QSharedPointer< DisplayConfigBase > dcb(
                        new DisplayConfigBase(
                            config_name, config_type, m_global.data()));
                    dcb->loadFromFile();
                    m_configs[i][config_name] = dcb;
                }
            }
        }
    }

    void GlobalSettingsManager::addConfig(ConfigTypes ct,
                                          QString config,
                                          QString base,
                                          bool project,
                                          QString folder)
    {
        QString settings_type;
        switch (ct)
        {
            case ConfigTypes::kMachine:
                settings_type = "machine";
                break;
            case ConfigTypes::kPrint:
                settings_type = "print";
                break;
            case ConfigTypes::kMaterial:
                settings_type = "material";
                break;
            case ConfigTypes::kNozzle:
                settings_type = "nozzle";
                break;
        }

        uint8_t ct_int = static_cast< uint8_t >(ct);

        // If setting is from a project, but not in the main settings folder
        if (project)
        {
            m_project_configs[ct_int].insert(
                config,
                QSharedPointer< DisplayConfigBase >(new DisplayConfigBase(
                    config, settings_type, m_global.data(), folder)));
            m_project_configs[ct_int][config]->loadFromFile();
        }
        else
        {
            // folder is set during the start up addConfigs
            if (!folder.isEmpty())
            {
                m_configs[ct_int][config] =
                    QSharedPointer< DisplayConfigBase >(new DisplayConfigBase(
                        config, settings_type, m_global.data(), folder));
                m_configs[ct_int][config]->loadFromFile();
            }
            // if base is set make a copy of the base
            else if (!base.isEmpty())
            {
                // Can not use copy constructor as we have a constructor with a
                // parent parameter
                m_configs[ct_int][config] =
                    m_configs[static_cast< int >(ct)][config]->copy(config);
            }
            else
            {
                // Values do not need to be set here as they will be set upon
                // saving in the settings window
                m_configs[ct_int][config] =
                    QSharedPointer< DisplayConfigBase >(new DisplayConfigBase(
                        config, settings_type, m_global.data()));
            }
        }

        emit updateConfig(ct);
    }

    QStringList GlobalSettingsManager::getConfigsNames(ConfigTypes ct)
    {
        return m_configs[static_cast< int >(ct)].keys();
    }

    QMultiMap< QString, QString > GlobalSettingsManager::getNozzleNames()
    {
        QMultiMap< QString, QString > rv;
        for (QString material_nozzle :
             m_configs[static_cast< uint8_t >(ConfigTypes::kNozzle)].keys())
        {
            int colon        = material_nozzle.indexOf(':');
            QString material = material_nozzle.left(colon);
            QString nozzle   = material_nozzle.mid(colon + 1);
            rv.insert(material, nozzle);
        }
        return rv;
    }

    void GlobalSettingsManager::saveConfig(ConfigTypes ct,
                                           QString config,
                                           bool project,
                                           QString folder)
    {
        uint8_t ct_int = static_cast< uint8_t >(ct);
        if (project)
        {
            if (m_project_configs[ct_int].contains(config))
            {
                QString settings_type;
                switch (ct)
                {
                    case ConfigTypes::kMachine:
                        settings_type = "machine";
                        break;
                    case ConfigTypes::kPrint:
                        settings_type = "print";
                        break;
                    case ConfigTypes::kMaterial:
                        settings_type = "material";
                        break;
                    case ConfigTypes::kNozzle:
                        settings_type = "nozzle";
                        break;
                }
                m_project_configs[ct_int][config]->saveToFile(folder);
                return;
            }
        }
        else
        {
            if (m_configs[ct_int].contains(config))
            {
                QString settings_type;
                switch (ct)
                {
                    case ConfigTypes::kMachine:
                        settings_type = "machine";
                        break;
                    case ConfigTypes::kPrint:
                        settings_type = "print";
                        break;
                    case ConfigTypes::kMaterial:
                        settings_type = "material";
                        break;
                    case ConfigTypes::kNozzle:
                        settings_type = "nozzle";
                        break;
                }
                m_configs[ct_int][config]->saveToFile(folder);
                return;
            }
        }


        QString error_message = "Could not find " + config;
        qWarning() << error_message;
        emit error(error_message);
    }

    void GlobalSettingsManager::removeConfig(ConfigTypes ct,
                                             QString config,
                                             bool project)
    {
        uint8_t ct_int = static_cast< uint8_t >(ct);
        // TODO: check if config is currently selected and ask user for
        // replacement if it is
        if (project)
        {
            if (!m_project_configs[ct_int].contains(config))
            {
                m_project_configs[ct_int][config]->deleteFile();
                m_project_configs[ct_int].remove(config);
                return;
            }
        }
        else
        {
            if (m_configs[ct_int].contains(config))
            {
                if (!m_configs[ct_int][config]->deleteFile())
                {
                    QString error_message;
                    switch (ct)
                    {
                        case ConfigTypes::kMachine:
                            error_message = "Machine ";
                            break;
                        case ConfigTypes::kMaterial:
                            error_message = "Material ";
                            break;
                        case ConfigTypes::kPrint:
                            error_message = "Print ";
                            break;
                        case ConfigTypes::kNozzle:
                            error_message = "Nozzle ";
                            break;
                    }
                    error_message = "config " + config + " file not deleted";
                    qWarning() << error_message;
                    emit error(error_message);
                }
                m_configs[ct_int].remove(config);
                return;
            }
        }

        QString error_message;
        switch (ct)
        {
            case ConfigTypes::kMachine:
                error_message = "Machine ";
                break;
            case ConfigTypes::kMaterial:
                error_message = "Material ";
                break;
            case ConfigTypes::kPrint:
                error_message = "Print ";
                break;
            case ConfigTypes::kNozzle:
                error_message = "Nozzle ";
                break;
        }
        error_message = "config " + config + " does not exist";
        qWarning() << error_message;
        emit error(error_message);
    }

    QSharedPointer< DisplayConfigBase > GlobalSettingsManager::getConfig(
        ConfigTypes ct,
        QString config,
        bool project)
    {
        uint8_t ct_int = static_cast< uint8_t >(ct);
        if (project)
        {
            if (m_project_configs[ct_int].contains(config))
            {
                return m_project_configs[ct_int][config];
            }
        }
        else
        {
            if (m_configs[ct_int].contains(config))
            {
                return m_configs[ct_int][config];
            }
        }

        QString error_message;
        switch (ct)
        {
            case ConfigTypes::kMachine:
                error_message = "Machine ";
                break;
            case ConfigTypes::kMaterial:
                error_message = "Material ";
                break;
            case ConfigTypes::kPrint:
                error_message = "Print ";
                break;
            case ConfigTypes::kNozzle:
                error_message = "Nozzle ";
                break;
        }

        error_message += "config " + config + " does not exist";
        qWarning() << error_message;
        emit error(error_message);
        return QSharedPointer< DisplayConfigBase >(nullptr);
    }

    void GlobalSettingsManager::updateGlobal(QString machine,
                                             QString material,
                                             QString print,
                                             QString nozzle)
    {
        uint8_t ct_int = static_cast< uint8_t >(ConfigTypes::kMachine);
        if (m_configs[ct_int].contains(machine))
        {
            m_global->update(m_configs[ct_int][machine]);
        }
        // TODO: Project settings

        ct_int = static_cast< uint8_t >(ConfigTypes::kMaterial);
        if (m_configs[ct_int].contains(material))
        {
            m_global->update(m_configs[ct_int][material]);
        }
        // TODO: Project settings

        ct_int = static_cast< uint8_t >(ConfigTypes::kPrint);
        if (m_configs[ct_int].contains(print))
        {
            m_global->update(m_configs[ct_int][print]);
        }
        // TODO: Project settings

        ct_int = static_cast< uint8_t >(ConfigTypes::kNozzle);
        if (m_configs[ct_int].contains(material + ":" + nozzle))
        {
            m_global->update(m_configs[ct_int][material + ":" + nozzle]);
        }
        // TODO: Project settings
    }

    QSharedPointer< SettingsBase > GlobalSettingsManager::getGlobal()
    {
        return m_global;
    }
}  // namespace ORNL
