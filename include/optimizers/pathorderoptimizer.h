#ifndef PATHORDEROPTIMIZER_H
#define PATHORDEROPTIMIZER_H

#include <QVector>

#include "geometry/path.h"
#include "utilities/enums.h"

namespace ORNL
{
    class PathOrderOptimizer
    {
    public:
        PathOrderOptimizer(Point current_position,
                           QVector< Path >& paths,
                           OrderOptimization order_optimization =
                               OrderOptimization::kShortestDistance);

        void compute();

        QVector< Path > paths();

    private:
        Point m_start;
        QVector< Path > m_paths;
        OrderOptimization m_order_optimization;
    };
}  // namespace ORNL

#endif  // PATHORDEROPTIMIZER_H
