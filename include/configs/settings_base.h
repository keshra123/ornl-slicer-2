#ifndef SETTINGSBASE_H
#define SETTINGSBASE_H

//! \file settingsbase.h

#include <QMap>
#include <QString>
#include <QtDebug>
#include <json.hpp>

#include "units/derivative_units.h"
#include "units/unit.h"
#include "utilities/enums.h"
#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    /*!
     * \class SettingsBase
     * \brief Base class for all settings containers
     */
    class SettingsBase
    {
    public:
        //! \brief Default Constructor
        SettingsBase();

        /*!
         * \brief Constructor
         *
         * \param parent If the current SettingsBase does not have a setting,
         *               the parent is checked
         */
        SettingsBase(SettingsBase* parent);

        //! \brief Destructor
        ~SettingsBase();

        //! \brief Set the parent base class to check for setting if not found
        void parent(SettingsBase* parent);

        //! \brief Returns the parent
        SettingsBase* parent();

        /*!
         * \brief For this specific setting, this specific parent is used to get
         * the setting \param key the setting key \param parent which
         * settingsbase to use for getting this specific setting
         */
        void setSettingInheritBase(QString key, SettingsBase* parent);

        /*!
         * \brief update the value of a setting
         *
         * \note This function is templated and needs to stay in the header
         * (best option)
         */
        template < typename T >
        void setSetting(QString key, T value)
        {
            m_values[key.toStdString()] = value;
        }

        /*!
         * \brief Returns value of setting
         *
         * \note This function is templated and needs to stay in the header
         * (best option)
         */
        template < typename T >
        T setting(QString key)
        {
            bool parent = false;
            return setting< T >(key, parent);
        }

        /*!
         * \brief Returns values of a settings. The parent parameter is marked
         * as whether this settingsbase owns the setting or one of its ancestors
         * does
         *
         * \note This function is templated and needs to stay in the header
         * (best option)
         */
        template < typename T >
        T setting(QString key, bool& parent)
        {
            // This SettingsBase owns the settings and returns it
            if (!m_values.empty() &&
                (m_values.find(key.toStdString()) != m_values.end()))
            {
                parent = false;
                return m_values[key.toStdString()].get< T >();
            }

            // This particular setting is inherited from a specific other
            // SettingsBase
            if (!m_settings_inherit_base.empty() &&
                m_settings_inherit_base.contains(key))
            {
                parent = true;
                return m_settings_inherit_base[key]->setting< T >(key);
            }

            // This SettingsBase does not own this setting, so check if the
            // parent does.
            if (m_parent)
            {
                parent = true;
                return m_parent->setting< T >(key);
            }

            // None of the ancestors own this setting
            qDebug() << "Setting " << key << "does not exist";
            parent = false;
            return T();
        }

        /*! \brief Returns whether a settings is contained in the settingsbase.
         *
         * \p include_parents determines whether to check parents
         *
         * \note This function is templated and needs to stay in the header
         * (best option)
         */
        bool contains(QString key, bool include_parents = false);

        //! \brief remove setting from this settingsbase (does not affect the
        //! parent)
        void remove(QString key);

        //! \brief Performs a deep copy of this SettingsBase
        SettingsBase* copy();

        void update(QSharedPointer< SettingsBase > other);

        //! \brief Creates json from the settings
        nlohmann::json& json();

        //! \brief Fills the settings base from json
        void json(nlohmann::json j);

    protected:
        SettingsBase* m_parent;
        nlohmann::json m_values;
        QMap< QString, SettingsBase* > m_settings_inherit_base;
    };  // class SettingsBase
}  // namespace ORNL
#endif  // SETTINGSBASE_H
