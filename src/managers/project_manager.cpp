#include "managers/project_manager.h"

#include <QDir>
#include <QReadLocker>
#include <QReadWriteLock>
#include <QStandardPaths>
#include <QWriteLocker>

#include "configs/display_config_base.h"
#include "gcode/gcode.h"
#include "gcode/gcode_layer.h"
#include "geometry/mesh.h"
#include "geometry/mesh_face.h"
#include "geometry/path.h"
#include "layer_regions/infill.h"
#include "layer_regions/island.h"
#include "layer_regions/perimeters.h"
#include "layer_regions/skin.h"
#include "managers/global_settings_manager.h"
#include "managers/preferences_manager.h"
#include "managers/window_manager.h"
#include "utilities/constants.h"
#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    QSharedPointer< ProjectManager > ProjectManager::m_singleton =
        QSharedPointer< ProjectManager >();

    QSharedPointer< ProjectManager > ProjectManager::getInstance()
    {
        if (m_singleton.isNull())
        {
            m_singleton.reset(new ProjectManager());
        }
        return m_singleton;
    }

    QSharedPointer< QReadWriteLock > ProjectManager::meshLock()
    {
        return m_mesh_lock;
    }

    ProjectManager::ProjectManager()
        : m_settings_manager(GlobalSettingsManager::getInstance())
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_window_manager(WindowManager::getInstance())

        , m_color(0)
        , m_selected_mesh("")
        , m_mesh_lock(new QReadWriteLock())
    {
        QString projectFolder(QStandardPaths::writableLocation(
                                  QStandardPaths::DocumentsLocation) +
                              QDir::separator() + "OS2_Projects");
        QDir project_folder(projectFolder);
        if (!project_folder.exists())
        {
            project_folder.mkpath(".");
        }

        QFile recent_projects_file(QStandardPaths::writableLocation(
                                       QStandardPaths::AppLocalDataLocation) +
                                   QDir::separator() + "recent_projects.json");
        if (recent_projects_file.exists())
        {
            recent_projects_file.open(QFile::ReadOnly);
            json recent_projects_json =
                json::parse(recent_projects_file.readAll().toStdString());
            for (json json_object : recent_projects_json)
            {
                QString filepath = json_object["filepath"];
                QString name     = json_object["name"];
                m_recent_projects.append(
                    QPair< QString, QString >(filepath, name));
            }
        }
        connect(this,
                SIGNAL(error(QString)),
                m_window_manager.data(),
                SLOT(createErrorDialog(QString)));
    }

    void ProjectManager::addMesh(QSharedPointer< Mesh > mesh)
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        // Can't be in constructor and gcode can't be created until a mesh has
        // been added
        if (m_gcode.isNull())
        {
            m_gcode.reset(new Gcode());
        }

        QString name = mesh->name();

        // Set the mesh's unselected color
        mesh->color(Constants::Colors::kModelColors[m_color]);
        m_color++;
        m_color %= Constants::Colors::kModelColors.size();


        name = QString("%1-%2").arg(name, QString::number(1));

        // Mesh are named after the STL file and then given a number (1 indexed)
        // based on the order in the STL
        if (m_meshes.contains(name))
        {
            for (int i = 2;; i++)
            {
                QString newname =
                    QString("%1-%2").arg(name, QString::number(i));
                if (m_meshes.contains(newname))
                {
                    continue;
                }
                name = newname;
                mesh->name(name);
                break;
            }
        }

        m_meshes[name] = mesh;
        write_locker.unlock();
        emit updateList(meshNameList());
        emit meshAdded(mesh);
        // Create the MeshGraphics here so it isn't bound by the ModelLoader
        // thread
        mesh->createGraphics();

        emit sendMeshGraphics(mesh->graphics().data());
    }

    void ProjectManager::removeMesh(QString mesh_name)
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        if (m_meshes.contains(mesh_name))
        {
            // If mesh has been copied or selected then clear them
            if (m_copied_mesh == mesh_name)
            {
                m_copied_mesh = "";
                emit enablePaste(false);
            }

            if (m_selected_mesh == mesh_name)
            {
                m_selected_mesh = "";
                emit enableCopy(false);
            }
            QSharedPointer< Mesh > mesh = m_meshes[mesh_name];
            m_meshes.remove(mesh_name);
            write_locker.unlock();
            emit updateList(meshNameList());
            emit meshRemoved(mesh);
        }
        else
        {
            qWarning() << "Attempt to remove mesh that doesn't exist";
            emit error("Attempt to remove mesh that doesn't exist");
        }
    }

    QStringList ProjectManager::meshNameList()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        return m_meshes.keys();
    }

    QList< QSharedPointer< Mesh > > ProjectManager::meshList()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        return m_meshes.values();
    }

    QSharedPointer< Mesh > ProjectManager::mesh(QString mesh_name)
    {
        QReadLocker read_locker(m_mesh_lock.data());
        if (m_meshes.contains(mesh_name))
        {
            return m_meshes[mesh_name];
        }
        qWarning() << QString("mesh: mesh_name %s not contained in m_meshes")
                          .arg(mesh_name);
        return QSharedPointer< Mesh >(nullptr);
    }

    QSharedPointer< Gcode > ProjectManager::gcode()
    {
        if (m_gcode.isNull())
        {
            m_gcode.reset(new Gcode());
        }
        return m_gcode;
    }

    void ProjectManager::writeGcode(QString filename)
    {
        if (m_gcode.isNull())
        {
            // Throw exception or create error dialog?
            return;
        }
        // TODO: syntaxbase
        // m_gcode->writeGcode(filename);
    }

    void ProjectManager::copyMesh(QString copied_mesh)
    {
        QReadLocker read_locker(m_mesh_lock.data());
        if (m_meshes.contains(copied_mesh))
        {
            m_copied_mesh = copied_mesh;
            emit enablePaste(true);
        }
        else
        {
            qWarning() << "Copy mesh string names a mesh that doesn't exist";
            emit error("Copy mesh string names a mesh that doesn't exist");
        }
    }

    void ProjectManager::copySelectedMesh()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        if (m_selected_mesh.isEmpty())  // Shouldn't happen
        {
            qWarning() << "Attempt to copy selected mesh when there isn't one";
            emit error("Attempt to copy selected mesh when there isn't one");
            return;
        }

        if (!m_meshes.contains(m_selected_mesh))  // Shouldn't happen
        {
            m_selected_mesh = "";
            emit enableCopy(false);
            qWarning() << "Attempt to copy selected mesh and found that "
                          "selected mesh doesn't exist";
            emit error("Attempt to copy selected mesh when there isn't one");
            return;
        }

        m_copied_mesh = m_selected_mesh;
        emit enablePaste(true);
    }

    void ProjectManager::selectLayer(uint layer_nr)
    {
        if (layer_nr >= m_gcode->size())
        {
            deselectGcode();
            return;
        }

        m_selected_layer        = static_cast< int >(layer_nr);
        m_selected_island       = -1;
        m_selected_region       = RegionType::kUnknown;
        m_selected_path         = -1;
        m_selected_path_segment = -1;
    }

    void ProjectManager::selectIsland(uint layer_nr, uint island_nr)
    {
        if (layer_nr >= m_gcode->size() ||
            island_nr >= (*m_gcode)[layer_nr]->size())
        {
            deselectGcode();
            return;
        }

        m_selected_layer        = static_cast< int >(layer_nr);
        m_selected_island       = static_cast< int >(island_nr);
        m_selected_region       = RegionType::kUnknown;
        m_selected_path         = -1;
        m_selected_path_segment = -1;
    }

    void ProjectManager::selectRegion(uint layer_nr,
                                      uint island_nr,
                                      RegionType region_type)
    {
        if (layer_nr >= m_gcode->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< GcodeLayer > gcode_layer = (*m_gcode)[layer_nr];
        if (island_nr >= gcode_layer->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< Island > island = (*gcode_layer)[island_nr];
        QSharedPointer< Region > region = (*island)[region_type];
        if (!region)
        {
            deselectGcode();
            return;
        }

        m_selected_layer        = static_cast< int >(layer_nr);
        m_selected_island       = static_cast< int >(island_nr);
        m_selected_region       = region_type;
        m_selected_path         = -1;
        m_selected_path_segment = -1;
    }

    void ProjectManager::selectPath(uint layer_nr,
                                    uint island_nr,
                                    RegionType region_type,
                                    uint path_nr)
    {
        if (layer_nr >= m_gcode->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< GcodeLayer > gcode_layer = (*m_gcode)[layer_nr];
        if (island_nr >= gcode_layer->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< Island > island = (*gcode_layer)[island_nr];
        QSharedPointer< Region > region = (*island)[region_type];
        if (!region || path_nr >= region->numberPaths())
        {
            deselectGcode();
            return;
        }

        m_selected_layer        = static_cast< int >(layer_nr);
        m_selected_island       = static_cast< int >(island_nr);
        m_selected_region       = region_type;
        m_selected_path         = static_cast< int >(path_nr);
        m_selected_path_segment = -1;
    }

    void ProjectManager::selectPathSegment(uint layer_nr,
                                           uint island_nr,
                                           RegionType region_type,
                                           uint path_nr,
                                           uint path_segment_nr)
    {
        if (layer_nr >= m_gcode->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< GcodeLayer > gcode_layer = (*m_gcode)[layer_nr];
        if (island_nr >= gcode_layer->size())
        {
            deselectGcode();
            return;
        }
        QSharedPointer< Island > island =
            (*gcode_layer)[static_cast< int >(island_nr)];
        QSharedPointer< Region > region = (*island)[region_type];
        if (!region || path_nr >= region->numberPaths())
        {
            deselectGcode();
            return;
        }
        Path& path = (*region)[static_cast< int >(path_nr)];
        if (static_cast< int >(path_segment_nr) >= path.size())
        {
            deselectGcode();
            return;
        }

        m_selected_layer        = static_cast< int >(layer_nr);
        m_selected_island       = static_cast< int >(island_nr);
        m_selected_region       = region_type;
        m_selected_path         = static_cast< int >(path_nr);
        m_selected_path_segment = static_cast< int >(path_segment_nr);
    }

    void ProjectManager::selectMesh(QString selected_mesh)
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        if (m_meshes.contains(m_selected_mesh))
        {
            m_meshes[m_selected_mesh]->selected(false);
        }
        m_selected_mesh = selected_mesh;
        m_meshes[m_selected_mesh]->selected(true);
        emit enableCopy(true);
        emit meshSelectionChanged();
    }

    QSharedPointer< Mesh > ProjectManager::selectedMesh()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        if (m_meshes.contains(m_selected_mesh))
        {
            return m_meshes[m_selected_mesh];
        }
        return QSharedPointer< Mesh >(nullptr);
    }

    QSharedPointer< GcodeLayer > ProjectManager::selectedLayer()
    {
        if (m_selected_layer > -1 &&
            m_gcode->size() > static_cast< uint >(m_selected_layer))
        {
            return (*m_gcode)[static_cast< uint >(m_selected_layer)];
        }
        return QSharedPointer< GcodeLayer >(nullptr);
    }

    QSharedPointer< Island > ProjectManager::selectedIsland()
    {
        if (m_selected_island <= -1)
        {
            return QSharedPointer< Island >(nullptr);
        }

        QSharedPointer< GcodeLayer > gcode_layer = selectedLayer();
        if (gcode_layer &&
            gcode_layer->size() > static_cast< uint >(m_selected_island))
        {
            return (*gcode_layer)[m_selected_island];
        }
        return QSharedPointer< Island >(nullptr);
    }

    QSharedPointer< Region > ProjectManager::selectedRegion()
    {
        if (m_selected_region == RegionType::kUnknown)
        {
            return QSharedPointer< Region >(nullptr);
        }

        QSharedPointer< Island > i = selectedIsland();
        if (i)
        {
            switch (m_selected_region)
            {
                case RegionType::kPerimeter:
                    return i->perimeters();
                    break;
                case RegionType::kInset:
                    return i->insets();
                    break;
                case RegionType::kSkin:
                    return i->skin();
                    break;
                case RegionType::kInfill:
                    return i->infill();
                    break;
            }
        }
        return QSharedPointer< Region >(nullptr);
    }

#ifdef DEBUG
    void ProjectManager::selectDebug(uint layer_nr,
                                     uint island_nr,
                                     RegionType region_type,
                                     QString option)
    {
        if (layer_nr >= m_gcode->size())
        {
            deselectDebug();
            return;
        }
        QSharedPointer< GcodeLayer > gcode_layer = (*m_gcode)[layer_nr];
        if (island_nr >= gcode_layer->size())
        {
            deselectDebug();
            return;
        }
        QSharedPointer< Island > island =
            (*gcode_layer)[static_cast< int >(island_nr)];
        QSharedPointer< Region > region = (*island)[region_type];
        if (!region || !region->isDebugOption(option))
        {
            deselectDebug();
            return;
        }
        m_selected_debug_layer  = static_cast< int >(layer_nr);
        m_selected_debug_island = static_cast< int >(island_nr);
        m_selected_debug_region = region_type;
        m_selected_debug_option = option;
    }

    void ProjectManager::paintSelectedDebug(QPainter* painter,
                                            Distance& ratio,
                                            int vertical_offset)
    {
        if (m_selected_debug_layer < 0 || m_selected_debug_island < 0 ||
            m_selected_debug_region == RegionType::kUnknown ||
            m_selected_debug_option.isEmpty())
        {
            deselectDebug();
            return;
        }

        if (m_selected_debug_layer >= m_gcode->size())
        {
            deselectDebug();
            return;
        }
        QSharedPointer< GcodeLayer > gcode_layer =
            (*m_gcode)[m_selected_debug_layer];
        if (m_selected_debug_island >= gcode_layer->size())
        {
            deselectDebug();
            return;
        }
        QSharedPointer< Island > island =
            (*gcode_layer)[static_cast< int >(m_selected_debug_island)];
        QSharedPointer< Region > region = (*island)[m_selected_debug_region];
        if (!region)
        {
            deselectDebug();
            return;
        }

        if (!region->isDebugOption(m_selected_debug_option))
        {
            deselectDebug();
            return;
        }

        region->paintDebug(
            m_selected_debug_option, painter, ratio, vertical_offset);
    }
#endif

    void ProjectManager::deselectMesh()
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        if (m_meshes.contains(m_selected_mesh))
        {
            m_meshes[m_selected_mesh]->selected(false);
        }
        m_selected_mesh = "";

        emit enableCopy(false);
        emit meshSelectionChanged();
    }

    void ProjectManager::deselectGcode()
    {
        m_selected_layer        = -1;
        m_selected_island       = -1;
        m_selected_region       = RegionType::kUnknown;
        m_selected_path         = -1;
        m_selected_path_segment = -1;
    }

#ifdef DEBUG
    void ProjectManager::deselectDebug()
    {
        m_selected_debug_layer  = -1;
        m_selected_debug_island = -1;
        m_selected_debug_region = RegionType::kUnknown;
        m_selected_debug_option = "";
    }
#endif

    QString ProjectManager::copiedMeshName()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        return m_copied_mesh;
    }

    QSharedPointer< Mesh > ProjectManager::copiedMesh()
    {
        QReadLocker read_locker(m_mesh_lock.data());
        if (m_meshes.contains(m_copied_mesh))
        {
            return m_meshes[m_copied_mesh];
        }
        return QSharedPointer< Mesh >(nullptr);
    }

    void ProjectManager::name(QString name)
    {
        m_name = name;
    }

    QString ProjectManager::name()
    {
        return m_name;
    }

    void ProjectManager::filepath(QString filepath)
    {
        m_filepath = filepath;
    }

    QString ProjectManager::filepath()
    {
        return m_filepath;
    }

    void ProjectManager::paste()
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        if (m_copied_mesh.isEmpty())  // Shouldn't happen
        {
            qWarning() << "Paste attempted when there was no copied mesh";
            return;
        }

        if (!m_meshes.contains(m_copied_mesh))  // Once again, shouldn't happen
        {
            qWarning() << "Paste attempted with mesh that doesn't exist";
            return;
        }

        QSharedPointer< Mesh > new_mesh(new Mesh(*m_meshes[m_copied_mesh]));
        write_locker.unlock();
        addMesh(new_mesh);
    }

    QString ProjectManager::currentMachineName()
    {
        return m_current_machine_setting;
    }

    QSharedPointer< SettingsBase > ProjectManager::currentMachine()
    {
        return m_settings_manager->getConfig(ConfigTypes::kMachine,
                                             m_current_machine_setting);
    }

    QString ProjectManager::currentMaterialName()
    {
        return m_current_material_setting;
    }

    QSharedPointer< SettingsBase > ProjectManager::currentMaterial()
    {
        return m_settings_manager->getConfig(ConfigTypes::kMaterial,
                                             m_current_material_setting);
    }

    QString ProjectManager::currentPrintName()
    {
        return m_current_print_setting;
    }

    QSharedPointer< SettingsBase > ProjectManager::currentPrint()
    {
        return m_settings_manager->getConfig(ConfigTypes::kPrint,
                                             m_current_print_setting);
    }

    QString ProjectManager::currentNozzleName()
    {
        return m_current_nozzle_setting;
    }

    QSharedPointer< SettingsBase > ProjectManager::currentNozzle()
    {
        return m_settings_manager->getConfig(ConfigTypes::kNozzle,
                                             m_current_nozzle_setting);
    }

    void ProjectManager::currentMachineName(QString machine)
    {
        m_current_machine_setting = machine;
        emit currentMachineChanged(machine);
        updateGlobalSettings();
    }

    void ProjectManager::currentPrintName(QString print)
    {
        m_current_print_setting = print;
        emit currentPrintChanged(print);
        updateGlobalSettings();
    }

    void ProjectManager::currentMaterialName(QString material)
    {
        m_current_material_setting = material;
        emit currentMaterialChanged(material);
        updateGlobalSettings();
    }

    void ProjectManager::currentNozzleName(QString nozzle)
    {
        m_current_nozzle_setting = nozzle;
        emit currentNozzleChanged(nozzle);
        updateGlobalSettings();
    }

    void ProjectManager::updateGlobalSettings()
    {
        m_settings_manager->updateGlobal(m_current_machine_setting,
                                         m_current_material_setting,
                                         m_current_print_setting,
                                         m_current_nozzle_setting);
    }

    QVector< QPair< QString, QString > > ProjectManager::recentProjects()
    {
        return m_recent_projects;
    }

    void ProjectManager::markRecent()
    {
        if (!m_filepath.isEmpty() && !m_name.isEmpty())
        {
            QPair< QString, QString > project(m_filepath, m_name);

            // Remove if exists
            m_recent_projects.removeAll(project);

            // Move to front
            m_recent_projects.prepend(project);
        }

        // 10 elements maximum
        if (m_recent_projects.size() == 11)
        {
            m_recent_projects.remove(10);
        }
    }

    void ProjectManager::closeProject()
    {
        QWriteLocker write_locker(m_mesh_lock.data());
        m_name = "";
        m_meshes.clear();
        m_selected_mesh = "";
        m_copied_mesh   = "";
        // No meshes left so empty QStringList
        emit updateList(QStringList());
    }

    void ProjectManager::exportRecentProjects()
    {
        QDir app_data(QStandardPaths::writableLocation(
            QStandardPaths::AppLocalDataLocation));
        if (!app_data.exists())
        {
            app_data.mkpath(".");
        }
        QFile recent_projects_file(QStandardPaths::writableLocation(
                                       QStandardPaths::AppLocalDataLocation) +
                                   QDir::separator() + "recent_projects.json");
        recent_projects_file.open(QFile::WriteOnly);
        json recent_projects_json;
        for (QPair< QString, QString > project : m_recent_projects)
        {
            json object;
            object["filepath"] = project.first;
            object["name"]     = project.second;
            recent_projects_json.push_back(object);
        }
        recent_projects_file.write(recent_projects_json.dump(4).c_str());
    }

}  // namespace ORNL
