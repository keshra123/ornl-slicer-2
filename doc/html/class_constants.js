var class_constants =
[
    [ "DataTypes", "class_constants_1_1_data_types.html", null ],
    [ "MachineCategories", "class_constants_1_1_machine_categories.html", null ],
    [ "MachineSettings", "class_constants_1_1_machine_settings.html", null ],
    [ "MaterialCategories", "class_constants_1_1_material_categories.html", null ],
    [ "OpenGL", "class_constants_1_1_open_g_l.html", null ],
    [ "ProfileCategories", "class_constants_1_1_profile_categories.html", null ],
    [ "SettingPart", "class_constants_1_1_setting_part.html", null ],
    [ "Units", "class_constants_1_1_units.html", null ],
    [ "SettingsTabs", "class_constants.html#a60bafe696a3e4c5fdb67042cdf4f8af4", [
      [ "MACHINE", "class_constants.html#a60bafe696a3e4c5fdb67042cdf4f8af4aff80abb88ab4ecc4e757b0025c160524", null ],
      [ "PROFILE", "class_constants.html#a60bafe696a3e4c5fdb67042cdf4f8af4a0a59641c685e96ff7bf37a127f2785da", null ],
      [ "MATERIAL", "class_constants.html#a60bafe696a3e4c5fdb67042cdf4f8af4a8a7771347e16d30eb481f85acfbf5153", null ]
    ] ]
];