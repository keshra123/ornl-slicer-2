#!/usr/bin/env bash

# Install git pre-commit hooks.
# The root directory of the target local git repository is expected as a parameter.

. "$(dirname -- "$0")/canonicalize_filename.sh"

# Exit on error
set -e

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT="$(canonicalize_filename "$0")"

# Absolute path this script is in, e.g. /home/user/bin
SCRIPTPATH="$(dirname -- "$SCRIPT")"

CONFIG="$SCRIPTPATH/pre-commit.cfg"

if [ ! -f "$CONFIG" ]; then
	echo "Missing config file $CONFIG."
	exit 1
else
	. "$CONFIG"
fi

# Copy hooks to the firectory specified as the parameter
copy_hooks(){
	# We take the hooks to be installed from the pre-commit config but need to
	# make sure that it is installed as well as it is required to actually run
	# the hooks.
	HOOKS="pre-commit $HOOKS"

	echo "Copying hooks to destination directory:"
	for hook in $HOOKS
	do
		echo "Copying $hook to $1/hooks"
		cp -i -- "$SCRIPTPATH/$hook" "$1/hooks" || true

		if [ -f "$SCRIPTPATH/$hook.cfg" ]; then
			echo "Copying config $hook.cfg."
			cp -i -- "$SCRIPTPATH/$hook.cfg" "$1/hooks" || true

		fi
	done

	echo ""
	echo "Checking if hooks are executable:"
	for hook in $HOOKS
	do
		if [ ! -x "$1/hooks/$hook" ]; then
			echo "$hook is not executable. Fix it by calling"
			echo "chmod +x $1/hooks/$hook"
		else
			echo "$hook OK."
		fi
	done
}

echo ""
echo "Git pre-commit hook installation."
echo ""
if [ $# = 1 ] ; then
	if [ -d "$1/.git" ] ; then
		# Create hooks subfilder if it does not yet exist
		mkdir -p -- "$1/.git/hooks"

		echo "Copying prerequisites."
        cp -i -- "$SCRIPTPATH/canonicalize_filename.sh" "$1/.git/hooks/" || true
        echo ""
        copy_hooks "$1/.git"
        echo ""
        echo "Finished installation."
	else
		echo "Error: $1/.git does not exist."
        echo "Are you sure $1 is the root directory of your local git repository?"
	fi
else
	echo "Call this script with the destination git repository as a parameter"
fi
