#include "gcode/parsers/common_parser.h"

#include <QString>
#include <QStringList>
#include <QVector>

#include "exceptions/exceptions.h"
#include "units/unit.h"

namespace ORNL
{
    CommonParser::CommonParser()
        : m_distance_unit(mm)
        , m_time_unit(s)
        , m_angle_unit(rad)
        , m_mass_unit(g)
        , m_velocity_unit(mm / s)
        , m_acceleration_unit(mm / s / s)
    {}

    CommonParser::CommonParser(Distance distance_unit,
                               Time time_unit,
                               Mass mass_unit,
                               Angle angle_unit,
                               Velocity velocity_unit,
                               Acceleration acceleration_unit)
        : m_distance_unit(distance_unit)
        , m_time_unit(time_unit)
        , m_angle_unit(angle_unit)
        , m_mass_unit(mass_unit)
        , m_velocity_unit(velocity_unit)
        , m_acceleration_unit(acceleration_unit)
    {}

    GcodeCommand CommonParser::getCurrentCommand()
    {
        return m_current_gcode_command;
    }


    void CommonParser::config()
    {
        // TODO: Add common commands that all machines use here.

        // Clears the command mappings to prevent any previous from interferring
        reset();

        addCommandMapping(
            "G0",
            std::bind(&CommonParser::G0Handler, this, std::placeholders::_1));
        addCommandMapping(
            "G1",
            std::bind(&CommonParser::G1Handler, this, std::placeholders::_1));
        addCommandMapping(
            "G2",
            std::bind(&CommonParser::G2Handler, this, std::placeholders::_1));
        addCommandMapping(
            "G3",
            std::bind(&CommonParser::G3Handler, this, std::placeholders::_1));
        addCommandMapping(
            "G4",
            std::bind(&CommonParser::G4Handler, this, std::placeholders::_1));

        setLineCommentString(";");
    }


    // TODO: Allow these functions to pass thier own conversion type but it
    // defaults to the current one.
    NT CommonParser::getXPos() const
    {
        return getXPos(m_distance_unit);
    }

    NT CommonParser::getXPos(Distance distance_unit) const
    {
        return m_current_x.to(distance_unit);
    }


    NT CommonParser::getYPos() const
    {
        return getYPos(m_distance_unit);
    }

    NT CommonParser::getYPos(Distance distance_unit) const
    {
        return m_current_y.to(distance_unit);
    }


    NT CommonParser::getZPos() const
    {
        return getZPos(m_distance_unit);
    }

    NT CommonParser::getZPos(Distance distance_unit) const
    {
        return m_current_z.to(distance_unit);
    }


    NT CommonParser::getWPos() const
    {
        return getWPos(m_distance_unit);
    }

    NT CommonParser::getWPos(Distance distance_unit) const
    {
        return m_current_w.to(distance_unit);
    }


    NT CommonParser::getArcXPos() const
    {
        return getArcXPos(m_distance_unit);
    }

    NT CommonParser::getArcXPos(Distance distance_unit) const
    {
        return m_current_arc_center_x.to(distance_unit);
    }


    NT CommonParser::getArcYPos() const
    {
        return getArcYPos(m_distance_unit);
    }

    NT CommonParser::getArcYPos(Distance distance_unit) const
    {
        return m_current_arc_center_y.to(distance_unit);
    }


    NT CommonParser::getSpeed() const
    {
        return getSpeed(m_velocity_unit);
    }

    NT CommonParser::getSpeed(Velocity velocity_unit) const
    {
        return m_current_speed.to(velocity_unit);
    }


    NT CommonParser::getSpindleSpeed() const
    {
        return getSpindleSpeed(rev / minute);
    }

    NT CommonParser::getSpindleSpeed(
        AngularVelocity angular_velocity_unit) const
    {
        return m_current_spindle_speed.to(angular_velocity_unit);
    }


    NT CommonParser::getAcceleration() const
    {
        return getAcceleration(m_acceleration_unit);
    }

    NT CommonParser::getAcceleration(Acceleration acceleration_unit) const
    {
        return m_current_acceleration.to(acceleration_unit);
    }


    NT CommonParser::getSleepTime() const
    {
        return getSleepTime(m_time_unit);
    }

    NT CommonParser::getSleepTime(Time time_unit) const
    {
        return m_sleep_time.to(time_unit);
    }

    void CommonParser::reset()
    {
        resetInternalState();
        m_current_x = 0 * m_distance_unit;
        m_current_y = 0 * m_distance_unit;
        m_current_z = 0 * m_distance_unit;
        m_current_w = 0 * m_distance_unit;

        m_current_arc_center_x = 0 * m_distance_unit;
        m_current_arc_center_y = 0 * m_distance_unit;

        m_current_speed = 0 * m_velocity_unit;

        m_current_spindle_speed = 0.0 * m_angle_unit / m_time_unit;

        m_current_acceleration = 0 * m_acceleration_unit;

        m_sleep_time               = 0 * m_time_unit;
        m_purge_time               = 0 * m_time_unit;
        m_wait_to_wipe_time        = 0 * m_time_unit;
        m_wait_time_to_start_purge = 0 * m_time_unit;

        m_pump_ON                 = false;
        m_dynamic_spindle_control = false;
        m_park                    = false;
    }

    const QString& CommonParser::getParserID() const
    {
        return m_parser_id;
    }


    void CommonParser::setXPos(NT value)
    {
        m_current_x = value * m_distance_unit;
    }

    void CommonParser::setYPos(NT value)
    {
        m_current_y = value * m_distance_unit;
    }

    void CommonParser::setZPos(NT value)
    {
        m_current_z = value * m_distance_unit;
    }

    void CommonParser::setWPos(NT value)
    {
        m_current_w = value * m_distance_unit;
    }

    void CommonParser::setArcXPos(NT value)
    {
        m_current_arc_center_x = value * m_distance_unit;
    }

    void CommonParser::setArcYPos(NT value)
    {
        m_current_arc_center_y = value * m_distance_unit;
    }

    void CommonParser::setArcZPos(NT value)
    {
        m_current_arc_center_z = value * m_distance_unit;
    }

    void CommonParser::setSpeed(NT value)
    {
        m_current_speed = value * m_velocity_unit;
    }

    // TODO: Figure out if this is a constnat RPM or not
    void CommonParser::setSpindleSpeed(NT value)
    {
        m_current_spindle_speed = value * rev / minute;
    }

    void CommonParser::setAcceleration(NT value)
    {
        m_current_acceleration = value * m_acceleration_unit;
    }

    void CommonParser::setSleepTime(NT value)
    {
        m_sleep_time = value * m_time_unit;
    }

    void CommonParser::G0Handler(QStringList& params)
    {
        if (params.empty())
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "No parameters for command G0, on line number "
                << m_current_gcode_command.getLineNumber()
                << ". Need at least one for this command." << endl
                << "GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        char current_parameter;
        NT current_value;
        bool no_error, x_not_used = true, y_not_used = true, z_not_used = true,
                       w_not_used = true;
        // bool parameter_used[4] = { false, false, false, false }; // Checks if
        // a parameter has already been used in a GCode command
        //                                                       // 0 - X, 1 -
        //                                                       Y, 2 - Z, 3 -
        //                                                       W.

        for (QStringList::Iterator i = params.begin(); i != params.end(); i++)
        {
            // Retriving the first character in the QString and making it a char
            current_parameter = (*i).at(0).toLatin1();
            current_value     = (*i).right((*i).size() - 1).toFloat(&no_error);
            if (!no_error)
            {
                throwFloatConversionErrorException();
            }

            m_current_gcode_command.addParameter(current_parameter,
                                                 current_value);

            switch (current_parameter)
            {
                case ('X'):
                case ('x'):
                    if (x_not_used)
                    {
                        setXPos(current_value);
                        x_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Y'):
                case ('y'):
                    if (y_not_used)
                    {
                        setYPos(current_value);
                        y_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Z'):
                case ('z'):
                    if (z_not_used)
                    {
                        setZPos(current_value);
                        z_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('W'):
                case ('w'):
                    if (w_not_used)
                    {
                        setWPos(current_value);
                        w_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                default:
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Error: Unknown parameter " << *i
                        << " on GCode line "
                        << m_current_gcode_command.getLineNumber()
                        << ", for GCode command G0" << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);
                    break;
            }
        }
    }

    void CommonParser::G1Handler(QStringList& params)
    {
        if (params.empty())
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "No parameters for command G1, on line number "
                << m_current_gcode_command.getLineNumber()
                << ". Need at least one for this command." << endl
                << "GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        char current_parameter;
        NT current_value;
        bool no_error, x_not_used = true, y_not_used = true, z_not_used = true,
                       w_not_used = true, f_not_used = true;
        // bool parameter_used[5] = { false, false, false, false, false }; //
        // Checks if a parameter has already been used in a GCode command
        //                                                              // 0 -
        //                                                              X, 1 -
        //                                                              Y, 2 -
        //                                                              Z, 3 -
        //                                                              W, 4 - F

        for (QStringList::Iterator i = params.begin(); i != params.end(); i++)
        {
            // Retriving the first character in the QString and making it a char
            current_parameter = (*i).at(0).toLatin1();
            current_value     = (*i).right((*i).size() - 1).toFloat(&no_error);
            if (!no_error)
            {
                throwFloatConversionErrorException();
            }

            m_current_gcode_command.addParameter(current_parameter,
                                                 current_value);

            switch (current_parameter)
            {
                case ('X'):
                case ('x'):
                    if (x_not_used)
                    {
                        setXPos(current_value);
                        x_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Y'):
                case ('y'):
                    if (y_not_used)
                    {
                        setYPos(current_value);
                        y_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Z'):
                case ('z'):
                    if (z_not_used)
                    {
                        setZPos(current_value);
                        z_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('W'):
                case ('w'):
                    if (w_not_used)
                    {
                        setWPos(current_value);
                        w_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('F'):
                case ('f'):
                    if (f_not_used)
                    {
                        setSpeed(current_value);
                        f_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                default:
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Error: Unknown parameter " << *i
                        << " on GCode line "
                        << m_current_gcode_command.getLineNumber()
                        << ", for GCode command G1" << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);

                    break;
            }
        }

        // Checks if the command did not use a movement command, but only a flow
        // command. Not needed. if( x_not_used && y_not_used && z_not_used &&
        // w_not_used )
        // {
        //     QString exceptionString;
        //    QTextStream(&exceptionString) << "Error no movement command passed
        //    with flow rate on GCode line "
        //                                  <<
        //                                  m_current_gcode_command.getLineNumber()
        //                                  << endl
        //                                  << "With GCode command string: "
        //                                  << getCurrentCommandString();
        //    throw IllegalParameterException(exceptionString);
        // }
    }

    void CommonParser::G2Handler(QStringList& params)
    {
        if (params.empty())
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "No parameters for command G2, on line number "
                << m_current_gcode_command.getLineNumber()
                << ". Need at least one for this command." << endl
                << "GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        char current_parameter;
        NT current_value;
        NT temp_x = getXPos(), temp_y = getYPos(), temp_z = getZPos();
        bool no_error,
            x_not_used = true  // true if the parameter has not been processed,
            ,
            y_not_used = true  // false if the parameter has been processed
            ,
            z_not_used = true, i_not_used = true, j_not_used = true,
            k_not_used = true, f_not_used = true;

        for (QStringList::Iterator i = params.begin(); i != params.end(); i++)
        {
            // Retriving the first character in the QString and making it a char
            current_parameter = (*i).at(0).toLatin1();
            current_value     = (*i).right((*i).size() - 1).toFloat(&no_error);
            if (!no_error)
            {
                throwFloatConversionErrorException();
            }

            m_current_gcode_command.addParameter(current_parameter,
                                                 current_value);

            switch (current_parameter)
            {
                case ('X'):
                case ('x'):
                    if (x_not_used)
                    {
                        temp_x     = current_value;
                        x_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Y'):
                case ('y'):
                    if (y_not_used)
                    {
                        temp_y     = current_value;
                        y_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;


                case ('Z'):
                case ('z'):
                    if (z_not_used)
                    {
                        temp_z     = current_value;
                        z_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('F'):
                case ('f'):
                    if (f_not_used)
                    {
                        setSpeed(current_value);
                        f_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('I'):
                case ('i'):
                    if (i_not_used)
                    {
                        setArcXPos(getXPos() + current_value);
                        i_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('J'):
                case ('j'):
                    if (j_not_used)
                    {
                        setArcYPos(getYPos() + current_value);
                        j_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;


                case ('K'):
                case ('k'):
                    if (k_not_used)
                    {
                        setArcZPos(getZPos() + current_value);
                        k_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                default:
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Error: Unknown parameter " << *i
                        << " on GCode line "
                        << m_current_gcode_command.getLineNumber()
                        << ", for GCode command G2" << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);
                    break;
            }
        }

        // Checks if all required paramters have been used
        // TODO: Need this to be 2/3 and the associated thing.
        if (x_not_used || y_not_used || i_not_used || j_not_used)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error not all required parameters passed for GCode command "
                   "on line  "
                << m_current_gcode_command.getLineNumber() << endl
                << "With GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        setXPos(temp_x);
        setYPos(temp_y);
    }

    void CommonParser::G3Handler(QStringList& params)
    {
        if (params.empty())
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "No parameters for command G3, on line number "
                << m_current_gcode_command.getLineNumber()
                << ". Need at least one for this command." << endl
                << "GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        char current_parameter;
        NT current_value;
        NT temp_x = getXPos(), temp_y = getYPos(), temp_z = getZPos();
        bool no_error,
            x_not_used = true  // true if the parameter has not been processed,
            ,
            y_not_used = true  // false if the parameter has been processed
            ,
            z_not_used = true, i_not_used = true, j_not_used = true,
            k_not_used = true, f_not_used = true;


        for (QStringList::Iterator i = params.begin(); i != params.end(); i++)
        {
            // Retriving the first character in the QString and making it a char
            current_parameter = (*i).at(0).toLatin1();
            current_value     = (*i).right((*i).size() - 1).toFloat(&no_error);
            if (!no_error)
            {
                QString exceptionString;
                QTextStream(&exceptionString)
                    << "Error with float conversion on GCode line "
                    << m_current_gcode_command.getLineNumber() << "." << endl
                    << "With GCode command string: "
                    << getCurrentCommandString();
                throw IllegalParameterException(exceptionString);
            }

            m_current_gcode_command.addParameter(current_parameter,
                                                 current_value);

            switch (current_parameter)
            {
                case ('X'):
                case ('x'):
                    if (x_not_used)
                    {
                        temp_x     = current_value;
                        x_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Y'):
                case ('y'):
                    if (y_not_used)
                    {
                        temp_y     = current_value;
                        y_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('Z'):
                case ('z'):
                    if (z_not_used)
                    {
                        temp_z     = current_value;
                        z_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('F'):
                case ('f'):
                    if (f_not_used)
                    {
                        setSpeed(current_value);
                        f_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('I'):
                case ('i'):
                    if (i_not_used)
                    {
                        setArcXPos(getXPos() + current_value);
                        i_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('J'):
                case ('j'):
                    if (j_not_used)
                    {
                        setArcYPos(getYPos() + current_value);
                        j_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                case ('K'):
                case ('k'):
                    if (k_not_used)
                    {
                        setArcZPos(getZPos() + current_value);
                        k_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;

                default:
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Error: Unknown parameter " << *i
                        << " on GCode line "
                        << m_current_gcode_command.getLineNumber()
                        << ", for GCode command G3" << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);
                    break;
            }
        }

        // Checks if all required paramters have been used
        // TODO: Need this to be 2/3 and the associated thing.
        // TODO: Need to add logic for Z.
        int num_not_used = 0;
        num_not_used += x_not_used + y_not_used + z_not_used;
        if (num_not_used > 1)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error: Too little parameters used for";
        }
        if (x_not_used || y_not_used || i_not_used || j_not_used)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error not all required parameters passed for GCode command "
                   "on line  "
                << m_current_gcode_command.getLineNumber() << endl
                << "With GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }
        setXPos(temp_x);
        setYPos(temp_y);
        setZPos(temp_z);
    }

    // TODO: Need to find out if G4 command is an int or a float
    //       Guide suggests int, Slicer 1 code suggests double
    void CommonParser::G4Handler(QStringList& params)
    {
        if (params.empty())
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "No parameters for command G4, on line number "
                << m_current_gcode_command.getLineNumber()
                << ". Need at least one for this command." << endl
                << "GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }

        char current_parameter;
        NT current_value;
        bool no_error, p_not_used = true;

        for (QStringList::Iterator i = params.begin(); i != params.end(); i++)
        {
            // Retriving the first character in the QString and making it a char
            current_parameter = (*i).at(0).toLatin1();
            current_value     = (*i).right((*i).size() - 1).toFloat(&no_error);
            if (!no_error)
            {
                throwFloatConversionErrorException();
            }

            m_current_gcode_command.addParameter(current_parameter,
                                                 current_value);

            switch (current_parameter)
            {
                case ('P'):
                case ('p'):
                    if (p_not_used)
                    {
                        setSleepTime(current_value);
                        p_not_used = false;
                    }
                    else
                    {
                        throwMultipleParameterException(current_parameter);
                    }
                    break;
                default:
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Error: Unknown parameter " << *i
                        << " on GCode line "
                        << m_current_gcode_command.getLineNumber()
                        << ", for GCode command G4" << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);
                    break;
            }
        }

        if (p_not_used)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error not all required parameters passed for GCode command "
                   "on line  "
                << m_current_gcode_command.getLineNumber() << endl
                << "With GCode command string: " << getCurrentCommandString();
            throw IllegalParameterException(exceptionString);
        }
    }

    void CommonParser::IGNORE()
    {}

    void CommonParser::IGNORE_WITH_PARAMS(QStringList& params)
    {}
    void CommonParser::throwMultipleParameterException(char parameter)
    {
        QString exceptionString;
        QTextStream(&exceptionString)
            << "Error: Multiple " << parameter
            << " parameters passed on GCode line "
            << m_current_gcode_command.getLineNumber() << endl
            << "With GCode command srting: " << getCurrentCommandString();
        throw IllegalParameterException(exceptionString);
    }

    void CommonParser::throwFloatConversionErrorException()
    {
        QString exceptionString;
        QTextStream(&exceptionString)
            << "Error with float conversion on GCode line "
            << m_current_gcode_command.getLineNumber() << "." << endl
            << "With GCode command string: " << getCurrentCommandString();
        throw IllegalParameterException(exceptionString);
    }

    void CommonParser::throwIntegerConversionErrorException()
    {
        QString exceptionString;
        QTextStream(&exceptionString)
            << "Error with interger conversion on GCode line "
            << m_current_gcode_command.getLineNumber() << "." << endl
            << "With GCode command string: " << getCurrentCommandString();
        throw IllegalParameterException(exceptionString);
    }
}  // namespace ORNL
