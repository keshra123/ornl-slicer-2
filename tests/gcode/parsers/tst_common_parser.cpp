#include <QPair>
#include <QList>
#include <iostream>

#include "gcode/gcode_command.h"
#include "gcode/parsers/common_parser.h"
#include "gcode/parsers/tst_common_parser.h"
#include "exceptions/exceptions.h"

#if 0 // TODO fix tests

void TestCommonParser::initTestCase()
{
    // test->config();
	test = new CommonParser();
    test->config();
}

void TestCommonParser::cleanupTestCase()
{
    //test->reset();
	delete test;
}

void TestCommonParser::init()
{
}

void TestCommonParser::cleanup()
{
   // test->reset();
}


// TODO: Make some QVERIFY messages for normal extraction tests use QVERIFY2
//       to have a message associated with them rather than just 'false'.
void TestCommonParser::testG0Handler()
{
    // TODO: Need to change all compares and verifies to use QVERIFY2, so a message can be appended to it for clarity.
    const QString GcodeString = "G0 X1.0 Y1.0 Z1.0"; // Normal extraction test
    const QString GcodeString_missing_X = "G0 Y220.0 Z300.0"; // Normal extraction w/o x
    const QString GcodeString_missing_Y = "G0 X220.0 Z300.0"; // Normal extraction w/o y
    const QString GcodeString_missing_Z = "G0 X220.0 Y300.0"; // Normal extraction w/o z
    const QString GcodeString_missing_X_and_Y = "G0 Z100.0"; // Normal extraaction w/o x & y
    const QString GcodeString_missing_X_and_Z = "G0 Y100.0"; // Normal extraaction w/o x & z
    const QString GcodeString_missing_Y_and_Z = "G0 X100.0"; // Normal extraaction w/o y & z
    const QString GcodeString_scrambled_parameters = "G0 Z1.0 X2.0 Y3.0"; // Normal extraction, just randomized
    const QString GcodeString_no_params = "G0"; // No parameteres passed with GCode
    // const QString GcodeString_no_period = "G0 X0.1 Y-0.1 Z032"; // Invalid parameter test
    const QString GcodeString_no_number = "G0 X0.1 Y-0.1 Z"; // No parameter test
    // const QString GcodeString_too_long_for_float = "G0 X0.1 Y-0.1 Z0000.4412321321321312312312321312"; // Float error test
    const QString GcodeString_random_parameter = "G0 X1.1 Y7.1 Z8.0 A11"; // Erroneous parameter input
    const QString GcodeString_multiple_of_the_same_param = "G0 X1.0 X2.0"; // Two of the same parameter (maybe will pass but probably a mistake and shouldn't run)


    // Test for GcodeString
    try { test->parseCommand(GcodeString, 1); }
    catch(IllegalArgumentException& e) { qDebug() << e.what(); }
    catch(...) { QVERIFY2(false, "Message should have parsed correctly but threw an exception."); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 1.0);
    const QPair<char, float> y_val = qMakePair('Y', 1.0);
    const QPair<char, float> z_val = qMakePair('Z', 1.0);


    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 3);
    QVERIFY2(*cmd.getParameters().find(x_val.first) == x_val.second, "Missing x value for GcodeString Test");
    QVERIFY2(*cmd.getParameters().find(y_val.first) == y_val.second, "Missing y value for GcodeString Test");
    QVERIFY2(*cmd.getParameters().find(z_val.first) == z_val.second, "Missing z value for GcodeString Test");
    QVERIFY2(cmd.getComment().isEmpty(), "Comment should be empty for Gcodestring test, but it is not");
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GcodeString_missing_X
    try { test->parseCommand(GcodeString_missing_X, 2); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val2 = qMakePair('Y', 220.0);
    const QPair<char, float> z_val2 = qMakePair('Z', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(y_val2.first) == y_val2.second);
    QVERIFY(*cmd.getParameters().find(z_val2.first) == z_val2.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 2);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 220.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GcodeString_missing_Y
    try { test->parseCommand(GcodeString_missing_Y, 3); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val3 = qMakePair('X', 220.0);
    const QPair<char, float> z_val3 = qMakePair('Z', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(x_val3.first) == x_val3.second);
    QVERIFY(*cmd.getParameters().find(z_val3.first) == z_val3.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 3);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 220.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GCodeString_missing_Z
    try { test->parseCommand(GcodeString_missing_Z, 4); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val4 = qMakePair('X', 220.0);
    const QPair<char, float> y_val4 = qMakePair('Y', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(x_val4.first) == x_val4.second);
    QVERIFY(*cmd.getParameters().find(y_val4.first) == y_val4.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 4);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 300.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GCodeString_missing_X_and_Y
    try { test->parseCommand(GcodeString_missing_X_and_Y, 5); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> z_val5 = qMakePair('Z', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(z_val5.first) == z_val5.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 5);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 300.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GCodeString_missing_X_and_Z
    try { test->parseCommand(GcodeString_missing_X_and_Z, 6); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val6 = qMakePair('Y', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(y_val6.first) == y_val6.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 6);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 100.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GCodeString_missing_Y_and_Z
    try { test->parseCommand(GcodeString_missing_Y_and_Z, 7); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val7 = qMakePair('X', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(x_val7.first) == x_val7.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 7);

    QCOMPARE(test->getXPos(), 100.0);
    QCOMPARE(test->getYPos(), 100.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Test for GCodeString_scrambled_parameters
    try { test->parseCommand(GcodeString_scrambled_parameters, 8); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val8 = qMakePair('X', 2.0);
    const QPair<char, float> y_val8 = qMakePair('Y', 3.0);
    const QPair<char, float> z_val8 = qMakePair('Z', 1.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QCOMPARE(cmd.getParameters().size(), 3);
    QVERIFY(*cmd.getParameters().find(x_val8.first) == x_val8.second);
    QVERIFY(*cmd.getParameters().find(y_val8.first) == y_val8.second);
    QVERIFY(*cmd.getParameters().find(z_val8.first) == z_val8.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 8);

    QCOMPARE(test->getXPos(), 2.0);
    QCOMPARE(test->getYPos(), 3.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G0"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Tests whether the function handler correctly throws an exception if there are no parameters
    // passed with the GCode command, as one is required with this command.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_params), IllegalParameterException);

    // Tests whether the function handler correctly checks for the numerical part of the
    // parameter is able to be correctly thrown when the float function cannot parse the
    // number.
    //QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_period), IllegalParameterException);

    // Tests whether the function handler correctly checks for parameters not
    // having a valid number or no number at all
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_number), IllegalParameterException);

    // Tests whether the funciotn correctly checks for numbers that
    // are too precise to convert into float values.
    // ACTUALLY JUST CHOPPED OFF BY FLOAT CONVERSION
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_too_long_for_float), IllegalParameterException);

    // Tests whether the fucntion handler correctly throws an exception when
    // random parameter is passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_random_parameter), IllegalParameterException);

    // Tests whether the funciton handler correctly throws an excetion when
    // multiple of the same parameter are passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_multiple_of_the_same_param), IllegalParameterException);
}



void TestCommonParser::testG1Handler()
{
    // TODO: Need to check for actual changes in the class variables.
    const QString GcodeString = "G1 X11.0 Y221.0 Z31.0 F100.0"; // Normal extraction test
    const QString GcodeString_no_flow = "G1 X112.0 Y41.0 Z21.0"; // Normal extraction test no flow rate
    const QString GcodeString_missing_X = "G1 Y220.0 Z300.0"; // Normal extraction w/o x
    const QString GcodeString_missing_Y = "G1 X220.0 Z300.0"; // Normal extraction w/o y
    const QString GcodeString_missing_Z = "G1 X220.0 Y300.0"; // Normal extraction w/o z
    const QString GcodeString_missing_X_and_Y = "G1 Z100.0"; // Normal extraaction w/o x & y
    const QString GcodeString_missing_X_and_Z = "G1 Y100.0"; // Normal extraaction w/o x & z
    const QString GcodeString_missing_Y_and_Z = "G1 X100.0"; // Normal extraaction w/o y & z
    const QString GcodeString_scrambled_parameters = "G1 Z1.0 X2.0 Y3.0"; // Normal extraction, just randomized
    const QString GcodeString_no_params = "G1"; // No parameteres passed with GCode
    // const QString GcodeString_no_period = "G1 X0.1 Y-0.1 Z032"; // Invalid parameter test
    const QString GcodeString_no_number = "G1 X0.1 Y-0.1 Z"; // No parameter test
    // const QString GcodeString_too_long_for_float = "G1 X0.1 Y-0.1 Z0000.4412321321321312312312321312"; // Float error test
    const QString GcodeString_random_parameter = "G1 X1.3 Y7.4 Z6.0 A3";
    const QString GcodeString_multiple_of_the_same_param = "G1 X1.0 X2.0"; // Two of the same parameter (maybe will pass but probably a mistake and shouldn't run)
    const QString GcodeString_flow_rate_only = "G1 F100.0"; // Only flow rate is passed


    // Test for GcodeString
    try { test->parseCommand(GcodeString, 1); } // Should not throw
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 11.0);
    const QPair<char, float> y_val = qMakePair('Y', 221.0);
    const QPair<char, float> z_val = qMakePair('Z', 31.0);
    const QPair<char, float> f_val = qMakePair('F', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 4);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QVERIFY(*cmd.getParameters().find(y_val.first) == y_val.second);
    QVERIFY(*cmd.getParameters().find(z_val.first) == z_val.second);
    QVERIFY(*cmd.getParameters().find(f_val.first) == f_val.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 11.0);
    QCOMPARE(test->getYPos(), 221.0);
    QCOMPARE(test->getZPos(), 31.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GcodeString_no_flow
    try { test->parseCommand(GcodeString_no_flow, 2); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val2 = qMakePair('X', 112.0);
    const QPair<char, float> y_val2 = qMakePair('Y', 41.0);
    const QPair<char, float> z_val2 = qMakePair('Z', 21.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 3);
    QVERIFY(*cmd.getParameters().find(x_val2.first) == x_val2.second);
    QVERIFY(*cmd.getParameters().find(y_val2.first) == y_val2.second);
    QVERIFY(*cmd.getParameters().find(z_val2.first) == z_val2.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 2);

    QCOMPARE(test->getXPos(), 112.0);
    QCOMPARE(test->getYPos(), 41.0);
    QCOMPARE(test->getZPos(), 21.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GcodeString_missing_X
    try { test->parseCommand(GcodeString_missing_X, 3); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val3 = qMakePair('Y', 220.0);
    const QPair<char, float> z_val3 = qMakePair('Z', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(y_val3.first) == y_val3.second);
    QVERIFY(*cmd.getParameters().find(z_val3.first) == z_val3.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 3);

    QCOMPARE(test->getXPos(), 112.0);
    QCOMPARE(test->getYPos(), 220.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GcodeString_missing_Y
    try { test->parseCommand(GcodeString_missing_Y, 4); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val4 = qMakePair('X', 220.0);
    const QPair<char, float> z_val4 = qMakePair('Z', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(x_val4.first) == x_val4.second);
    QVERIFY(*cmd.getParameters().find(z_val4.first) == z_val4.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 4);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 220.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GCodeString_missing_Z
    try { test->parseCommand(GcodeString_missing_Z, 5); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val5 = qMakePair('X', 220.0);
    const QPair<char, float> y_val5 = qMakePair('Y', 300.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 2);
    QVERIFY(*cmd.getParameters().find(x_val5.first) == x_val5.second);
    QVERIFY(*cmd.getParameters().find(y_val5.first) == y_val5.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 5);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 300.0);
    QCOMPARE(test->getZPos(), 300.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GCodeString_missing_X_and_Y
    try { test->parseCommand(GcodeString_missing_X_and_Y, 6); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> z_val6 = qMakePair('Z', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(z_val6.first) == z_val6.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 6);

    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 300.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GCodeString_missing_X_and_Z
    try { test->parseCommand(GcodeString_missing_X_and_Z, 7); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val7 = qMakePair('Y', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(y_val7.first) == y_val7.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 7);


    QCOMPARE(test->getXPos(), 220.0);
    QCOMPARE(test->getYPos(), 100.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GCodeString_missing_Y_and_Z
    try { test->parseCommand(GcodeString_missing_Y_and_Z, 8); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val8 = qMakePair('X', 100.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(x_val8.first) == x_val8.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 8);

    QCOMPARE(test->getXPos(), 100.0);
    QCOMPARE(test->getYPos(), 100.0);
    QCOMPARE(test->getZPos(), 100.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Test for GCodeString_scrambled_parameters
    try { test->parseCommand(GcodeString_scrambled_parameters, 9); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val9 = qMakePair('X', 2.0);
    const QPair<char, float> y_val9 = qMakePair('Y', 3.0);
    const QPair<char, float> z_val9 = qMakePair('Z', 1.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 3);
    QVERIFY(*cmd.getParameters().find(x_val9.first) == x_val9.second);
    QVERIFY(*cmd.getParameters().find(y_val9.first) == y_val9.second);
    QVERIFY(*cmd.getParameters().find(z_val9.first) == z_val9.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 9);

    QCOMPARE(test->getXPos(), 2.0);
    QCOMPARE(test->getYPos(), 3.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

    // Tests whether the function handler correctly throws an exception if there are no parameters
    // passed with the GCode command, as one is required with this command.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_params), IllegalParameterException);

    // Tests whether the function handler correctly checks for the numerical part of the
    // parameter is able to be correctly thrown when the float fucntion cannot parse the
    // number.
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_period), IllegalParameterException);

    // Tests whether the function handler correctly checks for parameters not
    // having a valid number or no number at all
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_number), IllegalParameterException);

    // Tests whether the funciotn correctly checks for numbers that
    // are too precise to convert into float values.
    // TODO: Need to check if this is needed to be checked
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_too_long_for_float), IllegalParameterException);

    // Tests whether the fucntion handler correctly throws an exception when
    // random parameter is passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_random_parameter), IllegalParameterException);

    // Tests whether the function handler correctly throws an exception when multiple of the
    // same parameter are passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_multiple_of_the_same_param), IllegalParameterException);


    const QPair<char, float> f_val10 = qMakePair('F', 100.0);
    try { test->parseCommand(GcodeString_flow_rate_only, 10); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(f_val10.first) == f_val10.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 10);

    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G1"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G1"));

}

void TestCommonParser::testG2Handler()
{
    // TODO: Need to check for actual changes in the class variables.
    const QString GcodeString = "G2 X5.0 Y71.0 I5.0 J5.0 F100.0"; // Normal extraction test
    const QString GcodeString_no_F = "G2 X2.0 Y1.0 I40.0 J77.0"; // Normal extraction test w/o F
    const QString GcodeString_scrambled_parameters = "G2 Y5.0 J71.0 X5.0 F5.0 I100.0"; // Normal extraction, scrambled parameters
    const QString GcodeString_no_params = "G2"; // No parameteres passed with GCode
    const QString GcodeString_missing_X_param = "G2 Y1.0 I1.0 J1.0"; // Missing X parameter
    const QString GcodeString_missing_Y_param = "G2 X1.0 I1.0 J1.0"; // Missing Y parameter
    const QString GcodeString_missing_I_param = "G2 X1.0 Y1.0 J1.0"; // Missing I parameter
    const QString GcodeString_missing_J_param = "G2 X1.0 Y1.0 I1.0"; // Missing J parameter
    // const QString GcodeString_no_period = "G2 X0.3 Y-0323278 I2.0 J2.0"; // Invalid parameter test
    const QString GcodeString_no_number = "G2 X1.1 Y I1.0 J1.0"; // No parameter test
    // const QString GcodeString_too_long_for_float = "G2 X0.1 Y-0.100000322222222111111132 I1.0 J1.0"; // Y should have too high of a precision for a float to hold
    const QString GcodeString_random_parameter = "G2 X1.3 Y7.4 Z6.0 I2.0 J2.0"; // Z is not used in the G2 command
    const QString GcodeString_multiple_of_the_same_parameter = "G2 X1.3 X2.0 Y5.0 I5.0 J5.0"; // X is used twice.
    const QString GcodeString_flow_rate_only = "G2 F100.0"; // Only flow rate is passed

    const QString GcodeString_RESET = "G0 X0.0 Y0.0 Z0.0";

    // Resetting the internal Gcode command.
    try { test->parseCommand(GcodeString_RESET, 1); }
    catch(...) { QVERIFY(false); }

    // Tests regular parsing with F
    try { test->parseCommand(GcodeString, 1); }
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 5.0);
    const QPair<char, float> y_val = qMakePair('Y', 71.0);
    const QPair<char, float> j_val = qMakePair('J', 5.0);
    const QPair<char, float> i_val = qMakePair('I', 5.0);
    const QPair<char, float> f_val = qMakePair('F', 100.0);


    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 2);
    QCOMPARE(cmd.getParameters().size(), 5);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QVERIFY(*cmd.getParameters().find(y_val.first) == y_val.second);
    QVERIFY(*cmd.getParameters().find(j_val.first) == j_val.second);
    QVERIFY(*cmd.getParameters().find(i_val.first) == i_val.second);
    QVERIFY(*cmd.getParameters().find(f_val.first) == f_val.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 5.0);
    QCOMPARE(test->getYPos(), 71.0);
    QCOMPARE(test->getArcXPos(), 5.0);
    QCOMPARE(test->getArcYPos(), 5.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G2"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G2"));

    // Tests regular parsig w/o F
    try { test->parseCommand(GcodeString_no_F, 2); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val2 = qMakePair('X', 2.0);
    const QPair<char, float> y_val2 = qMakePair('Y', 1.0);
    const QPair<char, float> j_val2 = qMakePair('J', 77.0);
    const QPair<char, float> i_val2 = qMakePair('I', 40.0);


    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 2);
    QCOMPARE(cmd.getParameters().size(), 4);
    QVERIFY(*cmd.getParameters().find(x_val2.first) == x_val2.second);
    QVERIFY(*cmd.getParameters().find(y_val2.first) == y_val2.second);
    QVERIFY(*cmd.getParameters().find(j_val2.first) == j_val2.second);
    QVERIFY(*cmd.getParameters().find(i_val2.first) == i_val2.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 2);

    QCOMPARE(test->getXPos(), 2.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getArcXPos(), 45.0);
    QCOMPARE(test->getArcYPos(), 148.0);
    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G2"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G2"));

    // Tests regular parsing witht he parameters scrambled
    try { test->parseCommand(GcodeString_scrambled_parameters, 3); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val3 = qMakePair('X', 5.0);
    const QPair<char, float> y_val3 = qMakePair('Y', 5.0);
    const QPair<char, float> j_val3 = qMakePair('J', 71.0);
    const QPair<char, float> i_val3 = qMakePair('I', 100.0);
    const QPair<char, float> f_val3 = qMakePair('F', 5.0);


    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 2);
    QCOMPARE(cmd.getParameters().size(), 5);
    QVERIFY(*cmd.getParameters().find(x_val3.first) == x_val3.second);
    QVERIFY(*cmd.getParameters().find(y_val3.first) == y_val3.second);
    QVERIFY(*cmd.getParameters().find(j_val3.first) == j_val3.second);
    QVERIFY(*cmd.getParameters().find(i_val3.first) == i_val3.second);
    QVERIFY(*cmd.getParameters().find(f_val3.first) == f_val3.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 3);

    QCOMPARE(test->getXPos(), 5.0);
    QCOMPARE(test->getYPos(), 5.0);
    QCOMPARE(test->getArcXPos(), 102.0);
    QCOMPARE(test->getArcYPos(), 72.0);
    QCOMPARE(test->getSpeed(), 5.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G2"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G2"));

    // Tests whether the function handler correctly throws an exception if there are no parameters
    // passed with the GCode command, as one is required with this command.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_params), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the X parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_X_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the Y parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_Y_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the J parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_J_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the I parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_I_param), IllegalParameterException);

    // Tests whether the function handler correctly checks for the numerical part of the
    // parameter is able to be correctly thrown when the float fucntion cannot parse the
    // number.
     // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_period), IllegalParameterException);

    // Tests whether the function handler correctly checks for parameters not
    // having a valid number or no number at all
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_number), IllegalParameterException);

    // Tests whether the funciotn correctly checks for numbers that
    // are too precise to convert into float values.
    // NOTE: Not checked by toInt method, it just cuts it off"
    //QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_too_long_for_float), IllegalParameterException);

    // Tests whether the fucntion handler correctly throws an exception when
    // random parameter is passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_random_parameter), IllegalParameterException);

    // Tests whether the function handler correctly throws an exception when
    // multiple of the same parameter are present within the GCode line.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_multiple_of_the_same_parameter), IllegalParameterException);

    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_flow_rate_only), IllegalParameterException);

    /*
    const QPair<char, float> f_val10 = qMakePair('F', 100.0);
    try { test->parseCommand(GcodeString_flow_rate_only, 10); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 2);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(f_val10.first) == f_val10.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 10);

    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G2"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G2"));
    */
}

void TestCommonParser::testG3Handler()
{

    const QString GcodeString = "G3 X2.0 Y1.0 I40.0 J77.0 F10.0"; // Normal extraction test
    const QString GcodeString_no_F = "G3 X2.0 Y1.0 I40.0 J77.0"; // Normal extraction test w/o F
    const QString GcodeString_scrambled_parameters = "G3 Y5.0 J71.0 X5.0 F5.0 I100.0"; // Normal extraction, scrambled parameters
    const QString GcodeString_no_params = "G3"; // No parameteres passed with GCode
    const QString GcodeString_missing_X_param = "G3 Y1.0 I1.0 J1.0"; // Missing X parameter
    const QString GcodeString_missing_Y_param = "G3 X1.0 I1.0 J1.0"; // Missing Y parameter
    const QString GcodeString_missing_I_param = "G3 X1.0 Y1.0 J1.0"; // Missing I parameter
    const QString GcodeString_missing_J_param = "G3 X1.0 Y1.0 I1.0"; // Missing J parameter
    // const QString GcodeString_no_period = "G3 X0.3 Y-0323278 I2.0 J2.0"; // Invalid parameter test
    const QString GcodeString_no_number = "G3 X1.1 Y I1.0 J1.0"; // No parameter test
    // const QString GcodeString_too_long_for_float = "G3 X0.1 Y-0.100000322222222111111132 I1.0 J1.0"; // Y should have too high of a precision for a float to hold
    const QString GcodeString_random_parameter = "G3 X1.3 Y7.4 Z6.0 I2.0 J2.0"; // Z is not used in the G2 command
    const QString GcodeString_multiple_of_the_same_parameter = "G3 X1.3 X2.0 Y5.0 I5.0 J5.0"; // X is used twice.
    const QString GcodeString_flow_rate_only = "G3 F100.0"; // Only flow rate is passed
    const QString GcodeString_RESET = "G0 X0.0 Y0.0 Z0.0";

    // Resetting the internal Gcode command.
    try { test->parseCommand(GcodeString_RESET, 1); }
    catch(...) { QVERIFY(false); }

    try { test->parseCommand(GcodeString, 1); }
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 2.0);
    const QPair<char, float> y_val = qMakePair('Y', 1.0);
    const QPair<char, float> j_val = qMakePair('J', 77.0);
    const QPair<char, float> i_val = qMakePair('I', 40.0);
    const QPair<char, float> f_val = qMakePair('F', 10.0);


    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 3);
    QCOMPARE(cmd.getParameters().size(), 5);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QVERIFY(*cmd.getParameters().find(y_val.first) == y_val.second);
    QVERIFY(*cmd.getParameters().find(j_val.first) == j_val.second);
    QVERIFY(*cmd.getParameters().find(i_val.first) == i_val.second);
    QVERIFY(*cmd.getParameters().find(f_val.first) == f_val.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 2.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getArcXPos(), 40.0);
    QCOMPARE(test->getArcYPos(), 77.0);
    QCOMPARE(test->getSpeed(), 10.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G3"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G3"));

    try { test->parseCommand(GcodeString_no_F, 2); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();

    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 3);
    QCOMPARE(cmd.getParameters().size(), 4);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QVERIFY(*cmd.getParameters().find(y_val.first) == y_val.second);
    QVERIFY(*cmd.getParameters().find(j_val.first) == j_val.second);
    QVERIFY(*cmd.getParameters().find(i_val.first) == i_val.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 2);

    QCOMPARE(test->getXPos(), 2.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getArcXPos(), 42.0);
    QCOMPARE(test->getArcYPos(), 78.0);
    QCOMPARE(test->getSpeed(), 10.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G3"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G3"));

    try { test->parseCommand(GcodeString_scrambled_parameters, 3); }
    catch(...) { QVERIFY(false); }

    const QPair<char, float> x_val3 = qMakePair('X', 5.0);
    const QPair<char, float> y_val3 = qMakePair('Y', 5.0);
    const QPair<char, float> j_val3 = qMakePair('J', 71.0);
    const QPair<char, float> i_val3 = qMakePair('I', 100.0);
    const QPair<char, float> f_val3 = qMakePair('F', 5.0);
    cmd = test->getCurrentCommand();

    // Tests whether the function correctly parsed the command string.
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 3);
    QCOMPARE(cmd.getParameters().size(), 5);
    QVERIFY(*cmd.getParameters().find(x_val3.first) == x_val3.second);
    QVERIFY(*cmd.getParameters().find(y_val3.first) == y_val3.second);
    QVERIFY(*cmd.getParameters().find(j_val3.first) == j_val3.second);
    QVERIFY(*cmd.getParameters().find(i_val3.first) == i_val3.second);
    QVERIFY(*cmd.getParameters().find(f_val3.first) == f_val3.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 3);

    QCOMPARE(test->getXPos(), 5.0);
    QCOMPARE(test->getYPos(), 5.0);
    QCOMPARE(test->getArcXPos(), 102.0);
    QCOMPARE(test->getArcYPos(), 72.0);
    QCOMPARE(test->getSpeed(), 5.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G3"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G3"));

    // Tests whether the function handler correctly throws an exception if there are no parameters
    // passed with the GCode command, as one is required with this command.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_params), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the X parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_X_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the Y parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_Y_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the J parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_J_param), IllegalParameterException);

    // Test whether the funciton handler correctly throws an exception when the I parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_missing_I_param), IllegalParameterException);

    // Tests whether the function handler correctly checks for the numerical part of the
    // parameter is able to be correctly thrown when the float fucntion cannot parse the
    // number.
    // NOTE: toFloat method ignores this
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_period), IllegalParameterException);

    // Tests whether the function handler correctly checks for parameters not
    // having a valid number or no number at all
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_no_number), IllegalParameterException);

    // Tests whether the funciotn correctly checks for numbers that
    // are too precise to convert into float values.
    // NOTE: toFloat method ignores this and just loses the precision
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_too_long_for_float), IllegalParameterException);

    // Tests whether the fucntion handler correctly throws an exception when
    // random parameter is passed to it.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_random_parameter), IllegalParameterException);

    // Tests whether the funciotn handler correctly throws an exception when
    // multiple of the same parameter are present within the GCode line.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_multiple_of_the_same_parameter), IllegalParameterException);

    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_flow_rate_only), IllegalParameterException);
    /*
    const QPair<char, float> f_val10 = qMakePair('F', 100.0);
    try { test->parseCommand(GcodeString_flow_rate_only, 10); } // Should not throw
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 3);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(f_val10.first) == f_val10.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 10);

    QCOMPARE(test->getSpeed(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G3"));
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G3"));
    */
}

void TestCommonParser::testG4Handler()
{
    const QString GCodeString = "G4 P100"; // Normal extraction
    const QString GCodeString_no_param = "G4"; // No parameters passed with GCode
    // const QString GCodeString_uses_float_not_int = "G4 P200.0"; // Invalid parameter, uses int not float
    const QString GCodeString_no_number = "G4 P"; // No number with P
    // const QString GCodeString_number_too_large_for_int = "G4 P10000000000000000000000000000000000000"; // Number to large for integer to hold
    const QString GCodeString_random_command = "G4 P100 A121"; // Random parameter passsed to function
    const QString GcodeString_multiple_of_same_parameter = "G4 P100 P200"; // P is passed twice
    const QString GcodeString_RESET = "G0 X0.0 Y0.0 Z0.0";

    // Resetting the internal Gcode command.
    try { test->parseCommand(GcodeString_RESET, 1); }
    catch(...) { QVERIFY(false); }

    try { test->parseCommand(GCodeString, 1); }
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> p_val = qMakePair('P', 100);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 4);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(p_val.first) == p_val.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getSleepTime(), 100.0);
    QCOMPARE(test->getCurrentCommandString(), QString("G4"));
    // Is G0 due to our reset command
    QCOMPARE(test->getPreviousMovementCommandString(), QString("G0"));

    // Tests whether the function handler correctly throws an exception if there are no parameters
    // passed with the GCode command, as one is required with this command.
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GCodeString_no_param), IllegalParameterException);

    // Tests whether the function handler correctly handles dealing with floats passed to it not ints
    // TODO: Check if parsing a flaot and losing precision matters
    //QVERIFY_EXCEPTION_THROWN(test->parseCommand(GCodeString_uses_float_not_int), IllegalParameterException);

    // Tests whether the function handler correctly throws an exception if the passed GCode command has no
    // number corresponding to the parameter is missing
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GCodeString_no_number), IllegalParameterException);

    // Tests whether the funciotn handler correctly throws an exception if the number with the parameter is too large
    // for an integer to hold.
    // TODO: Need to check if this is needed to be actually error checked.
    // QVERIFY_EXCEPTION_THROWN(test->parseCommand(GCodeString_number_too_large_for_int), IllegalParameterException);

    // Tests whether the fucntion handler properly throws an exception when a random parameter is passed to the command
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GCodeString_random_command), IllegalParameterException);

    // Tests whether the function handler properly throws an exception when multiple of the same acceptable parameters are passed
    QVERIFY_EXCEPTION_THROWN(test->parseCommand(GcodeString_multiple_of_same_parameter), IllegalParameterException);

}

void TestCommonParser::testM3Handler()
{
    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM5Handler()
{
    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for an extra command. (Won't give an error in the handler itself)
    // TODO: Check for trying to give it a parameter. (Won't throw an error in the handler itself)
}

void TestCommonParser::testM10Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for an extra command. (Won't give an error in the handler itself)
    // TODO: Check for trying to give it a parameter. (Won't throw an error in the handler itself)
}

void TestCommonParser::testM11Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for an extra command. (Won't give an error in the handler itself)
    // TODO: Check for trying to give it a parameter. (Won't throw an error in the handler itself)
}

void TestCommonParser::testM13Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM14Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM15Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM16Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check for an extra command. (Won't give an error in the handler itself)
    // TODO: Check for trying to give it a parameter. (Won't throw an error in the handler itself)
}

void TestCommonParser::testM64Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM65Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check that the command has been extracted correctly.
    // TODO: Check for an extra command. (Won't give an error in the handler itself)
    // TODO: Check for trying to give it a parameter. (Won't throw an error in the handler itself)
}

void TestCommonParser::testM66Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testM68Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}
void TestCommonParser::testM69Handler()
{

    // TODO: Check for regular parsing.
    // TODO: Check that the parameters have been extracted correctly.
    // TODO: Check for random command passed.
    // TODO: Check for extra command passed.
    // TODO: Check for No parameter passed.
    // TODO: Check for a parameter passed with no number.
    // TODO: Check for an unparseable number.
    // TODO: Check for a floatting number overflow.
}

void TestCommonParser::testLineComment()
{
    const QString GcodeString = "G0 X1.0 ;Hello";

    try
    {
        test->parseCommand(GcodeString, 1);
    }
    catch(...)
    {
        QVERIFY(false);
    }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 1.0);

    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QCOMPARE(cmd.getParameters().size(), 1);
    QCOMPARE(cmd.getComment(),  QString("Hello"));
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(QString("G0"), test->getCurrentCommandString());
    QCOMPARE(QString("G0"), test->getPreviousMovementCommandString());
}

// TODO: Need to check for multiple block comments
/*
void TestCommonParser::testBlockComment()
{
    const QString GcodeString = "G0 X0.0 Y0.0 Z0.0 ( Move to the starting position )";

    try { test->parseCommand(GcodeString); }
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val = qMakePair('X', 0.0);
    const QPair<char, float> y_val = qMakePair('Y', 0.0);
    const QPair<char, float> z_val = qMakePair('Z', 0.0);


    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 0);
    QVERIFY(*cmd.getParameters().find(x_val.first) == x_val.second);
    QVERIFY(*cmd.getParameters().find(y_val.first) == y_val.second);
    QVERIFY(*cmd.getParameters().find(z_val.first) == z_val.second);
    QCOMPARE(cmd.getComment(), QString("Move to the starting position"));

    QCOMPARE(test->getXPos(), 0.0);
    QCOMPARE(test->getYPos(), 0.0);
    QCOMPARE(test->getZPos(), 0.0);
}
*/
void TestCommonParser::testInvalidCommand()
{
    // TODO: Need to update these
    const QString GcodeString = "A1 X0.0 Y0.0 Z0.0";

    try
    {
        test->parseCommand(GcodeString);
    }
    catch(IllegalArgumentException& e)
    {
        QVERIFY(true);
    }
    catch(IllegalParameterException& e)
    {
        QVERIFY(true);
    }
    catch(...)
    {
        QVERIFY(false);
    }
}

void TestCommonParser::testInvalidParameter_InvalidFloatConversion()
{
    // TODO: Need to update these
    const QString GcodeString = "G0 X0.1 Y-0.1 Z032";
    const QString GcodeString2 = "G0 X0.1 Y-0.1 Z";
    const QString GcodeString3 = "G0 X0.1 Y-0.1 Z0000.4412321321321312312312321312";

    try
    {
        // Should Throw
        test->parseCommand(GcodeString);
    }
    catch(IllegalParameterException& e)
    {
        QVERIFY(true);
    }
    catch(...)
    {
        QVERIFY(false);
        return;
    }

    try
    {
        // Should Throw
        test->parseCommand(GcodeString2);
    }
    catch(IllegalParameterException& e)
    {
        QVERIFY(true);
    }
    catch(...)
    {
        QVERIFY(false);
        return;
    }

    try
    {
        // Should Throw
        test->parseCommand(GcodeString3);
    }
    catch(IllegalParameterException e)
    {
        QVERIFY(true);
    }
    catch(...)
    {
        QVERIFY(false);
        return;
    }
}

void TestCommonParser::testInvalidParameter_InvalidParameterPassed()
{

    const QString GcodeString = "G0 X0.1 Y-0.1 Z032";

    try
    {
        test->parseCommand(GcodeString);
    }
    catch(IllegalParameterException e)
    {
        QVERIFY(true);
    }
    catch(...)
    {
        QVERIFY(false);
    }
}

// Note: Don't think this is actually allowed but going to have it just incase.
void TestCommonParser::testChainnedCommands()
{

}


void TestCommonParser::testPreviousCommands()
{
    const QString Command = "G1";
    const char CommandChar = 'G';
    const int CommandID = 1;
    const QString GcodeString1 = "G1 X0.0 Y0.0 Z1.0 F200";
    const QString GcodeString2 = "X1.0";
    const QString GcodeString3 = "Y1.0";
    const QString GcodeString4 = "Z1.0";
    const QString GcodeString5 = "X0.0";
    const QString GcodeString6 = "X1.0";
    const QString GcodeString7 = "Y2.0";

    try { test->parseCommand(GcodeString1, 1); }
    catch(...) { QVERIFY(false); }

    GcodeCommand cmd = test->getCurrentCommand();
    const QPair<char, float> x_val1 = qMakePair('X', 0.0);
    const QPair<char, float> y_val1 = qMakePair('Y', 0.0);
    const QPair<char, float> z_val1 = qMakePair('Z', 1.0);
    const QPair<char, float> f_val1 = qMakePair('F', 200.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 4);
    QVERIFY(*cmd.getParameters().find(x_val1.first) == x_val1.second);
    QVERIFY(*cmd.getParameters().find(y_val1.first) == y_val1.second);
    QVERIFY(*cmd.getParameters().find(z_val1.first) == z_val1.second);
    QVERIFY(*cmd.getParameters().find(f_val1.first) == f_val1.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 1);

    QCOMPARE(test->getXPos(), 0.0);
    QCOMPARE(test->getYPos(), 0.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(Command, test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());

    try { test->parseCommand(GcodeString2, 2); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val2 = qMakePair('X', 1.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(x_val2.first) == x_val2.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 2);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 0.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());


    try { test->parseCommand(GcodeString3, 3); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val3 = qMakePair('Y', 1.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(y_val3.first) == y_val3.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 3);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());


    try { test->parseCommand(GcodeString4, 4); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> z_val4 = qMakePair('Z', 1.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(z_val4.first) == z_val4.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 4);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());


    try { test->parseCommand(GcodeString5, 5); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val5 = qMakePair('X', 0.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(x_val5.first) == x_val5.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 5);

    QCOMPARE(test->getXPos(), 0.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());

    try { test->parseCommand(GcodeString6, 6); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> x_val6 = qMakePair('X', 1.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(x_val6.first) == x_val6.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 6);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 1.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());

    try { test->parseCommand(GcodeString7, 7); }
    catch(...) { QVERIFY(false); }

    cmd = test->getCurrentCommand();
    const QPair<char, float> y_val7 = qMakePair('Y', 2.0);

    QCOMPARE(cmd.getCommand(), CommandChar);
    QCOMPARE(cmd.getCommandID(), CommandID);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*cmd.getParameters().find(y_val7.first) == y_val7.second);
    QVERIFY(cmd.getComment().isEmpty());
    QCOMPARE(cmd.getLineNumber(), 7);

    QCOMPARE(test->getXPos(), 1.0);
    QCOMPARE(test->getYPos(), 2.0);
    QCOMPARE(test->getZPos(), 1.0);
    QCOMPARE(test->getSpeed(), 200.0);
    QCOMPARE(QString(""), test->getCurrentCommandString());
    QCOMPARE(Command, test->getPreviousMovementCommandString());
}

void TestCommonParser::testMultipleCommands()
{
    // TODO: Test one of each command first.
    // TODO: Test different commands as well as the same in a row
    // TODO: Test ommitted command so it reverts back to the previous one
    // TODO: Do the same as above but with a control command inbetween each command.
    // TODO: Test with no command have being sent (The first command essentially)
    const QString GcodeString1 = "; Start";
    const QString GcodeString2 = "G0 X1.0 Y1.0 Z8.0 ; Move to starting position to start extrusion";
    const QString GcodeString3 = "G0 Z1.0";
    const QString GcodeString4 = "G1 X.5 F200";
    const QString GcodeString5 = "G1 Y.5";
    const QString GcodeString6 = "X0.0";
    const QString GcodeString7 = "Y0.0";
    const QString GcodeString8 = "G3 X0.99 Y0.99 I0.5 J0.5";
    const QString GcodeString9 = "G0 Z8.0";
    const QString GcodeString10 = "; Test";
    const QString GcodeString11 = "X0.0 Y0.05";
    const QString GcodeString12 = "G2 X0.99 Y0.99 I0.5 J0.5";
    const QString GcodeString13 = "; Finished";
}

void TestCommonParser::testDifferentUnits()
{
    // TODO: Test with the defualt units.
    // TODO: Test with any combination of basic units given.
    // TODO: Test with incorrect units, (see what happens?)
    // TODO: Test it with incorrect dimensions passed
    // Distance: micron, m, cm, in, ft
    // Angle rad, deg, rev
    // Mass kg, g, mg
    // Time s, ms, min, hour
}

void TestCommonParser::testDifferntBlockCommentDelimiters()
{
    // TODO: Test with the default block comments.
    // TODO: Test with weird block comments.
    // TODO: Test with a non ended block comment.
    // TODO: Test that the comment is actually extracted.
    // TODO: Test with a block comment that is a actually a parameter (Allow this????)

}

void TestCommonParser::testDifferntLineCommentDelimiters()
{
    // TODO: Test with the default block comments.
    // TODO: Test with weird block comments.
    // TODO: Test with a non ended block comment.
    // TODO: Test that the comment is actually extracted.
    // TODO: Test with a line comment that is a actually a parameter (Allow this????)
}

#endif
