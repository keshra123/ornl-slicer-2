var class_o_r_n_l_1_1_constants_1_1_nozzle_settings =
[
    [ "Acceleration", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_acceleration.html", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_acceleration" ],
    [ "BeadWidth", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_bead_width.html", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_bead_width" ],
    [ "Layer", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_layer.html", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_layer" ],
    [ "Speed", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_speed.html", "class_o_r_n_l_1_1_constants_1_1_nozzle_settings_1_1_speed" ]
];