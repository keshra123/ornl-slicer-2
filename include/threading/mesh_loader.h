#ifndef MODELLOADER_H
#define MODELLOADER_H
//! \file modelloader.h
#include <QThread>

struct aiMesh;
struct aiScene;

namespace ORNL
{
    class Mesh;
    class MeshVertex;
    class MeshFace;

    /*!
     * \class ModelLoader
     * \brief Thread that loads an STL file
     */
    class MeshLoader : public QThread
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        MeshLoader(QObject* parent, const QString& filename);

    protected:
        //! \brief runs upon starting the thread
        void run() override;

        //! \brief splits meshes into individual solid bodies
        void splitMesh(aiMesh* assimp_mesh, QString name);

    signals:

        //! \brief sends the model to the project manager
        void finishedMesh(QSharedPointer< ORNL::Mesh >);

        //! \brief signal that the file has been loaded
        void finishedLoading();

        //! \brief emits error signal
        void errorLoadingFile(QString filename, QString error);

    private:
        void extract(aiMesh* assimp_mesh,
                     QVector< MeshVertex >& vertices_and_normals,
                     QLinkedList< MeshFace >& faces);

        QVector< MeshVertex > extractVertices(aiMesh* assimp_mesh);

        QVector< uint > extractIndices(aiMesh* assimp_mesh);

        /* Rename "vertices" to coordinates or something */
        QVector< MeshVertex > dereferenceIndices(
            const QVector< MeshVertex >& vertices,
            const QVector< uint >& indices);

        /* Test functions */
        void faceData(QLinkedList< MeshFace >& faces);

        void faceData(QVector< MeshFace >& connected_faces);

        void checkData(QVector< MeshVertex >& mesh_vertices,
                       QVector< MeshFace >& mesh_faces);

        void vertexData(QVector< MeshVertex >& vertices);
        /* End test functions */

        void groupFaces(const QVector< uint >& indices,
                        QLinkedList< MeshFace >& faces);
        void groupFaces(const QVector< uint >& indices,
                        QVector< MeshFace >& faces);

        bool facesConnected(MeshFace& face, MeshFace& other_face);

        QVector< uint > flattenFaces(QVector< MeshFace >& connected_faces);

        int getFaceIndexWithPoints(int index0,
                                   int index1,
                                   int not_face_index,
                                   int not_face_vertex_index,
                                   QVector< MeshVertex >& vertices,
                                   QVector< MeshFace >& faces) const;

        void remap(QVector< MeshVertex >& assimp_vertices_and_normals,
                   QVector< MeshVertex >& vertices_and_normals,
                   QLinkedList< MeshFace >& faces);

        void remap(QVector< MeshVertex >& assimp_vertices_and_normals,
                   QVector< MeshVertex >& vertices_and_normals,
                   QVector< MeshFace >& faces);

        QString m_filename;

        float m_zero_threshold;
    };  // class ModelLoader
}  // namespace ORNL
#endif  // MODELLOADER_H
