#include "widgets/gcodetreewidget.h"

#include <QMenu>
#include <QMouseEvent>

#include "managers/project_manager.h"
#include "managers/window_manager.h"

namespace ORNL
{
    GcodeTreeWidget::GcodeTreeWidget(QWidget* parent)
        : QTreeWidget(parent)
        , m_window_manager(WindowManager::getInstance())
        , m_project_manager(ProjectManager::getInstance())
    {
        setColumnCount(1);
        // TODO: figure out why header isn't getting set
        if (QTreeWidgetItem* header = headerItem())
        {
            header->setText(0, "My Text");
        }
        else
        {
            setHeaderLabels(QStringList("My Text"));
        }
        setContextMenuPolicy(Qt::CustomContextMenu);
    }

    // happens on right click
    void GcodeTreeWidget::showContextMenu(const QPoint& point)
    {
        QMenu context_menu(tr("Context menu"), this);

        QTreeWidgetItem* twi = itemAt(point);

        QAction* action;

        context_menu.exec(mapToGlobal(point));
    }

    void GcodeTreeWidget::selected(QTreeWidgetItem* twi)
    {
        if (twi->text(1) == "layer")
        {
        }
        else if (twi->text(1) == "island")
        {}
        else if (twi->text(1) == "region")
        {}
        else if (twi->text(1) == "path")
        {}
        else if (twi->text(1) == "path_segment")
        {}
    }

    void GcodeTreeWidget::deselected()
    {
        m_project_manager->deselectGcode();
        clearSelection();
    }

    void GcodeTreeWidget::mousePressEvent(QMouseEvent* event)
    {
        // Right clicks bring up the context menu, but doesn't affect selection
        if (event->button() == Qt::RightButton)
        {
            showContextMenu(event->pos());
            return;
        }

        // If the expand/collapse arrow is clicked let the QTreeWidget handle it
        // No selection changes
        QModelIndex clicked_index = indexAt(event->pos());
        if (clicked_index.isValid())
        {
            QRect vrect          = visualRect(clicked_index);
            int item_indentation = vrect.x() - visualRect(rootIndex()).x();
            if (event->pos().x() < item_indentation)
            {
                QTreeWidget::mousePressEvent(event);
                return;
            }
        }

        QTreeWidgetItem* item = itemAt(event->pos());
        // If a new item is selected then mark it as selected
        if (item)
        {
            if (!item->isSelected())
            {
                selected(item);
            }
        }
        // If anywhere that isn't an item is selected clear the selection
        else
        {
            clearSelection();
            m_project_manager->deselectGcode();
        }
    }
}  // namespace ORNL
