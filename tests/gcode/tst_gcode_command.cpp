#include <QMap>
#include <QString>

#include "gcode/tst_gcode_command.h"
#include "gcode/gcode_command.h"

using namespace ORNL;

#if 0 // TODO fix tests

void TestGcodeCommand::testGettersAndSetters()
{
    QString Comment = "Hello!";
    int line_num = 556;
    char command = 'K';
    int command_id = 32;
    const QPair<char, float> param('K', 440.0);
    GcodeCommand a;


    QMap<char, float> test_map;
    test_map.insert(param.first, param.second);
    a.setCommand(command);
    a.setCommandID(command_id);
    a.setLineNumber(line_num);
    a.setComment(Comment);
    a.addParameter(param.first, param.second);

    QVERIFY2(command == a.getCommand(), "Command setter/getter failed on comparison");
    QVERIFY2(command_id == a.getCommandID(), "Command ID setter/getter failed on comparision");
    QVERIFY2(line_num == a.getLineNumber(), "Line Number setter/getter failed on comparision");
    QVERIFY2(Comment == a.getComment(), "Comment setter/getter failed on comparision");
    QVERIFY2(test_map == a.getParameters(), "Parameters setter/getterfailed on comparision");
}


void TestGcodeCommand::testEqualsOperator()
{
    GcodeCommand a, b;
    QString Comment = "Hello!";
    int line_num = 556;
    char command = 'K';
    int command_id = 32;
    const QPair<char, float> param('K', 440.0)
                            ,param2('L', 320)
                            ,param3('R', 32132);

    a.setCommand(command);       b.setCommand(command);
    a.setCommandID(command_id);  b.setCommandID(command_id);
    a.setLineNumber(line_num);   b.setLineNumber(line_num);
    a.setComment(Comment);       b.setComment(Comment);
    a.addParameter(param.first, param.second);
    a.addParameter(param2.first, param2.second);
    a.addParameter(param3.first, param3.second);

    b.addParameter(param.first, param.second);
    b.addParameter(param2.first, param2.second);
    b.addParameter(param3.first, param3.second);

    QVERIFY2(a == b, "GCode Equals Operator incorrectly false");
}

void TestGcodeCommand::testNotEqualsOperator()
{
    GcodeCommand a, b;
    QString Comment = "Hello!";
    int line_num = 556;
    char command = 'K';
    int command_id = 32;
    const QPair<char, float> param('K', 440.0)
                            ,param2('L', 320)
                            ,param3('R', 32132);

    a.setCommand(command);       b.setCommand(command);
    a.setCommandID(command_id);  b.setCommandID(command_id);
    a.setLineNumber(line_num);   b.setLineNumber(line_num);
    a.setComment(Comment);       b.setComment(Comment);
    a.addParameter(param.first, param.second);
    a.addParameter(param2.first, param2.second);

    b.addParameter(param.first, param.second);
    b.addParameter(param3.first, param3.second);

    QVERIFY2(a != b, "GCode Not Equals Operator fails on differing parameters");

    a.clearParameters(); b.clearParameters();
    b.setLineNumber(20);

    QVERIFY2(a != b, "GCode Not Equals Operator fails on differing line numbers");

    b.setLineNumber(line_num);
    b.setCommand('E');

    QVERIFY2(a != b, "GCode Not Equals Operator fails on differing Command characters");

    b.setCommand(command);
    b.setCommandID(23121);

    QVERIFY2(a != b, "GCode Not Equals Operator fails on differing CommandIDs");

    b.setCommandID(command_id);
    b.setComment("Bye!");

    QVERIFY2(a != b, "GCode Not Equals Operator fails on differing Comments");
}

#endif
