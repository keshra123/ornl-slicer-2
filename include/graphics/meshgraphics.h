#ifndef MESHGRAPHICS_H
#define MESHGRAPHICS_H

#include <QSharedPointer>
#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DRender/QPickEvent>
#include <Qt3DRender/Qt3DRender>

#include "managers/project_manager.h"

namespace Qt3DCore
{
    class QEntity;
    class QTransform;
}  // namespace Qt3DCore

namespace Qt3DRender
{
    class QMaterial;
    class QGeometryRenderer;
    class QGeometry;
    class QBuffer;
}  // namespace Qt3DRender

namespace ORNL
{
    class Mesh;

    class MeshGraphics : public QObject
    {
        Q_OBJECT

    public:
        MeshGraphics(Mesh* mesh);

        void setParentEntity(Qt3DCore::QEntity* parent);

    public slots:

        void pickMesh(Qt3DRender::QPickEvent* pickEvent);

    private:
        QByteArray* createVertexByteArray();

        QByteArray* createIndexByteArray();

        Qt3DCore::QEntity* createQEntity();

        Qt3DRender::QBuffer* createQBuffer(
            QByteArray* data,
            Qt3DRender::QBuffer::BufferType buffer_type,
            Qt3DRender::QGeometry* parent_geometry);

        Qt3DRender::QAttribute* createQAttribute(
            Qt3DRender::QAttribute::AttributeType attribute_type,
            Qt3DRender::QBuffer* buffer,
            uint byte_offset,
            uint byte_stride,
            uint count,
            Qt3DRender::QAttribute::VertexBaseType vertex_base_type,
            uint vertex_size,
            Qt3DRender::QGeometry* parent_geometry);

        Qt3DRender::QGeometryRenderer* createQGeometryRenderer(
            int first_instance,
            int first_vertex,
            Qt3DRender::QGeometry* geometry,
            int index_offset,
            int instance_count,
            bool primitive_restart_enabled,
            Qt3DRender::QGeometryRenderer::PrimitiveType primitive_type,
            int vertex_count,
            QString objectName,
            Qt3DCore::QEntity* parent_entity);

        Qt3DRender::QObjectPicker* createObjectPicker();

        Mesh* m_mesh;

        Qt3DCore::QEntity* m_entity;
        Qt3DCore::QTransform* m_transform;
        Qt3DExtras::QPhongMaterial* m_material;
        QSharedPointer< ProjectManager > m_project_manager;

        friend class Mesh;
    };
}  // namespace ORNL
#endif  // MESHGRAPHICS_H
