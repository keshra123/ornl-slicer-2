#include "tst_sanity_example.h"
#include "exceptions/exceptions.h"
#include "units/unit.h"

using namespace ORNL;

/*
 * Because 2+2 = Fish, right?
 */
void TestSanityExample::testFish()
{
    QString two("2");
    QString too("too");

    QEXPECT_FAIL("", "Two plus too should not equal fish.", Continue);
    QCOMPARE(QString("fish"), two + too);

    QCOMPARE(QString("too2"), too + two);

}

/*
 * Throws a Seal back in time. Because we can totally do that...
 *
 * Expect the function to throw us a seal.
 */
NEW_EX(Seal)
void throwSealsBackInTime()
{
    throw Seal("back in time.");
}
void TestSanityExample::testSealError()
{
    QVERIFY_EXCEPTION_THROWN(throwSealsBackInTime(), Seal);
}
