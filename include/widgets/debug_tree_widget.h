#ifndef DEBUGTREEWIDGET_H
#define DEBUGTREEWIDGET_H

#include <QTreeWidget>

#include "managers/project_manager.h"

namespace ORNL
{
    /*!
     * \class DebugTreeWidget
     *
     * \brief Tree Widgets that displays the options for the debug visual
     */
    class DebugTreeWidget : public QTreeWidget
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit DebugTreeWidget(QWidget* parent = 0);

#ifdef DEBUG

    public slots:
        void handleSliceUpdate();

        //! \brief Display the context menu for the heirarchy tree item that is
        //! clicked
        void showContextMenu(const QPoint& point);

        //! \brief
        void selected(QTreeWidgetItem* twi);

        //! \brief deselect any selected items
        void deselected();

    signals:
        //! \brief signal that there is a new selection
        //! Main Window connects this to the debug view widget
        void newSelection();

    private:
        //! \brief Handle left click selection and deselection and right click
        void mousePressEvent(QMouseEvent* event) override;

        //! \brief Handle selection through up and down arrow keys
        void keyPressEvent(QKeyEvent* event) override;

        QSharedPointer< ProjectManager > m_project_manager;
#endif  // NDEBUG
    };
}  // namespace ORNL
#endif  // DEBUGTREEWIDGET_H
