#ifndef INFILL_H
#define INFILL_H
//! \file infill.h

#include <QVector>

#include "geometry/point.h"
#include "region.h"

namespace ORNL
{
    class Island;

    /*!
     * \class Infill
     * \brief All the information needed for creating the gcode for the infill
     * of this particular island
     */
    class Infill : public Region
    {
    public:
        //! \brief Constructor
        Infill(Island* parent);

        ~Infill() = default;

        //! \brief Computes the infill path based on settings
        void computeInternal() override;

        //! \brief order the paths. previous location gets updated to the last
        //! point
        void orderInternal(Point& previous_location) override;

#ifdef DEBUG
        //! \brief Returns a list of debug options
        QStringList debugOptionsInternal() override;

        //! \brief Paints the selected debug option to the screen
        void paintDebugInternal(QString option,
                                QPainter* painter,
                                Distance& ratio,
                                int vertical_offset) override;
#endif
    private:
        //! \brief Fill the infill region with concentric rings
        void computeConcentric(PolygonList first_ring, Distance offset);

        //! \brief Fill the infill region with rastering lines
        void computeLine(Distance line_spacing, Angle rotation);

        //! \brief Fill the infill region with rastering lines in 2 directions
        void computeGrid();

        //! \brief Fill the infill region with a triangular grid pattern
        void computeTriangles();

        //! \brief Fill the infill region with honeycomb pattern
        void computeHoneycomb();

        //! \brief Fill the infill region with a shifting triangular grid, which
        //! combines with consecutive layers into a cubic pattern
        void computeCubic();

        //! \brief Fill the infill region with a double shifting square grid,
        //! which combines with consecutive layers into a tetrahedral pattern
        void computeTetrahedral();

        //! \brief Fill the infill reiong with
        void computeConcentric3D();

        //! \brief Fill the infill region based on the circle pack results
        void computeCirclePack();

#ifdef DEBUG
        QVector< PolygonList > m_debug_rings;
        QVector< Polyline > m_debug_lines;
#endif
    };  // class Infill
}  // namespace ORNL
#endif  // INFILL_H
