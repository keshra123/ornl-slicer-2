var class_settings_base =
[
    [ "SettingsBase", "class_settings_base.html#aea40111731ffbdf586eeafaf3ef748fc", null ],
    [ "SettingsBase", "class_settings_base.html#a6003b571c7c19bc3a33613c467ebc145", null ],
    [ "~SettingsBase", "class_settings_base.html#a8a2994e35eb536a5978f3a8097099bd4", null ],
    [ "getParent", "class_settings_base.html#a77dddc261abeb7b6a0444d9ab35e72ef", null ],
    [ "getSetting", "class_settings_base.html#a72e5758a70689ae7587486fae873f3fb", null ],
    [ "setParent", "class_settings_base.html#a5e314f9efe15eacf3ae70f8b071f7975", null ],
    [ "setSetting", "class_settings_base.html#af588ab98efb91c4b203404cb1b0ce2af", null ],
    [ "setSettingInheritBase", "class_settings_base.html#ae8fdcf1dcff2df17b0c60dfc360f9ce1", null ],
    [ "mParent", "class_settings_base.html#ad220485e7044a9f6f2b1b615e502108d", null ],
    [ "mSettingsInheritBase", "class_settings_base.html#af82ed60ff8a39090dfb5b9186c124232", null ],
    [ "mValues", "class_settings_base.html#a33241e20bdaab9c4a2f99ccbf8336467", null ]
];