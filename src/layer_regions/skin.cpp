#include "layer_regions/skin.h"
#ifdef QT_DEBUG
#    include <QPainter>
#endif
#include "geometry/mesh.h"
#include "geometry/path.h"
#include "layer_regions/island.h"
#include "layer_regions/layer.h"
namespace ORNL
{
    Skin::Skin(Island* island)
        : Region(island)
    {
#ifdef DEBUG
        m_polygon_pen.setColor(Constants::Colors::kSkin);
#endif
    }


    void Skin::computeInternal()
    {
        if (setting< bool >(Constants::PrintSettings::Skin::kUse))
        {
// Top skins and Bottom skins can be calculated in parrallel
#pragma omp parallel sections
            {
#pragma omp section
                {
                    computeTopSkin();
                }

#pragma omp section
                {
                    computeBottomSkin();
                }
            }
        }

        m_outline = m_top_outline + m_bottom_outline;

        // TODO: Skeletons and Variable Extrusion

        computeConcentric();
    }

    void Skin::computeTopSkin()
    {
        int below_layers =
            setting< int >(Constants::PrintSettings::Skin::kLayersBelow);


        Layer* layer = m_parent->parent();
        Mesh* mesh   = layer->parent();

        int current_layer = static_cast< int >(layer->layerNumber());
        int start_layer =
            current_layer - below_layers < 0 ? 0 : current_layer - below_layers;

        for (int layer_nr = start_layer; layer_nr < current_layer; layer_nr++)
        {
            for (QSharedPointer< Island > island : (*(*mesh)[layer_nr]))
            {
                PolygonList difference = m_outline - island->outlines();

                if (difference.size())
                {
                    m_top_outline += difference;
                }
            }
        }
    }

    void Skin::computeBottomSkin()
    {
        int above_layers =
            setting< int >(Constants::PrintSettings::Skin::kLayersAbove);

        Layer* layer = m_parent->parent();
        Mesh* mesh   = layer->parent();


        int current_layer = static_cast< int >(layer->layerNumber());
        int end_layer =
            current_layer + above_layers > static_cast< int >(mesh->size())
            ? static_cast< int >(mesh->size())
            : current_layer + above_layers + 1;

        for (int layer_nr = current_layer + 1; layer_nr < end_layer; layer_nr++)
        {
            for (QSharedPointer< Island > island : (*(*mesh)[layer_nr]))
            {
                PolygonList difference = m_outline - island->outlines();

                if (difference.size())
                {
                    m_bottom_outline += difference;
                }
            }
        }
    }

    void Skin::computeConcentric()
    {
        Distance bead_width =
            setting< Distance >(Constants::NozzleSettings::BeadWidth::kSkin);

        int ring_nr = 0;

        while (true)
        {
            PolygonList path_line =
                m_outline.offset(-(bead_width * ring_nr + bead_width / 2.0));

            if (!path_line.size())
            {
                break;
            }

            m_paths += Path::createPaths(this, RegionType::kSkin, path_line);

            ring_nr++;
        }
    }

    void Skin::orderInternal(Point& previous_location)
    {
        // TODO
    }

#ifdef DEBUG
    QStringList Skin::debugOptionsInternal()
    {
        QStringList rv;
        rv << "Outline";
        switch (
            setting< InfillPatterns >(Constants::PrintSettings::Skin::kPattern))
        {
            case InfillPatterns::kConcentric:
                for (int i = 0; i < m_debug_rings.size(); i++)
                {
                    rv << QString("Ring %1").arg(i + 1);
                }
                break;
            case InfillPatterns::kLines:
                for (int i = 0; i < m_debug_lines.size(); i++)
                {
                    rv << QString("Line %1").arg(i + 1);
                }
                break;
            default:
                Q_ASSERT(false);
        }
        rv << "Inline";
        for (int i = 0; i < m_paths.size(); i++)
        {
            rv << QString("Path %1").arg(i + 1);
        }
        return rv;
    }

    void Skin::paintDebugInternal(QString option,
                                  QPainter* painter,
                                  Distance& ratio,
                                  int vertical_offset)
    {
        if (option == "Outline")
        {
            QPainterPath painter_path;
            for (Polygon polygon : m_outline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            painter->setPen(m_outline_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_centerline_pen);
            painter->drawPath(painter_path);
        }
        else if (option == "Inline")
        {
            QPainterPath painter_path;
            for (Polygon polygon : m_outline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            painter->setPen(m_outline_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_centerline_pen);
            painter->drawPath(painter_path);
        }
        else if (option.contains("Path"))
        {
            int path_nr = option.mid(5).toInt();
            m_paths[path_nr].paintDebug(painter, ratio, vertical_offset);
        }
        else
        {
            switch (setting< InfillPatterns >(
                Constants::PrintSettings::Skin::kPattern))
            {
                case InfillPatterns::kConcentric:
                    if (option.contains("Ring"))
                    {
                        int ring_nr = option.mid(5).toInt() - 1;
                        QPainterPath painter_path;
                        Q_ASSERT(ring_nr < m_debug_rings.size());
                        for (Polygon polygon : m_debug_rings[ring_nr])
                        {
                            if (polygon.size())
                            {
                                Point p  = polygon[0];
                                double x = p.x() * ratio();
                                double y = p.y() * ratio() + vertical_offset;
                                painter_path.moveTo(x, y);
                                for (int i = 1; i <= polygon.size(); i++)
                                {
                                    p = polygon[i % polygon.size()];
                                    x = p.x() * ratio();
                                    y = p.y() * ratio() + vertical_offset;
                                    painter_path.lineTo(x, y);
                                }
                            }
                        }
                        painter->setPen(m_outline_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_polygon_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_centerline_pen);
                        painter->drawPath(painter_path);
                    }
                    else
                    {
                        Q_ASSERT(false);
                    }
                    break;
                case InfillPatterns::kLines:
                    if (option.contains("Line"))
                    {
                        int line_nr = option.mid(5).toInt() - 1;
                        QPainterPath painter_path;
                        Q_ASSERT(line_nr < m_debug_lines.size());
                        Polyline polyline = m_debug_lines[line_nr];

                        if (polyline.size())
                        {
                            Point p  = polyline[0];
                            double x = p.x() * ratio();
                            double y = p.y() * ratio() + vertical_offset;
                            painter_path.moveTo(x, y);
                            for (int i = 1; i < polyline.size(); i++)
                            {
                                p = polyline[i];
                                x = p.x() * ratio();
                                y = p.y() * ratio() + vertical_offset;
                                painter_path.lineTo(x, y);
                            }
                        }

                        painter->setPen(m_outline_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_polygon_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_centerline_pen);
                        painter->drawPath(painter_path);
                    }
                default:
                    Q_ASSERT(false);
            }
        }
    }
#endif
}  // namespace ORNL
