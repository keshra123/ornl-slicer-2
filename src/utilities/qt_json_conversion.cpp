#include "utilities/qt_json_conversion.h"

void to_json(json& j, const QString& s)
{
    j = s.toStdString();
}

void from_json(const json& j, QString& s)
{
    s = j.get< std::string >().c_str();
}


void to_json(json& j, const QQuaternion& q)
{
    QVector4D v = q.toVector4D();
    j           = json{{"w", v.w()}, {"x", v.x()}, {"y", v.y()}, {"z", v.z()}};
}

void from_json(const json& j, QQuaternion& q)
{
    q.setScalar(j["w"]);
    q.setX(j["x"]);
    q.setY(j["y"]);
    q.setZ(j["z"]);
}

void to_json(json& j, const QVector3D& v)
{
    j = json{{"x", v.x()}, {"y", v.y()}, {"z", v.z()}};
}

void from_json(const json& j, QVector3D& v)
{
    v.setX(j["x"]);
    v.setY(j["y"]);
    v.setZ(j["z"]);
}

/*
void to_json(json &j, const QVector &v)
{
    j = json{v.toStdVector()};
}

void from_json(const json &j, QVector &v)
{
    v = j.get<std::vector>();
}
*/
