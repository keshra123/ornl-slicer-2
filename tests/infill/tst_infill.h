#ifndef TST_INFILL_H
#define TST_INFILL_H

#include <QObject>
#include "auto_test.h"

using namespace AutoTest;

/*!
 *  \class InfillTest
 *
 *  \brief A class that tests teh functions of the Infill class
 */

class InfillTest : public QObject
{
    Q_OBJECT

private slots:

    //! \brief Test to see if the grid() function works
    void testComputeGrid();
};

DECLARE_TEST(InfillTest)

#endif // TST_INFILL_H
