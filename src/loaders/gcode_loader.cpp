
#include "loaders/gcode_loader.h"

#include <QTextStream>

#include "exceptions/exceptions.h"
#include "utilities/enums.h"

namespace ORNL
{
    GcodeLoader::GcodeLoader(QString filename, QObject* parent)
        : m_gcode_file(filename)
    {}

    GcodeLoader::~GcodeLoader()
    {}

    void GcodeLoader::run()
    {
        GcodeSyntax syntax = detectParser();
        QStringList gcode;
        try
        {
            gcode = readAll();
        }
        catch (IOException& e)
        {
            emit errorLoadingFile(filename(), e.what());
        }

        if (!gcode.isEmpty())
        {
            emit finishedGcode(gcode, syntax);
        }

        emit finished();
    }

    QString GcodeLoader::filename()
    {
        return m_gcode_file.fileName();
    }

    QStringList GcodeLoader::readAll()
    {
        QStringList temp_ret;
        qint64 temp_pos;
        if (!m_gcode_file.isOpen())
        {
            m_gcode_file.open(QIODevice::ReadOnly);
            if (!m_gcode_file.isOpen())
            {
                emit errorLoadingFile(m_gcode_file.fileName(),
                                      "Unable to open file.");
            }
        }
        if (!m_gcode_file.isReadable())
        {
            emit errorLoadingFile(m_gcode_file.fileName(),
                                  "Unable to read file contents.");
        }

        temp_pos = m_gcode_file.pos();
        m_gcode_file.seek(0);

        temp_ret = QString(m_gcode_file.readAll()).split('\n');
        m_gcode_file.seek(temp_pos);

        return temp_ret;
    }

    QString GcodeLoader::readLine()
    {
        if (!m_gcode_file.isOpen())
        {
            m_gcode_file.open(QIODevice::ReadOnly);
            if (!m_gcode_file.isOpen())
            {
                emit errorLoadingFile(m_gcode_file.fileName(),
                                      "Unable to open file.");
            }
        }
        if (!m_gcode_file.isReadable())
        {
            emit errorLoadingFile(m_gcode_file.fileName(),
                                  "Unable to read file contents.");
        }


        return m_gcode_file.readLine();
    }

    GcodeSyntax GcodeLoader::detectParser()
    {
        const QString GcodeSyntaxString = "GCODE Syntax: ";
        qint64 index;
        qint64 temp_pos = m_gcode_file.pos();
        if (!m_gcode_file.isOpen())
        {
            m_gcode_file.open(QIODevice::ReadOnly);
            if (!m_gcode_file.isOpen())
            {
                emit errorLoadingFile(m_gcode_file.fileName(),
                                      "Unable to open file.");
            }
        }
        if (!m_gcode_file.isReadable())
        {
            emit errorLoadingFile(m_gcode_file.fileName(),
                                  "Unable to read file contents.");
        }

        m_gcode_file.seek(0);

        for (QString temp = m_gcode_file.readLine(); !temp.isNull();
             temp         = m_gcode_file.readLine())
        {
            index = temp.indexOf(GcodeSyntaxString);
            if (index != -1)
            {
                m_gcode_file.seek(temp_pos);
                return selectParserFromString(
                    temp.right(temp.size() - index).trimmed());
            }
        }

        return GcodeSyntax::kNone;
    }


    void GcodeLoader::throwIOException(QString exceptionString)
    {
        throw IOException(exceptionString);
    }

    GcodeSyntax GcodeLoader::selectParserFromString(QString parserID)
    {
        if (parserID.contains(
                Constants::MachineSettings::Syntax::kCincinnati) ||
            parserID.contains(
                Constants::MachineSettings::Syntax::kCincinnatiLegacy))
        {
            return GcodeSyntax::kCincinnati;
        }
        else if (parserID.contains(
                     Constants::MachineSettings::Syntax::kBlueGantry))
        {
            return GcodeSyntax::kBlueGantry;
        }
        else if (parserID.contains(Constants::MachineSettings::Syntax::kWolf))
        {
            return GcodeSyntax::kWolf;
        }
        // TODO: Make Ingersoll parser
        else if (parserID.contains(
                     Constants::MachineSettings::Syntax::kIngersoll))
        {
            return GcodeSyntax::kIngersoll;
        }
        // else if(temp == Constants::MachineSettings::Syntax::kNorthrup)
        else if (false)
        {
            return GcodeSyntax::kNorthrup;
        }
        else if (parserID.contains(Constants::MachineSettings::Syntax::kMach3))
        {
            // selectParser(GcodeSyntax::kMach3);
        }
        else
        {
            throwInvalidParserException();
        }

        return GcodeSyntax::kNone;
    }

    void GcodeLoader::throwInvalidParserException()
    {
        QString exceptionString;
        QTextStream(&exceptionString)
            << "Parser string in gcode does not correlate to any known parser.";
        throw InvalidParserException(exceptionString);
    }


}  // namespace ORNL
