#include "geometry/path_segment.h"
namespace ORNL
{
    PathSegment::PathSegment(Path* parent,
                             Line line,
                             Distance width,
                             Distance height,
                             Velocity speed,
                             Acceleration acceleration,
                             AngularVelocity extruder_speed,
                             PathModifiers modifiers)
        : m_parent(parent)
        , m_line(line)
        , m_is_line(true)
        , m_width(width)
        , m_height(height)
        , m_speed(speed)
        , m_acceleration(acceleration)
        , m_extruder_speed(extruder_speed)
        , m_path_modifiers(modifiers)
    {}

    PathSegment::PathSegment(Path* parent,
                             Arc arc,
                             Distance width,
                             Distance height,
                             Velocity speed,
                             Acceleration acceleration,
                             AngularVelocity extruder_speed,
                             PathModifiers modifiers)
        : m_parent(parent)
        , m_arc(arc)
        , m_is_line(false)
        , m_width(width)
        , m_height(height)
        , m_speed(speed)
        , m_acceleration(acceleration)
        , m_extruder_speed(extruder_speed)
        , m_path_modifiers(modifiers)
    {}

    Distance PathSegment::beadWidth()
    {
        return m_width;
    }

    void PathSegment::beadWidth(Distance width)
    {
        m_width = width;
    }

    Distance PathSegment::beadHeight()
    {
        return m_height;
    }

    void PathSegment::beadHeight(Distance height)
    {
        m_height = height;
    }

    Velocity PathSegment::speed()
    {
        return m_speed;
    }

    void PathSegment::speed(Velocity speed)
    {
        m_speed = speed;
    }

    Acceleration PathSegment::acceleration()
    {
        return m_acceleration;
    }

    void PathSegment::acceleration(Acceleration acceleration)
    {
        m_acceleration = acceleration;
    }

    AngularVelocity PathSegment::extruderSpeed()
    {
        return m_extruder_speed;
    }

    void PathSegment::extruderSpeed(AngularVelocity extruder_speed)
    {
        m_extruder_speed = extruder_speed;
    }

    void PathSegment::modifier(PathModifiers modifier)
    {
        m_path_modifiers = modifier;
    }

    PathModifiers PathSegment::modifier()
    {
        return m_path_modifiers;
    }

    bool PathSegment::isLine()
    {
        return m_is_line;
    }

    Line PathSegment::line()
    {
        if (m_is_line)
        {
            return m_line;
        }
        throw IncorrectPathSegmentType(
            "Line was requested; however, this is an arc");
    }

    bool PathSegment::isArc()
    {
        return !m_is_line;
    }

    Arc PathSegment::arc()
    {
        if (!m_is_line)
        {
            return m_arc;
        }
        throw IncorrectPathSegmentType(
            "Arc was requested; however, this is a line");
    }

    QString PathSegment::gcode(WriterBase* syntax)
    {
        if (m_is_line)
        {
            return syntax->line(m_line,
                                m_speed,
                                m_acceleration,
                                m_parent->type(),
                                m_path_modifiers);
        }
        else
        {
            return syntax->arc(m_arc,
                               m_speed,
                               m_acceleration,
                               m_parent->type(),
                               m_path_modifiers);
        }
    }
}  // namespace ORNL
