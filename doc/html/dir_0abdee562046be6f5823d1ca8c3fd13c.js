var dir_0abdee562046be6f5823d1ca8c3fd13c =
[
    [ "settings_category_widgets", "dir_6b45bd28ae2bd8b98f6488bc2787c46f.html", "dir_6b45bd28ae2bd8b98f6488bc2787c46f" ],
    [ "debugtreewidget.cpp", "debugtreewidget_8cpp.html", null ],
    [ "debugtreewidget.h", "debugtreewidget_8h.html", [
      [ "DebugTreeWidget", "class_o_r_n_l_1_1_debug_tree_widget.html", "class_o_r_n_l_1_1_debug_tree_widget" ]
    ] ],
    [ "debugviewwidget.cpp", "debugviewwidget_8cpp.html", null ],
    [ "debugviewwidget.h", "debugviewwidget_8h.html", null ],
    [ "gcodetreewidget.cpp", "gcodetreewidget_8cpp.html", null ],
    [ "gcodetreewidget.h", "gcodetreewidget_8h.html", [
      [ "GcodeTreeWidget", "class_o_r_n_l_1_1_gcode_tree_widget.html", "class_o_r_n_l_1_1_gcode_tree_widget" ]
    ] ],
    [ "globalconfigwidget.cpp", "globalconfigwidget_8cpp.html", null ],
    [ "globalconfigwidget.h", "globalconfigwidget_8h.html", [
      [ "GlobalConfigWidget", "class_o_r_n_l_1_1_global_config_widget.html", "class_o_r_n_l_1_1_global_config_widget" ]
    ] ],
    [ "localconfigwidget.cpp", "localconfigwidget_8cpp.html", null ],
    [ "localconfigwidget.h", "localconfigwidget_8h.html", [
      [ "LocalConfigWidget", "class_o_r_n_l_1_1_local_config_widget.html", "class_o_r_n_l_1_1_local_config_widget" ]
    ] ],
    [ "meshtransformwidget.cpp", "meshtransformwidget_8cpp.html", null ],
    [ "meshtransformwidget.h", "meshtransformwidget_8h.html", [
      [ "MeshTransformWidget", "class_o_r_n_l_1_1_mesh_transform_widget.html", "class_o_r_n_l_1_1_mesh_transform_widget" ]
    ] ],
    [ "meshviewlistwidget.cpp", "meshviewlistwidget_8cpp.html", null ],
    [ "meshviewlistwidget.h", "meshviewlistwidget_8h.html", [
      [ "MeshViewListWidget", "class_o_r_n_l_1_1_mesh_view_list_widget.html", "class_o_r_n_l_1_1_mesh_view_list_widget" ]
    ] ],
    [ "modelplacementwidget.cpp", "modelplacementwidget_8cpp.html", "modelplacementwidget_8cpp" ],
    [ "modelplacementwidget.h", "modelplacementwidget_8h.html", [
      [ "ModelPlacementWidget", "class_o_r_n_l_1_1_model_placement_widget.html", "class_o_r_n_l_1_1_model_placement_widget" ]
    ] ]
];