# ORNL Slicer 2

## Dependencies

## How to Contribute
All contributions should be made through our [Git Workflow]() and by following our [Coding Standards]().

Once your contributions are ready to be reviewed, please open a pull request to the _dev_ branch. Pull
Requests should have a small number of changes that can be easily reviewed by the experienced members.
Ideally, mark your PR with the associated issues and the target milestone/version.
