#include "gcode/parsers/parser_base.h"

#include <QDebug>
#include <QString>
#include <QStringList>
#include <QStringListIterator>
#include <QTextStream>
#include <iostream>

#include "exceptions/exceptions.h"
namespace ORNL
{
    ParserBase::ParserBase()
    {}


    GcodeCommand ParserBase::parseCommand(QString command_string,
                                          QString delimiter)
    {
        // Clears the current command
        m_current_command_string.clear();
        m_current_gcode_command.clearParameters();
        m_current_gcode_command.clearComment();

        command_string = command_string.trimmed();

        extractComments(command_string);
        command_string = command_string.trimmed();

        // Check to see if the command is just a comment
        if (command_string.isEmpty())
        {
            return m_current_gcode_command;
        }

        QStringList command_string_split =
            command_string.split(delimiter, QString::SkipEmptyParts);

        // TODO: Add a function to remove leading zeros from each command.
        //       I.E. G03 and G3 should be equivalent.


        // Parses and removes and special control commands from the line
        parseControlCommands(command_string_split);

        // It is possible that the line is only control commands, so no need to
        // continue.
        if (command_string_split.isEmpty())
        {
            return m_current_gcode_command;
        }

        QHash< QString, std::function< void(QStringList&) > >::iterator
            temp_iter;
        // First element should always be the command
        if ((temp_iter = m_command_mapping.find(command_string_split[0])) !=
            m_command_mapping.end())
        {
            // If the command is found set it as the new currrent command.
            setPreviousMovementCommand(command_string_split[0]);
            setCurrentCommand(command_string_split[0]);
            command_string_split.removeFirst();

            // If command is found send the rest of the split string to its
            // function handler
            temp_iter.value()(command_string_split);
        }
        // No new command is within the GCode Line,
        // So it is assumed to be the previous command.
        else if (!m_previous_movement_command_string.isEmpty())
        {
            // NOTE: If a command is ever able to be removed then this needs to
            // check
            //       if that command still exists within the hashtable. For now
            //       there it is assumed that the current_command has already
            //       been processed previously and therefore is within the
            //       hashtable.
            m_command_mapping.find(m_previous_movement_command_string)
                .value()(command_string_split);
        }
        else
        {  // Command is not found condition
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Illegal argument " << command_string_split[0]
                << " within GCode file, on line "
                << m_current_gcode_command.getLineNumber() << ".";
            throw IllegalArgumentException(exceptionString);
        }

        return m_current_gcode_command;
    }

    GcodeCommand ParserBase::parseCommand(QString command_string,
                                          int line_number,
                                          QString delimiter)
    {
        setLineNumber(line_number);
        return parseCommand(command_string, delimiter);
    }

    void ParserBase::resetInternalState()
    {
        m_command_mapping.clear();
        m_control_command_mapping.clear();
        m_line_comment.clear();
        m_block_comment_ending_delimiter.clear();
        m_block_comment_starting_delimiter.clear();
        m_current_command_string.clear();
        m_previous_movement_command_string.clear();
    }

    void ParserBase::addCommandMapping(
        QString command_string,
        std::function< void(QStringList&) > function_handle)
    {
        m_command_mapping.insert(command_string, function_handle);
    }

    void ParserBase::addControlCommandMapping(
        QString command_string,
        std::function< void(void) > function_handle)
    {
        m_control_command_mapping.insert(command_string, function_handle);
    }

    void ParserBase::setBlockCommentDelimiters(
        const QString beginning_delimiter,
        const QString ending_delimiter)
    {
        m_block_comment_starting_delimiter = beginning_delimiter;
        m_block_comment_ending_delimiter   = ending_delimiter;
    }

    void ParserBase::setLineCommentString(const QString line_comment)
    {
        m_line_comment = line_comment;
    }

    bool ParserBase::parseControlCommand(QString command_string)
    {
        command_string = command_string.trimmed();

        QHash< QString, std::function< void() > >::iterator temp_iter;
        if ((temp_iter = m_control_command_mapping.find(command_string)) !=
            m_control_command_mapping.end())
        {
            temp_iter.value()();
            return true;
        }
        // else
        // {
        //     QString exceptionString;
        //     QTextStream(&exceptionString) << "Illegal argument "
        //                                   << command_string
        //                                   << " within GCode file, on line "
        //                                   <<
        //                                   m_current_gcode_command.getLineNumber()
        //                                   << ".";
        //     throw IllegalArgumentException(exceptionString);
        // }

        return false;
    }


    void ParserBase::extractComments(QString& command)
    {
        if (!m_line_comment.isEmpty())
        {
            // First extract the line comment.
            int comment_index = command.indexOf(m_line_comment);
            if (comment_index > -1)
            {
                // This retrieves the comment from the specified index,
                // removes the comment delimiter from the comment string,
                // and trims any extra whitespace.
                m_current_gcode_command.setComment(
                    command.right(command.size() - comment_index)
                        .remove(0, m_line_comment.size())
                        .trimmed());
                command.remove(comment_index, command.size());

                return;
            }
        }

        if (!m_block_comment_starting_delimiter.isEmpty() ||
            !m_block_comment_ending_delimiter.isEmpty())
        {
            int comment_index =
                command.indexOf(m_block_comment_starting_delimiter);
            if (comment_index > -1)
            {
                int ending_comment_index = command.indexOf(
                    m_block_comment_ending_delimiter, comment_index);
                if (ending_comment_index > -1)
                {
                    QString temp =
                        command.mid(comment_index, ending_comment_index);
                    temp.remove(0, m_block_comment_starting_delimiter.size())
                        .chop(m_block_comment_ending_delimiter.size());
                    temp = temp.trimmed();
                    m_current_gcode_command.setComment(temp);
                    command.remove(comment_index,
                                   ending_comment_index - comment_index + 1);
                }
                else
                {
                    QString exceptionString;
                    QTextStream(&exceptionString)
                        << "Comment not closed within GCode file, on line "
                        << m_current_gcode_command.getLineNumber() << "."
                        << endl
                        << "With GCode command string: "
                        << getCurrentCommandString();
                    throw IllegalParameterException(exceptionString);
                }
            }
        }
    }

    void ParserBase::parseComment(QString& comment)
    {
        // TODO: Need to flesh this out and possibly add another hashtable for
        // handlers.
    }


    void ParserBase::parseControlCommands(QStringList& command_list)
    {
        for (QStringList::Iterator i = command_list.begin();
             i != command_list.end();
             i++)
        {
            if (parseControlCommand(*i))
            {
                i = command_list.erase(i);
            }
        }
    }

    void ParserBase::setCurrentCommand(QString command)
    {
        bool no_error;
        m_current_command_string = command;
        m_current_gcode_command.setCommand(command.at(0).toLatin1());
        m_current_gcode_command.setCommandID(
            command.right(command.size() - 1).toInt(&no_error));
        if (!no_error)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error with numerical conversion for GCode command on GCode "
                   "line "
                << m_current_gcode_command.getLineNumber() << "." << endl
                << "With GCode command string: " << getCurrentCommandString();
            throw IllegalArgumentException(exceptionString);
        }
    }

    const QString& ParserBase::getCurrentCommandString() const
    {
        return m_current_command_string;
    }

    const QString& ParserBase::getPreviousMovementCommandString() const
    {
        return m_previous_movement_command_string;
    }

    void ParserBase::setPreviousMovementCommand(QString command)
    {
        bool no_error;
        char gCommandChecker = command.at(0).toLatin1();
        unsigned int gValueChecker =
            command.right(command.size() - 1).toUInt(&no_error);

        if (!no_error)
        {
            QString exceptionString;
            QTextStream(&exceptionString)
                << "Error with numerical conversion for movement command on "
                   "GCode line "
                << m_current_gcode_command.getLineNumber() << "." << endl
                << "With GCode command string: " << getCurrentCommandString();
            throw IllegalArgumentException(exceptionString);
        }

        if ((gCommandChecker == 'G' || gCommandChecker == 'g') &&
            gValueChecker < 4)
        {
            m_previous_movement_command_string = command;
        }
    }

    void ParserBase::setLineNumber(int linenumber)
    {
        m_current_gcode_command.setLineNumber(linenumber);
    }

}  // namespace ORNL
