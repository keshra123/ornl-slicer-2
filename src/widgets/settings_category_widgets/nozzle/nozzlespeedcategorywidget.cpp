#include "widgets/settings_category_widgets/nozzle/nozzlespeedcategorywidget.h"

#include "ui_nozzle_speed_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    NozzleSpeedCategoryWidget::NozzleSpeedCategoryWidget()
        : ui(new Ui::NozzleSpeedCategoryWidget)
    {
        ui->setupUi(this);

        ui->default_->label(Constants::NozzleSettings::Speed::Labels::kDefault);
        ui->default_->tooltip(
            Constants::NozzleSettings::Speed::Tooltips::kDefault);
        m_velocity[Constants::NozzleSettings::Speed::kDefault] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->default_);

        ui->perimeter->label(
            Constants::NozzleSettings::Speed::Labels::kPerimeter);
        ui->perimeter->tooltip(
            Constants::NozzleSettings::Speed::Tooltips::kPerimeter);
        m_velocity[Constants::NozzleSettings::Speed::kPerimeter] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->perimeter);

        ui->inset->label(Constants::NozzleSettings::Speed::Labels::kInset);
        ui->inset->tooltip(Constants::NozzleSettings::Speed::Tooltips::kInset);
        m_velocity[Constants::NozzleSettings::Speed::kInset] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->inset);

        ui->skin->label(Constants::NozzleSettings::Speed::Labels::kSkin);
        ui->skin->tooltip(Constants::NozzleSettings::Speed::Tooltips::kSkin);
        m_velocity[Constants::NozzleSettings::Speed::kSkin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->skin);

        ui->infill->label(Constants::NozzleSettings::Speed::Labels::kInfill);
        ui->infill->tooltip(
            Constants::NozzleSettings::Speed::Tooltips::kInfill);
        m_velocity[Constants::NozzleSettings::Speed::kInfill] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->infill);

        initializeSettings();
    }

    NozzleSpeedCategoryWidget::~NozzleSpeedCategoryWidget()
    {
        delete ui;
    }

    void NozzleSpeedCategoryWidget::handleConditions()
    {
        // TODO
    }
}  // namespace ORNL
