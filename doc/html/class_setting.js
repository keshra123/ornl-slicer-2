var class_setting =
[
    [ "Setting", "class_setting.html#ab786589ae3f267fe1f3770eae11c8ff7", null ],
    [ "Setting", "class_setting.html#af468e7d8e3be4f22634f9a0e9279f649", null ],
    [ "addAffectedArea", "class_setting.html#a0365d1f6eada2c649ba2f282b1f7d4a8", null ],
    [ "addCondition", "class_setting.html#acd253f3c297097a724a46636db68937f", null ],
    [ "getAffectedAreas", "class_setting.html#a2803d3b6f5e11d773447b6a963065c85", null ],
    [ "getConditions", "class_setting.html#ab4a382d57fbb78d67e316da6c7cceec0", null ],
    [ "getKey", "class_setting.html#a18aa160dfdff785d10d0e0352cbd1860", null ],
    [ "getLabel", "class_setting.html#add7c3615a983b7040e8f60bf49b52600", null ],
    [ "getTooltip", "class_setting.html#a8b47327881a0e55993e5bd2b5fbff90d", null ],
    [ "getValue", "class_setting.html#a1ec851191e1fa754fb94a0e7a78a070f", null ],
    [ "passesConditions", "class_setting.html#a4e50a736b67e94cc2f5cc6e9386ece16", null ],
    [ "setKey", "class_setting.html#a83006df7264a91d202375482be36b1b8", null ],
    [ "setLabel", "class_setting.html#ae19d68cc4e7db95cc71423c324da8e65", null ],
    [ "setTooltip", "class_setting.html#a9a62879e50623d17576f228ba7c3b930", null ],
    [ "setValue", "class_setting.html#ad56b13ec7b69c4b4667c34eb441f50ca", null ],
    [ "setValue", "class_setting.html#a79261ad03b18839e900561f23e499d49", null ],
    [ "setValue", "class_setting.html#ab54033b86a2eb446603ec6ba50335322", null ]
];