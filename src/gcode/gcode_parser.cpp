#include "gcode/gcode_parser.h"

#include <QDebug>
#include <QIODevice>
#include <QVariant>
#include <QVector>
#include <QtGlobal>
#include <functional>

#include "exceptions/exceptions.h"
#include "gcode/gcode_command.h"
#include "gcode/parsers/blue_gantry_parser.h"
#include "gcode/parsers/cincinnati_parser.h"
#include "gcode/parsers/common_parser.h"
#include "gcode/parsers/wolf_parser.h"
#include "utilities/constants.h"

namespace ORNL
{
    GcodeParser::GcodeParser()
        : m_project_manager(ProjectManager::getInstance())
        , m_current_parser(
              nullptr,
              std::bind(&GcodeParser::freeParser, this, std::placeholders::_1))
    {
        m_current_machine = m_project_manager->currentMachineName();

        connect(m_project_manager.data(),
                SIGNAL(currentMachineChanged(QString)),
                this,
                SLOT(machineChanged(QString)));

        selectParser(GcodeSyntax::kCommon);
    }

    GcodeParser::GcodeParser(GcodeSyntax id)
        : m_project_manager(ProjectManager::getInstance())
        , m_current_parser(
              nullptr,
              std::bind(&GcodeParser::freeParser, this, std::placeholders::_1))
    {
        m_current_machine = m_project_manager->currentMachineName();

        connect(m_project_manager.data(),
                SIGNAL(currentMachineChanged(QString)),
                this,
                SLOT(machineChanged(QString)));

        selectParser(id);
    }

    GcodeParser::~GcodeParser()
    {}

    GcodeCommand GcodeParser::parseLine(QString line)
    {
        if (m_current_parser == nullptr)
        {
            throwParserNotSetException();
        }

        return m_current_parser->parseCommand(line);
    }

    GcodeCommand GcodeParser::parseLine(QString line, int line_number)
    {
        if (m_current_parser == nullptr)
        {
            throwParserNotSetException();
        }

        return m_current_parser->parseCommand(line, line_number);
    }

    QPair< GcodeCommand, GcodeCommand > GcodeParser::updateLine(
        QString line,
        int line_number,
        QString previous_line,
        QString next_line)
    {
        GcodeCommand l, r;
        if (m_current_parser == nullptr)
        {
            throwParserNotSetException();
        }

        // Sets the internal state of the parser.
        m_current_parser->parseCommand(previous_line);
        l = m_current_parser->parseCommand(line, line_number);
        r = m_current_parser->parseCommand(next_line, line_number + 1);

        return QPair< GcodeCommand, GcodeCommand >(l, r);
    }

    QVector< GcodeCommand > GcodeParser::parse(QStringList::iterator begin,
                                               QStringList::iterator end,
                                               quint64 line_number)
    {
        if (m_current_parser == nullptr)
        {
            throwParserNotSetException();
        }

        QVector< GcodeCommand > command_list;
        for (; begin != end; begin++, line_number++)
        {
            command_list.push_back(
                m_current_parser->parseCommand(*begin, line_number));
        }

        return command_list;
    }

    void GcodeParser::selectParser(GcodeSyntax parserID)
    {
        if (m_current_parser_id == parserID)
        {
            return;
        }

        switch (parserID)
        {
            case GcodeSyntax::kCommon:
                m_current_parser.reset(new CommonParser());
                break;
            case GcodeSyntax::kCincinnati:
                m_current_parser.reset(new CincinnatiParser());
                break;
            case GcodeSyntax::kBlueGantry:
                m_current_parser.reset(new BluegantryParser());
                break;
            case GcodeSyntax::kWolf:
                m_current_parser.reset(new WolfParser());
                break;
            case GcodeSyntax::kIngersoll:
                // TODO: Make Ingersoll parser
                break;
            case GcodeSyntax::kNorthrup:
                // TODO: Make Northrup parser
                break;
            default:
                throwParserNotSetException();
                break;
        }

        selectMachine(parserID);
        m_current_parser->config();
    }


    void GcodeParser::machineChanged(QString machine_name)
    {
        m_current_machine = machine_name;
        // TODO: Select the correct parser based on the machine.
        // NOTE: Cannot do this unitl the settings manager actually works and
        // uses machine strings.
    }

    void GcodeParser::freeParser(CommonParser* parser)
    {
        if (parser != nullptr)
        {
            delete parser;
        }

        parser = nullptr;
    }

    void GcodeParser::throwParserNotSetException()
    {
        QString exceptionString;
        QTextStream(&exceptionString)
            << "No parser selected to parse the GCode.";
        throw ParserNotSetException(exceptionString);
    }

    void GcodeParser::selectMachine(GcodeSyntax id)
    {
        // TODO: This, once the strings are preset and are decided on.
    }


}  // namespace ORNL
