#ifndef GCODE_H
#define GCODE_H

#include <QFile>
#include <QSharedPointer>
#include <QVector>

#include "omp.h"

namespace ORNL
{
    class GcodeLayer;
    class ProjectManager;
    class Mesh;
    class WriterBase;

    class Gcode : public QVector< QSharedPointer< GcodeLayer > >
    {
    public:
        //! \brief Constructor
        Gcode();

        //! \brief Destructor
        ~Gcode();

        void updateMesh(QSharedPointer< Mesh > mesh);

        //! \brief Returns a string with all the gcode
        QString toGcode(WriterBase* syntax, QString filename);

        //! \brief Writes the gcode to the file with the specific file name (and
        //! creates it if necessary)
        void writeGcode(WriterBase* syntax, QString filename);

        //! \brief Writes the gcode to the file with the specific file (and
        //! creates it if necessary)
        void writeGcode(WriterBase* syntax, QFile& filename);

    private:
        QSharedPointer< ProjectManager > m_project_manager;

        QSharedPointer< omp_lock_t > m_lock;
    };
}  // namespace ORNL
#endif  // GCODE_H
