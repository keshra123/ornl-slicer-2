
set -ev

brew update

#git -C "$(brew --repo homebrew/versions)" fetch --unshallow

SELFDIR="$(dirname $0 )"
pushd "$SELFDIR"
brew bundle
popd

if [ "${GCC_V}" != "" ]; then
  brew install gcc@$GCC_V || brew link --overwrite gcc@$GCC_V
fi
