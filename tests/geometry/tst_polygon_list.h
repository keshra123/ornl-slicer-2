#ifndef TST_POLYGON_LIST_H
#define TST_POLYGON_LIST_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

/*!
 *  \class PolygonListTest
 *
 *  \brief A class that tests the functions of the PolygonList class
 */

class PolygonListTest : public QObject
{
    Q_OBJECT

private slots:

    //! \brief Tests the pointCount() function
    void testPointCount();

    //! \brief Tests the offset() function
    void testOffset();

    //! \brief Tests the inside() function
    void testInside();

    //! \brief Test the findInside() function
    void testFindInside();

    //! \brief Test convexHull() function
    void testConvexHull();

    //! \brief Test the removeEmptyHoles() function
    void testRemoveEmptyHoles();

    //! \brief Test the getEmptyHoles() function
    void testGetEmptyHoles();

    //! \brief Test splitIntoParts() function
    void testSplitIntoParts();

    //! \brief Test removeSmallAreas() function
    void testRemoveSmallAreas();

    //! \brief Test removeDegenerateVertices() function
    void testRemoveDegenerateVertices();

    //! \brief Test min() function
    void testMin();

    //! \brief Test rotate() function
    void testRotate();

    //! \brief Test intersect operator for PolygonList intersect Polyline
    void testPolygonListIntersectPolyline();
};

DECLARE_TEST(PolygonListTest)

#endif //TST_POLYGON_LIST_H
