#include "managers/camera_manager.h"

namespace ORNL
{
    const QVector3D CameraManager::LocalForward(0.0f, 0.0f, -1.0f);
    const QVector3D CameraManager::LocalUp(0.0f, 1.0f, 0.0f);
    const QVector3D CameraManager::LocalRight(1.0f, 0.0f, 0.0f);

    // Transform By (Add/Scale)
    void CameraManager::translate(const QVector3D& dt)
    {
        mDirty = true;
        mTranslation += dt;
    }

    void CameraManager::rotate(const QQuaternion& dr)
    {
        mDirty    = true;
        mRotation = dr * mRotation;
    }

    void CameraManager::rotateAround(const QVector3D& point,
                                     float angle,
                                     const QVector3D& axis)
    {
        QMatrix4x4 rotation =
            Qt3DCore::QTransform::rotateAround(point, angle, axis);
        mTranslation = rotation * mTranslation;
        rotate(angle, axis);
        mDirty = true;
    }


    // Transform To (Setters)
    void CameraManager::setTranslation(const QVector3D& t)
    {
        mDirty       = true;
        mTranslation = t;
    }

    void CameraManager::setRotation(const QQuaternion& r)
    {
        mDirty    = true;
        mRotation = r;
    }

    void CameraManager::lookAt(const QVector3D& eye,
                               const QVector3D& center,
                               const QVector3D& up)
    {
        setTranslation(eye);
        setRotation(QQuaternion::rotationTo(LocalForward, (center - eye)));
    }

    // Accessors
    const QMatrix4x4& CameraManager::toMatrix()
    {
        if (mDirty)
        {
            mDirty = false;
            mWorld.setToIdentity();
            mWorld.rotate(mRotation.conjugate());
            mWorld.translate(-mTranslation);
        }
        return mWorld;
    }

    // Queries
    QVector3D CameraManager::forward() const
    {
        return mRotation.rotatedVector(LocalForward);
    }

    QVector3D CameraManager::up() const
    {
        return mRotation.rotatedVector(LocalUp);
    }

    QVector3D CameraManager::right() const
    {
        return mRotation.rotatedVector(LocalRight);
    }

    // Qt Streams
    QDebug operator<<(QDebug dbg, const CameraManager& transform)
    {
        dbg << "CameraManager\n{\n";
        dbg << "Position: <" << transform.translation().x() << ", "
            << transform.translation().y() << ", "
            << transform.translation().z() << ">\n";
        dbg << "Rotation: <" << transform.rotation().x() << ", "
            << transform.rotation().y() << ", " << transform.rotation().z()
            << " | " << transform.rotation().scalar() << ">\n}";
        return dbg;
    }

    QDataStream& operator<<(QDataStream& out, const CameraManager& transform)
    {
        out << transform.mTranslation;
        out << transform.mRotation;
        return out;
    }

    QDataStream& operator>>(QDataStream& in, CameraManager& transform)
    {
        in >> transform.mTranslation;
        in >> transform.mRotation;
        transform.mDirty = true;
        return in;
    }
}  // namespace ORNL
