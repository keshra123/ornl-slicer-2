#include <QDebug>
#include <QFile>
#include <QPair>
#include <QList>
#include <iostream>

#include "gcode/gcode_command.h"
#include "gcode/parsers/cincinnati_parser.h"
#include "gcode/parsers/tst_cincinnati_parser.h"
#include "exceptions/exceptions.h"

#if 0 // TODO fix tests

void TestCincinnatiParser::initTestCase()
{
    // test->config();
}

void TestCincinnatiParser::cleanupTestCase()
{
    //test->reset();
}

void TestCincinnatiParser::init()
{
	test = new CincinnatiParser();
    test->config();
}

void TestCincinnatiParser::cleanup()
{
   // test->reset();
	delete test;
}

void TestCincinnatiParser::testWholeRead()
{
    // TODO: Make this general but this is for testing purposees only
    QFile temp("C:/Users/Administrator/Downloads/gcode.nc");
    temp.open(QIODevice::ReadOnly | QIODevice::Text);
    QStringList abc = QString(temp.readAll()).split('\n');
    try
    {
        unsigned int LineNum = 1;
        for(QString i : abc)
        {
            test->parseCommand(i, LineNum);
            LineNum++;
        }
    }
    catch(IllegalArgumentException& e)
    {
        qDebug() << e.what();
    }
    catch(IllegalParameterException& e)
    {
        qDebug() << e.what();
    }
}

#endif
