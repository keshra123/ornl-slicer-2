#ifndef PARSERBASE_H
#define PARSERBASE_H

//! \file parserbase.h

#include <QHash>
#include <QString>
#include <QStringList>
#include <functional>

#include "gcode/gcode_command.h"

namespace ORNL
{
    /*!
     * \class ParserBase
     * \brief This class implements the core functionality for parsing
     * Gcodecommands that are character delimted with parameters , comments, and
     * comment commands.
     */
    // NOTE: Could possibly make this class completely static, bad idea with
    // multiple parsers.
    class ParserBase
    {
    public:
        //! \brief Parses the command that is passed from the command string,
        //!        and sends the command to its corresponding handler.
        //! \param command_string Gcode command string
        //! \param delimiter Optional delimiter for use with other delimited
        //! types i.e. comma separated lists \return The command split into
        //! parameters, comments, command, and commandID. \throws
        //! IllegalArgumentException This is thorwn when the first part of a
        //! command is unable to be
        //!         mapped to any funciton handler.
        //! \throws IllegalParameterException This is thown when a parameter is
        //! incorrectly formatted or is missing
        //!         required parameters, this could also occur when a block type
        //!         comment is not closed correctly.
        GcodeCommand parseCommand(QString command_string,
                                  QString delimiter = " ");


        //! \brief Parsers the command that is passed from the command string,
        //!        and sends the command to its corresponding handler. This
        //!        function also sets the line number with the corresponding
        //!        command.
        //! \param command_string Gcode command string
        //! \param line_number The line number corresponding to the
        //! GCodeCommand's location within the file. \param delimiter Optional
        //! delimiter for use with other delimited types i.e. comma separated
        //! lists \return The command split into parameters, comments, command,
        //! and commandID. \throws IllegalArgumentException This is thorwn when
        //! the first part of a command is unable to be
        //!         mapped to any funciton handler.
        //! \throws IllegalParameterException This is thown when a parameter is
        //! incorrectly formatted or is missing
        //!         required parameters, this could also occur when a block type
        //!         comment is not closed correctly.
        GcodeCommand parseCommand(QString command_string,
                                  int line_number,
                                  QString delimiter = " ");


        //! \brief Clears all mappings to any command handler pairing
        // void clearAllMappings();

        //! \brief Pure virtual function that configures the parsers command
        //! strings, handlers pairs, for commands
        //!        , control commands, and comment delimiters.
        virtual void config() = 0;

        //! \brief Gets the current command of the parser.
        //! \return Last used command string that was parsed.
        const QString& getCurrentCommandString() const;

        //! \brief Gets the current movement command of the parser.
        //! \note This is only limited to G0, G1, G2, G3.
        //! \return Last used movement command.
        const QString& getPreviousMovementCommandString() const;


    protected:
        //! \brief Default Constructor
        ParserBase();

        //! \brief Adds a command to the command mapping hash with its
        //! correspoding handler. These commands
        //!        will be passed thier parameters in a QStringList separated by
        //!        the delimiter, by defualt the space character.
        //! \param command_string The Gcode command string.
        //! \param function_handle Function that accepts one argument and will
        //! handle manipulating the internal data
        //!                        structures when passed.
        void addCommandMapping(
            QString command_string,
            std::function< void(QStringList&) > function_handle);

        //! \brief Adds a control command to the hashtable. Control commands are
        //! designed to be simple
        //!        toggleable commands, any command with parameters should be
        //!        added via the command mapping function.
        //! \note This is meant for simple toggling commands.
        //! \param command_string The Gcode command string.
        //! \param function_handle Function that accepts ZERO arguments and will
        //! handle manipulating the internal data
        //!                        structures when passed.
        void addControlCommandMapping(
            QString command_string,
            std::function< void(void) > function_handle);

        //! \brief Sets the comment delimters for use when extracting comments
        //! from the line of text.
        //!        If a pair of these delimiters if found within a command. Any
        //!        characters between them will be ignored from the actual
        //!        command. But can have special comment parsing later.
        //! \param beginning_delimter String to indicate the begining of a block
        //! style comment. \param ending_delimter String to indicate the end of
        //! a block style comment.
        void setBlockCommentDelimiters(const QString beginning_delimiter,
                                       const QString ending_delimiter);

        //! \brief Sets the line comment string that will be used to signify
        //! that anything
        //!        after this string and before a newline, should be set as a
        //!        comment and not parsed. Special comment parsing can be
        //!        performed afterwards.
        //! \param line_comment String to indicate the begining of a line style
        //! comment.
        void setLineCommentString(const QString line_comment);


        //! \brief Parses a single control command
        //! \param command_string A single Gcode command.
        //! \return true if command was successfully processed, false if not.
        bool parseControlCommand(QString command_string);

        //! \brief Parses a List of Control Commands, removing them from the
        //! list if
        //!        are found. Any commands not used will remain within the list.
        //! \param command_list List of split Gcode commands.
        //! \note This function does modify the list passed.
        void parseControlCommands(QStringList& command_list);

        //! \brief Parses and removes any comments from the command string.
        //! \param command A GCode command line.
        //! \note This function does modify the string passed.
        void extractComments(QString& command);

        //! \brief This function handles all comment commands provided in the
        //! configuration. \param comment A Comment within a Gcode command.
        void parseComment(QString& comment);

        //! \brief Sets the current command of the parser to the current string.
        //! \param command Sets the current command in use.
        void setCurrentCommand(QString command);


        //! \brief Sets the previous movement command of the parser.
        //! \param command Command to attempt to set the previous command
        //! string. \note This is different from setCurrentCommand in that there
        //! are commands
        //!       that provide no movement.
        //! \note The only commands used by this function are G0, G1, G2, G3.
        void setPreviousMovementCommand(QString command);

        //! \brief Sets the line number for the current Gcode command
        //! \param linenumber Linenumber associated with the GCode command.
        void setLineNumber(int linenumber);

        void resetInternalState();

        // TODO: Need to figure out a way to make this private and use accessor
        // methods.
        GcodeCommand m_current_gcode_command;

    private:
        //! \brief Maps a GCode command to a function handlerm
        //! \brief This function throws a multiple parameter exception.
        QHash< QString, std::function< void(QStringList&) > >
            m_command_mapping;  //!< Mappings of GCode command strings
                                //!< to function handlers which take parameters

        QHash< QString, std::function< void() > >
            m_control_command_mapping;  //!< Mappings of GCode command strings
                                        //!< to function handler which have
                                        //!< predefined functionality and take
                                        //!< no parameters
        QString m_line_comment;
        QString m_block_comment_starting_delimiter;
        QString m_block_comment_ending_delimiter;
        QString m_current_command_string;
        QString m_previous_movement_command_string;


    };  // class ParserBase
}  // namespace ORNL
#endif  // PARSERBASE_H
