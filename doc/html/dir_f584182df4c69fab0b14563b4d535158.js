var dir_f584182df4c69fab0b14563b4d535158 =
[
    [ "globalsettingswindow.cpp", "globalsettingswindow_8cpp.html", null ],
    [ "globalsettingswindow.h", "globalsettingswindow_8h.html", [
      [ "GlobalSettingsWindow", "class_o_r_n_l_1_1_global_settings_window.html", "class_o_r_n_l_1_1_global_settings_window" ]
    ] ],
    [ "localsettingswindow.cpp", "localsettingswindow_8cpp.html", null ],
    [ "localsettingswindow.h", "localsettingswindow_8h.html", [
      [ "LocalSettingsWindow", "class_o_r_n_l_1_1_local_settings_window.html", "class_o_r_n_l_1_1_local_settings_window" ]
    ] ],
    [ "mainwindow.cpp", "mainwindow_8cpp.html", null ],
    [ "mainwindow.h", "mainwindow_8h.html", [
      [ "MainWindow", "class_o_r_n_l_1_1_main_window.html", "class_o_r_n_l_1_1_main_window" ]
    ] ],
    [ "preferenceswindow.cpp", "preferenceswindow_8cpp.html", null ],
    [ "preferenceswindow.h", "preferenceswindow_8h.html", [
      [ "PreferencesWindow", "class_o_r_n_l_1_1_preferences_window.html", "class_o_r_n_l_1_1_preferences_window" ]
    ] ]
];