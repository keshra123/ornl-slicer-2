#include "geometry/linearalg2d.h"

namespace ORNL
{
    short LinearAlg2D::pointLiesOnTheRightOfLine(Point p, Point p0, Point p1)
    {
        // no tests unless the segment p0-p1 is at least partly at, or to right
        // of, p.x()
        if (std::max(p0.x(), p1.x()) >= p.x())
        {
            double pdY = p1.y() - p0.y();
            if (pdY < 0)  // p0->p1 is 'falling'
            {
                if (p1.y() <= p.y() && p0.y() > p.y())  // candidate
                {
                    // dx > 0 if intersection is to right of p.x()
                    double dx = (p1.x() - p0.x()) * (p1.y() - p.y()) -
                        (p1.x() - p.x()) * pdY;
                    if (dx == 0)  // includes p == p1
                    {
                        return 0;
                    }
                    if (dx > 0)
                    {
                        return 1;
                    }
                }
            }
            else if (p.y() >= p0.y())
            {
                if (p.y() <
                    p1.y())  // candidate for p0->p1 'rising' and includes p.y()
                {
                    // dx > 0 if intersection is to right of p.x()
                    double dx = (p1.x() - p0.x()) * (p.y() - p0.y()) -
                        (p.x() - p0.x()) * pdY;
                    if (dx == 0)  // includes p == p0
                    {
                        return 0;
                    }
                    if (dx > 0)
                    {
                        return 1;
                    }
                }
                else if (p.y() == p1.y())
                {
                    // some special cases here, points on border:
                    // - p1 exactly matches p (might otherwise be missed)
                    // - p0->p1 exactly horizontal, and includes p.
                    // (we already tested std::max(p0.x(),p1.x()) >= p.x() )
                    if (p.x() == p1.x() ||
                        (pdY == 0 && std::min(p0.x(), p1.x()) <= p.x()))
                    {
                        return 0;
                    }
                }
            }
        }
        return -1;
    }
}  // namespace ORNL
