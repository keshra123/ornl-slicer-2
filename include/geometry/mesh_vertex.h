#ifndef MESHVERTEX_H
#define MESHVERTEX_H

#include <QVector>
#include <Qt3DCore/QTransform>

#include "geometry/point.h"

namespace ORNL
{
    /*!
     * \class MeshVertex
     *
     * \brief Vertex type to be used in a Mesh.
     *
     * Keeps track of which faces connect to it.
     */
    class MeshVertex
    {
    public:
        /*!
         * \brief Constructor
         *
         * \param location The location of this vertex
         * \param normal The normal vector for this vertex
         */
        MeshVertex(Point location = Point(), Point normal = Point());

        /*!
         * \brief Return a MeshVertex object that is a transformed version of
         * this one
         *
         * \note This MeshVertex is not modified
         *
         * \param transform The transform applied to the location
         * \return The transformed vertex
         */
        MeshVertex transform(const Qt3DCore::QTransform& transform) const;

        Point location;  //!< Location of the vertex
        Point normal;
        QVector< int >
            connected_faces;  //!< list of the indices of connected faces
    };

    /*!
     * \brief comparison operator used for QMap
     * \param lhs Left side of the less than operator
     * \param rhs Right side of the less then operator
     * \return Whether \p lhs is less than \p rhs
     */
    bool operator<(const MeshVertex& lhs, const MeshVertex& rhs);
}  // namespace ORNL
#endif  // MESHVERTEX_H
