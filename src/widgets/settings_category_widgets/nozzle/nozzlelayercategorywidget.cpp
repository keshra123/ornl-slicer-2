#include "widgets/settings_category_widgets/nozzle/nozzlelayercategorywidget.h"

#include "ui_nozzle_layer_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    NozzleLayerCategoryWidget::NozzleLayerCategoryWidget()
        : ui(new Ui::NozzleLayerCategoryWidget)
    {
        ui->setupUi(this);

        ui->layer_height->label(
            Constants::NozzleSettings::Layer::Labels::kLayerHeight);
        ui->layer_height->tooltip(
            Constants::NozzleSettings::Layer::Tooltips::kLayerHeight);
        m_distance[Constants::NozzleSettings::Layer::kLayerHeight] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->layer_height);

        initializeSettings();
    }

    NozzleLayerCategoryWidget::~NozzleLayerCategoryWidget()
    {
        delete ui;
    }

    void NozzleLayerCategoryWidget::handleConditions()
    {
        // TODO
    }
}  // namespace ORNL
