#include "widgets/gcodehighlighter.h"

#include <QColor>
#include <QFont>
#include <QRegularExpression>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QVector>

#include "utilities/constants.h"

namespace ORNL
{
    GcodeHighlighter::GcodeHighlighter(QTextDocument* parent)
        : QSyntaxHighlighter(parent)
    {
        setupBrimHighlighting();
        setupCoastingHighlighting();
        setupInfillHighlighting();
        setupInitalStartupHighlighting();
        setupInsetHighlighting();
        setupPerimeterHighlighting();
        setupPrestartHighlighting();
        setupRaftHighlighting();
        setupSkeletonHighlighting();
        setupSkinHighlighting();
        setupSkirtHighlighting();
        setupSlowDownHighlighting();
        setupSpiralLiftHighlighting();
        setupSupportHighlighting();
        setupSupportRoofHighlighting();
        setupTipWipeHighlighting();
        setupTravelHighlighting();
        setupUnknownHighlighting();

        /*        QRegularExpression re("G1");
                QTextCharFormat format;

                format.setFontWeight(QFont::Bold);
                format.setForeground(Qt::red);

                GcodeHighlightingRules temp;
                temp.pattern = re;
                temp.formatting = format;

                m_highlighting_rules.push_back(temp);
                */
    }

    void GcodeHighlighter::highlightBlock(const QString& text)
    {
        QRegularExpressionMatch temp;
        for (GcodeHighlightingRules i : m_path_modifier_highlighting_rules)
        {
            temp = i.pattern.match(text);
            if (temp.hasMatch())
            {
                setFormat(0, text.length(), i.formatting);
                setCurrentBlockState(1);
                return;
            }
            else
            {
                setCurrentBlockState(0);
            }
        }

        for (GcodeHighlightingRules i : m_path_type_highlighting_rules)
        {
            temp = i.pattern.match(text);
            if (temp.hasMatch())
            {
                setFormat(0, text.length(), i.formatting);
                setCurrentBlockState(1);
                return;
            }
            else
            {
                setCurrentBlockState(0);
            }
        }
    }

    void GcodeHighlighter::setupPerimeterHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kPerimeter);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kPerimeter);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }


    void GcodeHighlighter::setupInfillHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kInfill);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kInfill);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }


    void GcodeHighlighter::setupSkinHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kSkin);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kSkin);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }


    void GcodeHighlighter::setupInsetHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kInset);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kInset);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupTravelHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kTravel);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kTravel);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupSupportHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kSupport);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kSupport);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupSupportRoofHighlighting()
    {
        QRegularExpression re(Constants::RegionTypeStrings::kSupportRoof);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kSupportRoof);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupSkeletonHighlighting()
    {
        // TODO: Figure out string and color for skeleton, none exist
    }

    void GcodeHighlighter::setupSkirtHighlighting()
    {
        // TODO: Figure out string and color for skirt, none exists.
    }

    void GcodeHighlighter::setupPrestartHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kPrestart);
        QTextCharFormat format;
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

        format.setForeground(Constants::Colors::kPrestart);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupInitalStartupHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kInitialStartup);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kInitialStartup);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupSlowDownHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kSlowDown);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kSlowDown);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupTipWipeHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kTipWipe);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kTipWipe);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupCoastingHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kCoasting);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kCoasting);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupSpiralLiftHighlighting()
    {
        QRegularExpression re(Constants::PathModifierStrings::kSpiralLift);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kSpiralLift);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_modifier_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupUnknownHighlighting()
    {
        // TODO: Figure out what is meant by 'unknown'
        QRegularExpression re(Constants::RegionTypeStrings::kUnknown);
        re.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
        QTextCharFormat format;

        format.setForeground(Constants::Colors::kUnknown);

        GcodeHighlightingRules temp;
        temp.pattern    = re;
        temp.formatting = format;

        m_path_type_highlighting_rules.push_back(temp);
    }

    void GcodeHighlighter::setupRaftHighlighting()
    {
        // TODO: Figure out string and color for raft
    }

    void GcodeHighlighter::setupBrimHighlighting()
    {
        // TODO: Figure out string and color for brim
    }

}  // namespace ORNL
