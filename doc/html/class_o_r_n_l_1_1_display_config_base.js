var class_o_r_n_l_1_1_display_config_base =
[
    [ "DisplayConfigBase", "class_o_r_n_l_1_1_display_config_base.html#a86c5eabd67984de0dd016d5ab739dfa4", null ],
    [ "copy", "class_o_r_n_l_1_1_display_config_base.html#a3cc0cb529d64e189d46a53e4ac57fd96", null ],
    [ "deleteFile", "class_o_r_n_l_1_1_display_config_base.html#a7806d65c8f7543ca925f31bdeb70b0bb", null ],
    [ "getExtension", "class_o_r_n_l_1_1_display_config_base.html#a3a3ac665eed3c6f58199b5c9a868c9f3", null ],
    [ "getName", "class_o_r_n_l_1_1_display_config_base.html#a4af4d3d4a247e1700322be0bf9131c13", null ],
    [ "getPath", "class_o_r_n_l_1_1_display_config_base.html#a4eab19371a4cd79f81c337182b1ca9d8", null ],
    [ "loadFromFile", "class_o_r_n_l_1_1_display_config_base.html#a8b72d9b7bf85b847bc47bedf39fa9c6a", null ],
    [ "saveToFile", "class_o_r_n_l_1_1_display_config_base.html#ad17ed1440454a631384e53e1af7deb15", null ]
];