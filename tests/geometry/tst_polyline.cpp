#include "tst_polyline.h"
#include "geometry/polygon_list.h"

using namespace ORNL;

void PolylineTest::testShorterThan()
{
    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point(45, 65);
    cutline << Point(50, 70);
    cutline << Point(60, 60);
    cutline << Point(54, 58);
    cutline << Point(53, 56);
    cutline << Point(60, 50);
    cutline << Point(60, 20);
    cutline << Point(30, 20);
    cutline << Point(20, 30);
    cutline << Point(30, 40);
    cutline << Point(40, 30);

    bool boolean = cutline.shorterThan(40);
    QVERIFY(boolean == false);
    boolean = cutline.shorterThan(181);
    QVERIFY(boolean == false);
    boolean = cutline.shorterThan(182);
    QVERIFY(boolean == true);
}

void PolylineTest::testClosestPointTo()
{
    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point(45, 65);
    cutline << Point(50, 70);
    cutline << Point(60, 60);
    cutline << Point(54, 58);
    cutline << Point(53, 56);
    cutline << Point(60, 50);
    cutline << Point(60, 20);
    cutline << Point(30, 20);
    cutline << Point(20, 30);
    cutline << Point(30, 40);
    cutline << Point(40, 30);

    Polyline cutline2;
    cutline2 << Point(45, 1);
    cutline2 << Point(45, 3);

    Point point (50, 60);
    Point point2;
    Point point3 (45, 2);

    point2 = cutline.closestPointTo(point);
    QVERIFY(point2.x() == 54);
    QVERIFY(point2.y() == 58);
    point2 = cutline2.closestPointTo(point3);
    QVERIFY(point2.x() == 45);
    QVERIFY(point2.y() == 1);
}

void PolylineTest::testClose()
{
    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point(45, 65);
    cutline << Point(50, 70);
    cutline << Point(60, 60);
    cutline << Point(54, 58);
    cutline << Point(53, 56);
    cutline << Point(60, 50);
    cutline << Point(60, 20);
    cutline << Point(30, 20);
    cutline << Point(20, 30);
    cutline << Point(30, 40);
    cutline << Point(40, 30);

    Polygon polygon;
    polygon = cutline.close();

    for (int i = 0; i < polygon.length(); i++)
    {
        QVERIFY(polygon[i].x() == cutline[i].x());
        QVERIFY(polygon[i].y() == cutline[i].y());
    }
}

void PolylineTest::testRotation()
{
    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point(45, 65);
    cutline << Point(50, 70);
    cutline << Point(60, 60);
    cutline << Point(54, 58);
    cutline << Point(53, 56);
    cutline << Point(60, 50);
    cutline << Point(60, 20);
    cutline << Point(30, 20);
    cutline << Point(20, 30);
    cutline << Point(30, 40);
    cutline << Point(40, 30);

    Polyline rotated_polyline;
    Polygon polygon;

    rotated_polyline = cutline.rotate(90 * deg);
    polygon = cutline.close();
    polygon = polygon.rotate(90 * deg);

    for (int i = 0; i < polygon.size(); i++)
    {
        QVERIFY(polygon[i].x() == rotated_polyline[i].x());
        QVERIFY(polygon[i].y() == rotated_polyline[i].y());
    }
}

void PolylineTest::testPolylinePlusPolyline()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    Polyline polyline_two;
    polyline_two << Point(38, 28);
    polyline_two << Point(36, 26);

    Polyline polyline_three;
    polyline_three = polyline_one + polyline_two;

    QVERIFY(polyline_three[12].x() == 38);
    QVERIFY(polyline_three[12].y() == 28);
    QVERIFY(polyline_three[13].x() == 36);
    QVERIFY(polyline_three[13].y() == 26);
}

void PolylineTest::testPolylinePlusPoint()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    Point point (30, 30);

    Polyline polyline_two;
    polyline_two = polyline_one + point;

    QVERIFY(polyline_two[12].x() == 30);
    QVERIFY(polyline_two[12].y() == 30);
}

void PolylineTest::testPolylinePlusEqualsPolyline()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    Polyline polyline_two;
    polyline_two << Point(39, 29);
    polyline_two << Point(38, 28);

    polyline_one += polyline_two;

    QVERIFY(polyline_one[12].x() == 39);
    QVERIFY(polyline_one[12].y() == 29);
    QVERIFY(polyline_one[13].x() == 38);
    QVERIFY(polyline_one[13].y() == 28);
}

void PolylineTest::testPolylinePlusEqualsPoint()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    Point point (30, 30);
    polyline_one += point;

    QVERIFY(polyline_one[12].x() == point.x());
    QVERIFY(polyline_one[12].y() == point.y());
}

void PolylineTest::testPolylineMinusPolygon()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    QVector<Point> path;
    path.append(Point(44, 62));
    path.append(Point(60, 62));
    path.append(Point(60, 75));
    path.append(Point(44, 75));
    Polygon polygon (path);

    QVector<Polyline> vector_polyline;
    vector_polyline = polyline_one - polygon;

    QVERIFY(vector_polyline.size() == 2);
    QVERIFY(vector_polyline[0][0].x() == 45);
    QVERIFY(vector_polyline[0][1].x() == 45);
    QVERIFY(vector_polyline[0][0].y() == 62);
    QVERIFY(vector_polyline[0][1].y() == 25);

    QVERIFY(vector_polyline[1][0].x() == 40);
    QVERIFY(vector_polyline[1][1].x() == 30);
    QVERIFY(vector_polyline[1][2].x() == 20);
    QVERIFY(vector_polyline[1][3].x() == 30);
    QVERIFY(vector_polyline[1][4].x() == 60);
    QVERIFY(vector_polyline[1][5].x() == 60);
    QVERIFY(vector_polyline[1][6].x() == 53);
    QVERIFY(vector_polyline[1][7].x() == 54);
    QVERIFY(vector_polyline[1][8].x() == 60);
    QVERIFY(vector_polyline[1][9].x() == 58);
    QVERIFY(vector_polyline[1][0].y() == 30);
    QVERIFY(vector_polyline[1][1].y() == 40);
    QVERIFY(vector_polyline[1][2].y() == 30);
    QVERIFY(vector_polyline[1][3].y() == 20);
    QVERIFY(vector_polyline[1][4].y() == 20);
    QVERIFY(vector_polyline[1][5].y() == 50);
    QVERIFY(vector_polyline[1][6].y() == 56);
    QVERIFY(vector_polyline[1][7].y() == 58);
    QVERIFY(vector_polyline[1][8].y() == 60);
    QVERIFY(vector_polyline[1][9].y() == 62);
}

void PolylineTest::testPolylineMinusPolygonList()
{
    Polyline polyline_one;
    polyline_one << Point(40, 30);
    polyline_one << Point(30, 40);
    polyline_one << Point(20, 30);
    polyline_one << Point(30, 20);
    polyline_one << Point(60, 20);
    polyline_one << Point(60, 50);
    polyline_one << Point(53, 56);
    polyline_one << Point(54, 58);
    polyline_one << Point(60, 60);
    polyline_one << Point(50, 70);
    polyline_one << Point(45, 65);
    polyline_one << Point(45, 25);

    QVector<Point> path;
    path.append(Point(44, 62));
    path.append(Point(60, 62));
    path.append(Point(60, 75));
    path.append(Point(44, 75));
    Polygon polygon (path);

    QVector<Point> path_two;
    path_two.append(Point(56, 44));
    path_two.append(Point(56, 36));
    path_two.append(Point(64, 36));
    path_two.append(Point(64, 44));
    Polygon polygon_two (path_two);

    QVector<Point> path_three;
    path_three.append(Point(20, 40));
    path_three.append(Point(30, 30));
    path_three.append(Point(40, 40));
    path_three.append(Point(30, 50));
    Polygon polygon_three (path_three);

    PolygonList list_polygon;
    list_polygon = polygon + polygon_two + polygon_three;

    QVector<Polyline> vector_polyline;
    vector_polyline = polyline_one - list_polygon;

    QVERIFY(vector_polyline.size() == 4);
    QVERIFY(vector_polyline[0][0].x() == 45);
    QVERIFY(vector_polyline[0][1].x() == 45);
    QVERIFY(vector_polyline[0][0].y() == 62);
    QVERIFY(vector_polyline[0][1].y() == 25);

    QVERIFY(vector_polyline[1][0].x() == 60);
    QVERIFY(vector_polyline[1][1].x() == 60);
    QVERIFY(vector_polyline[1][2].x() == 53);
    QVERIFY(vector_polyline[1][3].x() == 54);
    QVERIFY(vector_polyline[1][4].x() == 60);
    QVERIFY(vector_polyline[1][5].x() == 58);
    QVERIFY(vector_polyline[1][0].y() == 44);
    QVERIFY(vector_polyline[1][1].y() == 50);
    QVERIFY(vector_polyline[1][2].y() == 56);
    QVERIFY(vector_polyline[1][3].y() == 58);
    QVERIFY(vector_polyline[1][4].y() == 60);
    QVERIFY(vector_polyline[1][5].y() == 62);

    QVERIFY(vector_polyline[2][0].x() == 25);
    QVERIFY(vector_polyline[2][1].x() == 20);
    QVERIFY(vector_polyline[2][2].x() == 30);
    QVERIFY(vector_polyline[2][3].x() == 60);
    QVERIFY(vector_polyline[2][4].x() == 60);
    QVERIFY(vector_polyline[2][0].y() == 35);
    QVERIFY(vector_polyline[2][1].y() == 30);
    QVERIFY(vector_polyline[2][2].y() == 20);
    QVERIFY(vector_polyline[2][3].y() == 20);
    QVERIFY(vector_polyline[2][4].y() == 36);

    QVERIFY(vector_polyline[3][0].x() == 40);
    QVERIFY(vector_polyline[3][1].x() == 35);
    QVERIFY(vector_polyline[3][0].y() == 30);
    QVERIFY(vector_polyline[3][1].y() == 35);
}
