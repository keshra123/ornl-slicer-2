#ifndef TST_GCODEPARSER_H
#define TST_GCODEPARSER_H

//! \file tst_gcodeparser.h

#include <QtTest/QtTest>

#include "auto_test.h"
#include "gcode/gcode_parser.h"

#if 0 // TODO fix these

using namespace ORNL;
using namespace AutoTest;
    /*!
     * \brief The TestGCodeParser class tests the parsing of whole gcode files
     *        and automatic gcode syntax deduction through reading of header files.
     */
    class TestGCodeParser : public QObject
    {
    public:
        Q_OBJECT
        GcodeParser* test;
    private:
    private slots:

        //! \brief Runs once for this test class.
        void initTestCase();
        //! \brief Runs once all test cases have been completed
        void cleanupTestCase();

        //! \brief Runs before each test case to setup the correct testing environment for each individual test case.
        void init();
        //! \brief Runs before each test case to prevent spilling over of data between cases
        void cleanup();

        void testParseLine();
        void testParseFile();
        void testAutoSyntaxSelection();
        void testParserSelection();
    };

// DECLARE_TEST(TestGCodeParser)

#endif // PARSERTESTS_H

#endif
