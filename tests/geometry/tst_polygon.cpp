#include "tst_polygon.h"
#include "geometry/polygon.h"

using namespace ORNL;

void PolygonTest::testClosestPointTo()
{
    /*
     * If you give a point that lies equal distance from any other two or more points
     * then closestPointTo seems to always return either the largest or smallest
     * possible point in the polygon.
     * Example: If we have a polygon at the points (-20, -20), (20, -20), (20, 20)
     * (-20, 20) and we do closestPointTo(Point(0,0)) then it will return (-20, -20)
     */

    QVector<Point> path;
    path.append(Point(-20, -20));
    path.append(Point(20, -20));
    path.append(Point(20, 20));
    path.append(Point(-20, 20));
    Polygon polygon (path);

    Point point (2, -2);
    Point closest = polygon.closestPointTo(point);

    QVERIFY(closest.x() == 20);
    QVERIFY(closest.y() == -20);
}

void PolygonTest::testCenterOfMass()
{
    QVector<Point> path;
    path.append(Point(-20, -20));
    path.append(Point(20, -20));
    path.append(Point(20, 20));
    path.append(Point(-20, 20));
    Polygon polygon (path);

    Point point = polygon.centerOfMass();

    QVERIFY(point.x() == 0);
    QVERIFY(point.y() == 0);

    QVector<Point> path_2;
    path_2.append(Point(30, 30));
    path_2.append(Point(60, 30));
    path_2.append(Point(60, 60));
    path_2.append(Point(30, 60));

    Polygon polygon_2 (path_2);

    QVERIFY(point.x() == 0);
    QVERIFY(point.y() == 0);
}

void PolygonTest::testArea()
{
    QVector<Point> path;
    path.append(Point(-20, -20));
    path.append(Point(20, -20));
    path.append(Point(20, 20));
    path.append(Point(-20, 20));
    Polygon polygon (path);

    Area area = polygon.area();

    QVERIFY(area() == 1600);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_2 (path_2);

    area = polygon_2.area();

    QVERIFY(area() == -100);
}

void PolygonTest::testInside()
{
    QVector<Point> path;
    path.append(Point(-20, -20));
    path.append(Point(20, -20));
    path.append(Point(20, 20));
    path.append(Point(-20, 20));
    Polygon polygon (path);

    QVERIFY(polygon.inside(Point(19, 19)) == true);
    QVERIFY(polygon.inside(Point(20, 20)) == false);
    QVERIFY(polygon.inside(Point(-2, 2)) == true);
}

void PolygonTest::testShorterThan()
{
    QVector<Point> path;
    path.append(Point(30, 30));
    path.append(Point(60, 30));
    path.append(Point(60, 60));
    path.append(Point(30, 60));

    Polygon polygon (path);

    QVERIFY(polygon.shorterThan(20) == false);
    QVERIFY(polygon.shorterThan(120) == false);
    QVERIFY(polygon.shorterThan(121) == true);
}

void PolygonTest::testRotate()
{
    QVector<Point> path;
    path.append(Point(30, 30));
    path.append(Point(40, 40));
    path.append(Point(30, 50));
    path.append(Point(20, 40));

    Polygon polygon (path);

    Polygon rotated_polygon = polygon.rotate(90 * deg);

    QVERIFY(rotated_polygon[0].x() == -30);
    QVERIFY(rotated_polygon[1].x() == -40);
    QVERIFY(rotated_polygon[2].x() == -50);
    QVERIFY(rotated_polygon[3].x() == -40);
    QVERIFY(rotated_polygon[0].y() == 30);
    QVERIFY(rotated_polygon[1].y() == 40);
    QVERIFY(rotated_polygon[2].y() == 30);
    QVERIFY(rotated_polygon[3].y() == 20);
}

void PolygonTest::testCenter()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));

    Polygon polygon (path_1);
    Point point = polygon.center();
    QVERIFY(point.x() == 45);
    QVERIFY(point.y() == 45);

    QVector<Point> path_2;
    path_2.append(Point(-20, -20));
    path_2.append(Point(20, -20));
    path_2.append(Point(20, 20));
    path_2.append(Point(-20, 20));
    Polygon polygon_2 (path_2);

    Point point_2 = polygon_2.center();
    QVERIFY(point_2.x() == 0);
    QVERIFY(point_2.y() == 0);
}

void PolygonTest::testMin()
{
    QVector<Point> path;
    path.append(Point(30, 30));
    path.append(Point(40, 40));
    path.append(Point(30, 50));
    path.append(Point(20, 40));

    Polygon polygon (path);
    Point point = polygon.min();

    QVERIFY(point.x() == 20);
    QVERIFY(point.y() == 30);

    Polygon rotated_polygon = polygon.rotate(90 * deg);
    Point point_2 = rotated_polygon.min();

    QVERIFY(point_2.x() == -50);
    QVERIFY(point_2.y() == 20);
}

void PolygonTest::testPolygonLength()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));

    Polygon polygon_1 (path_1);
    int64_t result = polygon_1.polygonLength();
    QVERIFY(result == 120);
}

void PolygonTest::testOffset()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));

    Polygon polygon_1 (path_1);

    PolygonList polygon_list;

    //the points the funciton gives out are not in the order originally inserted, but the polygon's
    //shape remains the same, so technically it works
    polygon_list = polygon_1.offset(5);

    QVERIFY(polygon_list[0][0].x() == 65);
    QVERIFY(polygon_list[0][1].x() == 25);
    QVERIFY(polygon_list[0][2].x() == 25);
    QVERIFY(polygon_list[0][3].x() == 65);
    QVERIFY(polygon_list[0][0].y() == 65);
    QVERIFY(polygon_list[0][1].y() == 65);
    QVERIFY(polygon_list[0][2].y() == 25);
    QVERIFY(polygon_list[0][3].y() == 25);

    PolygonList polygon_list_2;
    polygon_list_2 = polygon_1.offset(-5);

    QVERIFY(polygon_list_2[0][0].x() == 55);
    QVERIFY(polygon_list_2[0][1].x() == 35);
    QVERIFY(polygon_list_2[0][2].x() == 35);
    QVERIFY(polygon_list_2[0][3].x() == 55);
    QVERIFY(polygon_list_2[0][0].y() == 55);
    QVERIFY(polygon_list_2[0][1].y() == 55);
    QVERIFY(polygon_list_2[0][2].y() == 35);
    QVERIFY(polygon_list_2[0][3].y() == 35);
}

void PolygonTest::testOrientation()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    //path_1.append(Point(30, 30));

    Polygon polygon_1 (path_1);

    QVERIFY2(polygon_1.orientation() == true, "polygon_1 is counterclockwise but the function says it isn't");

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    //path_2.append(Point(35, 40));

    Polygon polygon_2 (path_2);

    QVERIFY2(polygon_2.orientation() == false, "polygon_2 is clockwise but the function says it isn't");
}

void PolygonTest::testPolygonPlusPolygonList()
{
    QVector<Point> path_1;
    path_1.append(Point(10, 10));
    path_1.append(Point(70, 10));
    path_1.append(Point(70, 70));
    path_1.append(Point(10, 70));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(20, 30));
    path_2.append(Point(20, 40));
    path_2.append(Point(30, 40));
    path_2.append(Point(30, 30));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(50, 30));
    path_3.append(Point(50, 40));
    path_3.append(Point(60, 40));
    path_3.append(Point(60, 30));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two;
    polygon_list = polygon_three + polygon_list;

    QVERIFY(polygon_list[1][0].x() == polygon_three[0].x());
    QVERIFY(polygon_list[1][1].x() == polygon_three[1].x());
    QVERIFY(polygon_list[1][2].x() == polygon_three[2].x());
    QVERIFY(polygon_list[1][3].x() == polygon_three[3].x());
    QVERIFY(polygon_list[1][0].y() == polygon_three[0].y());
    QVERIFY(polygon_list[1][1].y() == polygon_three[1].y());
    QVERIFY(polygon_list[1][2].y() == polygon_three[2].y());
    QVERIFY(polygon_list[1][3].y() == polygon_three[3].y());

    QVector<Point> path_4;
    path_4.append(Point(30, 30));
    path_4.append(Point(60, 30));
    path_4.append(Point(60, 60));
    path_4.append(Point(30, 60));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(35, 40));
    path_5.append(Point(35, 50));
    path_5.append(Point(45, 50));
    path_5.append(Point(45, 40));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(36, 41));
    path_6.append(Point(44, 41));
    path_6.append(Point(44, 49));
    path_6.append(Point(36, 49));
    Polygon polygon_six (path_6);

    PolygonList polygon_list_2;
    polygon_list_2 = polygon_four + polygon_five;
    polygon_list_2 = polygon_six + polygon_list_2;

    QVERIFY(polygon_list_2[0] == polygon_four);
    QVERIFY(polygon_list_2[1] == polygon_five);
    QVERIFY(polygon_list_2[2] == polygon_six);
}

void PolygonTest::testPolygonPlusPolygon()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    PolygonList polygon_list = polygon_one + polygon_two;

    QVERIFY(polygon_list[1][0].x() == polygon_two[0].x());
    QVERIFY(polygon_list[1][1].x() == polygon_two[1].x());
    QVERIFY(polygon_list[1][2].x() == polygon_two[2].x());
    QVERIFY(polygon_list[1][3].x() == polygon_two[3].x());
    QVERIFY(polygon_list[1][0].y() == polygon_two[0].y());
    QVERIFY(polygon_list[1][1].y() == polygon_two[1].y());
    QVERIFY(polygon_list[1][2].y() == polygon_two[2].y());
    QVERIFY(polygon_list[1][3].y() == polygon_two[3].y());
}

void PolygonTest::testPolygonMinusPolygonList()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two;
    polygon_list = polygon_one - polygon_list;

    /*So after the subtraction of polygon_one only polygon_two should be left.
     * However polygon_two was an inner polygon which means it was clockwise.
     * Now that its an outer polygon, it should rotate counterclockwise
     * Which is why polygon_list[0] != polygon_two
     */
    QVERIFY(polygon_list[0] != polygon_two);

}

void PolygonTest::testPolygonMinusPolygon()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(50, 30));
    path_2.append(Point(60, 30));
    path_2.append(Point(60, 60));
    path_2.append(Point(50, 60));
    Polygon polygon_two (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_one - polygon_two;

    QVERIFY(polygon_list[0][0].x() == 50);
    QVERIFY(polygon_list[0][1].x() == 30);
    QVERIFY(polygon_list[0][2].x() == 30);
    QVERIFY(polygon_list[0][3].x() == 50);
    QVERIFY(polygon_list[0][0].y() == 60);
    QVERIFY(polygon_list[0][1].y() == 60);
    QVERIFY(polygon_list[0][2].y() == 30);
    QVERIFY(polygon_list[0][3].y() == 30);
}

void PolygonTest::testPolygonUnionPolygonList()
{
    QVector<Point> path_one;
    path_one.append(Point(30, 30));
    path_one.append(Point(60, 30));
    path_one.append(Point(60, 60));
    path_one.append(Point(30, 60));
    Polygon polygon_one (path_one);

    QVector<Point> path_two;
    path_two.append(Point(45, 65));
    path_two.append(Point(40, 25));
    path_two.append(Point(50, 25));
    Polygon polygon_two (path_two);

    QVector<Point> path_three;
    path_three.append(Point(60, 30));
    path_three.append(Point(70, 30));
    path_three.append(Point(70, 60));
    path_three.append(Point(60, 60));
    Polygon polygon_three (path_three);

    PolygonList polygon_list;
    polygon_list = polygon_one | polygon_two;
    polygon_list = polygon_three | polygon_list;

    QVERIFY(polygon_list[0][0].x() == 45);
    QVERIFY(polygon_list[0][1].x() == 44);
    QVERIFY(polygon_list[0][2].x() == 30);
    QVERIFY(polygon_list[0][3].x() == 30);
    QVERIFY(polygon_list[0][4].x() == 41);
    QVERIFY(polygon_list[0][5].x() == 40);
    QVERIFY(polygon_list[0][6].x() == 50);
    QVERIFY(polygon_list[0][7].x() == 49);
    QVERIFY(polygon_list[0][8].x() == 70);
    QVERIFY(polygon_list[0][9].x() == 70);
    QVERIFY(polygon_list[0][10].x() == 46);
    QVERIFY(polygon_list[0][0].y() == 65);
    QVERIFY(polygon_list[0][1].y() == 60);
    QVERIFY(polygon_list[0][2].y() == 60);
    QVERIFY(polygon_list[0][3].y() == 30);
    QVERIFY(polygon_list[0][4].y() == 30);
    QVERIFY(polygon_list[0][5].y() == 25);
    QVERIFY(polygon_list[0][6].y() == 25);
    QVERIFY(polygon_list[0][7].y() == 30);
    QVERIFY(polygon_list[0][8].y() == 30);
    QVERIFY(polygon_list[0][9].y() == 60);
    QVERIFY(polygon_list[0][10].y() == 60);
}

void PolygonTest::testPolygonIntersectPolygonList()
{
    QVector<Point> path_one;
    path_one.append(Point(30, 30));
    path_one.append(Point(60, 30));
    path_one.append(Point(60, 60));
    path_one.append(Point(30, 60));
    Polygon polygon_one (path_one);

    QVector<Point> path_two;
    path_two.append(Point(45, 65));
    path_two.append(Point(40, 25));
    path_two.append(Point(50, 25));
    Polygon polygon_two (path_two);

    QVector<Point> path_three;
    path_three.append(Point(50, 30));
    path_three.append(Point(70, 30));
    path_three.append(Point(70, 60));
    path_three.append(Point(50, 60));
    Polygon polygon_three (path_three);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two;
    polygon_list = polygon_three & polygon_list;

    QVERIFY(polygon_list[0][0].x() == 60);
    QVERIFY(polygon_list[0][1].x() == 60);
    QVERIFY(polygon_list[0][2].x() == 50);
    QVERIFY(polygon_list[0][3].x() == 50);
    QVERIFY(polygon_list[0][0].y() == 30);
    QVERIFY(polygon_list[0][1].y() == 60);
    QVERIFY(polygon_list[0][2].y() == 60);
    QVERIFY(polygon_list[0][3].y() == 30);
}

void PolygonTest::testPolygonIntersectPolyline()
{
    QVector<Point> path_one;
    path_one.append(Point(30, 30));
    path_one.append(Point(60, 30));
    path_one.append(Point(60, 60));
    path_one.append(Point(30, 60));
    Polygon polygon_one (path_one);

    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point(45, 65);

    QVector<Polyline> polyline_vector;
    polyline_vector = polygon_one & cutline;

    QVERIFY(polyline_vector[0][0].x() == 45);
    QVERIFY(polyline_vector[0][1].x() == 45);
    QVERIFY(polyline_vector[0][0].y() == 30);
    QVERIFY(polyline_vector[0][1].y() == 60);
}

void PolygonTest::testPolygonExclusiveOrPolygon()
{
    QVector<Point> path_1;
    path_1.append(Point(20, 10));
    path_1.append(Point(50, 30));
    path_1.append(Point(80, 10));
    path_1.append(Point(70, 40));
    path_1.append(Point(90, 60));
    path_1.append(Point(60, 60));
    path_1.append(Point(50, 90));
    path_1.append(Point(40, 60));
    path_1.append(Point(10, 60));
    path_1.append(Point(30, 40));
    //path_1.append(Point(20, 10));
    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(28, 62));
    path_2.append(Point(28, 13));
    path_2.append(Point(72, 13));
    path_2.append(Point(72, 62));
    //path_2.append(Point(28, 62));
    Polygon polygon_2 (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_1 ^ polygon_2;

    QVERIFY(polygon_list[0][0].x() == 72);
    QVERIFY(polygon_list[0][1].x() == 72);
    QVERIFY(polygon_list[0][2].x() == 90);
    QVERIFY(polygon_list[0][3].x() == 72);
    QVERIFY(polygon_list[0][4].x() == 72);
    QVERIFY(polygon_list[0][5].x() == 59);
    QVERIFY(polygon_list[0][6].x() == 50);
    QVERIFY(polygon_list[0][7].x() == 41);
    QVERIFY(polygon_list[0][8].x() == 59);
    QVERIFY(polygon_list[0][9].x() == 60);
    QVERIFY(polygon_list[0][10].x() == 72);
    QVERIFY(polygon_list[0][11].x() == 72);
    QVERIFY(polygon_list[0][12].x() == 70);
    QVERIFY(polygon_list[0][13].x() == 72);
    QVERIFY(polygon_list[0][14].x() == 72);
    QVERIFY(polygon_list[0][15].x() == 80);
    QVERIFY(polygon_list[0][0].y() == 34);
    QVERIFY(polygon_list[0][1].y() == 42);
    QVERIFY(polygon_list[0][2].y() == 60);
    QVERIFY(polygon_list[0][3].y() == 60);
    QVERIFY(polygon_list[0][4].y() == 62);
    QVERIFY(polygon_list[0][5].y() == 62);
    QVERIFY(polygon_list[0][6].y() == 90);
    QVERIFY(polygon_list[0][7].y() == 62);
    QVERIFY(polygon_list[0][8].y() == 62);
    QVERIFY(polygon_list[0][9].y() == 60);
    QVERIFY(polygon_list[0][10].y() == 60);
    QVERIFY(polygon_list[0][11].y() == 42);
    QVERIFY(polygon_list[0][12].y() == 40);
    QVERIFY(polygon_list[0][13].y() == 34);
    QVERIFY(polygon_list[0][14].y() == 15);
    QVERIFY(polygon_list[0][15].y() == 10);

    QVERIFY(polygon_list[1][0].x() == 28);
    QVERIFY(polygon_list[1][1].x() == 28);
    QVERIFY(polygon_list[1][2].x() == 30);
    QVERIFY(polygon_list[1][3].x() == 28);
    QVERIFY(polygon_list[1][4].x() == 28);
    QVERIFY(polygon_list[1][5].x() == 40);
    QVERIFY(polygon_list[1][6].x() == 41);
    QVERIFY(polygon_list[1][7].x() == 28);
    QVERIFY(polygon_list[1][8].x() == 28);
    QVERIFY(polygon_list[1][9].x() == 10);
    QVERIFY(polygon_list[1][10].x() == 28);
    QVERIFY(polygon_list[1][11].x() == 28);
    QVERIFY(polygon_list[1][12].x() == 20);
    QVERIFY(polygon_list[1][0].y() == 15);
    QVERIFY(polygon_list[1][1].y() == 34);
    QVERIFY(polygon_list[1][2].y() == 40);
    QVERIFY(polygon_list[1][3].y() == 42);
    QVERIFY(polygon_list[1][4].y() == 60);
    QVERIFY(polygon_list[1][5].y() == 60);
    QVERIFY(polygon_list[1][6].y() == 62);
    QVERIFY(polygon_list[1][7].y() == 62);
    QVERIFY(polygon_list[1][8].y() == 60);
    QVERIFY(polygon_list[1][9].y() == 60);
    QVERIFY(polygon_list[1][10].y() == 42);
    QVERIFY(polygon_list[1][11].y() == 34);
    QVERIFY(polygon_list[1][12].y() == 10);

    QVERIFY(polygon_list[2][0].x() == 72);
    QVERIFY(polygon_list[2][1].x() == 50);
    QVERIFY(polygon_list[2][2].x() == 28);
    QVERIFY(polygon_list[2][3].x() == 28);
    QVERIFY(polygon_list[2][4].x() == 72);
    QVERIFY(polygon_list[2][0].y() == 15);
    QVERIFY(polygon_list[2][1].y() == 30);
    QVERIFY(polygon_list[2][2].y() == 15);
    QVERIFY(polygon_list[2][3].y() == 13);
    QVERIFY(polygon_list[2][4].y() == 13);
}

void PolygonTest::testPolygonEqualsPolygon()
{
    /*
     * Polygon 1 and Polygon 3 are exactly the same.
     * Polygon 2 is completely different from Polygons 1, 3, and 4
     * Polygon 4 and polygon 1 are the same except they start at different points
     * Polygon 5 and polygon 1 are same except polyogn 5 is clockwise and polygon 1 is counter clockwise
     */
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    //path_1.append(Point(30, 30));

    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    //path_2.append(Point(35, 40));

    Polygon polygon_2 (path_2);

    bool same_poly = polygon_1 == polygon_2;
    QVERIFY2(same_poly == false, "1. The algorithm thinks the two polygons are equal, even though they aren't");

    QVector<Point> path_3;
    path_3.append(Point(30, 30));
    path_3.append(Point(60, 30));
    path_3.append(Point(60, 60));
    path_3.append(Point(30, 60));

    Polygon polygon_3 (path_3);

    same_poly = polygon_1 == polygon_3;

    QVERIFY2(same_poly == true, "2. The algorithm thinks the two polygons are not equal, even though they are");

    QVector<Point> path_4;
    path_4.append(Point(60, 30));
    path_4.append(Point(60, 60));
    path_4.append(Point(30, 60));
    path_4.append(Point(30, 30));

    Polygon polygon_4 (path_4);

    same_poly = polygon_1 == polygon_4;
    QVERIFY2(same_poly == true, "3. The algorithm thinks the two polygons are not equal, even though they are");

    QVector<Point> path_5;
    path_5.append(Point(30, 30));
    path_5.append(Point(30, 60));
    path_5.append(Point(60, 60));
    path_5.append(Point(60, 30));

    Polygon polygon_5 (path_5);

    same_poly = polygon_1 == polygon_5;
    QVERIFY2(same_poly == false, "4. The algorithm thinks the two polygons are equal, even though they aren't");
}

void PolygonTest::testPolygonNotEqualPolygon()
{
    /*
     * Polygon 1 and Polygon 3 are exactly the same.
     * Polygon 2 is completely different from Polygons 1, 3, and 4
     * Polygon 4 and polygon 1 are the same except they start at different points
     * Polygon 5 and polygon 1 are same except polyogn 5 is clockwise and polygon 1 is counter clockwise
     */
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    //path_1.append(Point(30, 30));

    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    //path_2.append(Point(35, 40));

    Polygon polygon_2 (path_2);

    bool same_poly = polygon_1 != polygon_2;
    QVERIFY2(same_poly == true, "1. The algorithm thinks the two polygons are equal, even though they aren't");

    QVector<Point> path_3;
    path_3.append(Point(30, 30));
    path_3.append(Point(60, 30));
    path_3.append(Point(60, 60));
    path_3.append(Point(30, 60));

    Polygon polygon_3 (path_3);

    same_poly = polygon_1 != polygon_3;
    QVERIFY2(same_poly == false, "2. The algorithm thinks the two polygons are equal, even though they aren't");

    QVector<Point> path_4;
    path_4.append(Point(60, 30));
    path_4.append(Point(60, 60));
    path_4.append(Point(30, 60));
    path_4.append(Point(30, 30));

    Polygon polygon_4 (path_4);

    same_poly = polygon_1 != polygon_4;
    QVERIFY2(same_poly == false, "3. The algorithm thinks the two polygons are equal, even though they arne't");

    QVector<Point> path_5;
    path_5.append(Point(30, 30));
    path_5.append(Point(30, 60));
    path_5.append(Point(60, 60));
    path_5.append(Point(60, 30));

    Polygon polygon_5 (path_5);

    same_poly = polygon_1 != polygon_5;
    QVERIFY2(same_poly == true, "4. The algorithm thinks the two polygons are equal, even though they aren't");
}
