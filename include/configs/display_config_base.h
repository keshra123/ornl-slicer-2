#ifndef DISPLAYCONFIGBASE_H
#define DISPLAYCONFIGBASE_H
#include "settings_base.h"

namespace ORNL
{
    /*!
     * \class DisplayConfigBase
     * \brief Base class for configs that are displayed in the settings window
     */
    class DisplayConfigBase : public SettingsBase
    {
    public:
        //! \brief Constructor
        DisplayConfigBase(QString name,
                          QString settings_type,
                          SettingsBase* parent,
                          QString folder = "");

        QString getName();
        QString getExtension();
        QString getPath();

        //! \brief Save this config to a file
        bool saveToFile(QString folder = "");

        //! \brief Load this config from a file
        bool loadFromFile(QString folder = "");

        //! \brief Delete the file for this config
        bool deleteFile();

        //! \brief Copy this config's SettingBase to a new config
        QSharedPointer< DisplayConfigBase > copy(QString name);

    private:
        QString m_name;       //!< Name for this config
        QString m_extension;  //!< Extension for the filename for this config
        QString m_path;       //!< Filepath for this config
    };
}  // namespace ORNL

#endif  // DISPLAYCONFIGBASE_H
