#include "widgets/settings_category_widgets/material/materialpropertiescategorywidget.h"

#include "ui_material_properties_category_widget.h"

namespace ORNL
{
    MaterialPropertiesCategoryWidget::MaterialPropertiesCategoryWidget()
        : ui(new Ui::MaterialPropertiesCategoryWidget)
    {
        ui->setupUi(this);

        initializeSettings();
    }

    MaterialPropertiesCategoryWidget::~MaterialPropertiesCategoryWidget()
    {
        delete ui;
    }

    void MaterialPropertiesCategoryWidget::handleConditions()
    {
        // None of these settings are affected by other settings
    }
}  // namespace ORNL
