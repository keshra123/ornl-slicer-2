#include <QApplication>
#include <QDir>
#include <QFile>
#include <QSurfaceFormat>
#include <QTextStream>

#include "managers/window_manager.h"
#include "threading/slicing_thread.h"
#include "utilities/qt_json_conversion.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);


    Q_INIT_RESOURCE(icons);
    Q_INIT_RESOURCE(shaders);
    Q_INIT_RESOURCE(style);

    // Set OpenGL Version information
    // Note: This format must be set before show() is called.
    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setVersion(4, 5);
    QSurfaceFormat::setDefaultFormat(format);

    QFile f(":qdarkstyle/style.qss");
    if (!f.exists())
    {
        printf("Unable to set stylesheet, file not found\n");
    }
    else
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    // Start the slicing thread in the background
    QSharedPointer< ORNL::SlicingThread > slicing_thread =
        ORNL::SlicingThread::getInstance();
    slicing_thread->start();

    // Create the main window
    QSharedPointer< ORNL::WindowManager > window_manager =
        ORNL::WindowManager::getInstance();
    window_manager->createMainWindow();

    return app.exec();
}
