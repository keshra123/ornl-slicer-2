#include "widgets/meshviewcontroller.h"

#include <QWidget>
#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QPointLight>

#include "graphics/meshgraphics.h"
//#include "src/graphics/cameracontroller.h"

namespace ORNL
{
    MeshViewController::MeshViewController()
        : m_window(new Qt3DExtras::Qt3DWindow)
        , m_root_entity(new Qt3DCore::QEntity)
    {
        // Camera setup
        m_camera = m_window->camera();
        m_camera->lens()->setPerspectiveProjection(
            45.0f, 16.0f / 9.0f, 0.1f, 1000.0f);
        m_camera->setPosition(QVector3D(0, 0, 40));
        m_camera->setUpVector(QVector3D(0, 1, 0));
        m_camera->setViewCenter(QVector3D(0, 0, 0));

        // Light setup
        m_light_entity = new Qt3DCore::QEntity(m_root_entity);
        m_point_light  = new Qt3DRender::QPointLight(m_light_entity);
        m_point_light->setColor("white");
        m_point_light->setIntensity(1.0);
        m_light_entity->addComponent(m_point_light);
        m_light_transform = new Qt3DCore::QTransform(m_light_entity);
        m_light_transform->setTranslation(m_camera->position());
        m_light_entity->addComponent(m_light_transform);

        Qt3DExtras::QOrbitCameraController* camController =
            new Qt3DExtras::QOrbitCameraController(m_root_entity);

        camController->setCamera(m_camera);

        // Window setup

        Qt3DRender::QPickingSettings* settings =
            m_window->renderSettings()->pickingSettings();
        settings->setPickMethod(Qt3DRender::QPickingSettings::TrianglePicking);

        m_window->setRootEntity(m_root_entity);
        m_window->defaultFrameGraph()->setClearColor(QColor(0, 1, 1));
        m_widget = QWidget::createWindowContainer(m_window);
    }

    MeshViewController::~MeshViewController()
    {}

    QWidget* MeshViewController::widget()
    {
        return m_widget;
    }

    void MeshViewController::addToWindow(MeshGraphics* mesh_graphic)
    {
        mesh_graphic->setParentEntity(m_root_entity);
    }
}  // namespace ORNL
