#include "threading/project_loader.h"

#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    ProjectLoader::ProjectLoader(QObject* parent, const QString& filename)
        : QThread(parent)
        , m_filename(filename)
        , m_project_manager(ProjectManager::getInstance())
        , m_settings_manager(GlobalSettingsManager::getInstance())
    {
        // Nothing to do here
    }

    void ProjectLoader::run()
    {
        // FIXME
        /*
        QFile file(m_filename);
        file.open(QFile::ReadOnly);
        json j = json::parse(file.readAll().toStdString());

        m_project_manager->closeProject();
        m_project_manager->name(j["name"]);

        QStringList split_filename = m_filename.split("/");
        QString filepath;
        for(int i = 0; i < split_filename.size() - 1 ; i++)
        {
            filepath += split_filename[i] + QDir::separator();
        }

        m_project_manager->filepath(filepath);
        m_project_manager->currentMachineName(j["machine_settings"]);
        m_project_manager->currentMaterialName(j["material_settings"]);
        m_project_manager->currentPrintName(j["print_settings"]);

        for(json model_json: j["meshes"])
        {
            Mesh* model = import_mesh(filepath, model_json);
            model->json(model_json);
            if(!model)
            {
                m_project_manager->closeProject();
                return;
            }
            m_project_manager->addModel(model);
        }
        */
    }

    /*
    Model* ProjectLoader::import_model(QString filepath, json model_json)
    {
        Assimp::Importer importer;

        filepath = filepath + model_json["filename"].get<QString>();

        const aiScene* scene = importer.ReadFile(filepath.toStdString(),
                    aiProcess_GenSmoothNormals | // GenSmoothNormals generates
    normals if they are not already in the model aiProcess_CalcTangentSpace | //
    CalcTangentSpace calculates tangent space, only necessary if doing normal
    mapping aiProcess_Triangulate | // Triangulate splits up primitives with
    more than three vertices to triangles aiProcess_JoinIdenticalVertices | //
    JoinIdenticalVertices joins identical vertex data, improves performance with
    indexed drawing aiProcess_SortByPType
                    );
        if (!scene)
        {
            qDebug() << "Error loading file: (assimp:) " <<
    importer.GetErrorString(); emit errorLoadingFile(m_filename,
    importer.GetErrorString()); return nullptr;
        }

        if (scene->HasMeshes())
        {
            Model* model = new Model(model_json["filename"],
    m_settings_manager->getGlobal());

            for (uint i = 0; i < scene->mNumMeshes; i++)
            {
                Mesh* mesh = process_mesh(model, scene->mMeshes[i]);
                model->addMesh(mesh);
            }
            return model;
        }
        else
        {
            qDebug() << "Error: No meshes found";
            emit errorLoadingFile(m_filename, "Error: No meshes found");
            return nullptr;
        }
    }

    Mesh* ProjectLoader::process_mesh(Model* model, aiMesh *m)
    {
        Mesh* mesh = new Mesh(model);

        // Get Vertices
        if (m->mNumVertices > 0)
        {
            for (uint i = 0; i < m->mNumVertices; i++)
            {
                aiVector3D &vec = m->mVertices[i];
                mesh->addVertex(vec.x, vec.y, vec.z);
            }
        }

        // Get Normals
        if (m->HasNormals())
        {
            for (uint i = 0; i < m->mNumVertices; i++)
            {
                aiVector3D &vec = m->mNormals[i];
                mesh->addNormal(vec.x, vec.y, vec.z);
             };
         }

        // Get mesh indexes
        for (uint i = 0; i < m->mNumFaces; i++)
        {
            aiFace* face = &m->mFaces[i];
            if (face->mNumIndices != 3)
            {
                qDebug() << "Warning: Mesh face with not exactly 3 indices,
    ignoring this primitive."; continue;
            }
            for(uint j = 0; j < 3; j++)
            {
                mesh->addIndex(face->mIndices[j]);
            }
        }

        mesh->meshFinished();

        return mesh;
    }
    */
}  // namespace ORNL
