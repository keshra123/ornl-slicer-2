#ifndef TST_POLYLINE_H
#define TST_POLYLINE_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

/*!
 *  \class PolylineTest
 *
 *  \brief A class that tests the functions of the Polyline class
 */

class PolylineTest : public QObject
{
    Q_OBJECT

private slots:

    //! \brief Test the shorterThan() function
    void testShorterThan();

    //! \brief Test the closestPointTo() function
    void testClosestPointTo();

    //! \brief Test the simply() function
    void testClose();

    //! \brief Test the rotate() function
    void testRotation();

    //! \brief Test Polyline + Polyline operator
    void testPolylinePlusPolyline();

    //! \brief Test Polyline + Point operator
    void testPolylinePlusPoint();

    //! \brief Test Polyline += Polyline operator
    void testPolylinePlusEqualsPolyline();

    //! \brief Test Polyline += Point operator
    void testPolylinePlusEqualsPoint();

    //! \brief Test Polyline - Polygon
    void testPolylineMinusPolygon();

    //! \brief Test Polyline - PolygonList
    void testPolylineMinusPolygonList();
};

DECLARE_TEST(PolylineTest)

#endif //TST_POLYGON_H
