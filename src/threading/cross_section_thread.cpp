#include "threading/cross_section_thread.h"

#include "exceptions/exceptions.h"
#include "geometry/mesh.h"
#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"
#include "layer_regions/layer.h"
#include "managers/window_manager.h"
#include "threading/cross_section/cross_section.h"
#include "threading/cross_section/cross_section_segment.h"
#include "utilities/constants.h"

namespace ORNL
{
    CrossSectionThread::CrossSectionThread(QSharedPointer< Mesh > mesh)
        : m_mesh(mesh)
        , m_window_manager(WindowManager::getInstance())
    {
        connect(this,
                SIGNAL(error(QString)),
                m_window_manager.data(),
                SLOT(createErrorDialog(QString)));
    }

    void CrossSectionThread::run()
    {
        qDebug() << "Slicer starting";

        m_vertices = m_mesh->vertices();
        m_faces    = m_mesh->faces();

        qDebug() << "Mesh faces and vertices created";

        horizontalCrossSection();

        qDebug() << "Slicing done";
    }

    void CrossSectionThread::horizontalCrossSection()
    {
        qDebug() << "Normal Slicing Starting";

        QMap< uint, Distance > layer_to_height;

        if (!m_mesh->size())
        {
            try
            {
                m_mesh->updateLayers();
            }
            catch (ZeroLayerHeightException e)
            {
                emit error("No layer height set");
                return;
            }
        }

        for (int m = 0; m < m_faces.size(); m++)
        {
            const MeshFace& face = m_faces[m];
            const MeshVertex& v0 = m_vertices[face.vertex_index[0]];
            const MeshVertex& v1 = m_vertices[face.vertex_index[1]];
            const MeshVertex& v2 = m_vertices[face.vertex_index[2]];

            Point p0 = v0.location;
            Point p1 = v1.location;
            Point p2 = v2.location;

            Distance min_z = qMin(qMin(p0.z(), p1.z()), p2.z());
            Distance max_z = qMax(qMax(p0.z(), p1.z()), p2.z());

            Distance z = 0;
            for (int layer_nr = 0; layer_nr < (*m_mesh).size(); layer_nr++)
            {
                if (layer_to_height.contains(layer_nr))
                {
                    z = layer_to_height[layer_nr];
                }
                else
                {
                    m_layers.append(CrossSection(m_mesh));
                    z += (*m_mesh)[layer_nr]->setting< Distance >(
                        Constants::NozzleSettings::Layer::kLayerHeight);
                    layer_to_height[layer_nr] = z;
                }

                if (z < min_z)
                {
                    continue;
                }

                if (z > max_z)
                {
                    break;
                }

                CrossSectionSegment s;
                s.end_vertex     = nullptr;
                int end_edge_idx = -1;
                if (p0.z() < z && p1.z() >= z && p2.z() >= z)
                {
                    // p1   p2
                    // --------
                    //   p0
                    s            = project2D(p0, p2, p1, z);
                    end_edge_idx = 0;
                    if (p1.z() == z)
                    {
                        s.end_vertex = &v1;
                    }
                }
                else if (p0.z() > z && p1.z() < z && p2.z() < z)
                {
                    //   p0
                    // --------
                    // p1  p2
                    s            = project2D(p0, p1, p2, z);
                    end_edge_idx = 2;
                }
                else if (p1.z() < z && p0.z() >= z && p2.z() >= z)
                {
                    // p0   p2
                    // --------
                    //   p1
                    s            = project2D(p1, p0, p2, z);
                    end_edge_idx = 1;
                    if (p2.z() == z)
                    {
                        s.end_vertex = &v2;
                    }
                }
                else if (p1.z() > z && p0.z() < z && p2.z() < z)
                {
                    //   p1
                    // --------
                    // p0  p2
                    s            = project2D(p1, p2, p0, z);
                    end_edge_idx = 0;
                }
                else if (p2.z() < z && p1.z() >= z && p0.z() >= z)
                {
                    // p1   p0
                    // --------
                    //   p2
                    s            = project2D(p2, p1, p0, z);
                    end_edge_idx = 2;
                    if (p0.z() == z)
                    {
                        s.end_vertex = &v0;
                    }
                }
                else if (p2.z() > z && p1.z() < z && p0.z() < z)
                {
                    //   p2
                    // --------
                    // p1  p0
                    s            = project2D(p2, p0, p1, z);
                    end_edge_idx = 1;
                }
                else
                {
                    // Not all cases create a segment, because a point of a face
                    // could create just a dot, and two touching faces
                    //  on the slice would create two segments
                    continue;
                }

                m_layers[layer_nr].insertFaceToSegment(
                    m, m_layers[layer_nr].segments().size());
                s.face_index         = m;
                s.end_other_face_idx = face.connected_face_index[end_edge_idx];
                s.added_to_polygon   = false;
                m_layers[layer_nr].addSegment(s);
            }
        }

        qDebug() << "Segments gathered";

        for (int layer_nr = 0; layer_nr < m_layers.size(); layer_nr++)
        {
            PolygonList& polygons = m_layers[layer_nr].makePolygons();
            (*m_mesh)[layer_nr]->setOutlines(polygons);
        }

        qDebug() << "Outlines set";
    }

    double CrossSectionThread::interpolate(double x,
                                           double x0,
                                           double x1,
                                           double y0,
                                           double y1)
    {
        double dx_01 = x1 - x0;
        double num   = (y1 - y0) * (x - x0);
        double y     = y0 + num / dx_01;
        return y;
    }

    CrossSectionSegment CrossSectionThread::project2D(const Point& p0,
                                                      const Point& p1,
                                                      const Point& p2,
                                                      Distance z)
    {
        CrossSectionSegment seg;
        seg.start.x(interpolate(z(), p0.z(), p1.z(), p0.x(), p1.x()));
        seg.start.y(interpolate(z(), p0.z(), p1.z(), p0.y(), p1.y()));
        seg.end.x(interpolate(z(), p0.z(), p2.z(), p0.x(), p2.x()));
        seg.end.y(interpolate(z(), p0.z(), p2.z(), p0.y(), p2.y()));

        return seg;
    }
}  // namespace ORNL
