#ifndef TST_POLYGON_H
#define TST_POLYGON_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

/*!
 *  \class PolygonTest
 *
 *  \brief A class that tests the functions of the Polygon class
 */

class PolygonTest : public QObject
{
    Q_OBJECT

private slots:

    //! \brief Test to see if the closestPointTo function works
    void testClosestPointTo();

    //! \brief Test to see if the centerOfMass function works
    void testCenterOfMass();

    //! \brief Test to see if area() function works
    void testArea();

    //! \brief Test to see if the inside() function works
    void testInside();

    //! \brief Test to see if the shorterThan() function works
    void testShorterThan();

    //! \brief Test to see if the rotate() function works
    void testRotate();

    //! \brief Test to see if the min() function works
    void testMin();

    //! \brief Test to see if the center() function works
    void testCenter();

    //! \brief Test to see if the polygonLength() function works properly
    void testPolygonLength();

    //! \brief Test to see if the offset() function works properly
    void testOffset();

    //! \brief Test to see if the orientation() function works properly
    void testOrientation();

    //! \brief Test to see if the add operator for Polygon + PolygonList works
    void testPolygonPlusPolygonList();

    //! \brief Test to see if the add operator for Polygon + Polygon works
    void testPolygonPlusPolygon();

    //! \brief Test to see if the subtract operator for Polygon - PolygonList works
    void testPolygonMinusPolygonList();

    //! \brief Test to see if the subtract operator for Polygon - Polygon works
    void testPolygonMinusPolygon();

    //! \brief Test to see if the union operator for Polygon union PolygonList works
    void testPolygonUnionPolygonList();

    //! \brief Test to see if the intersection operator for Polygon intersection PolygonList works
    void testPolygonIntersectPolygonList();

    //! \brief Test to see if the intersection operator for Polygon intersection Polyline works
    void testPolygonIntersectPolyline();

    //! \brief Test to see if the "exclusive or" operator for Polygon "exclusive or" Polygon works
    void testPolygonExclusiveOrPolygon();

    //! \brief Test to see if two polygons are equal
    void testPolygonEqualsPolygon();

    //! \brief Test to see if two polygons are not equal
    void testPolygonNotEqualPolygon();

};

DECLARE_TEST(PolygonTest)

#endif //TST_POLYGON_H
