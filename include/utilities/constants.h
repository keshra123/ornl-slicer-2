#ifndef CONSTANTS_H
#define CONSTANTS_H

//! \file constants.h

#include <QColor>
#include <QVector3D>
#include <QVector>
#include <string>

#include "units/unit.h"

namespace ORNL
{
    /*!
     * \class Constants
     * \brief Class that holds all static constants
     */
    class Constants
    {
    public:
        static QString VERSION;

        /*!
         * \class Units
         * \brief Units strings used for preferences
         */
        class Units
        {
        public:
            static const QString kInch;
            static const QString kInchPerSec;
            static const QString kInchPerSec2;
            static const QString kInchPerSec3;

            static const QString kFeet;
            static const QString kFeetPerSec;
            static const QString kFeetPerSec2;
            static const QString kFeetPerSec3;

            static const QString kMm;
            static const QString kMmPerSec;
            static const QString kMmPerSec2;
            static const QString kMmPerSec3;

            static const QString kCm;
            static const QString kCmPerSec;
            static const QString kCmPerSec2;
            static const QString kCmPerSec3;

            static const QString kMicron;
            static const QString kMicronPerSec;
            static const QString kMicronPerSec2;
            static const QString kMicronPerSec3;

            static const QString kDegree;
            static const QString kRadian;
            static const QString kRevolution;

            static const QString kSecond;
            static const QString kMillisecond;
            static const QString kMinute;

            static const QString kKg;
            static const QString kG;
            static const QString kMg;

            static const QStringList kDistanceUnits;
            static const QStringList kVelocityUnits;
            static const QStringList kAccelerationUnits;
            static const QStringList kJerkUnits;
            static const QStringList kAngleUnits;
            static const QStringList kTimeUnits;
        };

        class RegionTypeStrings
        {
        public:
            static const QString kUnknown;
            static const QString kPerimeter;
            static const QString kInset;
            static const QString kInfill;
            static const QString kTopSkin;
            static const QString kBottomSkin;
            static const QString kSkin;
            static const QString kSupport;
            static const QString kSupportRoof;
            static const QString kTravel;
        };

        class LegacyRegionTypeStrings
        {
        public:
            static const QString kThing;
        };

        class PathModifierStrings
        {
        public:
            static const QString kPrestart;
            static const QString kInitialStartup;
            static const QString kSlowDown;
            static const QString kTipWipe;
            static const QString kCoasting;
            static const QString kSpiralLift;
        };

        /*!
         * \class MachineSettings
         * \brief Keys for machine settings
         */
        class MachineSettings
        {
        public:
            // Categories
            static const QString kDimensionsCategory;
            static const QString kSyntaxCategory;
            static const QStringList kCategories;

            /*!
             * \class Dimensions
             *
             * \brief Keys for machine dimensions
             */
            class Dimensions
            {
            public:
                static const QString kXMin;
                static const QString kXMax;
                static const QString kYMin;
                static const QString kYMax;
                static const QString kZMin;
                static const QString kZMax;
                static const QString kEnableW;
                static const QString kWMin;
                static const QString kWMax;
                static const QString kLayerChangeAxis;
                static const QString kZOffset;
                static const QString kEnableDoffing;
                static const QString kDoffingHeight;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the machine dimensions
                 */
                class Labels
                {
                public:
                    static const QString kXMin;
                    static const QString kXMax;
                    static const QString kYMin;
                    static const QString kYMax;
                    static const QString kZMin;
                    static const QString kZMax;
                    static const QString kEnableW;
                    static const QString kWMin;
                    static const QString kWMax;
                    static const QString kLayerChangeAxis;
                    static const QString kZOffset;
                    static const QString kEnableDoffing;
                    static const QString kDoffingHeight;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the machine dimensions
                 */
                class Tooltips
                {
                public:
                    static const QString kXMin;
                    static const QString kXMax;
                    static const QString kYMin;
                    static const QString kYMax;
                    static const QString kZMin;
                    static const QString kZMax;
                    static const QString kEnableW;
                    static const QString kWMin;
                    static const QString kWMax;
                    static const QString kLayerChangeAxis;
                    static const QString kZOffset;
                    static const QString kEnableDoffing;
                    static const QString kDoffingHeight;
                };
            };


            /*!
             * \class Syntax
             *
             * \brief Keys for machine syntax
             */
            class Syntax
            {
            public:
                static const QString kMachineSyntax;

                static QString kCincinnati;
                static QString kCincinnatiLegacy;
                static QString kBlueGantry;
                static QString kWolf;
                static QString kMach3;
                static QString kIngersoll;
                static QStringList kSyntaxOptions;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the machine syntax
                 */
                class Labels
                {
                public:
                    static const QString kMachineSyntax;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the machine syntax
                 */
                class Tooltips
                {
                public:
                    static const QString kMachineSyntax;
                };
            };

            /*!
             *  \class BlueGantry
             *
             *  \brief Constant Machine settings for the BlueGantry printer
             */
            class BlueGantry
            {
            public:
                /*!
                 *  \class Units
                 *
                 *  \brief The Defualt units for the BlueGantry Printer.
                 */
                class Units
                {
                public:
                    static const Distance kDistanceUnit;
                    static const Time kTimeUnit;
                    static const Angle kAngleUnit;
                    static const Mass kMassUnit;
                    static const Velocity kVelocityUnit;
                    static const Acceleration kAccelerationUnit;
                };
            };

            /*!
             *  \class Cincinnati
             *
             *  \brief Constant Machine settings for the Cincinnati printer(s)
             */
            class Cincinnati
            {
            public:
                /*!
                 *  \class Units
                 *
                 *  \brief The Defualt units for the Cincinnati Printer(s).
                 */
                class Units
                {
                public:
                    static const Distance kDistanceUnit;
                    static const Time kTimeUnit;
                    static const Angle kAngleUnit;
                    static const Mass kMassUnit;
                    static const Velocity kVelocityUnit;
                    static const Acceleration kAccelerationUnit;
                };
            };

            /*!
             *  \class Wolf
             *
             *  \brief Constant Machine settings for the Wolf printer
             */
            class Wolf
            {
            public:
                /*!
                 *  \class Units
                 *
                 *  \brief The Defualt units for the Wolf Printer.
                 */
                class Units
                {
                public:
                    static const Distance kDistanceUnit;
                    static const Time kTimeUnit;
                    static const Angle kAngleUnit;
                    static const Mass kMassUnit;
                    static const Velocity kVelocityUnit;
                    static const Acceleration kAccelerationUnit;
                };
            };

            /*!
             *  \class Mach3
             *
             *  \brief Constant Machine settings for the Mach3 printer
             */
            class Mach3
            {
            public:
                /*!
                 *  \class Units
                 *
                 *  \brief The Defualt units for the Mach3 Printer.
                 */
                class Units
                {
                public:
                    static const Distance kDistanceUnit;
                    static const Time kTimeUnit;
                    static const Angle kAngleUnit;
                    static const Mass kMassUnit;
                    static const Velocity kVelocityUnit;
                    static const Acceleration kAccelerationUnit;
                };
            };


            /*!
             *  \class Ingersoll
             *
             *  \brief Constant Machine settings for the Ingersoll printer(s)
             */
            class Ingersoll
            {
            public:
                /*!
                 *  \class Units
                 *
                 *  \brief The Defualt units for the Ingersoll Printer(s).
                 */
                class Units
                {
                public:
                    static const Distance kDistanceUnit;
                    static const Time kTimeUnit;
                    static const Angle kAngleUnit;
                    static const Mass kMassUnit;
                    static const Velocity kVelocityUnit;
                    static const Acceleration kAccelerationUnit;
                };
            };
        };


        /*!
         * \class MaterialSettings
         * \brief Keys for material settings
         */
        class MaterialSettings
        {
        public:
            // Categories
            static const QString kPropertiesCategory;
            static const QString kSpeedsCategory;
            static const QString kAccelerationCategory;
            static const QStringList kCategories;

            // Properties
            class Properties
            {
            public:
                /*!
                 * \class Labels
                 *
                 * \brief Labels for the material properties
                 */
                class Labels
                {
                public:
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the material properties
                 */
                class Tooltips
                {
                public:
                };
            };

            /*!
             * \class Speed
             *
             * \brief Settings for various speeds
             */
            class Speed
            {
            public:
                static const QString kDefault;
                static const QString kPerimeter;
                static const QString kInset;
                static const QString kInfill;
                static const QString kSkin;
                static const QString kTipWipe;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the speeds
                 */
                class Labels
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kInfill;
                    static const QString kSkin;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the speeds
                 */
                class Tooltips
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kInfill;
                    static const QString kSkin;
                };
            };

            /*!
             * \class Acceleration
             *
             * \brief Settings for various acceleration
             */
            class Acceleration
            {
            public:
                static const QString kDefault;
                static const QString kPerimeter;
                static const QString kInset;
                static const QString kInfill;
                static const QString kSkin;
                static const QString kTipWipe;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the speeds
                 */
                class Labels
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kInfill;
                    static const QString kSkin;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the speeds
                 */
                class Tooltips
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kInfill;
                    static const QString kSkin;
                };
            };
        };

        /*!
         * \class NozzleSettings
         * \brief Keys for nozzle settings
         */
        class NozzleSettings
        {
        public:
            // Categories
            static const QString kLayerCategory;
            static const QString kBeadWidthCategory;
            static const QString kSpeedCategory;
            static const QString kAccelerationCategory;
            static const QStringList kCategories;

            /*!
             * \class Layer
             *
             * \brief Keys for layer settings
             */
            class Layer
            {
            public:
                static const QString kLayerHeight;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for layer settings
                 */
                class Labels
                {
                public:
                    static const QString kLayerHeight;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for layer settings
                 */
                class Tooltips
                {
                public:
                    static const QString kLayerHeight;
                };
            };

            /*!
             * \class BeadWidth
             *
             * \brief Keys for the bead width settings
             */
            class BeadWidth
            {
            public:
                static const QString kDefault;
                static const QString kPerimeter;
                static const QString kInset;
                static const QString kSkin;
                static const QString kInfill;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the bead width settings
                 */
                class Labels
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the bead width settings
                 */
                class Tooltips
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };
            };

            /*!
             * \class Speed
             *
             * \brief Keys for the speed settings
             */
            class Speed
            {
            public:
                static const QString kDefault;
                static const QString kPerimeter;
                static const QString kInset;
                static const QString kSkin;
                static const QString kInfill;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the speed settings
                 */
                class Labels
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the speed settings
                 */
                class Tooltips
                {
                public:
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };
            };

            /*!
             * \class Acceleration
             *
             * \brief Keys for the acceleration settings
             */
            class Acceleration
            {
            public:
                static const QString kEnable;
                static const QString kDefault;
                static const QString kPerimeter;
                static const QString kInset;
                static const QString kSkin;
                static const QString kInfill;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the acceleration settings
                 */
                class Labels
                {
                public:
                    static const QString kEnable;
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the acceleration settings
                 */
                class Tooltips
                {
                public:
                    static const QString kEnable;
                    static const QString kDefault;
                    static const QString kPerimeter;
                    static const QString kInset;
                    static const QString kSkin;
                    static const QString kInfill;
                };
            };
        };


        /*!
         * \class PrintSettings
         * \brief Keys for print settings
         */
        class PrintSettings
        {
        public:
            // Categories
            static const QString kPerimetersCategory;
            static const QString kInsetsCategory;
            static const QString kInfillCategory;
            static const QString kSkinsCategory;
            static const QString kSupportCategory;
            static const QString kFixModelCategory;
            static const QStringList kCategories;

            /*!
             * \class Perimeters
             *
             * \brief Keys for the perimeter settings
             */
            class Perimeters
            {
            public:
                static const QString kEnable;
                static const QString kExtruderId;
                static const QString kNumber;
                static const QString kExternalFirst;
                static const QString kEnableSkeletons;
                static const QString kEnableVariableWidthSkeletons;
                static const QString kOnlyRetractWhenCrossing;
                static const QString kSmallLength;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the perimeter settings
                 */
                class Labels
                {
                public:
                    static const QString kEnable;
                    static const QString kExtruderId;
                    static const QString kNumber;
                    static const QString kExternalFirst;
                    static const QString kEnableSkeletons;
                    static const QString kEnableVariableWidthSkeletons;
                    static const QString kOnlyRetractWhenCrossing;
                    static const QString kSmallLength;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the perimeter settings
                 */
                class Tooltips
                {
                public:
                    static const QString kEnable;
                    static const QString kExtruderId;
                    static const QString kNumber;
                    static const QString kExternalFirst;
                    static const QString kEnableSkeletons;
                    static const QString kEnableVariableWidthSkeletons;
                    static const QString kOnlyRetractWhenCrossing;
                    static const QString kSmallLength;
                };
            };

            /*!
             * \class Insets
             *
             * \brief Keys for the inset settings
             */
            class Insets
            {
            public:
                static const QString kEnable;
                static const QString kExtruderId;
                static const QString kNumber;
                static const QString kExternalFirst;
                static const QString kEnableSkeletons;
                static const QString kEnableVariableWidthSkeletons;
                static const QString kOnlyRetractWhenCrossing;
                static const QString kSmallLength;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the inset settings
                 */
                class Labels
                {
                public:
                    static const QString kEnable;
                    static const QString kExtruderId;
                    static const QString kNumber;
                    static const QString kExternalFirst;
                    static const QString kEnableSkeletons;
                    static const QString kEnableVariableWidthSkeletons;
                    static const QString kOnlyRetractWhenCrossing;
                    static const QString kSmallLength;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the inset settings
                 */
                class Tooltips
                {
                public:
                    static const QString kEnable;
                    static const QString kExtruderId;
                    static const QString kNumber;
                    static const QString kExternalFirst;
                    static const QString kEnableSkeletons;
                    static const QString kEnableVariableWidthSkeletons;
                    static const QString kOnlyRetractWhenCrossing;
                    static const QString kSmallLength;
                };
            };

            /*!
             * \class Infill
             *
             * \brief Keys for infill settings
             */
            class Infill
            {
            public:
                static const QString kUse;
                static const QString kPattern;
                static const QString kLineSpacing;
                static const QString
                    kDensity;  //!< Percentage of space filled by infill
                static const QString kOverlap;
                static const QString kInitialAngle;
                static const QString kRotationAngle;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the infill settings
                 */
                class Labels
                {
                public:
                    static const QString kUse;
                    static const QString kPattern;
                    static const QString kLineSpacing;
                    static const QString kDensity;
                    static const QString kOverlap;
                    static const QString kInitialAngle;
                    static const QString kRotationAngle;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the infill settings
                 */
                class Tooltips
                {
                public:
                    static const QString kUse;
                    static const QString kPattern;
                    static const QString kLineSpacing;
                    static const QString kDensity;
                    static const QString kOverlap;
                    static const QString kInitialAngle;
                    static const QString kRotationAngle;
                };
            };


            /*!
             * \class Skin
             *
             * \brief Keys for skin settings
             */
            class Skin
            {
            public:
                static const QString kUse;
                static const QString kLayersBelow;
                static const QString kLayersAbove;
                static const QString kPattern;
                static const QString kLineOverlap;
                static const QString kOverlap;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the skin settings
                 */
                class Labels
                {
                public:
                    static const QString kUse;
                    static const QString kLayersBelow;
                    static const QString kLayersAbove;
                    static const QString kPattern;
                    static const QString kLineOverlap;
                    static const QString kOverlap;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the skin settings
                 */
                class Tooltips
                {
                public:
                    static const QString kUse;
                    static const QString kLayersBelow;
                    static const QString kLayersAbove;
                    static const QString kPattern;
                    static const QString kLineOverlap;
                    static const QString kOverlap;
                };
            };

            /*!
             * \class FixModel
             *
             * \brief Keys for model fixing settings
             */
            class FixModel
            {
            public:
                static const QString kStitch;
                static const QString kExtensiveStitch;

                /*!
                 * \class Labels
                 *
                 * \brief Labels for the model fixing settings
                 */
                class Labels
                {
                public:
                    static const QString kStitch;
                    static const QString kExtensiveStitch;
                };

                /*!
                 * \class Tooltips
                 *
                 * \brief Tooltips for the model fixing settings
                 */
                class Tooltips
                {
                public:
                    static const QString kStitch;
                    static const QString kExtensiveStitch;
                };
            };
        };

        class Colors
        {
        public:
            static const QVector3D kYellow;

            static const QVector3D kRed;
            static const QVector3D kBlue;
            static const QVector3D kGreen;
            static const QVector3D kPurple;
            static const QVector3D kOrange;
            static const QVector3D kWhite;
            static const QVector3D kBlack;
            static const QVector< QVector3D > kModelColors;

            static const QColor kPerimeter;
            static const QColor kInset;
            static const QColor kSkin;
            static const QColor kInfill;
            static const QColor kTravel;
            static const QColor kSupport;
            static const QColor kSupportRoof;
            static const QColor kPrestart;
            static const QColor kInitialStartup;
            static const QColor kSlowDown;
            static const QColor kTipWipe;
            static const QColor kCoasting;
            static const QColor kSpiralLift;
            static const QColor kUnknown;
        };

        /*!
         * \class OpenGL
         * \brief The constants for the OpenGL widgets
         */
        class OpenGL
        {
        public:
            static const QVector3D kUp;
            static const double kRotationSpeed;
            static const double kRotationIncrement;
            static const double kTranslationSpeed;
            static const double kTranslationIncrement;
            static const double kZoomSpeed;
            static const double kZoomIncrement;

            static const double kFov;
            static const double kNearPlane;
            static const double kFarPlane;

            static const char* kCameraToView;
            static const char* kWorldToCamera;
            static const char* kMeshToWorld;
            static const char* kColor;
        };
    };
}  // namespace ORNL
#endif  // CONSTANTS_H
