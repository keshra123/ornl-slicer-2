var class_polygons =
[
    [ "add", "class_polygons.html#a78fdb39aa3c0c5c19fc9ad8236b19255", null ],
    [ "add", "class_polygons.html#a490c098669d94bd781f1d75d7d378534", null ],
    [ "convexHull", "class_polygons.html#a2c157080236e892c1e60755aaef9c3bb", null ],
    [ "findInside", "class_polygons.html#abf6e4b45ad307868abe2090aa2d30a16", null ],
    [ "inside", "class_polygons.html#a520fe22f5bcadffcedc86f657ca4c8ab", null ],
    [ "intersected", "class_polygons.html#af697b613e650ff42328eb132f1f99145", null ],
    [ "offset", "class_polygons.html#a5d1434839e3f71222c0232f94e7a880b", null ],
    [ "pointCount", "class_polygons.html#afb0e62ca1051d4b25ba4bb7dd8bd269a", null ],
    [ "smooth", "class_polygons.html#abd559147895dcbd156cff8d2e71681ef", null ],
    [ "subtracted", "class_polygons.html#ad6b8fc39c35a421d81fd3c1a303ce313", null ],
    [ "united", "class_polygons.html#a506ce533cbe1b3ae6d43366568f8d921", null ],
    [ "xored", "class_polygons.html#a97e4f991df796e2718ab32cdee05d042", null ]
];