#ifndef TEMPLATESETTINGITEM_H
#define TEMPLATESETTINGITEM_H

#include "templatecheckboxsettingitem.h"
#include "templatecountsettingitem.h"
#include "templatedropdownsettingitem.h"
#include "templatepercentsettingitem.h"
#include "templatestringsettingitem.h"
#include "templateunitssettingitem.h"

#endif  // TEMPLATESETTINGITEM_H
