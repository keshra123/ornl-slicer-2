#ifndef TEMPLATEUNITSSETTINGITEM_H
#define TEMPLATEUNITSSETTINGITEM_H

#include <QDoubleValidator>
#include <QWidget>

#include "exceptions/exceptions.h"

namespace Ui
{
    class TemplateUnitsSettingItem;
}

namespace ORNL
{
    class TemplateUnitsSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit TemplateUnitsSettingItem(QWidget* parent = 0);
        ~TemplateUnitsSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        //! \brief Change the unit text
        void unit(QString text);

        //! \brief Returns the value from the textbox
        double value();

        //! \brief Sets the stylesheet for the checkbox
        void setStyleSheet(const QString& stylesheet);

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        void valueChanged(double b);

    public slots:

        void value(double v);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    private slots:
        void handleUndo();
        void handleRedo();

        void handleValueChanged();

    private:
        Ui::TemplateUnitsSettingItem* ui;
    };
}  // namespace ORNL


#endif  // TEMPLATEUNITSSETTINGITEM_H
