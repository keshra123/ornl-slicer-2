#include <QTest>

#include "auto_test.h"

int main(int argc, char** argv)
{
    return AutoTest::run(argc, argv);
}
