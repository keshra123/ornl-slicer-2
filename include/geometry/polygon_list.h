#ifndef POLYGONLIST_H
#define POLYGONLIST_H

//! \file polygonlist.h

#include <QMetaType>
#include <QVector>
#include <Qt>

#include "clipper.hpp"
#include "linearalg2d.h"
#include "point.h"
#include "polygon.h"

namespace ORNL
{
    class Polygon;
    class Polyline;

    const static int clipper_init = (0);
#define NO_INDEX (std::numeric_limits< uint >::max())

    /*!
     * \class PolygonList
     * \brief A class that contains multiple individual Polygon's.
     *
     * The class contains multiple individual Polygon and various
     * operations (including binary, e.g. union, intersection, difference, xor).
     * Based in part on [CuraEngine version by
     * Ultimaker](https://github.com/Ultimaker/CuraEngine/blob/master/src/utils/polygon.h)
     */
    class PolygonList : public QVector< Polygon >
    {
    public:
        PolygonList();

        //! The total number of points in all the individual polygons
        uint pointCount() const;

        /*!
         * \brief Offsets the each of polygons
         *
         * Offsets each of the Polygon by moving the vertices along their angle
         * bisectors. Individual Polygon's can merge if they overlap or seperate
         * into multiple Polygon's.
         */
        PolygonList offset(
            Distance distance,
            ClipperLib::JoinType joinType = ClipperLib::jtMiter) const;

        /*!
         * \brief checks if the point is inside of the polygon
         *
         * We do this by counting the number of polygons inside which this point
         * lies. An odd number is inside, while an even number is outside.
         *
         * Returns false if outside, true if inside; if the point lies exactly
         * on the border, will return \p border_result.
         */
        bool inside(Point p, bool border_result = false);

        /*!
         * \brief returns the index of polygon that contains \p p
         * We do this by tracing from the point towards the positive X
         * direction, every line we cross increments the crossings counter. If
         * we have an even number of crossings then we are not inside the
         * polygon. We then find the polygon with an uneven number of crossings
         * which is closest to \p p.
         *
         * If \p border_result, we return the first polygon which is exactly on
         * \p p.
         */
        uint findInside(Point p, bool border_result = false);

        /*!
         * \brief Implements monotone chain convex hull algorithm
         *
         * See <a
         * href="https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain">Monotone
         * Chain</a>
         */
        Polygon convexHull() const;

        /*!
         * \brief Smooth out small perpendicular segments
         *
         * Smoothing is performed by removing the inner most vertex of a line
         * segment smaller than \p remove_length which has an angle with the
         * next and previous line segment smaller than roughly 150*
         *
         * Note that in its current implementation this function doesn't remove
         * line segments with an angle smaller than 30* Such would be the case
         * for an N shape.
         */
        PolygonList smooth(Distance remove_length);

        /*!
         * \brief removes points connected to similarly oriented lines
         */
        PolygonList simplify(Distance smallest_line_segment  = 2 * in,
                             Distance allowed_error_distance = 1 * in);

        /*!
         * Remove all but the polygons on the very outside.
         * Exclude holes and parts within holes.
         * \return the resulting polygons.
         */
        PolygonList getOutsidePolygons() const;

        /*!
         * Exclude holes which have no parts inside of them.
         * \return the resulting polygons.
         */
        PolygonList removeEmptyHoles() const;

        /*!
         * Return hole polygons which have no parts inside of them.
         * \return the resulting polygons.
         */
        PolygonList getEmptyHoles() const;

    private:
        /*!
         * recursive part of \ref Polygons::removeEmptyHoles and \ref
         * Polygons::getEmptyHoles \param node The node of the polygons part to
         * process \param remove_holes Whether to remove empty holes or
         * everything but the empty holes \param ret Where to store polygons
         * which are not empty holes
         */
        void removeEmptyHoles_processPolyTreeNode(
            const ClipperLib::PolyNode& node,
            const bool remove_holes,
            PolygonList& ret) const;

    public:
        /*!
         * Split up the polygons into groups according to the even-odd rule.
         * Each PolygonsPart in the result has an outline as first polygon,
         * whereas the rest are holes.
         */
        QVector< PolygonList > splitIntoParts(bool unionAll = false) const;

    private:
        void splitIntoParts_processPolyTreeNode(
            ClipperLib::PolyNode* node,
            QVector< PolygonList >& ret) const;

    public:
        //! \brief Removes polygons with area smaller than \p minAreaSize.
        PolygonList removeSmallAreas(Area minAreaSize);

        //! \brief Removes overlapping consecutive line segments which don't
        //! delimit a positive area.
        PolygonList removeDegenerateVertices();

        //! \brief Returns a point containing the minimum x, y, and z values
        Point min();

        //! \brief Returns a point containing the maximum x, y, and z values
        Point max();

        //! \brief Rotates around the origin with the specific axis
        PolygonList rotate(Angle angle, QVector3D axis = {0, 0, 1});

        //! \brief Rotates around a specified point
        PolygonList rotateAround(Point center,
                                 Angle angle,
                                 QVector3D axis = {0, 0, 1});

        //! \brief Returns whether the two Polygons objects are equal as
        //! determined by each of the individual Polygon objects
        bool operator==(const PolygonList& other);

        //! \brief Returns whether the two Polygons objects are equal as
        //! determined by each of the individual Polygon objects
        bool operator!=(const PolygonList& other);

        //! \brief Equivalent to | (union)
        PolygonList operator+(const PolygonList& rhs);

        //! \brief Equivalent to | (union)
        PolygonList operator+(const Polygon& rhs);

        //! \brief Equivalent to |= (union equals)
        PolygonList operator+=(const PolygonList& rhs);

        //! \brief Equivalent to |= (union equals)
        PolygonList operator+=(const Polygon& rhs);

        //! \brief difference
        PolygonList operator-(const PolygonList& rhs);

        //! \brief difference
        PolygonList operator-(const Polygon& rhs);

        //! \brief difference
        PolygonList operator-=(const PolygonList& rhs);

        //! \brief difference
        PolygonList operator-=(const Polygon& rhs);

        //! \brief Equivalent to | (union)
        PolygonList operator<<(const PolygonList& rhs);

        //! \brief Equivalent to | (union)
        PolygonList operator<<(const Polygon& rhs);

        //! \brief union
        PolygonList operator|(const PolygonList& rhs);

        //! \brief union
        PolygonList operator|(const Polygon& rhs);

        //! \brief union equals
        PolygonList operator|=(const PolygonList& rhs);

        //! \brief union equals
        PolygonList operator|=(const Polygon& rhs);

        //! \brief intersection
        PolygonList operator&(const PolygonList& rhs);

        //! \brief intersection
        PolygonList operator&(const Polygon& rhs);

        //! \brief intersection
        QVector< Polyline > operator&(const Polyline& rhs);

        //! \brief intersection equals
        PolygonList operator&=(const PolygonList& rhs);

        //! \brief intersection equals
        PolygonList operator&=(const Polygon& rhs);

        //! \brief xor
        PolygonList operator^(const PolygonList& rhs);

        //! \brief xor
        PolygonList operator^(const Polygon& rhs);

        //! \brief xor equals
        PolygonList operator^=(const PolygonList& rhs);

        //! \brief xor equals
        PolygonList operator^=(const Polygon& rhs);

        friend class Polygon;
        friend class Polyline;

    private:
        //! \brief Private constructor for use with internal clipper functions
        PolygonList(const ClipperLib::Paths& paths);

        // Replaces the current polygons with whatever is in paths. Used with
        // internal clipper functions
        void clipperLoad(const ClipperLib::Paths& paths);

        //! \brief Private operator for use with internal clipper functions
        ClipperLib::Paths operator()() const;

        /*!
         * \brief Add an instance of Polygon with this this one.
         *        Any overlapping Polygon are merged into one.
         *        The result is returned.
         */
        PolygonList _add(const Polygon& rhs);

        /*!
         * \brief Merge another instance of Polygons with this one.
         *        Any overlapping Polygon are merged into one.
         *        The result is returned.
         */
        PolygonList _add(const PolygonList& rhs);

        /*!
         * \brief Add an instance of Polygon into this Polygons.
         *        Any overlapping Polygon are merged into one.
         */
        PolygonList _add_to_this(const Polygon& rhs);

        /*!
         * \brief Merge another instance of Polygons into this Polygons.
         *        Any overlapping Polygon are merged into one.
         */
        PolygonList _add_to_this(const PolygonList& rhs);

        /*!
         * \brief Remove any regions of this that overlap with other
         *        The result is returned.
         */
        PolygonList _subtract(const Polygon& rhs);

        /*!
         * \brief Remove any regions of the polygon that overlap with other's
         * Polygons The result is returned.
         */
        PolygonList _subtract(const PolygonList& rhs);

        /*!
         * \brief Remove any regions of this that overlap with other
         */
        PolygonList _subtract_from_this(const Polygon& rhs);

        /*!
         * \brief Remove any regions of this polygon that overlap with other's
         * Polygons
         */
        PolygonList _subtract_from_this(const PolygonList& rhs);

        //! \brief Remove any region of the Polygons that do not overlap (Result
        //! is returned)
        PolygonList _intersect(const Polygon& rhs);

        //! \brief Remove any region of the Polygons that do not overlap (Result
        //! is returned)
        PolygonList _intersect(const PolygonList& rhs);

        //! \brief Remove any part of the Polyline that does not overlap
        QVector< Polyline > _intersect(const Polyline& rhs);

        //! \brief Remove any region of this Polygons that do not overlap
        PolygonList _intersect_with_this(const Polygon& rhs);

        //! \brief Remove any region of this Polygons that do not overlap
        PolygonList _intersect_with_this(const PolygonList& rhs);

        //! \brief Remove any region of the Polygons that overlaps (Result is
        //! returned)
        PolygonList _xor(const PolygonList& rhs);

        //! \brief Remove any region of the Polygons that overlaps (Result is
        //! returned)
        PolygonList _xor(const Polygon& rhs);

        //! \brief Remove any region of the Polygons that overlaps
        PolygonList _xor_with_this(const PolygonList& rhs);

        //! \brief Remove any region of the Polygons that overlaps
        PolygonList _xor_with_this(const Polygon& rhs);

        // Make all functions that add an element private so that they funnel
        // through _add/_union
        using QVector< Polygon >::append;
        using QVector< Polygon >::prepend;
        using QVector< Polygon >::push_back;
        using QVector< Polygon >::push_front;
        using QVector< Polygon >::insert;
    };  // class PolygonList

}  // namespace ORNL
#endif  // POLYGONS_H
