#ifndef SETTINGSCONFIGWIDGETBASE_H
#define SETTINGSCONFIGWIDGETBASE_H

#include <QSharedPointer>
#include <QStringList>
#include <json.hpp>

#include "configs/settings_base.h"
#include "exceptions/exceptions.h"
#include "managers/preferences_manager.h"
#include "units/unit.h"
#include "utilities/mathutils.h"
#include "widgets/settings_category_widgets/template_setting_items/templatesettingitem.h"

using json = nlohmann::json;

class QSignalMapper;

namespace ORNL
{
    /*!
     * \class SettingsCategoryWidgetBase
     * \brief Partially abstract class used as the base of the settings category
     * widgets (Mainly so they can all be held in the same map and to force them
     * to have some common functions)
     */
    class SettingsCategoryWidgetBase : public QWidget
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        SettingsCategoryWidgetBase();

        //! \brief connects the settings to their respective textboxes,
        //! checkboxes, etc
        void initializeSettings();

        //! \brief connects the temp settings map in DisplayConfigWidget to this
        //! Widget
        void setSettings(QSharedPointer< SettingsBase > temp_settings,
                         QSharedPointer< json > undo_history,
                         QSharedPointer< json > redo_history);

        //! \brief virtual function that needs to be overwriten by a inheiriting
        //! class to handle conditionals
        virtual void handleConditions() = 0;

    private:
        template < typename T >
        void loadUnitSetting(
            QMap< QString, QSharedPointer< TemplateUnitsSettingItem > >& map,
            T& unit)
        {
            for (auto it = map.begin(); it != map.end(); it++)
            {
                // unsaved value for the setting
                if (m_temp_settings->contains(it.key(), false))
                {
                    if (m_global ||
                        (!m_active.contains(it.key()) || !m_active[it.key()]))
                    {
                        map[it.key()]->setStyleSheet(m_active_text);
                    }

                    // Active only if the setting comes from the current
                    // SettingsBase and not one of its ancestors
                    m_active[it.key()] = true;
                }
                // saved value for the setting or no value for setting
                else
                {
                    // If parent owned mark the row as not active
                    if (!m_global &&
                        (!m_active.contains(it.key()) || m_active[it.key()]))
                    {
                        map[it.key()]->setStyleSheet(m_inactive_text);
                    }

                    // Active only if the setting comes from the current
                    // SettingsBase and not one of its ancestors
                    m_active[it.key()] = false;
                }

                T setting = m_temp_settings->setting< T >(it.key());
                // If value changed then update everything
                if (MathUtils::notEquals(it.value()->value(), setting.to(unit)))
                {
                    // If the setting doesn't exist the value is 0
                    it.value()->value(setting.to(unit));
                }

                // Show or hide undo button based on whether it is possible
                if ((*m_undo_history)[it.key().toStdString()].size() ||
                    m_temp_settings->contains(it.key(), false))
                {
                    it.value()->showUndo();
                }
                else
                {
                    it.value()->hideUndo();
                }

                // Show or hide redo button based on whether it is possible
                if ((*m_redo_history)[it.key().toStdString()].size())
                {
                    it.value()->showRedo();
                }
                else
                {
                    it.value()->hideRedo();
                }
            }
        }

    public:
        //! \brief updates the textboxes, checkboxes, etc based on the current
        //! settings
        void load();

        //! \brief function that sets the units for this category widget based
        //! on preferences
        void setUnits();

        //! \brief Set if this is part of a GlobalConfigWidget or a
        //! LocalConfigWidget
        void setGlobal(bool global);

    signals:
        //! \brief Emits that something has been modified (value changed or undo
        //! pressed)
        void modified();

    private:
        template < typename T >
        void updateUnitSetting(QString& key,
                               QSharedPointer< TemplateUnitsSettingItem > tusi,
                               T& unit)
        {
            try
            {
                double current = tusi->value();

                T previous = m_temp_settings->setting< T >(key);

                if (MathUtils::equals(previous.to(unit), current))
                {
                    return;
                }

                if (m_temp_settings->contains(key, false))
                {
                    if (m_undo_history->find(key.toStdString()) ==
                        m_undo_history->end())
                    {
                        (*m_undo_history)[key.toStdString()] = json::array();
                    }

                    if (m_redo_history->find(key.toStdString()) ==
                        m_redo_history->end())
                    {
                        (*m_redo_history)[key.toStdString()] = json::array();
                    }

                    (*m_undo_history)[key.toStdString()].push_back(previous);
                    (*m_redo_history)[key.toStdString()].clear();
                }
                m_temp_settings->setSetting< T >(key, current * unit);
                m_active[key] = true;
                tusi->setStyleSheet(m_active_text);

                tusi->showUndo();
                tusi->hideRedo();

                // Run conditionals
                handleConditions();

                emit modified();
            }
            catch (SettingValueException e)
            {
                tusi->setStyleSheet(m_error_text);
            }
        }
    public slots:

        //! \brief Updates the value of a distance setting with the specified
        //! key in the settings base
        void updateDistanceSetting(QString key);

        //! \brief Updates the value of a velocity setting with the specified
        //! key in the settings base
        void updateVelocitySetting(QString key);

        //! \brief Updates the value of an acceleration setting with the
        //! specified key in the settings base
        void updateAccelerationSetting(QString key);

        //! \brief Updates the value of a time setting with the specified key in
        //! the settings base
        void updateTimeSetting(QString key);

        //! \brief Updates the value of an angle setting with the specified key
        //! in the settings base
        void updateAngleSetting(QString key);

        //! \brief Updates the value of a integer setting with the specified key
        //! in the settings base
        void updateCountSetting(QString key);

        //! \brief Updates the value of a percentage setting with the specified
        //! key in the settings base
        void updatePercentageSetting(QString key);

        //! \brief Updates the value of a boolean setting with the specified key
        //! in the settings base
        void updateBoolSetting(QString key);

        //! \brief Updates the value of a string setting with the specified key
        //! in the settings base
        void updateStringSetting(QString key);

        //! \brief Updates the value of a enum setting with the specified key in
        //! the settings base
        void updateStringEnumSetting(QString key);

        //! \brief Undoes a setting value
        void undoSetting(QString key);

        //! \brief Redoes a setting value
        void redoSetting(QString key);

        //! \brief updates the distance value displayed based on new preference
        void updateDistanceUnit();

        //! \brief updates the distance value displayed based on new preference
        void updateDistanceUnit(Distance new_unit);

        //! \brief updates the velocity value displayed based on new preference
        void updateVelocityUnit();

        //! \brief updates the velocity value displayed based on new preference
        void updateVelocityUnit(Velocity new_unit);

        //! \brief updates the acceleration value displayed based on new
        //! preference
        void updateAccelerationUnit();

        //! \brief updates the acceleration value displayed based on new
        //! preference
        void updateAccelerationUnit(Acceleration new_unit);

        //! \brief updates the angle value displayed based on new preference
        void updateAngleUnit();

        //! \brief updates the angle value displayed based on new preference
        void updateAngleUnit(Angle new_unit);

        //! \brief updates the time value displayed based on new preference
        void updateTimeUnit();

        //! \brief updates the time value displayed based on new preference
        void updateTimeUnit(Time new_unit);

    protected:
        QSharedPointer< PreferencesManager > m_preferences_manager;

        QMap< QString, QSharedPointer< TemplateCountSettingItem > > m_count;
        QSharedPointer< QSignalMapper > m_count_signal_mapper;

        QMap< QString, QSharedPointer< TemplatePercentSettingItem > > m_percent;
        QSharedPointer< QSignalMapper > m_percentage_signal_mapper;

        QMap< QString, QSharedPointer< TemplateCheckboxSettingItem > >
            m_checkbox;
        QSharedPointer< QSignalMapper > m_bool_signal_mapper;

        QMap< QString, QSharedPointer< TemplateStringSettingItem > > m_string;
        QSharedPointer< QSignalMapper > m_string_signal_mapper;

        QMap< QString, QSharedPointer< TemplateDropdownSettingItem > > m_enum;
        QSharedPointer< QSignalMapper > m_string_enum_signal_mapper;

        Distance m_distance_unit;
        QMap< QString, QSharedPointer< TemplateUnitsSettingItem > > m_distance;
        QSharedPointer< QSignalMapper > m_distance_signal_mapper;

        Velocity m_velocity_unit;
        QMap< QString, QSharedPointer< TemplateUnitsSettingItem > > m_velocity;
        QSharedPointer< QSignalMapper > m_velocity_signal_mapper;

        Acceleration m_acceleration_unit;
        QMap< QString, QSharedPointer< TemplateUnitsSettingItem > >
            m_acceleration;
        QSharedPointer< QSignalMapper > m_acceleration_signal_mapper;

        Time m_time_unit;
        QMap< QString, QSharedPointer< TemplateUnitsSettingItem > > m_time;
        QSharedPointer< QSignalMapper > m_time_signal_mapper;

        Angle m_angle_unit;
        QMap< QString, QSharedPointer< TemplateUnitsSettingItem > > m_angle;
        QSharedPointer< QSignalMapper > m_angle_signal_mapper;

        QSharedPointer< QSignalMapper > m_undo_signal_mapper;
        QSharedPointer< QSignalMapper > m_redo_signal_mapper;

        QMap< QString, bool > m_active;

        QString m_active_text;
        QString m_inactive_text;
        QString m_error_text;

        bool m_global;

        QSharedPointer< SettingsBase >
            m_temp_settings;  //!< Current tempory settings that have not been
                              //!< saved
        QSharedPointer< json >
            m_undo_history;  //!< If undo is clicked on a specific setting it
                             //!< comes from here
        QSharedPointer< json >
            m_redo_history;  //!< If redo is clicked on a specific setting it
                             //!< comes from here
    };                       // class SettingsConfigWidgetBase
}  // namespace ORNL
#endif  // SETTINGSCONFIGWIDGETBASE_H
