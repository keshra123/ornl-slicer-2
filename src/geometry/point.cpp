#include "geometry/point.h"

#include "utilities/mathutils.h"

namespace ORNL
{
    Point::Point()
    {
        m_x = 0;
        m_y = 0;
        m_z = 0;
    }

    Point::Point(const Distance& x, const Distance& y, const Distance& z)
    {
        m_x = x();
        m_y = y();
        m_z = z();
    }

    Point::Point(const Distance2D& d)
    {
        m_x = d.x();
        m_y = d.y();
        m_z = 0;
    }

    Point::Point(const Distance3D& d)
    {
        m_x = d.x();
        m_y = d.y();
        m_z = d.z();
    }

    Point::Point(const ClipperLib::IntPoint& p)
    {
        m_x = p.X;
        m_y = p.Y;
        m_z = 0;
    }

    Point::Point(const QPoint& p)
    {
        m_x = p.x();
        m_y = p.y();
    }

    Point Point::fromQVector2D(const QVector2D& p)
    {
        Point point;
        point.m_x = p.x();
        point.m_y = p.y();
        return point;
    }

    Point Point::fromQVector3D(const QVector3D& p)
    {
        Point point;
        point.m_x = p.x();
        point.m_y = p.y();
        point.m_z = p.z();
        return point;
    }


    Point::Point(const Point& p)
    {
        m_x = p.m_x;
        m_y = p.m_y;
        m_z = p.m_z;
    }

    Distance Point::distance() const
    {
        return qSqrt(qPow(m_x, 2) + qPow(m_y, 2) + qPow(m_z, 2));
    }

    Distance Point::distance(const Point& rhs) const
    {
        return qSqrt(qPow(rhs.m_x - m_x, 2) + qPow(rhs.m_y - m_y, 2) +
                     qPow(rhs.m_z - m_z, 2));
    }

    float Point::dot(const Point& rhs) const
    {
        return m_x * rhs.m_x + m_y * rhs.m_y + m_z * rhs.m_z;
    }

    float Point::dot(const Point& lhs, const Point& rhs)
    {
        return lhs.m_x * rhs.m_x + lhs.m_y * rhs.m_y + lhs.m_z * rhs.m_z;
    }

    Point Point::cross(const Point& rhs) const
    {
        return Point(m_y * rhs.m_z - m_z * rhs.m_y,
                     m_z * rhs.m_x - m_x * rhs.m_z,
                     m_x * rhs.m_y - m_y * rhs.m_x);
    }

    Point Point::rotate(Angle angle, QVector3D axis)
    {
        return rotateAround({0, 0, 0}, angle, axis);
    }

    Point Point::rotateAround(Point center, Angle angle, QVector3D axis)
    {
        QVector3D c = center.toQVector3D();
        QMatrix4x4 m;
        m.rotate(angle.to(deg), axis);
        QVector3D p = toQVector3D();
        p -= c;
        p = m * p;
        p += c;
        return Point::fromQVector3D(p);
    }

    bool Point::shorterThan(Distance rhs) const
    {
        return distance() < rhs;
    }

    ClipperLib::IntPoint Point::toIntPoint() const
    {
        return ClipperLib::IntPoint(static_cast< long long >(m_x),
                                    static_cast< long long >(m_y));
    }

    Distance2D Point::toDistance2D() const
    {
        return Distance2D(m_x, m_y);
    }

    Distance3D Point::toDistance3D() const
    {
        return Distance3D(m_x, m_y, m_z);
    }

    QPoint Point::toQPoint() const
    {
        return QPoint(static_cast< int >(m_x), static_cast< int >(m_y));
    }

    QVector2D Point::toQVector2D() const
    {
        return QVector2D(m_x, m_y);
    }

    QVector3D Point::toQVector3D() const
    {
        return QVector3D(m_x, m_y, m_z);
    }

    Point Point::operator+(const Point& rhs)
    {
        return Point(m_x + rhs.m_x, m_y + rhs.m_y, m_z + rhs.m_z);
    }

    Point Point::operator+=(const Point& rhs)
    {
        return Point(m_x + rhs.m_x, m_y + rhs.m_y, m_z + rhs.m_z);
    }

    Point Point::operator-(const Point& rhs)
    {
        return Point(m_x - rhs.m_x, m_y - rhs.m_y, m_z - rhs.m_z);
    }

    Point Point::operator-=(const Point& rhs)
    {
        return Point(m_x - rhs.m_x, m_y - rhs.m_y, m_z - rhs.m_z);
    }

    Point Point::operator*(const float rhs)
    {
        return Point(rhs * m_x, rhs * m_y, rhs * m_z);
    }

    Point Point::operator*=(const float rhs)
    {
        return Point(rhs * m_x, rhs * m_y, rhs * m_z);
    }

    Point Point::operator/(const float rhs)
    {
        return Point(m_x / rhs, m_y / rhs, m_z / rhs);
    }

    Point Point::operator/=(const float m)
    {
        return Point(m_x / m, m_y / m, m_z / m);
    }

    bool Point::operator==(const Point& rhs)
    {
        return MathUtils::equals(m_x, rhs.m_x) &&
            MathUtils::equals(m_y, rhs.m_y) && MathUtils::equals(m_z, rhs.m_z);
    }

    bool Point::operator!=(const Point& rhs)
    {
        return MathUtils::notEquals(m_x, rhs.m_x) ||
            MathUtils::notEquals(m_y, rhs.m_y) ||
            MathUtils::notEquals(m_z, rhs.m_z);
    }

    float Point::x()
    {
        return m_x;
    }

    float Point::x() const
    {
        return m_x;
    }

    void Point::x(float x)
    {
        m_x = x;
    }

    void Point::x(const Distance& x)
    {
        m_x = x();
    }

    float Point::y()
    {
        return m_y;
    }

    float Point::y() const
    {
        return m_y;
    }

    void Point::y(float y)
    {
        m_y = y;
    }

    void Point::y(const Distance& y)
    {
        m_y = y();
    }

    float Point::z()
    {
        return m_z;
    }

    float Point::z() const
    {
        return m_z;
    }

    void Point::z(float z)
    {
        m_z = z;
    }

    void Point::z(const Distance& z)
    {
        m_z = z();
    }

    Point operator*(const QMatrix4x4& lhs, const Point& rhs)
    {
        QVector3D v(rhs.x(), rhs.y(), rhs.z());
        QVector3D v2 = lhs * v;
        return Point(v2.x(), v2.y(), v2.z());
    }

    Point operator*(const float lhs, Point& rhs)
    {
        return rhs * lhs;
    }

    Point operator+(const Point& lhs, const Point& rhs)
    {
        return Point(lhs.x() + rhs.x(), lhs.y() + rhs.y(), lhs.z() + rhs.z());
    }

    Point operator-(const Point& lhs, const Point& rhs)
    {
        return Point(lhs.x() - rhs.x(), lhs.y() - rhs.y(), lhs.z() - rhs.z());
    }

    bool operator==(const Point& lhs, const Point& rhs)
    {
        return MathUtils::equals(lhs.x(), rhs.x()) &&
            MathUtils::equals(lhs.y(), rhs.y()) &&
            MathUtils::equals(lhs.z(), rhs.z());
    }

    bool operator!=(const Point& lhs, const Point& rhs)
    {
        return MathUtils::notEquals(lhs.x(), rhs.x()) ||
            MathUtils::notEquals(lhs.y(), rhs.y()) ||
            MathUtils::notEquals(lhs.z(), rhs.z());
    }
}  // namespace ORNL
