#include "dialogs/errordialog.h"

#include "ui_error_dialog.h"
namespace ORNL
{
    ErrorDialog::ErrorDialog(QString error, QWidget* parent)
        : QDialog(parent)
        , ui(new Ui::ErrorDialog)
    {
        ui->setupUi(this);

        QLabel* error_label = new QLabel(this);
        ui->verticalLayout->addWidget(error_label);
        error_label->setText(error);

        ui->verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        ui->horizontalLayout->setSizeConstraint(QLayout::SetFixedSize);

        updateGeometry();
    }

    ErrorDialog::ErrorDialog(QString error, QString filename, QWidget* parent)
        : QDialog(parent)
        , ui(new Ui::ErrorDialog)
    {
        ui->setupUi(this);

        QLabel* filename_label = new QLabel(this);
        ui->verticalLayout->addWidget(filename_label);
        filename_label->setText(filename);

        QLabel* error_label = new QLabel(this);
        ui->verticalLayout->addWidget(error_label);
        error_label->setText(error);

        ui->verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        ui->horizontalLayout->setSizeConstraint(QLayout::SetFixedSize);

        updateGeometry();
    }

    ErrorDialog::~ErrorDialog()
    {
        delete ui;
    }
}  // namespace ORNL
