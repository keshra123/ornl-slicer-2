#include "tst_polygon_list.h"
#include "geometry/polygon_list.h"

using namespace ORNL;

void PolygonListTest::testPointCount()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_2 (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_1 + polygon_2;

    uint integer = polygon_list.pointCount();

    QVERIFY(integer == 8);
}

void PolygonListTest::testOffset()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_2 (path_2);

    PolygonList polygon_list;
    PolygonList polygon_list_offset;
    polygon_list = polygon_list + polygon_1;
    polygon_list = polygon_list + polygon_2;

    polygon_list_offset = polygon_list.offset(4);

    PolygonList polygon_list2;
    PolygonList polygon_list3;
    polygon_list2 = polygon_1.offset(4);

    QVERIFY(polygon_list_offset[0] == polygon_list2[0]);
    QVERIFY(polygon_list_offset[1][0].x() == 39);
    QVERIFY(polygon_list_offset[1][1].x() == 39);
    QVERIFY(polygon_list_offset[1][2].x() == 41);
    QVERIFY(polygon_list_offset[1][3].x() == 41);
    QVERIFY(polygon_list_offset[1][0].y() == 44);
    QVERIFY(polygon_list_offset[1][1].y() == 46);
    QVERIFY(polygon_list_offset[1][2].y() == 46);
    QVERIFY(polygon_list_offset[1][3].y() == 44);
}

void PolygonListTest::testInside()
{
    QVector<Point> path_1;
    path_1.append(Point(10, 10));
    path_1.append(Point(70, 10));
    path_1.append(Point(70, 70));
    path_1.append(Point(10, 70));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(20, 30));
    path_2.append(Point(20, 40));
    path_2.append(Point(30, 40));
    path_2.append(Point(30, 30));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(50, 30));
    path_3.append(Point(50, 40));
    path_3.append(Point(60, 40));
    path_3.append(Point(60, 30));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two + polygon_three;

    Point point (40, 45);
    bool inside = polygon_list.inside(point);

    QVERIFY(inside == true);

    Point point2 (25, 35);
    inside = polygon_list.inside(point2);

    QVERIFY(inside == false);

    Point point3 (0, 0);
    inside = polygon_list.inside(point3);

    QVERIFY(inside == false);

    QVector<Point> path_4;
    path_4.append(Point(30, 30));
    path_4.append(Point(60, 30));
    path_4.append(Point(60, 60));
    path_4.append(Point(30, 60));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(35, 40));
    path_5.append(Point(35, 50));
    path_5.append(Point(45, 50));
    path_5.append(Point(45, 40));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(36, 42));
    path_6.append(Point(43, 42));
    path_6.append(Point(43, 49));
    path_6.append(Point(36, 49));
    Polygon polygon_six (path_6);

    PolygonList polygon_list_2;
    polygon_list_2 = polygon_four + polygon_five;
    polygon_list_2 = polygon_six + polygon_list_2;

    inside = polygon_list_2.inside(point);
    QVERIFY(inside == true);
    inside = polygon_list_2.inside(point2);
    QVERIFY(inside == false);
    Point point4 (40, 41);
    Point point5 (50, 40);
    inside = polygon_list_2.inside(point4);
    QVERIFY(inside == false);
    inside = polygon_list_2.inside(point5);
    QVERIFY(inside == true);
}

void PolygonListTest::testFindInside()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(36, 42));
    path_3.append(Point(43, 42));
    path_3.append(Point(43, 49));
    path_3.append(Point(36, 49));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two + polygon_three;

    Point point (40, 45);
    Point point2 (35, 35);
    Point point3 (0, 0);
    Point point4 (25, 35);
    Point point5 (44, 41);

    uint integer = polygon_list.findInside(point);
    QVERIFY(integer == 2);
    integer = polygon_list.findInside(point2);
    QVERIFY(integer == 0);
    integer = polygon_list.findInside(point3);
    QVERIFY(integer == NO_INDEX);
    integer = polygon_list.findInside(point5);
    QVERIFY(integer == NO_INDEX);

    QVector<Point> path_4;
    path_4.append(Point(10, 10));
    path_4.append(Point(70, 10));
    path_4.append(Point(70, 70));
    path_4.append(Point(10, 70));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(20, 30));
    path_5.append(Point(20, 40));
    path_5.append(Point(30, 40));
    path_5.append(Point(30, 30));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(50, 30));
    path_6.append(Point(50, 40));
    path_6.append(Point(60, 40));
    path_6.append(Point(60, 30));
    Polygon polygon_six (path_6);

    PolygonList polygon_list2;
    polygon_list2 = polygon_four + polygon_five + polygon_six;

    integer = polygon_list2.findInside(point);
    QVERIFY(integer == 0);
    integer = polygon_list2.findInside(point4);
    QVERIFY(integer == NO_INDEX);
}

void PolygonListTest::testConvexHull()
{
//    QVector<Point> path_1;
//    path_1.append(Point(20, 10));
//    path_1.append(Point(50, 30));
//    path_1.append(Point(80, 10));
//    path_1.append(Point(70, 40));
//    path_1.append(Point(90, 60));
//    path_1.append(Point(60, 60));
//    path_1.append(Point(50, 90));
//    path_1.append(Point(40, 60));
//    path_1.append(Point(10, 60));
//    path_1.append(Point(30, 40));
//    Polygon polygon_1 (path_1);

//    QVector<Point> path_2;
//    path_2.append(Point(20, 20));
//    path_2.append(Point(20, 60));
//    path_2.append(Point(60, 60));
//    path_2.append(Point(40, 60));
//    path_2.append(Point(60, 20));
//    Polygon polygon_2 (path_2);

//    QVector<Point> path_3;
//    path_3.append(Point(30, 30));
//    path_3.append(Point(60, 30));
//    path_3.append(Point(60, 60));
//    path_3.append(Point(30, 60));
//    Polygon polygon_three (path_3);

//    QVector<Point> path_4;
//    path_4.append(Point(45, 65));
//    path_4.append(Point(40, 25));
//    path_4.append(Point(50, 25));
//    Polygon polygon_four (path_4);

//    PolygonList polygon_list;
//    polygon_list = polygon_three | polygon_four;

//    Polygon hull_polygon;
//    hull_polygon = polygon_list.convexHull();

//    qDebug() << hull_polygon.size();

//    PolygonList polygon_list;
//    polygon_list = polygon_list + polygon_1;

//    PolygonList polygon_list_2;
//    polygon_list_2 = polygon_list_2 + polygon_2;

//    polygon_list = polygon_list + polygon_2;

//    Polygon penta;
//    penta = polygon_list.convexHull();

//    qDebug() << penta.size();
//    for (int i = 0; i < penta.size(); i++){
//        qDebug() << penta[i].x() << " , " << penta[i].y();
//    }
}

void PolygonListTest::testRemoveEmptyHoles()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(36, 41));
    path_3.append(Point(44, 41));
    path_3.append(Point(44, 49));
    path_3.append(Point(36, 49));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_list + polygon_one;
    polygon_list = polygon_list + polygon_two;
    polygon_list = polygon_list + polygon_three;

    PolygonList polygon_list_removed;
    polygon_list_removed = polygon_list.removeEmptyHoles();

    QVERIFY(polygon_list_removed.size() == 3);
    QVERIFY(polygon_list_removed[0] == polygon_one);
    QVERIFY(polygon_list_removed[1] == polygon_two);
    QVERIFY(polygon_list_removed[2] == polygon_three);

    polygon_list = polygon_list - polygon_three;
    PolygonList polygon_list_removed_2;
    polygon_list_removed_2 = polygon_list.removeEmptyHoles();

    QVERIFY(polygon_list_removed_2.size() == 1);
    QVERIFY(polygon_list_removed_2[0] == polygon_one);

    QVector<Point> path_4;
    path_4.append(Point(10, 10));
    path_4.append(Point(70, 10));
    path_4.append(Point(70, 70));
    path_4.append(Point(10, 70));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(20, 30));
    path_5.append(Point(20, 40));
    path_5.append(Point(30, 40));
    path_5.append(Point(30, 30));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(50, 30));
    path_6.append(Point(50, 40));
    path_6.append(Point(60, 40));
    path_6.append(Point(60, 30));
    Polygon polygon_six (path_6);

    PolygonList polygon_list_2;
    polygon_list_2 = polygon_list_2 + polygon_four;
    polygon_list_2 = polygon_list_2 + polygon_five;
    polygon_list_2 = polygon_list_2 + polygon_six;

    polygon_list_removed = polygon_list_2.removeEmptyHoles();
    QVERIFY(polygon_list_removed.size() == 1);
    QVERIFY(polygon_list_removed[0] == polygon_four);
}

void PolygonListTest::testGetEmptyHoles()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(36, 42));
    path_3.append(Point(43, 42));
    path_3.append(Point(43, 49));
    path_3.append(Point(36, 49));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_list + polygon_one;
    polygon_list = polygon_list + polygon_two;
    polygon_list = polygon_list + polygon_three;

    PolygonList polygon_list_removed;
    polygon_list_removed = polygon_list.getEmptyHoles();

    QVERIFY(polygon_list_removed.size() == 0);

    QVector<Point> path_4;
    path_4.append(Point(10, 10));
    path_4.append(Point(70, 10));
    path_4.append(Point(70, 70));
    path_4.append(Point(10, 70));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(20, 30));
    path_5.append(Point(20, 40));
    path_5.append(Point(30, 40));
    path_5.append(Point(30, 30));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(50, 30));
    path_6.append(Point(50, 40));
    path_6.append(Point(60, 40));
    path_6.append(Point(60, 30));
    Polygon polygon_six (path_6);

    PolygonList polygon_list2;
    polygon_list2 = polygon_list2 + polygon_four;
    polygon_list2 = polygon_list2 + polygon_five;
    polygon_list2 = polygon_list2 + polygon_six;

    polygon_list2 = polygon_list2.getEmptyHoles();

    QVERIFY(polygon_list2[0].orientation() == true);
    QVERIFY(polygon_list2[1].orientation() == true);
}

void PolygonListTest::testSplitIntoParts()
{

    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(36, 42));
    path_3.append(Point(43, 42));
    path_3.append(Point(43, 49));
    path_3.append(Point(36, 49));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    QVector<PolygonList> vector_polygon_list;
    polygon_list = polygon_one + polygon_two + polygon_three;
    vector_polygon_list = polygon_list.splitIntoParts();

    QVERIFY(vector_polygon_list.size() == 2);
    QVERIFY(vector_polygon_list[0][0] == polygon_three);
    QVERIFY(vector_polygon_list[1][0] == polygon_one);
    QVERIFY(vector_polygon_list[1][1] == polygon_two);

    QVector<Point> path_4;
    path_4.append(Point(10, 10));
    path_4.append(Point(70, 10));
    path_4.append(Point(70, 70));
    path_4.append(Point(10, 70));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(20, 30));
    path_5.append(Point(20, 40));
    path_5.append(Point(30, 40));
    path_5.append(Point(30, 30));
    Polygon polygon_five (path_5);

    QVector<Point> path_six;
    path_six.append(Point(50, 30));
    path_six.append(Point(50, 40));
    path_six.append(Point(60, 40));
    path_six.append(Point(60, 30));
    Polygon polygon_six (path_six);

    QVector<Point> path_seven;
    path_seven.append(Point(20, 80));
    path_seven.append(Point(30, 80));
    path_seven.append(Point(25, 90));
    Polygon polygon_seven (path_seven);

    QVector<Point> path_eight;
    path_eight.append(Point(23, 81));
    path_eight.append(Point(25, 86));
    path_eight.append(Point(27, 81));
    Polygon polygon_eight (path_eight);

    PolygonList polygon_list_2;
    polygon_list_2 = polygon_four + polygon_five + polygon_six + polygon_seven + polygon_eight;
    vector_polygon_list = polygon_list_2.splitIntoParts();

    QVERIFY(vector_polygon_list.size() == 2);
    QVERIFY(vector_polygon_list[0][0] == polygon_seven);
    QVERIFY(vector_polygon_list[0][1] == polygon_eight);
    QVERIFY(vector_polygon_list[1][0] == polygon_four);
    QVERIFY(vector_polygon_list[1][1] == polygon_five);
    QVERIFY(vector_polygon_list[1][2] == polygon_six);
}

void PolygonListTest::testRemoveSmallAreas()
{
    QVector<Point> path_1;
    path_1.append(Point(10, 10));
    path_1.append(Point(70, 10));
    path_1.append(Point(70, 70));
    path_1.append(Point(10, 70));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(20, 30));
    path_2.append(Point(20, 40));
    path_2.append(Point(30, 40));
    path_2.append(Point(30, 30));
    Polygon polygon_two (path_2);

    QVector<Point> path_3;
    path_3.append(Point(50, 30));
    path_3.append(Point(50, 40));
    path_3.append(Point(60, 40));
    path_3.append(Point(60, 30));
    Polygon polygon_three (path_3);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two + polygon_three;

    PolygonList removed_area;
    removed_area = polygon_list.removeSmallAreas(101);

    QVERIFY(removed_area.size() == 1);
    QVERIFY (removed_area[0] == polygon_one);

    QVector<Point> path_4;
    path_4.append(Point(30, 30));
    path_4.append(Point(60, 30));
    path_4.append(Point(60, 60));
    path_4.append(Point(30, 60));
    Polygon polygon_four (path_4);

    QVector<Point> path_5;
    path_5.append(Point(35, 40));
    path_5.append(Point(35, 50));
    path_5.append(Point(45, 50));
    path_5.append(Point(45, 40));
    Polygon polygon_five (path_5);

    QVector<Point> path_6;
    path_6.append(Point(36, 42));
    path_6.append(Point(43, 42));
    path_6.append(Point(43, 49));
    path_6.append(Point(36, 49));
    Polygon polygon_six (path_6);

    polygon_list = polygon_four + polygon_five + polygon_six;
    removed_area = polygon_list.removeSmallAreas(101);

    QVERIFY(removed_area.size() == 1);
    QVERIFY(removed_area[0] == polygon_four);
}

void PolygonListTest::testRemoveDegenerateVertices()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_one (path_1);

    QVector<Point> path_2;
    path_2.append(Point(50, 40));
    path_2.append(Point(50, 20));
    path_2.append(Point(70, 20));
    path_2.append(Point(70, 40));
    Polygon polygon_two (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_one + polygon_two;
    polygon_list = polygon_list.removeDegenerateVertices();

    QVERIFY(polygon_list[0][0].x() == 70);
    QVERIFY(polygon_list[0][1].x() == 60);
    QVERIFY(polygon_list[0][2].x() == 60);
    QVERIFY(polygon_list[0][3].x() == 30);
    QVERIFY(polygon_list[0][4].x() == 30);
    QVERIFY(polygon_list[0][5].x() == 50);
    QVERIFY(polygon_list[0][6].x() == 50);
    QVERIFY(polygon_list[0][7].x() == 70);
    QVERIFY(polygon_list[0][0].y() == 40);
    QVERIFY(polygon_list[0][1].y() == 40);
    QVERIFY(polygon_list[0][2].y() == 60);
    QVERIFY(polygon_list[0][3].y() == 60);
    QVERIFY(polygon_list[0][4].y() == 30);
    QVERIFY(polygon_list[0][5].y() == 30);
    QVERIFY(polygon_list[0][6].y() == 20);
    QVERIFY(polygon_list[0][7].y() == 20);
}

void PolygonListTest::testMin()
{
    QVector<Point> path_one;
    path_one.append(Point(30, 30));
    path_one.append(Point(60, 30));
    path_one.append(Point(60, 60));
    path_one.append(Point(30, 60));
    Polygon polygon_one (path_one);

    QVector<Point> path_two;
    path_two.append(Point(45, 65));
    path_two.append(Point(40, 25));
    path_two.append(Point(50, 25));
    Polygon polygon_two (path_two);

    PolygonList polygon_list;
    polygon_list = polygon_one | polygon_two;
    Point min;
    min = polygon_list.min();

    QVERIFY(min.x() == 30);
    QVERIFY(min.y() == 25);
}

void PolygonListTest::testRotate()
{
    QVector<Point> path_one;
    path_one.append(Point(30, 30));
    path_one.append(Point(60, 30));
    path_one.append(Point(60, 60));
    path_one.append(Point(30, 60));
    Polygon polygon_one (path_one);

    PolygonList polygon_list;
    polygon_list = polygon_list + polygon_one;
    polygon_list = polygon_list.rotate(90 * deg);

    QVERIFY(polygon_list[0][0].x() == -30);
    QVERIFY(polygon_list[0][1].x() == -60);
    QVERIFY(polygon_list[0][2].x() == -60);
    QVERIFY(polygon_list[0][3].x() == -30);
    QVERIFY(polygon_list[0][0].y() == 60);
    QVERIFY(polygon_list[0][1].y() == 60);
    QVERIFY(polygon_list[0][2].y() == 30);
    QVERIFY(polygon_list[0][3].y() == 30);
}

void PolygonListTest::testPolygonListIntersectPolyline()
{
    QVector<Point> path_1;
    path_1.append(Point(30, 30));
    path_1.append(Point(60, 30));
    path_1.append(Point(60, 60));
    path_1.append(Point(30, 60));
    Polygon polygon_1 (path_1);

    QVector<Point> path_2;
    path_2.append(Point(35, 40));
    path_2.append(Point(35, 50));
    path_2.append(Point(45, 50));
    path_2.append(Point(45, 40));
    Polygon polygon_2 (path_2);

    PolygonList polygon_list;
    polygon_list = polygon_1 + polygon_2;

    Polyline cutline;
    cutline << Point(45, 25);
    cutline << Point (45, 65);

    Polyline cutline2;
    cutline2 << Point(55, 25);
    cutline2 << Point(55, 65);

    Polyline cutline3;
    cutline3 << Point(70, 45);
    cutline3 << Point(20, 45);

    QVector<Polyline> polyline_vector;
    polyline_vector += polygon_list & cutline;
    polyline_vector += polygon_list & cutline2;
    polyline_vector += polygon_list & cutline3;

    QVERIFY(polyline_vector[0][0].x() == 45);
    QVERIFY(polyline_vector[0][1].x() == 45);
    QVERIFY(polyline_vector[1][0].x() == 45);
    QVERIFY(polyline_vector[1][1].x() == 45);
    QVERIFY(polyline_vector[2][0].x() == 55);
    QVERIFY(polyline_vector[2][1].x() == 55);
    QVERIFY(polyline_vector[3][0].x() == 45);
    QVERIFY(polyline_vector[3][1].x() == 60);
    QVERIFY(polyline_vector[4][0].x() == 30);
    QVERIFY(polyline_vector[4][1].x() == 35);

    QVERIFY(polyline_vector[0][0].y() == 50);
    QVERIFY(polyline_vector[0][1].y() == 60);
    QVERIFY(polyline_vector[1][0].y() == 30);
    QVERIFY(polyline_vector[1][1].y() == 40);
    QVERIFY(polyline_vector[2][0].y() == 30);
    QVERIFY(polyline_vector[2][1].y() == 60);
    QVERIFY(polyline_vector[3][0].y() == 45);
    QVERIFY(polyline_vector[3][1].y() == 45);
    QVERIFY(polyline_vector[4][0].y() == 45);
    QVERIFY(polyline_vector[4][1].y() == 45);
}
