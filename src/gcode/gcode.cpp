#include "gcode/gcode.h"

// #include <omp.h>

#include "gcode/gcode_layer.h"
#include "gcode/writers/writer_base.h"
#include "geometry/mesh.h"
#include "managers/project_manager.h"

namespace ORNL
{
    Gcode::Gcode()
        : m_project_manager(ProjectManager::getInstance())
        , m_lock(new omp_lock_t)
    {
        // omp_init_lock(m_lock.data());
    }

    Gcode::~Gcode()
    {
        // omp_destroy_lock(m_lock.data());
    }

    void Gcode::updateMesh(QSharedPointer< Mesh > mesh)
    {
        // omp_set_lock(m_lock.data());
        // Update all previously existing layers
        // #pragma omp parallel for
        for (int layer_nr = 0; layer_nr < size(); layer_nr++)
        {
            operator[](layer_nr)->updateMesh(mesh);
        }

        // Add any new layers
        if (mesh->size() > size())
        {
            for (int layer_nr = size(); layer_nr < mesh->size(); layer_nr++)
            {
                append(QSharedPointer< GcodeLayer >(new GcodeLayer(layer_nr)));
                back()->updateMesh(mesh);
            }
        }
        // omp_unset_lock(m_lock.data());
    }

    QString Gcode::toGcode(WriterBase* syntax, QString filename)
    {
        QString gcode;

        int num_layers = size();
        QVector< QString > gcode_layers_text;
        gcode_layers_text.resize(num_layers);
        QVector< omp_lock_t > locks;
        locks.resize(num_layers);

        // Initialize all the locks and set them (to be unset after text is
        // collected from their respective layer) #pragma omp parallel for
        for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
        {
            // omp_init_lock(&locks[layer_nr]);
            // omp_set_lock(&locks[layer_nr]);
        }

        // Collect the gcode for the layers at the same time as writing it
        // (writing is done sequentially) #pragma omp parallel sections
        {
            // #pragma omp section
            {
                // #pragma omp parallel for
                for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
                {
                    gcode_layers_text[layer_nr] = operator[](layer_nr)->gcode(
                        syntax);
                    // omp_unset_lock(&locks[layer_nr]);
                }
            }

            // #pragma omp section
            {
                gcode += syntax->slicerHeader(filename);
                gcode += syntax->header();

                for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
                {
                    // Forces waiting until the layer is finished creating the
                    // string omp_set_lock(&locks[layer_nr]);
                    gcode += gcode_layers_text[layer_nr];
                    // omp_unset_lock(&locks[layer_nr]);
                }

                gcode += syntax->footer();
            }
        }

        // Destroy all the locks
        // #pragma omp parallel for
        for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
        {
            // omp_destroy_lock(&locks[layer_nr]);
        }

        return gcode;
    }

    void Gcode::writeGcode(WriterBase* syntax, QString filename)
    {
        QFile f(filename);
        writeGcode(syntax, f);
    }

    void Gcode::writeGcode(WriterBase* syntax, QFile& file)
    {
        if (!file.isOpen())
        {
            if (!file.open(QIODevice::WriteOnly))
            {
                // TODO: raise exception
            }
        }

        int num_layers = size();
        QVector< QString > gcode_layers_text;
        gcode_layers_text.resize(num_layers);
        QVector< omp_lock_t > locks;
        locks.resize(num_layers);

        // Initialize all the locks and set them (to be unset after text is
        // collected from their respective layer) #pragma omp parallel for
        for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
        {
            // omp_init_lock(&locks[layer_nr]);
            // omp_set_lock(&locks[layer_nr]);
        }

        // Collect the gcode for the layers at the same time as writing it
        // (writing is done sequentially) #pragma omp parallel sections
        {
            // #pragma omp section
            {
                // #pragma omp parallel for
                for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
                {
                    gcode_layers_text[layer_nr] = operator[](layer_nr)->gcode(
                        syntax);
                    // omp_unset_lock(&locks[layer_nr]);
                }
            }

            // #pragma omp section
            {
                file.write(syntax->slicerHeader(file.fileName())
                               .toStdString()
                               .c_str());
                file.write(syntax->header().toStdString().c_str());

                for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
                {
                    // Forces waiting until the layer is finished creating the
                    // string omp_set_lock(&locks[layer_nr]);
                    file.write(
                        gcode_layers_text[layer_nr].toStdString().c_str());
                    // omp_unset_lock(&locks[layer_nr]);
                }

                file.write(syntax->footer().toStdString().c_str());
            }
        }

        // Destroy all the locks
        // #pragma omp parallel for
        for (int layer_nr = 0; layer_nr < num_layers; layer_nr++)
        {
            // omp_destroy_lock(&locks[layer_nr]);
        }

        file.close();
    }
}  // namespace ORNL
