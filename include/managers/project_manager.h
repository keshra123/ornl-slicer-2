#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H
//! \file projectmanager.h

#include <QMap>
#include <QObject>
#include <QSharedPointer>
#include <QVector>

#ifdef DEBUG
class QPainter;
class QPaintEvent;
#endif
class QReadWriteLock;

namespace ORNL
{
    class Gcode;
    class GcodeLayer;
    class GlobalSettingsManager;
    class Island;
    class Mesh;
    class Path;
    class PathSegment;
    class Region;
    class SettingsBase;
    class WindowManager;
    enum class RegionType : uint8_t;
    class Distance;
    class PreferencesManager;
    class MeshGraphics;

    /*!
     * \class ProjectManager
     * \brief Manager for the open project.
     *
     * A project includes:
     *  - List of meshes
     *  - Global settings
     *
     * The manager also handles copy, paste, and selection
     */
    class ProjectManager : public QObject
    {
        Q_OBJECT
    public:
        //! \brief Returns the singleton
        static QSharedPointer< ProjectManager > getInstance();

        /*!
         * \brief Return the lock for threads for the meshes
         *
         * \note The slicing thread uses this when meshes
         *       are added or removed
         *
         * \returns The lock for the meshes
         */
        QSharedPointer< QReadWriteLock > meshLock();

        /*!
         * \brief Returns a list of the mesh names
         *
         * \returns A list of the names of the meshes
         */
        QStringList meshNameList();

        /*!
         * \brief Returns a list of the meshes
         *
         * \returns A list of the meshes for the current project
         */
        QList< QSharedPointer< Mesh > > meshList();

        /*!
         * \brief Returns a specific mesh
         *
         * \param mesh_name The name of the mesh to be returned
         * \returns The mesh with the corresponding name
         */
        QSharedPointer< Mesh > mesh(QString mesh_name);

        /*!
         * \brief Returns the gcode for the most recent slice
         *
         * \returns A shared pointer to the Gcode object for the current project
         */
        QSharedPointer< Gcode > gcode();

        /*!
         * \brief Writes gcode to a file
         *
         * \param filename The path to the file where the gcode will be written
         */
        void writeGcode(QString filename);

        /*!
         * \brief Returns the selected mesh (or nullptr if none is selected)
         *
         * \returns The mesh marked as selected if it exists otherwise nullptr
         */
        QSharedPointer< Mesh > selectedMesh();

        /*!
         * \brief Returns the selected layer (or nullptr if none is selected)
         *
         * \returns The selected gcode layer if it exists otherwise nullptr
         */
        QSharedPointer< GcodeLayer > selectedLayer();

        /*!
         * \brief Returns the selected island (or nullptr if none is selected)
         *
         * \returns The selected gcode island if it exists otherwise nullptr
         */
        QSharedPointer< Island > selectedIsland();

        /*!
         * \brief Returns the selected region (or nullptr if none is selected)
         *
         * \returns The selected gcode region if it exists otherwise nullptr
         */
        QSharedPointer< Region > selectedRegion();

        /*!
         * \brief Returns the selected path (or nullptr if none is selected)
         *
         * \returns The selected gcode path if it exists otherwise nullptr
         */
        Path* selectedPath();

        /*!
         * \brief Returns the selected path segment (of nullptr if none is
         * selected)
         *
         * \returns The selected gcode path segment if it exists otherwise
         * nullptr
         */
        PathSegment* selectedPathSegment();

        /*!
         * \brief Returns the name of the copied mesh
         *
         * \returns The name of the mesh that is marked as copied
         */
        QString copiedMeshName();

        /*!
         * \brief Returns the copied mesh
         *
         * \returns The mesh that is marked as copied
         */
        QSharedPointer< Mesh > copiedMesh();

        /*!
         * \brief Returns the name of the current global machine config
         *
         * \returns The name of the current global machine config
         */
        QString currentMachineName();

        /*!
         * \brief Returns the current global machine config
         *
         * \returns The current global machine config
         */
        QSharedPointer< SettingsBase > currentMachine();

        /*!
         * \brief Returns the name of the current global print config
         *
         * \returns The name of the current global print config
         */
        QString currentPrintName();

        /*!
         * \brief Returns the current global print config
         *
         * \returns The current global print config
         */
        QSharedPointer< SettingsBase > currentPrint();

        /*!
         * \brief Returns the name of the current global material config
         *
         * \returns The name of the current global material config
         */
        QString currentMaterialName();

        /*!
         * \brief Returns the current global material config
         *
         * \returns The current global material config
         */
        QSharedPointer< SettingsBase > currentMaterial();

        /*!
         * \brief Returns the name of the current global nozzle config
         *
         * \returns The name of the current global nozzle config
         */
        QString currentNozzleName();

        /*!
         * \brief Returns the current global nozzle config
         *
         * \returns The current global nozzle config
         */
        QSharedPointer< SettingsBase > currentNozzle();

        /*!
         * \brief Sets the name of the current project
         *
         * \param name The name to set for the current project (used for the
         * file and the folder)
         */
        void name(QString name);

        /*!
         * \brief Returns the name of the current project
         *
         * \returns The name of the current project
         */
        QString name();

        /*!
         * \brief Sets the filepath for the folder containing the project
         *
         * \param filepath The path to the folder that contains the project
         * folder or will contain the project folder if saved
         */
        void filepath(QString filepath);

        /*!
         * \brief Returns the filepath for the folder containing the project
         *
         * \returns The filepath for the folder containing the project
         */
        QString filepath();

        /*!
         * \brief Updates the combined global config based on the currently
         * selected individual global configs (machine, material, print, nozzle)
         */
        void updateGlobalSettings();

        /*!
         * \brief Returns a list of the recent projects as a pair of filepaths
         * and project names
         *
         * \returns A list of the recent project as pairs of filepaths to the
         * project folder and the project names
         */
        QVector< QPair< QString, QString > > recentProjects();

        /*!
         * \brief Marks the current project as recent
         */
        void markRecent();

        /*!
         * \brief Exports the recent projects to file
         */
        void exportRecentProjects();

    public slots:
        /*!
         * \brief Adds the mesh to the project
         *
         * \param mesh A mesh to add to the project
         */
        void addMesh(QSharedPointer< ORNL::Mesh > mesh);

        /*!
         * \brief Removes the mesh from the project
         *
         * \param mesh_name The name of the mesh to remove from the project
         */
        void removeMesh(QString mesh_name);

        /*!
         * \brief Set the mesh that can be pasted
         *
         * \param copied_mesh The name of the mesh that has been copied
         */
        void copyMesh(QString copied_mesh);

        /*!
         * \brief Set the mesh that is currenly selected to be copied
         */
        void copySelectedMesh();

        /*!
         * \brief Set the selected mesh
         *
         * \param selected_mesh The name of the mesh that has been selected
         */
        void selectMesh(QString selected_mesh);

        /*!
         * \brief Set the selected gcode layer
         *
         * \param layer_nr The number of the gcode layer that is selected
         */
        void selectLayer(uint layer_nr);

        /*!
         * \brief Set the selected gcode island
         *
         * \param layer_nr The number of the gcode layer that the island that is
         * selected is from \param island_nr The number of the gcode island that
         * is selected
         */
        void selectIsland(uint layer_nr, uint island_nr);

        /*!
         * \brief Set the selected region
         *
         * \param layer_nr The number of the gcode layer that the region that is
         * selected is from \param island_nr The number of the gcode island that
         * the region that is selected is from \param region_type The region
         * type that is selected
         */
        void selectRegion(uint layer_nr,
                          uint island_nr,
                          RegionType region_type);

        /*!
         * \brief Set the selected path
         *
         * \param layer_nr The number of the gcode layer that the path that is
         * selected is from \param island_nr The number of the gcode island that
         * the path that is selected is from \param region_type The region type
         * that the path that is selected is from \param path_nr The number of
         * the path that is selected
         */
        void selectPath(uint layer_nr,
                        uint island_nr,
                        RegionType region_type,
                        uint path_nr);

        /*!
         * \brief Set the selected path segment
         *
         * \param layer_nr The number of the gcode layer that the path segment
         * that is selected is from \param island_nr The number of the gcode
         * island that the path segment that is selected is from \param
         * region_type The region type that the path segment that is selected is
         * from \param path_nr The number of the path that the path segment that
         * is selected is from \param path_segment_nr The of the path segment
         * that is selected
         */
        void selectPathSegment(uint layer_nr,
                               uint island_nr,
                               RegionType region_type,
                               uint path_nr,
                               uint path_segment_nr);

#ifdef DEBUG
        /*!
         * \brief Set the selected debug option
         *
         * \param layer_nr
         * \param island_nr
         * \param region_type
         * \param option
         */
        void selectDebug(uint layer_nr,
                         uint island_nr,
                         RegionType region_type,
                         QString option);

        /*!
         * \brief Paint function for the debug visual
         *
         * \param painter
         * \param ratio The ratio from the real world to the screen
         * \param vertical_offset
         *
         * \see class DebugDisplay
         * \see class Region
         */
        void paintSelectedDebug(QPainter* painter,
                                Distance& ratio,
                                int vertical_offset);
#endif

        /*!
         * \brief unselects a selected mesh
         */
        void deselectMesh();

        /*!
         * \brief unselects a selected gcode item
         */
        void deselectGcode();

#ifdef DEBUG
        /*!
         * \brief Remove any selections from debug
         */
        void deselectDebug();
#endif

        /*!
         * \brief "Pastes" a copy of the copied mesh
         */
        void paste();

        /*!
         * \brief Set the current global machine config
         *
         * \param machine Name of the machine config to set
         *                as the global machine config
         */
        void currentMachineName(QString machine);

        /*!
         *
         * \brief Set the current global print config
         *
         * \param print Name of the print config to set
         *              as the global print config
         */
        void currentPrintName(QString print);

        /*!
         *
         * \brief Set the current global material config
         *
         * \param material Name of the material config to set
         *                 as the global material config
         */
        void currentMaterialName(QString material);

        /*!
         *
         * \brief Set the current global nozzle config
         *
         * \param nozzle Name of the nozzle config to set
         *               as the global nozzle config
         */
        void currentNozzleName(QString nozzle);

        /*!
         * \brief Clear the project
         */
        void closeProject();

    signals:
        //! \brief Sends the mesh graphics object
        void sendMeshGraphics(MeshGraphics* mesh_graphic);

        /*!
         * \brief The list of meshes has been updated
         *
         * \param mesh_list List of the mesh names
         */
        void updateList(QStringList mesh_list);

        /*!
         * \brief signal that there has been an error
         *
         * \param error Error Message
         */
        void error(QString error);

        /*!
         * \brief signal that the selected mesh has changed
         */
        void meshSelectionChanged();

        /*!
         * \brief signal that a mesh has been added
         *
         * \param mesh The mesh that has been added
         */
        void meshAdded(QSharedPointer< Mesh > mesh);

        /*!
         * \brief signal that a mesh has been removed
         *
         * \param mesh The mesh that has been removed
         */
        void meshRemoved(QSharedPointer< Mesh > mesh);

        /*!
         * \brief signal to the main window that copy is to be enabled/disabled
         *
         * \param enable Whether copy is enabled/disabled
         */
        void enableCopy(bool enable);

        /*!
         * \brief signal to the main window that paste is to be enabled/disabled
         *
         * \param enable Whether paste is enabled/disabled
         */
        void enablePaste(bool enable);

        /*!
         * \brief signal that the current global machine config has changed
         *
         * \param machine The name of the global machine config
         */
        void currentMachineChanged(QString machine);

        /*!
         * \brief signal that the current global material config has changed
         *
         * \param material The name of the global material config
         */
        void currentMaterialChanged(QString material);

        /*!
         * \brief signal that the current global print config has changed
         *
         * \param print The name of the global print config
         */
        void currentPrintChanged(QString print);

        /*!
         * \brief signal that the current global nozzle config has changed
         *
         * \param nozzle The name of the global nozzle config
         */
        void currentNozzleChanged(QString nozzle);

    private:
        //! \brief Constructor
        ProjectManager();

        static QSharedPointer< ProjectManager > m_singleton;
        QMap< QString, QSharedPointer< Mesh > > m_meshes;
        QSharedPointer< Gcode > m_gcode;

        QSharedPointer< QReadWriteLock > m_mesh_lock;

        QString m_copied_mesh;

        // Mesh visual
        QString m_selected_mesh;

        // Gcode visual
        int m_selected_layer;
        int m_selected_island;
        RegionType m_selected_region;
        int m_selected_path;
        int m_selected_path_segment;

#ifdef DEBUG
        // Debug visual
        int m_selected_debug_layer;
        int m_selected_debug_island;
        RegionType m_selected_debug_region;
        QString m_selected_debug_option;  // Better name?
#endif

        QString m_name;
        QString m_filepath;

        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;
        QSharedPointer< WindowManager > m_window_manager;

        QString m_current_machine_setting;
        QString m_current_material_setting;
        QString m_current_print_setting;
        QString m_current_nozzle_setting;

        int m_color;

        QVector< QPair< QString, QString > > m_recent_projects;
    };  // class ProjectManager
}  // namespace ORNL
#endif  // PROJECTMANAGER_H
