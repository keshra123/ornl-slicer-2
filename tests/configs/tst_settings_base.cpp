#include "configs/tst_settings_base.h"
#include "configs/settings_base.h"

using namespace ORNL;

#if 0 // TODO fix first

void TestSettingsBase::testTypes()
{
    SettingsBase* sb = new SettingsBase();

    Distance distance = 1 * in;
    sb->setSetting<Distance>("distance", distance);
    Distance other_distance = sb->setting<Distance>("distance");
    QVERIFY2(distance == other_distance, "Distance");


    Velocity velocity = 1 * in / s;
    sb->setSetting<Velocity>("velocity", velocity);
    Velocity other_velocity = sb->setting<Velocity>("velocity");
    QVERIFY2(velocity == other_velocity, "Velocity");

    Acceleration acceleration = 1 * in / s / s;
    sb->setSetting<Acceleration>("acceleration", acceleration);
    Acceleration other_acceleration = sb->setting<Acceleration>("acceleration");
    QVERIFY2(acceleration == other_acceleration, "Acceleration");

    Time time = 1 * s;
    sb->setSetting<Time>("time", time);
    Time other_time = sb->setting<Time>("time");
    QVERIFY2(time == other_time, "Time");

    Angle angle = 1 * deg;
    sb->setSetting<Angle>("angle", angle);
    Angle other_angle = sb->setting<Angle>("angle");
    QVERIFY2(angle == other_angle, "Angle");

    QString string = "something something";
    sb->setSetting<QString>("string", string);
    QString other_string = sb->setting<QString>("string");
    QVERIFY2(string == other_string, "QString");

    bool boolean = true;
    sb->setSetting<bool>("boolean", boolean);
    bool other_boolean = sb->setting<bool>("boolean");
    QVERIFY2(boolean == other_boolean, "bool");

    int integer = 1;
    sb->setSetting<int>("integer", integer);
    int other_integer = sb->setting<int>("integer");
    QVERIFY2(integer == other_integer, "int");

    float f = 1.5f;
    sb->setSetting<float>("float", f);
    float other_f = sb->setting<float>("float");
    QVERIFY2(f == other_f, "float");

    double d = 1.0;
    sb->setSetting<double>("double", d);
    double other_d = sb->setting<double>("double");
    QVERIFY2(d == other_d, "double");

    QVector3D qvector_3d(1.0, 2.0, -45.0);
    sb->setSetting<QVector3D>("qvector_3d", qvector_3d);
    QVector3D other_qvector_3d = sb->setting<QVector3D>("qvector_3d");
    QVERIFY2(qvector_3d == other_qvector_3d, "QVector3D");

    QQuaternion qquaternion(1.0, 0.5, -0.5, 1.0);
    sb->setSetting<QQuaternion>("qquaternion", qquaternion);
    QQuaternion other_qquaternion = sb->setting<QQuaternion>("qquaternion");
    QVERIFY2(qquaternion == other_qquaternion, "QQuaternion");
}

void TestSettingsBase::testParent()
{
    SettingsBase* parent = new SettingsBase();
    SettingsBase* sb = new SettingsBase(parent);

    bool from_parent = false;
    Distance distance = 1 * in;
    parent->setSetting<Distance>("distance", distance);
    Distance other_distance = sb->setting<Distance>("distance", from_parent);
    QCOMPARE(distance, other_distance);
    QVERIFY(from_parent);
    other_distance = sb->setting<Distance>("distance");
    QCOMPARE(distance, other_distance);

    from_parent = false;

    Velocity velocity = 1 * in / s;
    parent->setSetting<Velocity>("velocity", velocity);
    Velocity other_velocity = sb->setting<Velocity>("velocity", from_parent);
    QCOMPARE(velocity, other_velocity);
    QVERIFY(from_parent);
    other_velocity = sb->setting<Velocity>("velocity");
    QCOMPARE(velocity, other_velocity);

    from_parent = false;

    Acceleration acceleration = 1 * in / s / s;
    parent->setSetting<Acceleration>("acceleration", acceleration);
    Acceleration other_acceleration = sb->setting<Acceleration>("acceleration", from_parent);
    QCOMPARE(acceleration, other_acceleration);
    QVERIFY(from_parent);
    other_acceleration = sb->setting<Acceleration>("acceleration");
    QCOMPARE(acceleration, other_acceleration);

    from_parent = false;

    Time time = 1 * s;
    parent->setSetting<Time>("time", time);
    Time other_time = sb->setting<Time>("time", from_parent);
    QCOMPARE(time, other_time);
    QVERIFY(from_parent);
    other_time = sb->setting<Time>("time");
    QCOMPARE(time, other_time);

    from_parent = false;

    Angle angle = 1 * deg;
    parent->setSetting<Angle>("angle", angle);
    Angle other_angle = sb->setting<Angle>("angle", from_parent);
    QCOMPARE(angle, other_angle);
    QVERIFY(from_parent);
    other_angle = sb->setting<Angle>("angle");
    QCOMPARE(angle, other_angle);

    from_parent = false;

    QString string = "something something";
    parent->setSetting<QString>("string", string);
    QString other_string = sb->setting<QString>("string", from_parent);
    QCOMPARE(string, other_string);
    QVERIFY(from_parent);
    other_string = sb->setting<QString>("string");
    QCOMPARE(string, other_string);

    from_parent = false;

    bool boolean = true;
    parent->setSetting<bool>("boolean", boolean);
    bool other_boolean = sb->setting<bool>("boolean", from_parent);
    QCOMPARE(boolean, other_boolean);
    QVERIFY(from_parent);
    other_boolean = sb->setting<bool>("boolean");
    QCOMPARE(boolean, other_boolean);

    from_parent = false;

    int integer = 1;
    parent->setSetting<int>("integer", integer);
    int other_integer = sb->setting<int>("integer", from_parent);
    QCOMPARE(integer, other_integer);
    QVERIFY(from_parent);
    other_integer = sb->setting<int>("integer");
    QCOMPARE(integer, other_integer);

    from_parent = false;

    float f = 1.5f;
    parent->setSetting<float>("float", f);
    float other_f = sb->setting<float>("float", from_parent);
    QCOMPARE(f, other_f);
    QVERIFY(from_parent);
    other_f = sb->setting<float>("float");
    QCOMPARE(f, other_f);

    from_parent = false;

    double d = 1.0;
    parent->setSetting<double>("double", d);
    double other_d = sb->setting<double>("double", from_parent);
    QCOMPARE(d, other_d);
    QVERIFY(from_parent);
    other_d = sb->setting<double>("double");
    QCOMPARE(d, other_d);

    from_parent = false;

    QVector3D qvector_3d(1.0, 2.0, -45.0);
    parent->setSetting<QVector3D>("qvector_3d", qvector_3d);
    QVector3D other_qvector_3d = sb->setting<QVector3D>("qvector_3d", from_parent);
    QCOMPARE(qvector_3d, other_qvector_3d);
    QVERIFY(from_parent);
    other_qvector_3d = sb->setting<QVector3D>("qvector_3d");
    QCOMPARE(qvector_3d, other_qvector_3d);

    from_parent = false;

    QQuaternion qquaternion(1.0, 0.5, -0.5, 1.0);
    parent->setSetting<QQuaternion>("qquaternion", qquaternion);
    QQuaternion other_qquaternion = sb->setting<QQuaternion>("qquaternion", from_parent);
    QCOMPARE(qquaternion, other_qquaternion);
    QVERIFY(from_parent);
    other_qquaternion = sb->setting<QQuaternion>("qquaternion");
    QCOMPARE(qquaternion, other_qquaternion);
}


//QTEST_MAIN(TestSettingsBase)

#endif
