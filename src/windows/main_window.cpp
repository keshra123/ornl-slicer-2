#include "windows/main_window.h"

#include <QCloseEvent>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QSignalMapper>
#include <QStandardPaths>

#include "managers/preferences_manager.h"
#include "threading/mesh_loader.h"
#include "threading/project_exporter.h"
#include "threading/project_loader.h"
#include "threading/slicing_thread.h"
#include "ui_main_window.h"
#include "widgets/debug_view_widget.h"
#include "widgets/meshviewcontroller.h"

namespace ORNL
{
    MainWindow::MainWindow(QWidget* parent)
        : QMainWindow(parent)
        , ui(new Ui::MainWindow)
        , m_window_manager(WindowManager::getInstance())
        , m_project_manager(ProjectManager::getInstance())
        , m_settings_manager(GlobalSettingsManager::getInstance())
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_mesh_view_controller(new MeshViewController)
    {
        ui->setupUi(this);

        // If in a debug build configuration setup the debug_tree_widget,
        // otherwise remove the debug tab
#ifdef NDEBUG
        // Remove debug tab
        ui->tab_widget->removeTab(3);
#else
        connect(ui->debug_tree_widget,
                SIGNAL(newSelection()),
                ui->debug_view_widget,
                SLOT(update()));
#endif

        // Create the app data folder for ORNL Slicer 2 if it doesn't exist
        QDir app_data(
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        if (!app_data.exists())
        {
            app_data.mkpath(".");
        }

        m_preferences_manager->importPreferences();

        // File Menu
        connect(
            ui->open_model, SIGNAL(triggered(bool)), this, SLOT(openModel()));
        connect(ui->open_project,
                SIGNAL(triggered(bool)),
                this,
                SLOT(openProject()));
        connect(ui->save_project,
                SIGNAL(triggered(bool)),
                this,
                SLOT(saveProject()));
        connect(ui->close_project,
                SIGNAL(triggered(bool)),
                m_project_manager.data(),
                SLOT(closeProject()));
        connect(ui->import_gcode,
                SIGNAL(triggered(bool)),
                this,
                SLOT(importGcode()));
        connect(ui->export_gcode,
                SIGNAL(triggered(bool)),
                this,
                SLOT(exportGcode()));
        connect(ui->preferences,
                SIGNAL(triggered(bool)),
                m_window_manager.data(),
                SLOT(createPreferencesWindow()));
        // Edit Menu
        connect(m_project_manager.data(),
                SIGNAL(enableCopy(bool)),
                ui->copy,
                SLOT(setEnabled(bool)));
        connect(ui->copy,
                SIGNAL(triggered(bool)),
                m_project_manager.data(),
                SLOT(copySelectedMesh()));
        connect(m_project_manager.data(),
                SIGNAL(enablePaste(bool)),
                ui->paste,
                SLOT(setEnabled(bool)));
        connect(ui->paste,
                SIGNAL(triggered(bool)),
                m_project_manager.data(),
                SLOT(paste()));
        // View Menu
        /*
        connect(ui->back_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewPlusY()));
        connect(ui->front_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewMinusY()));
        connect(ui->right_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewPlusX()));
        connect(ui->left_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewMinusX()));
        connect(ui->top_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewPlusZ()));
        connect(ui->bottom_orientation, SIGNAL(triggered(bool)),
        ui->model_placement_widget, SLOT(viewMinusZ()));
        */

        // Global Settings Comboboxes
        connect(ui->machine_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_project_manager.data(),
                SLOT(currentMachineName(QString)));
        connect(ui->material_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_project_manager.data(),
                SLOT(currentMaterialName(QString)));
        connect(ui->material_combobox,
                SIGNAL(currentIndexChanged(QString)),
                this,
                SLOT(handleNozzleListUpdate(QString)));
        connect(ui->print_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_project_manager.data(),
                SLOT(currentPrintName(QString)));
        connect(ui->nozzle_combobox,
                SIGNAL(currentIndexChanged(QString)),
                m_project_manager.data(),
                SLOT(currentNozzleName(QString)));

        ui->slicing_progress_bar->hide();
        QSharedPointer< SlicingThread > slicing_thread =
            SlicingThread::getInstance();
        connect(slicing_thread.data(),
                SIGNAL(sliceStarting()),
                ui->slicing_progress_bar,
                SLOT(show()));
        connect(slicing_thread.data(),
                SIGNAL(sliceFinished()),
                ui->slicing_progress_bar,
                SLOT(hide()));
        connect(slicing_thread.data(),
                SIGNAL(total(int)),
                ui->slicing_progress_bar,
                SLOT(setMaximum(int)));
        connect(slicing_thread.data(),
                SIGNAL(progress(int, int)),
                ui->slicing_progress_bar,
                SLOT(setValue(int)));

        m_gcode_controller = new GcodeController(this, ui->gcode_text_edit);

        reloadSettings();

        loadRecentProjects();

        ui->mesh_transform_widget->hide();

        // Added the mesh view widget
        QHBoxLayout* h_layout =
            static_cast< QHBoxLayout* >(ui->mesh_view_tab->layout());
        h_layout->addWidget(m_mesh_view_controller->widget());
        h_layout->setStretch(1, 4);
        connect(m_project_manager.data(),
                SIGNAL(sendMeshGraphics(MeshGraphics*)),
                m_mesh_view_controller.data(),
                SLOT(addToWindow(MeshGraphics*)));

        setInitalGcodeSize();

        setFocus();
    }

    MainWindow::~MainWindow()
    {
        delete ui;
    }

    void MainWindow::reloadSettings()
    {
        // Reset the list on the Machine combobox
        m_machine_options =
            m_settings_manager->getConfigsNames(ConfigTypes::kMachine);
        QString current_machine = ui->machine_combobox->currentText();
        ui->machine_combobox->clear();
        ui->machine_combobox->addItems(m_machine_options);
        if (m_machine_options.contains(current_machine))
        {
            ui->machine_combobox->setCurrentText(current_machine);
            m_project_manager->currentMachineName(current_machine);
        }

        // Reset the actions on the machine settings menu
        ui->machines_menu->clear();
        QAction* action;
        QSignalMapper* editSignalMapper = new QSignalMapper(this);
        QSignalMapper* addSignalMapper  = new QSignalMapper(this);
        foreach (QString machine, m_machine_options)
        {
            action = new QAction();
            action->setIcon(QIcon(":/icons/edit_property.png"));
            action->setText(machine);
            connect(
                action, SIGNAL(triggered(bool)), editSignalMapper, SLOT(map()));
            editSignalMapper->setMapping(action, "MACHINE" + machine);
            ui->machines_menu->addAction(action);
        }

        // Add the "Add Machine" action
        ui->machines_menu->addSeparator();
        action = new QAction();
        action->setText("Add Machine");
        action->setIcon(QIcon(":/icons/plus.png"));
        connect(action, SIGNAL(triggered(bool)), addSignalMapper, SLOT(map()));
        addSignalMapper->setMapping(action,
                                    static_cast< int >(ConfigTypes::kMachine));
        ui->machines_menu->addAction(action);

        // Reset the list on the material combobox
        m_material_options =
            m_settings_manager->getConfigsNames(ConfigTypes::kMaterial);
        QString current_material = ui->material_combobox->currentText();
        ui->material_combobox->clear();
        ui->material_combobox->addItems(m_material_options);
        if (m_material_options.contains(current_material))
        {
            ui->material_combobox->setCurrentText(current_material);
        }
        else
        {
            current_material = ui->material_combobox->currentText();
        }

        // Reset the list on the nozzle combobox
        QString current_nozzle = ui->nozzle_combobox->currentText();
        m_nozzle_options.clear();
        m_nozzle_options = m_settings_manager->getNozzleNames();
        if (!current_material.isEmpty())
        {
            ui->nozzle_combobox->addItems(
                m_nozzle_options.values(current_material));
            if (m_nozzle_options.contains(current_material) &&
                m_nozzle_options.values(current_material)
                    .contains(current_nozzle))
            {
                ui->nozzle_combobox->setCurrentText(current_nozzle);
                m_project_manager->currentNozzleName(current_nozzle);
            }
        }

        // Reset the actions on the material settings menu
        ui->material_menu->clear();
        QMenu* menu;
        for (QString material :
             m_settings_manager->getConfigsNames(ConfigTypes::kMaterial))
        {
            menu = new QMenu(material, ui->material_menu);
            ui->material_menu->addMenu(menu);

            action = new QAction(menu);
            action->setIcon(QIcon(":/icons/edit_property.png"));
            action->setText("Edit Material");
            connect(
                action, SIGNAL(triggered(bool)), editSignalMapper, SLOT(map()));
            editSignalMapper->setMapping(action, "MATERIAL" + material);
            menu->addAction(action);

            menu->addSeparator();

            if (m_nozzle_options.contains(material))
            {
                for (QString nozzle_name : m_nozzle_options.values(material))
                {
                    action = new QAction(menu);
                    action->setIcon(QIcon(":/icons/edit_property.png"));
                    action->setText(nozzle_name);
                    connect(action,
                            SIGNAL(triggered(bool)),
                            editSignalMapper,
                            SLOT(map()));
                    editSignalMapper->setMapping(
                        action, "NOZZLE" + material + ":" + nozzle_name);
                    menu->addAction(action);
                }
            }

            menu->addSeparator();

            action = new QAction(menu);
            action->setText("Add Nozzle");
            action->setIcon(QIcon(":/icons/plus.png"));
            connect(
                action, SIGNAL(triggered(bool)), addSignalMapper, SLOT(map()));
            addSignalMapper->setMapping(
                action, static_cast< int >(ConfigTypes::kNozzle));
            menu->addAction(action);
        }

        // Add the "Add Material" action
        ui->material_menu->addSeparator();
        action = new QAction();
        action->setText("Add Material");
        action->setIcon(QIcon(":/icons/plus.png"));
        connect(action, SIGNAL(triggered(bool)), addSignalMapper, SLOT(map()));
        addSignalMapper->setMapping(action,
                                    static_cast< int >(ConfigTypes::kMaterial));
        ui->material_menu->addAction(action);

        // Reset the list on the print combobox
        m_print_options =
            m_settings_manager->getConfigsNames(ConfigTypes::kPrint);
        QString current_print = ui->print_combobox->currentText();
        ui->print_combobox->clear();
        ui->print_combobox->addItems(m_print_options);
        if (m_print_options.contains(current_print))
        {
            ui->print_combobox->setCurrentText(current_print);
            m_project_manager->currentPrintName(current_print);
        }

        // Reset the actions on the print settings menu
        ui->print_menu->clear();
        for (QString print :
             m_settings_manager->getConfigsNames(ConfigTypes::kPrint))
        {
            action = new QAction();
            action->setIcon(QIcon(":/icons/edit_property.png"));
            action->setText(print);
            connect(
                action, SIGNAL(triggered(bool)), editSignalMapper, SLOT(map()));
            editSignalMapper->setMapping(action, "PRINT" + print);
            ui->print_menu->addAction(action);
        }

        // Add the "Add Print" action
        ui->print_menu->addSeparator();
        action = new QAction();
        action->setText("Add Print");
        action->setIcon(QIcon(":/icons/plus.png"));
        connect(action, SIGNAL(triggered(bool)), addSignalMapper, SLOT(map()));
        addSignalMapper->setMapping(action,
                                    static_cast< int >(ConfigTypes::kPrint));
        ui->print_menu->addAction(action);

        connect(editSignalMapper,
                SIGNAL(mapped(QString)),
                m_window_manager.data(),
                SLOT(createGlobalSettingsWindow(QString)));
        connect(addSignalMapper,
                SIGNAL(mapped(int)),
                m_window_manager.data(),
                SLOT(createNameConfigDialog(int)));

        // ui->model_placement_widget->reloadMachine();
    }

    void MainWindow::openModel()
    {
        QString filepath = QFileDialog::getOpenFileName(
            this,
            "Load .stl file",
            QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
            "STL (*.stl)");
        if (!filepath.isNull())
        {
            MeshLoader* loader = new MeshLoader(this, filepath);
            m_window_manager->createLoadingDialog(filepath);
            connect(loader,
                    SIGNAL(errorLoadingFile(QString, QString)),
                    m_window_manager.data(),
                    SLOT(createErrorDialog(QString, QString)));

            /* Register the metatype */
            qRegisterMetaType< QSharedPointer< ORNL::Mesh > >(
                "shared_mesh_ptr");

            connect(loader,
                    SIGNAL(finishedMesh(QSharedPointer< ORNL::Mesh >)),
                    m_project_manager.data(),
                    SLOT(addMesh(QSharedPointer< ORNL::Mesh >)));
            connect(loader,
                    SIGNAL(finishedLoading()),
                    m_window_manager.data(),
                    SLOT(removeLoadingDialog()));
            connect(
                loader, SIGNAL(finishedLoading()), loader, SLOT(deleteLater()));
            loader->start();
        }
    }

    void MainWindow::openProject(QString filepath)
    {
        m_project_manager->closeProject();
        if (filepath.isEmpty())
        {
            filepath = QFileDialog::getOpenFileName(
                this,
                "Load project file",
                QStandardPaths::writableLocation(
                    QStandardPaths::DocumentsLocation) +
                    QDir::separator() + "OS2_Projects",
                "OS2 (*.os2)");
        }
        if (!filepath.isNull())
        {
            ProjectLoader* loader = new ProjectLoader(this, filepath);
            m_window_manager->createLoadingDialog(filepath);
            connect(loader,
                    SIGNAL(errorLoadingFile(QString, QString)),
                    m_window_manager.data(),
                    SLOT(createErrorDialog(QString, QString)));
            connect(loader,
                    SIGNAL(finished()),
                    m_window_manager.data(),
                    SLOT(removeLoadingDialog()));
            connect(loader, SIGNAL(finished()), loader, SLOT(deleteLater()));
            loader->start();
        }
    }

    void MainWindow::saveProject()
    {
        ProjectExporter* exporter;
        QString filepath;
        if (m_project_manager->name().isEmpty())
        {
            filepath = QFileDialog::getSaveFileName(
                this,
                "Save project file",
                QStandardPaths::writableLocation(
                    QStandardPaths::DocumentsLocation) +
                    QDir::separator() + "OS2_Projects");
            if (filepath.isNull() || filepath.isEmpty())
            {
                return;
            }
            exporter = new ProjectExporter(this, filepath);
        }
        else
        {
            filepath = m_project_manager->filepath();
            exporter = new ProjectExporter(this);
        }

        m_window_manager->createSavingDialog(filepath);
        connect(exporter,
                SIGNAL(errorSavingFile(QString, QString)),
                m_window_manager.data(),
                SLOT(createErrorDialog(QString, QString)));
        connect(exporter,
                SIGNAL(finished()),
                m_window_manager.data(),
                SLOT(removeSavingDialog()));
        connect(exporter, SIGNAL(finished()), exporter, SLOT(deleteLater()));
        connect(exporter, SIGNAL(finished()), this, SLOT(loadRecentProjects()));
        exporter->start();
    }

    void MainWindow::importGcode()
    {
        QString filepath = QFileDialog::getOpenFileName(
            this,
            "Load gcode file",
            QStandardPaths::writableLocation(
                QStandardPaths::DocumentsLocation));
        if (!filepath.isNull())
        {
            GcodeLoader* loader = new GcodeLoader(filepath, this);
            m_window_manager->createLoadingDialog(filepath);
            connect(loader,
                    SIGNAL(errorLoadingFile(QString, QString)),
                    m_window_manager.data(),
                    SLOT(createErrorDialog(QString, QString)));
            connect(loader,
                    SIGNAL(finishedGcode(QStringList, GcodeSyntax)),
                    m_gcode_controller,
                    SLOT(setGcode(QStringList, GcodeSyntax)));
            connect(loader,
                    SIGNAL(finished()),
                    m_window_manager.data(),
                    SLOT(removeLoadingDialog()));
            connect(loader, SIGNAL(finished()), loader, SLOT(deleteLater()));
            loader->run();
            delete loader;
        }
    }

    void MainWindow::exportGcode()
    {
        // TODO
    }

    void MainWindow::handleNozzleListUpdate(QString material)
    {
        ui->nozzle_combobox->clear();
        ui->nozzle_combobox->addItems(m_nozzle_options.values(material));
    }

    void MainWindow::loadRecentProjects()
    {
        ui->recent_projects_menu->clear();

        QVector< QPair< QString, QString > > recent_projects =
            m_project_manager->recentProjects();


        if (recent_projects.size())
        {
            QSignalMapper* signal_mapper = new QSignalMapper();

            for (QPair< QString, QString > project : recent_projects)
            {
                QAction* action = new QAction();
                action->setText(
                    QString("%1 - %2").arg(project.second, project.first));

                connect(action,
                        SIGNAL(triggered(bool)),
                        signal_mapper,
                        SLOT(map()));
                signal_mapper->setMapping(
                    action,
                    QString("%1/%2.os2").arg(project.first, project.second));
                ui->recent_projects_menu->addAction(action);
            }

            connect(signal_mapper,
                    SIGNAL(mapped(QString)),
                    this,
                    SLOT(openProject(QString)));
            ui->recent_projects_menu->setEnabled(true);
        }
        else
        {
            ui->recent_projects_menu->setEnabled(false);
        }
    }

    void MainWindow::closeEvent(QCloseEvent* event)
    {
        // Export everything that needs it upon exitting
        m_project_manager->exportRecentProjects();
        m_preferences_manager->exportPreferences();
        event->accept();
        qApp->exit();
    }

    void MainWindow::setInitalGcodeSize()
    {
        // TODO: Make this happen with scaling screen sizes and differing order
        // of widgets.
        const int kTEXTBOX_SIZE = 300;
        const int kVIEWER_SIZE  = 700;


        ui->gcode_splitter->setSizes(QList< int >{kVIEWER_SIZE, kTEXTBOX_SIZE});
    }

}  // namespace ORNL
