var class_display_config_base =
[
    [ "DisplayConfigBase", "class_display_config_base.html#ae2a27826600934c09f7525a0ed1f2c5c", null ],
    [ "addSetting", "class_display_config_base.html#a6275976d45dba2ee5805e5e747c3c624", null ],
    [ "deleteFile", "class_display_config_base.html#a48fbc2059d8cac989cd43cd0465c4ed2", null ],
    [ "getCategorySettings", "class_display_config_base.html#aa79fd5e261c7a20d0b21c205d0e8e05f", null ],
    [ "getExtension", "class_display_config_base.html#a0d8a21384b2bfb99780eb1028d238342", null ],
    [ "getName", "class_display_config_base.html#a8f42ad53d51fc749ab207cb561b762e8", null ],
    [ "getPath", "class_display_config_base.html#a9124385f975d46a97e76bc559d9329bc", null ],
    [ "loadFromFile", "class_display_config_base.html#a3414c79e9dc0ca14997c5c8ea7c0548f", null ],
    [ "saveToFile", "class_display_config_base.html#a66f8b785dbaef2b6272aafab1c6f7def", null ],
    [ "mConfig", "class_display_config_base.html#a199d94cd2c3bdb7f0764b59f1ee6c178", null ],
    [ "mExtension", "class_display_config_base.html#a2556e80e633c3ef6475edb0276e50c19", null ],
    [ "mName", "class_display_config_base.html#a37267cca37cfef54b1eed1c23e4d4bb3", null ],
    [ "mPath", "class_display_config_base.html#abaf85559e13c3f36f4f77414b888fb60", null ]
];