#ifndef BLUE_GANTRY_WRITER_H
#define BLUE_GANTRY_WRITER_H

//! \file blue_gantry_writer.h

#include "writer_base.h"

namespace ORNL
{
    /*!
     * \class BlueGantry
     * \brief The gcode writer for ORNL's Blue Gantry syntax
     */
    class BlueGantryWriter : public WriterBase
    {
    public:
        //! \brief Constructor
        BlueGantryWriter() = default;

        //! \brief Write the header to the file
        QString header();

        //! \brief Write the line for a layer change
        QString onLayerChange();

        //! \brief Write the footer to the file
        QString footer();

        //! \brief Write a path segment
        QString pathSegment(PathSegment& path_segment);

        //! \brief write a line path
        QString line(Line& line);

        //! \brief write an arc path
        QString arc(Arc& arc);

        //! \brief Write a comment
        QString comment(QString text);
    };  // class BlueGantryWriter
}  // namespace ORNL
#endif  // BLUE_GANTRY_WRITER_H
