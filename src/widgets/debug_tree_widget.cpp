#include "widgets/debug_tree_widget.h"

#ifdef DEBUG
#    include <QMenu>
#    include <QMouseEvent>

#    include "gcode/gcode.h"
#    include "gcode/gcode_layer.h"
#    include "layer_regions/island.h"
#    include "layer_regions/perimeters.h"
#    include "threading/slicing_thread.h"
#    include "utilities/constants.h"
#endif  // NDEBUG

namespace ORNL
{
    DebugTreeWidget::DebugTreeWidget(QWidget* parent)
        : QTreeWidget(parent)
#ifndef DEBUG
    {}
#else
        , m_project_manager(ProjectManager::getInstance())
    {
        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(SlicingThread::getInstance().data(),
                SIGNAL(sliceFinished()),
                this,
                SLOT(handleSliceUpdate()));
    }

    void DebugTreeWidget::handleSliceUpdate()
    {
        clear();
        QSharedPointer< Gcode > gcode = m_project_manager->gcode();
        QList< QTreeWidgetItem* > top_level_items;
        for (QSharedPointer< GcodeLayer > layer : (*gcode))
        {
            QTreeWidgetItem* layer_twi = new QTreeWidgetItem(
                this,
                QStringList(
                    {QString::asprintf("Layer %d", layer->layerNumber() + 1),
                     "layer"}));
            for (int island_nr = 0; island_nr < layer->size(); island_nr++)
            {
                QTreeWidgetItem* island_twi = new QTreeWidgetItem(
                    layer_twi,
                    QStringList({QString::asprintf("Island %d", island_nr + 1),
                                 "island"}));
                layer_twi->addChild(island_twi);

                QSharedPointer< Island > island = (*layer)[island_nr];

                if (island->setting< bool >(
                        Constants::PrintSettings::Perimeters::kEnable))
                {
                    QStringList perimeter_debug_options =
                        island->perimeters()->debugOptions();
                    QTreeWidgetItem* perimeter_twi = new QTreeWidgetItem(
                        island_twi,
                        QStringList(
                            {"Perimeter", toString(RegionType::kPerimeter)}));

                    for (QString debug_option : perimeter_debug_options)
                    {
                        QTreeWidgetItem* debug_twi = new QTreeWidgetItem(
                            perimeter_twi,
                            QStringList({debug_option, "debug"}));
                        perimeter_twi->addChild(debug_twi);
                    }
                }

                if (island->setting< bool >(
                        Constants::PrintSettings::Insets::kEnable))
                {
                    QStringList inset_debug_options =
                        island->insets()->debugOptions();
                    QTreeWidgetItem* inset_twi = new QTreeWidgetItem(
                        island_twi,
                        QStringList({"Inset", toString(RegionType::kInset)}));

                    for (QString debug_option : inset_debug_options)
                    {
                        QTreeWidgetItem* debug_twi = new QTreeWidgetItem(
                            inset_twi, QStringList({debug_option, "debug"}));
                        inset_twi->addChild(debug_twi);
                    }
                }

                // TODO: Insets, Infill, and Skins
            }
            top_level_items.append(layer_twi);
        }
        addTopLevelItems(top_level_items);
    }

    // happens on right click
    void DebugTreeWidget::showContextMenu(const QPoint& point)
    {
        QMenu context_menu(tr("Context menu"), this);

        QTreeWidgetItem* twi = itemAt(point);

        context_menu.exec(mapToGlobal(point));
    }

    // happens on left click
    void DebugTreeWidget::selected(QTreeWidgetItem* twi)
    {
        if (twi->text(1) == "debug")
        {
            QString debug = twi->text(0);
            if (debug.isEmpty())
            {
                deselected();
                return;
            }

            QTreeWidgetItem* region = twi->parent();
            RegionType region_type;
            try
            {
                region_type = fromString(region->text(1));
            }
            catch (UnknownRegionTypeException e)
            {
                qWarning() << e.what();
                deselected();
                return;
            }

            QTreeWidgetItem* island = region->parent();
            bool ok                 = false;
            uint island_nr          = island->text(0).mid(7).toUInt(&ok);
            if (!ok)
            {
                deselected();
                return;
            }
            island_nr--;  // convert to zero index

            QTreeWidgetItem* layer = island->parent();
            ok                     = false;
            uint layer_nr          = layer->text(0).mid(6).toUInt(&ok);
            if (!ok)
            {
                deselected();
                return;
            }
            layer_nr--;  // convert to zero index

            m_project_manager->selectDebug(
                layer_nr, island_nr, region_type, debug);

            emit newSelection();
        }
        else
        {
            deselected();
        }
    }

    void DebugTreeWidget::deselected()
    {
        clearSelection();
        m_project_manager->deselectDebug();
    }

    void DebugTreeWidget::mousePressEvent(QMouseEvent* event)
    {
        // Right clicks bring up the context menu, but don't affect selection
        if (event->button() == Qt::RightButton)
        {
            showContextMenu(event->pos());
            return;
        }

        // If the expand/collapse arrow is clicked let the QTreeWidget handle it
        // No selection changes
        QModelIndex clickedIndex = indexAt(event->pos());
        if (clickedIndex.isValid())
        {
            QRect vrect        = visualRect(clickedIndex);
            int itemIdentation = vrect.x() - visualRect(rootIndex()).x();
            if (event->pos().x() < itemIdentation)
            {
                QTreeWidget::mousePressEvent(event);
                return;
            }
        }

        QTreeWidgetItem* item = itemAt(event->pos());

        // If a new item is selected then mark it as selected
        if (item)
        {
            if (!item->isSelected())
            {
                selected(item);
            }
        }
        // If anywhere that isn't an item is selected clear the selection
        else
        {
            deselected();
        }

        QTreeWidget::mousePressEvent(event);
    }

    void DebugTreeWidget::keyPressEvent(QKeyEvent* event)
    {
        // Handles the selection changing
        QTreeWidget::keyPressEvent(event);

        if (event->key() == Qt::Key_Down || event->key() == Qt::Key_Up)
        {
            QTreeWidgetItem* twi = currentItem();
            selected(twi);
        }
    }
#endif  // QT_DEBUG
}  // namespace ORNL
