#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//! \file mainwindow.h

#include <QMainWindow>
#include <QMap>

#include "controllers/gcodecontroller.h"

class QActionGroup;

namespace Ui
{
    class MainWindow;
}

namespace ORNL
{
    class WindowManager;
    class EngineManager;
    class GlobalSettingsManager;
    class ProjectManager;
    class PreferencesManager;
    class MeshViewController;

    /*!
     * \class MainWindow
     * \brief The main window that hold most of the widgets and from which
     * everything starts.
     *
     * This window is the main interface and has multiple tabs for manipulating
     * the STLs that are loading, the gcode that is generated, and debugging the
     * internal slicing computations. It also lets the user select settings and
     * is the main way for the use to interface with the program.
     */
    class MainWindow : public QMainWindow
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit MainWindow(QWidget* parent = nullptr);

        //! \brief Destructor
        ~MainWindow();

        //! \brief Reload all the configs for selection
        void reloadSettings();

    private slots:
        // File Menu
        //! \brief Handle opening a model (usually an STL model)
        void openModel();

        //! \brief Handle opening a project (loads in STL and settings for
        //! previous worked on project)
        void openProject(QString filepath = "");

        //! \brief Handle saving a project
        void saveProject();

        //! \brief Handle importing gcode
        void importGcode();

        //! \brief Handle exporting gcode
        void exportGcode();

        //! \brief Handle updating the list of nozzle names when the material
        //! changes
        void handleNozzleListUpdate(QString material);

        //! \brief loads the recent projects to the menu
        void loadRecentProjects();

    private:
        //! \brief Handles closing of the window and clearing the pointer in the
        //! window manager
        void closeEvent(QCloseEvent* event);

        //! \brief Handles setting the inital Gcode Textbox/viewing widget
        //! sizes.
        void setInitalGcodeSize();

        Ui::MainWindow* ui;
        QSharedPointer< WindowManager > m_window_manager;
        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;
        QSharedPointer< EngineManager > m_engine_manager;

        GcodeController* m_gcode_controller;

        QActionGroup* m_machine_group;
        QActionGroup* m_material_group;
        QActionGroup* m_print_group;

        QStringList m_machine_options;
        QStringList m_material_options;
        QStringList m_print_options;
        QMultiMap< QString, QString >
            m_nozzle_options;  //!< Keys are the material names

        QSharedPointer< MeshViewController > m_mesh_view_controller;
    };  // class MainWindow
}  // namespace ORNL
#endif  // MAINWINDOW_H
