#ifndef MODELADJUSTMENTWIDGET_H
#define MODELADJUSTMENTWIDGET_H

//! \file modeladjustmentwidget.h

#include <QWidget>
#include <Qt3DCore/QTransform>

#include "geometry/mesh.h"
#include "managers/project_manager.h"
#include "utilities/constants.h"

namespace Ui
{
    class MeshTransformWidget;
}

namespace ORNL
{
    /*!
     * \class ModelAdjustmentWidget
     * \brief Widget for modifying translation, rotation, scale, unit, and
     * settings for a model/mesh
     */
    class MeshTransformWidget : public QWidget
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit MeshTransformWidget(QWidget* parent = nullptr);

        //! \brief Destructor
        ~MeshTransformWidget();

    public slots:
        //! \brief Reload inputs by changing the mesh
        void changeMesh();

        //! \brief handles the uniform scale checkbox
        void scaleUniformChanged(bool);

        //! \brief updates the translation distance unit label
        void updateDistanceUnit();

        //! \brief updates the translation distance unit label
        void updateDistanceUnit(Distance new_unit, Distance old_unit);

        //! \brief updates the rotation angle unit label
        void updateAngleUnit();

        //! \brief updates the rotation angle unit label
        void updateAngleUnit(Angle new_unit, Angle old_unit);

        //! \brief Modify the x translation for the selected mesh
        void translationXEdited(QString x);

        //! \brief Modify the y translation for the selected mesh
        void translationYEdited(QString y);

        //! \brief Modify the z translation for the selected mesh
        void translationZEdited(QString z);

        //! \brief Modify the x rotation for the selected mesh
        void rotationXEdited(QString x);

        //! \brief Modify the y rotation for the selected mesh
        void rotationYEdited(QString y);

        //! \brief Modify the z rotation for the selected mesh
        void rotationZEdited(QString z);

        //! \brief Modify the x scale for the selected mesh
        void scaleXEdited(QString x);

        //! \brief Modify the y scale for the selected mesh
        void scaleYEdited(QString y);

        //! \brief Modify the z scale for the selected mesh
        void scaleZEdited(QString z);

        //! \brief Modify the unit for the selected mesh
        void unitChanged(QString unit);

    private:
        Ui::MeshTransformWidget* ui;
        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;

        QSharedPointer< Mesh > m_mesh;
    };  // class ModelAdjustmentWidget
}  // namespace ORNL
#endif  // MODELADJUSTMENTWIDGET_H
