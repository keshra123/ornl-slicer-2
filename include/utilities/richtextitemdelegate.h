#ifndef RICHTEXTITEMDELEGATE_H
#define RICHTEXTITEMDELEGATE_H

#include <QAbstractTextDocumentLayout>
#include <QApplication>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QTextDocument>

namespace ORNL
{
    /*!
     * \class RichTextItemDelegate
     *
     * \brief A delegate for QComboBox, QListView, etc that support rich text
     * (html)
     */
    class RichTextItemDelegate : public QStyledItemDelegate
    {
    protected:
        void paint(QPainter* painter,
                   const QStyleOptionViewItem& option,
                   const QModelIndex& index) const;
        QSize sizeHint(const QStyleOptionViewItem& option,
                       const QModelIndex& index) const;
    };
}  // namespace ORNL


#endif  // RICHTEXTITEMDELEGATE_H
