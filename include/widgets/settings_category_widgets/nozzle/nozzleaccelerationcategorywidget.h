#ifndef NOZZLEACCELERATIONCATEGORYWIDGET_H
#define NOZZLEACCELERATIONCATEGORYWIDGET_H

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class NozzleAccelerationCategoryWidget;
}

namespace ORNL
{
    class NozzleAccelerationCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        NozzleAccelerationCategoryWidget();

        ~NozzleAccelerationCategoryWidget();

        void handleConditions();

    private:
        Ui::NozzleAccelerationCategoryWidget* ui;
    };
}  // namespace ORNL


#endif  // NOZZLEACELERATIONCATEGORYWIDGET_H
