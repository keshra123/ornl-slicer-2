#include "managers/preferences_manager.h"

#include <QDir>
#include <QStandardPaths>

#include "exceptions/exceptions.h"
#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    QSharedPointer< PreferencesManager > PreferencesManager::m_singleton =
        QSharedPointer< PreferencesManager >();

    QSharedPointer< PreferencesManager > PreferencesManager::getInstance()
    {
        if (m_singleton.isNull())
        {
            m_singleton.reset(new PreferencesManager());
        }
        return m_singleton;
    }

    PreferencesManager::PreferencesManager()
        : m_distance_unit(in)
        , m_velocity_unit(in / s)
        , m_acceleration_unit(in / s / s)
        , m_angle_unit(deg)
        , m_time_unit(s)
    {}

    void PreferencesManager::importPreferences(QString filepath)
    {
        if (filepath.isEmpty())
        {
            filepath = QStandardPaths::writableLocation(
                           QStandardPaths::AppDataLocation) +
                QDir::separator() + "app.preferences";
        }
        QFile file(filepath);
        if (!file.exists())
        {
            qWarning() << "Prefernces import: file does not exist";
            return;
        }
        file.open(QIODevice::ReadOnly);
        QString preferences = file.readAll();
        json j              = json::parse(preferences.toStdString());
        setDistanceUnit(j["distance"].get< Distance >());
        setVelocityUnit(j["velocity"].get< Velocity >());
        setAccelerationUnit(j["acceleration"].get< Acceleration >());
        setAngleUnit(j["angle"].get< Angle >());
        setTimeUnit(j["time"].get< Time >());
    }

    void PreferencesManager::exportPreferences(QString filepath)
    {
        if (filepath.isEmpty())
        {
            filepath = QStandardPaths::writableLocation(
                           QStandardPaths::AppDataLocation) +
                QDir::separator() + "app.preferences";
        }
        json j;
        j["distance"]     = m_distance_unit;
        j["velocity"]     = m_velocity_unit;
        j["acceleration"] = m_acceleration_unit;
        j["angle"]        = m_angle_unit;
        j["time"]         = m_time_unit;

        QFile file(filepath);
        file.open(QIODevice::WriteOnly);
        file.write(j.dump(4).c_str());
    }

    Distance PreferencesManager::getDistanceUnit()
    {
        return m_distance_unit;
    }

    Velocity PreferencesManager::getVelocityUnit()
    {
        return m_velocity_unit;
    }

    Acceleration PreferencesManager::getAccelerationUnit()
    {
        return m_acceleration_unit;
    }

    Angle PreferencesManager::getAngleUnit()
    {
        return m_angle_unit;
    }

    Time PreferencesManager::getTimeUnit()
    {
        return m_time_unit;
    }

    QString PreferencesManager::getDistanceUnitText()
    {
        return m_distance_unit.toString();
    }

    QString PreferencesManager::getVelocityUnitText()
    {
        return m_velocity_unit.toString();
    }

    QString PreferencesManager::getAccelerationUnitText()
    {
        return m_acceleration_unit.toString();
    }

    QString PreferencesManager::getAngleUnitText()
    {
        return m_angle_unit.toString();
    }

    QString PreferencesManager::getTimeUnitText()
    {
        return m_time_unit.toString();
    }


    void PreferencesManager::setDistanceUnit(QString du)
    {
        try
        {
            setDistanceUnit(Distance::fromString(du));
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
        }
    }

    void PreferencesManager::setDistanceUnit(Distance d)
    {
        Distance old    = m_distance_unit;
        m_distance_unit = d;
        emit distanceUnitChanged(m_distance_unit, old);
    }

    void PreferencesManager::setVelocityUnit(QString vu)
    {
        try
        {
            setVelocityUnit(Velocity::fromString(vu));
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
        }
    }

    void PreferencesManager::setVelocityUnit(Velocity v)
    {
        Velocity old    = m_velocity_unit;
        m_velocity_unit = v;
        emit velocityUnitChanged(m_velocity_unit, old);
    }

    void PreferencesManager::setAccelerationUnit(QString au)
    {
        try
        {
            setAccelerationUnit(Acceleration::fromString(au));
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
        }
    }

    void PreferencesManager::setAccelerationUnit(Acceleration a)
    {
        Acceleration old    = m_acceleration_unit;
        m_acceleration_unit = a;
        emit accelerationUnitChanged(m_acceleration_unit, old);
    }

    void PreferencesManager::setAngleUnit(QString au)
    {
        try
        {
            setAngleUnit(Angle::fromString(au));
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
        }
    }

    void PreferencesManager::setAngleUnit(Angle a)
    {
        Angle old    = m_angle_unit;
        m_angle_unit = a;
        emit angleUnitChanged(m_angle_unit, old);
    }

    void PreferencesManager::setTimeUnit(QString t)
    {
        try
        {
            setTimeUnit(Time::fromString(t));
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
        }
    }

    void PreferencesManager::setTimeUnit(Time t)
    {
        Time old    = m_time_unit;
        m_time_unit = t;
        emit timeUnitChanged(m_time_unit, old);
    }
}  // namespace ORNL
