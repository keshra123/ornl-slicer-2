#include "widgets/meshtransformwidget.h"

#include "exceptions/exceptions.h"
#include "managers/preferences_manager.h"
#include "ui_mesh_transform_widget.h"

namespace ORNL
{
    MeshTransformWidget::MeshTransformWidget(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::MeshTransformWidget)
        , m_project_manager(ProjectManager::getInstance())
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_mesh(nullptr)
    {
        ui->setupUi(this);

        ui->unitsComboBox->addItems(Constants::Units::kDistanceUnits);

        QDoubleValidator* dv = new QDoubleValidator();
        dv->setDecimals(3);
        ui->translationXLineEdit->setValidator(dv);
        ui->translationYLineEdit->setValidator(dv);
        ui->translationZLineEdit->setValidator(dv);
        ui->rotationXLineEdit->setValidator(dv);
        ui->rotationYLineEdit->setValidator(dv);
        ui->rotationZLineEdit->setValidator(dv);
        ui->scaleXLineEdit->setValidator(dv);
        ui->scaleYLineEdit->setValidator(dv);
        ui->scaleZLineEdit->setValidator(dv);

        connect(m_project_manager.data(),
                SIGNAL(meshSelectionChanged()),
                this,
                SLOT(changeMesh()));

        connect(ui->translationXLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(translationXEdited(QString)));
        connect(ui->translationYLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(translationYEdited(QString)));
        connect(ui->translationZLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(translationZEdited(QString)));
        connect(ui->rotationXLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(rotationXEdited(QString)));
        connect(ui->rotationYLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(rotationYEdited(QString)));
        connect(ui->rotationZLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(rotationZEdited(QString)));
        connect(ui->scaleXLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(scaleXEdited(QString)));
        connect(ui->scaleYLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(scaleYEdited(QString)));
        connect(ui->scaleZLineEdit,
                SIGNAL(textEdited(QString)),
                this,
                SLOT(scaleZEdited(QString)));
        connect(ui->scaleUniformCheckbox,
                SIGNAL(toggled(bool)),
                this,
                SLOT(scaleUniformChanged(bool)));
        connect(ui->unitsComboBox,
                SIGNAL(currentTextChanged(QString)),
                this,
                SLOT(unitChanged(QString)));

        updateDistanceUnit();
        updateAngleUnit();

        connect(m_preferences_manager.data(),
                SIGNAL(distanceUnitChanged(Distance, Distance)),
                this,
                SLOT(updateDistanceUnit(Distance, Distance)));
        connect(m_preferences_manager.data(),
                SIGNAL(angleUnitChanged(Angle, Angle)),
                this,
                SLOT(updateAngleUnit(Angle, Angle)));
    }

    MeshTransformWidget::~MeshTransformWidget()
    {
        delete ui;
    }

    void MeshTransformWidget::changeMesh()
    {
        m_mesh = m_project_manager->selectedMesh();
        if (m_mesh)
        {
            QVector3D translation = m_mesh->translation();
            ui->translationXLineEdit->setText(QString::number(translation.x()));
            ui->translationYLineEdit->setText(QString::number(translation.y()));
            ui->translationZLineEdit->setText(QString::number(translation.z()));
            QVector3D rotation = m_mesh->rotation().toEulerAngles();
            ui->rotationXLineEdit->setText(QString::number(rotation.x()));
            ui->rotationYLineEdit->setText(QString::number(rotation.y()));
            ui->rotationZLineEdit->setText(QString::number(rotation.z()));
            QVector3D scale = m_mesh->scale();
            ui->scaleXLineEdit->setText(QString::number(scale.x()));
            ui->scaleYLineEdit->setText(QString::number(scale.y()));
            ui->scaleZLineEdit->setText(QString::number(scale.z()));
            show();
        }
        else
        {
            hide();
        }
    }

    void MeshTransformWidget::scaleUniformChanged(bool checked)
    {
        if (checked)
        {
            QString scale_text = ui->scaleXLineEdit->text();
            ui->scaleYLineEdit->setText(scale_text);
            ui->scaleZLineEdit->setText(scale_text);
            scaleXEdited(scale_text);
        }
    }

    void MeshTransformWidget::updateDistanceUnit()
    {
        ui->translation_label->setText(
            QString("Translation (%1):")
                .arg(m_preferences_manager->getDistanceUnitText()));
    }

    void MeshTransformWidget::updateDistanceUnit(Distance new_unit,
                                                 Distance old_unit)
    {
        ui->translation_label->setText(
            QString("Translation (%1):")
                .arg(m_preferences_manager->getDistanceUnitText()));

        double ratio = old_unit() / new_unit();

        bool ok  = false;
        double x = ui->translationXLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->translationXLineEdit->setText(QString::number(x * ratio));
        }

        double y = ui->translationYLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->translationYLineEdit->setText(QString::number(y * ratio));
        }

        double z = ui->translationZLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->translationZLineEdit->setText(QString::number(z * ratio));
        }
    }

    void MeshTransformWidget::updateAngleUnit()
    {
        ui->rotation_label->setText(
            QString("Rotation (%1):")
                .arg(m_preferences_manager->getAngleUnitText()));
    }

    void MeshTransformWidget::updateAngleUnit(Angle new_unit, Angle old_unit)
    {
        ui->rotation_label->setText(
            QString("Rotation (%1):")
                .arg(m_preferences_manager->getAngleUnitText()));

        double ratio = old_unit() / new_unit();

        bool ok  = false;
        double x = ui->rotationXLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->rotationXLineEdit->setText(QString::number(x * ratio));
        }

        double y = ui->rotationYLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->rotationYLineEdit->setText(QString::number(y * ratio));
        }

        double z = ui->rotationZLineEdit->text().toDouble(&ok);
        if (ok)
        {
            ui->rotationZLineEdit->setText(QString::number(z * ratio));
        }
    }


    void MeshTransformWidget::translationXEdited(QString x_text)
    {
        bool ok;
        double x = x_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        x *= m_preferences_manager->getDistanceUnit()();

        if (m_mesh)
        {
            m_mesh->setTranslationX(x);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to move mesh when none selected";
        }
    }

    void MeshTransformWidget::translationYEdited(QString y_text)
    {
        bool ok;
        double y = y_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        y *= m_preferences_manager->getDistanceUnit()();

        if (m_mesh)
        {
            m_mesh->setTranslationY(y);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to move mesh when none selected";
        }
    }

    void MeshTransformWidget::translationZEdited(QString z_text)
    {
        bool ok;
        double z = z_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        z *= m_preferences_manager->getDistanceUnit()();

        if (m_mesh)
        {
            m_mesh->setTranslationZ(z);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to move mesh when none selected";
        }
    }

    void MeshTransformWidget::rotationXEdited(QString x_text)
    {
        bool ok;
        double x = x_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        x *= m_preferences_manager->getAngleUnit().to(rad);

        if (m_mesh)
        {
            m_mesh->setRotationX(x);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to rotate mesh when none selected";
        }
    }

    void MeshTransformWidget::rotationYEdited(QString y_text)
    {
        bool ok;
        double y = y_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        y *= m_preferences_manager->getAngleUnit().to(rad);

        if (m_mesh)
        {
            m_mesh->setRotationY(y);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to rotate mesh when none selected";
        }
    }

    void MeshTransformWidget::rotationZEdited(QString z_text)
    {
        bool ok;
        double z = z_text.toDouble(&ok);
        if (!ok)
        {
            return;
        }
        z *= m_preferences_manager->getAngleUnit().to(rad);

        if (m_mesh)
        {
            m_mesh->setRotationZ(z);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to rotate mesh when none selected";
        }
    }

    void MeshTransformWidget::scaleXEdited(QString x_text)
    {
        bool ok;
        double x = x_text.toDouble(&ok);

        if (!ok || x <= 0)
        {
            return;
        }

        if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
        {
            ui->scaleYLineEdit->setText(x_text);
            ui->scaleZLineEdit->setText(x_text);
        }

        if (m_mesh)
        {
            if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
            {
                m_mesh->setScale(x);
            }
            else
            {
                m_mesh->setScaleX(x);
            }
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to scale mesh when none selected";
        }
    }

    void MeshTransformWidget::scaleYEdited(QString y_text)
    {
        bool ok;
        double y = y_text.toDouble(&ok);

        if (!ok || y <= 0)
        {
            return;
        }

        if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
        {
            ui->scaleXLineEdit->setText(y_text);
            ui->scaleZLineEdit->setText(y_text);
        }

        if (m_mesh)
        {
            if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
            {
                m_mesh->setScale(y);
            }
            else
            {
                m_mesh->setScaleY(y);
            }
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to scale mesh when none selected";
        }
    }

    void MeshTransformWidget::scaleZEdited(QString z_text)
    {
        bool ok;
        double z = z_text.toDouble(&ok);

        if (!ok || z <= 0)
        {
            return;
        }

        if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
        {
            ui->scaleYLineEdit->setText(z_text);
            ui->scaleXLineEdit->setText(z_text);
        }

        if (m_mesh)
        {
            if (ui->scaleUniformCheckbox->checkState() == Qt::Checked)
            {
                m_mesh->setScale(z);
            }
            else
            {
                m_mesh->setScaleZ(z);
            }
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to scale mesh when none selected";
        }
    }


    void MeshTransformWidget::unitChanged(QString unit_text)
    {
        Distance new_unit;
        try
        {
            new_unit = Distance::fromString(unit_text);
        }
        catch (UnknownUnitException e)
        {
            qWarning() << e.what();
            return;
        }

        if (m_mesh)
        {
            m_mesh->unit(new_unit);
        }
        else  // Shouldn't happen
        {
            qWarning() << "Attempt to set unit for mesh when none is selected";
        }
    }
}  // namespace ORNL
