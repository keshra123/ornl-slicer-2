#include "widgets/gcodetextboxwidget.h"

#include <QtWidgets>
#include <algorithm>
#include <cmath>

#include "gcode/gcode_command.h"
#include "widgets/linenumberdisplay.h"

namespace ORNL
{
    GcodeTextBoxWidget::GcodeTextBoxWidget(QWidget* parent)
        : QPlainTextEdit(parent)
        , m_highlighter(this->document())
    {
        setLineWrapMode(QPlainTextEdit::NoWrap);

        m_LineNumberDisplayArea = new LineNumberDisplay(this);

        connect(this,
                SIGNAL(blockCountChanged(int)),
                this,
                SLOT(updateLineNumberDisplayAreaWidth(int)));
        connect(this,
                SIGNAL(updateRequest(QRect, int)),
                this,
                SLOT(updateLineNumberDisplayArea(QRect, int)));


        updateLineNumberDisplayAreaWidth(0);
    }

    void GcodeTextBoxWidget::lineNumbersPaintEvent(QPaintEvent* event)
    {
        QPainter painter(m_LineNumberDisplayArea);
        painter.fillRect(event->rect(), Qt::transparent);

        QTextBlock block = firstVisibleBlock();
        int blockNumber  = block.blockNumber();
        int top          = static_cast< int >(
            blockBoundingGeometry(block).translated(contentOffset()).top());
        int bottom =
            top + static_cast< int >(blockBoundingRect(block).height());

        while (block.isValid() && top <= event->rect().bottom())
        {
            if (block.isVisible() && bottom >= event->rect().top())
            {
                QString number = QString::number(blockNumber + 1);
                // Slightly Transparent White
                painter.setPen(QColor(255, 255, 255, 127));
                painter.drawText(0,
                                 top,
                                 m_LineNumberDisplayArea->width(),
                                 fontMetrics().height(),
                                 Qt::AlignRight,
                                 number);
            }

            block = block.next();
            top   = bottom;
            bottom =
                top + static_cast< int >(blockBoundingRect(block).height());
            ++blockNumber;
        }
    }

    int GcodeTextBoxWidget::calculateLineNumbersDisplayWidth()
    {
        return 3 +
            fontMetrics().width(QLatin1Char('9')) *
            ceil(log10(std::max< int >(2, blockCount())) + 1);
    }

    int GcodeTextBoxWidget::getCursorBlockNumber()
    {
        return textCursor().block().blockNumber();
    }


    void GcodeTextBoxWidget::resizeEvent(QResizeEvent* event)
    {
        QPlainTextEdit::resizeEvent(event);
        QRect rectangle = contentsRect();
        m_LineNumberDisplayArea->setGeometry(
            QRect(rectangle.left(),
                  rectangle.top(),
                  calculateLineNumbersDisplayWidth(),
                  rectangle.height()));
    }

    void GcodeTextBoxWidget::updateLineNumberDisplayAreaWidth(int blockCount)
    {
        setViewportMargins(calculateLineNumbersDisplayWidth(), 0, 0, 0);
    }

    void GcodeTextBoxWidget::updateLineNumberDisplayArea(const QRect& rect,
                                                         int height)
    {
        if (height)
        {
            m_LineNumberDisplayArea->scroll(0, height);
        }
        else
        {
            m_LineNumberDisplayArea->update(
                0, rect.y(), m_LineNumberDisplayArea->width(), rect.height());
        }

        if (rect.contains(viewport()->rect()))
        {
            updateLineNumberDisplayAreaWidth(0);
        }
    }

}  // namespace ORNL
