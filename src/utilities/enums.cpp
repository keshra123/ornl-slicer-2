#include "utilities/enums.h"

namespace ORNL
{
    void to_json(json& j, const InfillPatterns& i)
    {
        j = json{{"infill_pattern", static_cast< int >(i)}};
    }

    void from_json(const json& j, InfillPatterns& i)
    {
        i = static_cast< InfillPatterns >(j["infill_pattern"].get< int >());
    }
}  // namespace ORNL
