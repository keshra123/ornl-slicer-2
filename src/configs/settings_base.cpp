#include "configs/settings_base.h"

namespace ORNL
{
    SettingsBase::SettingsBase()
        : m_parent(nullptr)
        , m_values(json::object())
    {}

    SettingsBase::SettingsBase(SettingsBase* parent)
        : m_parent(parent)
        , m_values(json::object())
    {}

    SettingsBase::~SettingsBase()
    {}

    void SettingsBase::parent(SettingsBase* parent)
    {
        m_parent = parent;
    }

    SettingsBase* SettingsBase::parent()
    {
        return m_parent;
    }

    void SettingsBase::setSettingInheritBase(QString key, SettingsBase* parent)
    {
        m_settings_inherit_base.insert(key, parent);
    }

    bool SettingsBase::contains(QString key, bool include_parents)
    {
        if (m_values.find(key.toStdString()) != m_values.end())
        {
            return true;
        }

        if (include_parents)
        {
            return m_parent->contains(key, true);
        }

        return false;
    }

    void SettingsBase::remove(QString key)
    {
        m_values.erase(key.toStdString());
    }

    SettingsBase* SettingsBase::copy()
    {
        SettingsBase* settings_base            = new SettingsBase();
        settings_base->m_parent                = m_parent;
        settings_base->m_values                = m_values;
        settings_base->m_settings_inherit_base = m_settings_inherit_base;
        return settings_base;
    }

    void SettingsBase::update(QSharedPointer< SettingsBase > other)
    {
        for (auto it = other->m_values.begin(); it != other->m_values.end();
             it++)
        {
            m_values[it.key()] = it.value();
        }
    }

    nlohmann::json& SettingsBase::json()
    {
        return m_values;
    }

    void SettingsBase::json(nlohmann::json j)
    {
        m_values = j;
    }
}  // namespace ORNL
