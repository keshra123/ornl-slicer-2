#ifndef LOCALSETTINGSWINDOW_H
#define LOCALSETTINGSWINDOW_H

#include <QMainWindow>

#include "managers/project_manager.h"
#include "managers/window_manager.h"

namespace Ui
{
    class LocalSettingsWindow;
}

namespace ORNL
{
    /*!
     * \class LocalSettingsWindow
     * \brief Settings window for model/mesh/layer specific settings
     */
    class LocalSettingsWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        LocalSettingsWindow(QString mesh_number, int layer_number = -1);
        //! \brief Destructor
        ~LocalSettingsWindow();
    public slots:
        void addMeshSettings();
        void addLayerSettings();
        void removeMeshSettings();
        void removeLayerSettings();

        void changeModel(QString model_name);
        void changeMesh();
        void changeLayer();

        void save();

    private:
        //! \brief handles alerting the window manager that this window has
        //! closed
        void closeEvent(QCloseEvent* event);

        Ui::LocalSettingsWindow* ui;

        QMap< QString, SettingsBase* > m_model_temp_settings;
        QMap< QString, SettingsBase* > m_model_undo_settings;

        QMap< QString, QMap< int, SettingsBase* > > m_mesh_temp_settings;
        QMap< QString, QMap< int, SettingsBase* > > m_mesh_undo_settings;

        QMap< QString, QMap< int, QMap< int, SettingsBase* > > >
            m_layer_temp_settings;
        QMap< QString, QMap< int, QMap< int, SettingsBase* > > >
            m_layer_undo_settings;

        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< WindowManager > m_window_manager;
    };
}  // namespace ORNL
#endif  // LOCALSETTINGSWINDOW_H
