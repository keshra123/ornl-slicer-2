#include "threading/island_thread.h"

#include "layer_regions/infill.h"
#include "layer_regions/island.h"
#include "layer_regions/perimeters.h"
#include "layer_regions/skin.h"

namespace ORNL
{
    IslandThread::IslandThread(QObject* parent, QSharedPointer< Island > island)
        : QThread(parent)
        , m_island(island)
    {
        connect(this,
                SIGNAL(slicingFinished(QSharedPointer< Island >)),
                parent,
                SLOT(islandThreadFinished(QSharedPointer< Island >)));
        connect(this,
                SIGNAL(slicingInterrupted(QSharedPointer< Island >)),
                parent,
                SLOT(islandThreadInterrupted(QSharedPointer< Island >)));
    }

    void IslandThread::run()
    {
        // TODO: allow order to be changed

        if (isInterruptionRequested())
        {
            return;
        }

        m_island->m_perimeters->compute();

        if (isInterruptionRequested())
        {
            emit slicingInterrupted(m_island);
            return;
        }

        m_island->m_insets->compute();

        if (isInterruptionRequested())
        {
            emit slicingInterrupted(m_island);
            return;
        }

        m_island->m_skin->compute();

        if (isInterruptionRequested())
        {
            emit slicingInterrupted(m_island);
            return;
        }

        m_island->m_infill->compute();

        if (isInterruptionRequested())
        {
            emit slicingInterrupted(m_island);
            return;
        }

        emit slicingFinished(m_island);
    }

}  // namespace ORNL
