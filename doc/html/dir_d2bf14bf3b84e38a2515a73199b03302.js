var dir_d2bf14bf3b84e38a2515a73199b03302 =
[
    [ "nozzleaccelerationcategorywidget.cpp", "nozzleaccelerationcategorywidget_8cpp.html", null ],
    [ "nozzleaccelerationcategorywidget.h", "nozzleaccelerationcategorywidget_8h.html", [
      [ "NozzleAccelerationCategoryWidget", "class_o_r_n_l_1_1_nozzle_acceleration_category_widget.html", "class_o_r_n_l_1_1_nozzle_acceleration_category_widget" ]
    ] ],
    [ "nozzlebeadwidthcategorywidget.cpp", "nozzlebeadwidthcategorywidget_8cpp.html", null ],
    [ "nozzlebeadwidthcategorywidget.h", "nozzlebeadwidthcategorywidget_8h.html", [
      [ "NozzleBeadWidthCategoryWidget", "class_o_r_n_l_1_1_nozzle_bead_width_category_widget.html", "class_o_r_n_l_1_1_nozzle_bead_width_category_widget" ]
    ] ],
    [ "nozzlelayercategorywidget.cpp", "nozzlelayercategorywidget_8cpp.html", null ],
    [ "nozzlelayercategorywidget.h", "nozzlelayercategorywidget_8h.html", [
      [ "NozzleLayerCategoryWidget", "class_o_r_n_l_1_1_nozzle_layer_category_widget.html", "class_o_r_n_l_1_1_nozzle_layer_category_widget" ]
    ] ],
    [ "nozzlesettingcategorywidget.h", "nozzlesettingcategorywidget_8h.html", null ],
    [ "nozzlespeedcategorywidget.cpp", "nozzlespeedcategorywidget_8cpp.html", null ],
    [ "nozzlespeedcategorywidget.h", "nozzlespeedcategorywidget_8h.html", [
      [ "NozzleSpeedCategoryWidget", "class_o_r_n_l_1_1_nozzle_speed_category_widget.html", "class_o_r_n_l_1_1_nozzle_speed_category_widget" ]
    ] ]
];