#ifndef PRINTSETTINGCATEGORYWIDGETS_H
#define PRINTSETTINGCATEGORYWIDGETS_H

#include "printinfillcategorywidget.h"
#include "printinsetscategorywidget.h"
#include "printperimetercategorywidget.h"
#include "printskincategorywidget.h"

#endif  // PRINTSETTINGCATEGORYWIDGETS_H
