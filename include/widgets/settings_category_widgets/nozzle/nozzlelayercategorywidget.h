#ifndef NOZZLELAYERCATEGORYWIDGET_H
#define NOZZLELAYERCATEGORYWIDGET_H

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class NozzleLayerCategoryWidget;
}

namespace ORNL
{
    class NozzleLayerCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        NozzleLayerCategoryWidget();

        //! \brief Destructor
        ~NozzleLayerCategoryWidget();

        //! \brief applys the conditions for enabling/disabling
        void handleConditions();

    private:
        Ui::NozzleLayerCategoryWidget* ui;
    };
}  // namespace ORNL


#endif  // NOZZLELAYERCATEGORYWIDGET_H
