#ifndef TST_SETTINGSBASE_H
#define TST_SETTINGSBASE_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

#if 0 // TODO fix tests

/*!
 * \class TestSettingsBase
 *
 * \brief Unit test class for SettingsBase
 */
class TestSettingsBase: public QObject
{
    Q_OBJECT
private slots:
    //! \brief Tests all the type get converted to json and back correctly
    void testTypes();

    //! \brief Test that the parent system works
    void testParent();
};

DECLARE_TEST(TestSettingsBase)
#endif // TST_SETTINGSBASE_H

#endif
