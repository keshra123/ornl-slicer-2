#ifndef POLYGON_H
#define POLYGON_H

#include <stdint.h>

#include <QMetaType>
#include <QPoint>
#include <QPolygon>
#include <QtMath>

#include "clipper.hpp"
#include "point.h"
#include "polygon_list.h"
#include "polyline.h"
#include "units/derivative_units.h"
#include "units/unit.h"

namespace ORNL
{
    class PolygonList;
    class Polyline;

    /*!
     * \class Polygon
     * \brief An individual polygon
     *
     * An individual polygon represented as a vector of points
     */
    class Polygon : public QVector< Point >
    {
    public:
        //! \brief Constructor
        Polygon() = default;

        //! \brief Conversion Constructor
        Polygon(const QVector< Point >& path);

        //! \brief Conversion Constructor
        Polygon(const QVector< ClipperLib::IntPoint >& path);

        //! \brief Conversion Constructor
        Polygon(const ClipperLib::Path& path);

        //! \brief Conversion Constructor
        Polygon(const QVector< Distance2D >& path);

        /*!
         * \brief Checks the orientation of the Polygon
         *
         * Checks the orientation of the polygon based on the area.
         * [Uses
         * Clipper](http://www.angusj.com/delphi/clipper/documentation/Docs/Units/ClipperLib/Functions/Orientation.htm)
         *
         * \return
         *        - True: counterclockwise (outer polygon)
         *        - False: clockwise (hole polygon)
         */
        bool orientation() const;

        /*!
         * \brief Offsets the polygons
         *
         * Offsets the Polygon by moving the vertices along their angle
         * bisectors. Can seperate into multiple Polygon's contained by the
         * return value.
         */
        PolygonList offset(
            Distance distance,
            ClipperLib::JoinType joinType = ClipperLib::jtMiter) const;

        //! \brief The length of each of sides of the polygon summed
        int64_t polygonLength() const;

        /*!
         * \brief returns whether the length of the sides of the polygon is
         *        shorter than `checkLength`
         */
        bool shorterThan(Distance rhs) const;

        //! \brief Returns the center point of the polygon
        Point center() const;

        //! \brief Smallest x, y, and z coordinates (may not be from the same
        //! point)
        Point min() const;

        //! \brief Largest x, y, and z coordinates (may not be from the same
        //! point)
        Point max() const;

        //! \brief Rotate the polygon around the origin
        Polygon rotate(Angle angle, QVector3D axis = {0, 0, 1});

        //! \brief Rotate the polygon around a specified point
        Polygon rotateAround(Point center,
                             Angle angle,
                             QVector3D axis = {0, 0, 1});

        /*!
         * \brief Returns whether the point is inside the polygon
         */
        bool inside(Point point, bool border_result = false) const;

        /*!
         * \brief The area of the polygon
         *
         * - Positve: counterclockwise (outer polygon)
         * - Negative: clockwise (hole polygon)
         */
        Area area() const;

        //! \brief Returns the center of mass of the polygon
        Point centerOfMass() const;

        //! \brief Returns the closest point to `rhs`
        Point closestPointTo(const Point& rhs) const;

        // TODO: add psmooth
        //! \brief
        Polygon smooth(Distance removeLength);

        //! \brief
        Polygon simplify(Area smallestLineSegmentSquared,
                         Area allowedErrorDistanceSquared);

        /*!
         * \brief Add an instance of Polygon to this Polygon to make Polygons.
         *        Any overlapping Polygon are merged into one.
         */
        PolygonList operator+(const PolygonList& rhs);

        /*!
         * \brief Add an instance of Polygon to this Polygon to make Polygons.
         *        Any overlapping Polygon are merged into one.
         */
        PolygonList operator+(const Polygon& rhs);

        //! \brief Subtracts an instance of Polygon from this Polygon to make
        //! Polygons.
        PolygonList operator-(const PolygonList& rhs);

        //! \brief Subtracts an instance of Polygon from this Polygon to make
        //! Polygons.
        PolygonList operator-(const Polygon& rhs);

        /*!
         * \brief Unions an instance of Polygon to this Polygon to make
         * Polygons. Any overlapping Polygon are merged into one.
         */
        PolygonList operator|(const PolygonList& rhs);

        /*!
         * \brief Union an instance of Polygon to this Polygon to make Polygons.
         *        Any overlapping Polygon are merged into one.
         */
        PolygonList operator|(const Polygon& rhs);

        /*!
         * \brief Returns the intersection of this Polygon and other Polygons
         *        as 0 or more Polygon's
         */
        PolygonList operator&(const PolygonList& rhs);

        /*!
         * \brief Returns the intersection of this Polygon and other Polygons
         *        as 0 or more Polygons
         */
        PolygonList operator&(const Polygon& rhs);

        /*!
         * \brief Returns the intersection of this polygon and a polygon as
         *        0 or more polylines
         */
        QVector< Polyline > operator&(const Polyline& rhs);

        /*!
         * \brief Returns the xor of this Polygon and other Polygons
         *        as 0 or more Polygons
         */
        PolygonList operator^(const PolygonList& rhs);

        /*!
         * \brief Returns the xor of this Polygon and other Polygons
         *        as 0 or more Polygons
         */
        PolygonList operator^(const Polygon& rhs);

        /*!
         * \brief Returns whether two Polygons are equal by looping around the
         * points of both Polygons and checking if they match
         */
        bool operator==(const Polygon& right);

        /*!
         * \brief Reterns whether two Polygons are not equal
         */
        bool operator!=(const Polygon& right);

        // friend classes can use each other's private functions/members
        friend class PolygonList;
        friend class Polyline;

    private:
        //! \brief operator for use with ClipperLib
        ClipperLib::Path operator()() const;
    };  // Class Polygon
}  // namespace ORNL
#endif  // POLYGON_H
