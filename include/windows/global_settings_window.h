#ifndef GLOBALSETTINGSWINDOW_H
#define GLOBALSETTINGSWINDOW_H

//! \file globalsettingswindow.h

#include <QCloseEvent>
#include <QMainWindow>

#include "managers/global_settings_manager.h"
#include "managers/project_manager.h"
#include "managers/window_manager.h"

namespace Ui
{
    class GlobalSettingsWindow;
}

namespace ORNL
{
    class GlobalSettingsManager;
    class WindowManager;

    /*!
     * \class
     * \brief The SettingsWindow class
     */
    class GlobalSettingsWindow : public QMainWindow
    {
        Q_OBJECT
    public:
        //! \brief Constructor that specifies which tab starts open and which
        //! config starts selected
        GlobalSettingsWindow(ConfigTypes configType, QString start);

        //! \brief Destructor
        ~GlobalSettingsWindow();

    private slots:
        //! \brief Handles import action in the menu
        void importSetting();

        //! \brief Handles export action in the menu
        void exportSetting();

        //! \brief Handles updating one of the tabs with new configs
        void updateConfig(ConfigTypes ct);

    private:
        //! \brief Handles closing of the window and clearing the pointer in the
        //! window manager
        void closeEvent(QCloseEvent* event);

        Ui::GlobalSettingsWindow* ui;
        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        QSharedPointer< WindowManager > m_window_manager;
        QSharedPointer< ProjectManager > m_project_manager;
    };  // class GloablSettingsWindow
}  // namespace ORNL
#endif  // GLOBALSETTINGSWINDOW_H
