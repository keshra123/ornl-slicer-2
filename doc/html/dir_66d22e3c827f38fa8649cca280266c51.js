var dir_66d22e3c827f38fa8649cca280266c51 =
[
    [ "SettingsCategoryWidgets", "dir_9ed9fcd08deeb82a448edd968a4fc4fc.html", "dir_9ed9fcd08deeb82a448edd968a4fc4fc" ],
    [ "globalconfigwidget.cpp", "globalconfigwidget_8cpp.html", null ],
    [ "globalconfigwidget.h", "globalconfigwidget_8h.html", [
      [ "GlobalConfigWidget", "class_o_r_n_l_1_1_global_config_widget.html", "class_o_r_n_l_1_1_global_config_widget" ]
    ] ],
    [ "localconfigwidget.cpp", "localconfigwidget_8cpp.html", null ],
    [ "localconfigwidget.h", "localconfigwidget_8h.html", [
      [ "LocalConfigWidget", "class_o_r_n_l_1_1_local_config_widget.html", "class_o_r_n_l_1_1_local_config_widget" ]
    ] ],
    [ "modeladjustmentwidget.cpp", "modeladjustmentwidget_8cpp.html", null ],
    [ "modeladjustmentwidget.h", "modeladjustmentwidget_8h.html", [
      [ "ModelAdjustmentWidget", "class_o_r_n_l_1_1_model_adjustment_widget.html", "class_o_r_n_l_1_1_model_adjustment_widget" ]
    ] ],
    [ "modelplacementwidget.cpp", "modelplacementwidget_8cpp.html", "modelplacementwidget_8cpp" ],
    [ "modelplacementwidget.h", "modelplacementwidget_8h.html", [
      [ "ModelPlacementWidget", "class_o_r_n_l_1_1_model_placement_widget.html", "class_o_r_n_l_1_1_model_placement_widget" ]
    ] ],
    [ "modeltreewidget.cpp", "modeltreewidget_8cpp.html", null ],
    [ "modeltreewidget.h", "modeltreewidget_8h.html", [
      [ "ModelTreeWidget", "class_o_r_n_l_1_1_model_tree_widget.html", "class_o_r_n_l_1_1_model_tree_widget" ]
    ] ]
];