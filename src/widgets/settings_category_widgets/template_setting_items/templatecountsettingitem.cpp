#include "widgets/settings_category_widgets/template_setting_items/templatecountsettingitem.h"

#include "ui_template_count_setting_item.h"

namespace ORNL
{
    TemplateCountSettingItem::TemplateCountSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplateCountSettingItem)
        , m_validator(new QIntValidator())
    {
        ui->setupUi(this);

        ui->line_edit->setValidator(m_validator);
        ui->line_edit->setText("0");
        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->line_edit,
                SIGNAL(editingFinished()),
                this,
                SLOT(handleValueChanged()));
    }

    TemplateCountSettingItem::~TemplateCountSettingItem()
    {
        delete ui;
    }

    void TemplateCountSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }

    void TemplateCountSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }

    void TemplateCountSettingItem::value(int number)
    {
        ui->line_edit->setText(QString::number(number));
    }

    int TemplateCountSettingItem::value()
    {
        bool ok;
        int i = ui->line_edit->text().toInt(&ok);
        if (ok)
        {
            return i;
        }
        throw SettingValueException("count");
    }

    void TemplateCountSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->line_edit->setStyleSheet(stylesheet);
    }

    void TemplateCountSettingItem::minimum(int m)
    {
        m_validator->setBottom(m);
    }

    void TemplateCountSettingItem::maximum(int m)
    {
        m_validator->setTop(m);
    }

    void TemplateCountSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplateCountSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplateCountSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplateCountSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplateCountSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplateCountSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplateCountSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
