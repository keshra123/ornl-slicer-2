var class_o_r_n_l_1_1_global_settings_manager =
[
    [ "addConfig", "class_o_r_n_l_1_1_global_settings_manager.html#a7af294c0d5cf60b499b000f2a953ddc5", null ],
    [ "error", "class_o_r_n_l_1_1_global_settings_manager.html#a750931689a018d1e45793612e2cd705a", null ],
    [ "getConfig", "class_o_r_n_l_1_1_global_settings_manager.html#a42a73b0e34f6386ba2aff992ab1b6c10", null ],
    [ "getConfigsNames", "class_o_r_n_l_1_1_global_settings_manager.html#a37f016de07d0823ddfa73a441b5b0b1d", null ],
    [ "getGlobal", "class_o_r_n_l_1_1_global_settings_manager.html#aa7767764bbabf195712df2da323f034e", null ],
    [ "getNozzleNames", "class_o_r_n_l_1_1_global_settings_manager.html#adb1fcd54268d586a86f033b67fdb6db7", null ],
    [ "removeConfig", "class_o_r_n_l_1_1_global_settings_manager.html#af3e7c54cb245ad1a85dcbfd095fb5b46", null ],
    [ "saveConfig", "class_o_r_n_l_1_1_global_settings_manager.html#a78dcf8821cb82927db399d61c67a9903", null ],
    [ "updateConfig", "class_o_r_n_l_1_1_global_settings_manager.html#aa993e89df7bce3d82c57254cbcad89b5", null ],
    [ "updateGlobal", "class_o_r_n_l_1_1_global_settings_manager.html#aa6b9593c7692d932c0f8704c79e29a45", null ]
];