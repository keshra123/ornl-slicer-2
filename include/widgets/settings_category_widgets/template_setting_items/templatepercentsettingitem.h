#ifndef TEMPLATEPERCENTSETTINGITEM_H
#define TEMPLATEPERCENTSETTINGITEM_H

#include <QDoubleValidator>
#include <QWidget>

#include "exceptions/exceptions.h"

namespace Ui
{
    class TemplatePercentSettingItem;
}

namespace ORNL
{
    class TemplatePercentSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit TemplatePercentSettingItem(QWidget* parent = 0);
        ~TemplatePercentSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        double value();

        //! \brief Sets the stylesheet for the checkbox
        void setStyleSheet(const QString& stylesheet);

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        //! \brief Signals that the value has changed
        void valueChanged(double);

    public slots:
        //! \brief Sets the value for the textbox
        void value(double p);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    private slots:
        void handleUndo();
        void handleRedo();

        void handleValueChanged();

    private:
        Ui::TemplatePercentSettingItem* ui;
    };
}  // namespace ORNL

#endif  // TEMPLATEPERCENTSETTINGITEM_H
