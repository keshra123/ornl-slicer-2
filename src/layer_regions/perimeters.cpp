#include "layer_regions/perimeters.h"

#include "layer_regions/island.h"
#ifdef DEBUG
#    include <QPainter>
#endif
#include "geometry/path.h"
namespace ORNL
{
    Perimeters::Perimeters(Island* island, RegionType region_type)
        : Region(island)
        , m_region_type(region_type)
    {
        // Asserts that the region is one or the other, but not both
        Q_ASSERT(region_type == RegionType::kPerimeter ||
                 region_type == RegionType::kInset);
#ifdef DEBUG
        m_polygon_pen.setColor(region_type == RegionType::kPerimeter
                                   ? Constants::Colors::kPerimeter
                                   : Constants::Colors::kInset);
#endif

        if (region_type == RegionType::kPerimeter)
        {
            initialCache(Constants::PrintSettings::Perimeters::kEnable, false);
            initialCache(Constants::PrintSettings::Perimeters::kExtruderId, -1);
            initialCache(Constants::PrintSettings::Perimeters::kNumber, -1);
            initialCache(Constants::PrintSettings::Perimeters::kExternalFirst,
                         false);
            initialCache(Constants::PrintSettings::Perimeters::kEnableSkeletons,
                         false);
            initialCache(Constants::PrintSettings::Perimeters::
                             kEnableVariableWidthSkeletons,
                         false);
            initialCache(
                Constants::PrintSettings::Perimeters::kOnlyRetractWhenCrossing,
                false);
            initialCache(Constants::PrintSettings::Perimeters::kSmallLength,
                         false);
            initialCache(Constants::NozzleSettings::BeadWidth::kPerimeter, -1);
            initialCache(Constants::NozzleSettings::Speed::kPerimeter, -1);
            initialCache(Constants::NozzleSettings::Acceleration::kPerimeter,
                         -1);
        }
        else
        {
            initialCache(Constants::PrintSettings::Insets::kEnable, false);
            initialCache(Constants::PrintSettings::Insets::kExtruderId, -1);
            initialCache(Constants::PrintSettings::Insets::kNumber, -1);
            initialCache(Constants::PrintSettings::Insets::kExternalFirst,
                         false);
            initialCache(Constants::PrintSettings::Insets::kEnableSkeletons,
                         false);
            initialCache(
                Constants::PrintSettings::Insets::kEnableVariableWidthSkeletons,
                false);
            initialCache(
                Constants::PrintSettings::Insets::kOnlyRetractWhenCrossing,
                false);
            initialCache(Constants::PrintSettings::Insets::kSmallLength, false);
            initialCache(Constants::NozzleSettings::BeadWidth::kInset, -1);
            initialCache(Constants::NozzleSettings::Speed::kInset, -1);
            initialCache(Constants::NozzleSettings::Acceleration::kInset, -1);
        }
    }

    void Perimeters::computeInternal()
    {
        PolygonList perimeter_outline = m_parent->outlines();
        PolygonList perimeter_inline;
        Distance bead_width;
        int rings;

        // If perimeters are enabled calculate the inline
        if (cacheValue< bool >(Constants::PrintSettings::Perimeters::kEnable))
        {
            bead_width = cacheValue< Distance >(
                Constants::NozzleSettings::BeadWidth::kPerimeter);
            rings = cacheValue< int >(
                Constants::PrintSettings::Perimeters::kNumber);
            perimeter_inline = perimeter_outline.offset(-bead_width * rings);
        }
        // If not the inline and outline are the same
        else
        {
            perimeter_inline = perimeter_outline;
        }

        // If this is currently the perimeter use the perimeter inline and
        // outline
        if (static_cast< bool >(m_region_type & RegionType::kPerimeter))
        {
            m_outline = perimeter_outline;
            m_inline  = perimeter_inline;

            // If disabled clear everything
            if (!cacheValue< bool >(
                    Constants::PrintSettings::Perimeters::kEnable))
            {
                m_paths.clear();
#ifdef DEBUG
                m_debug_rings.clear();
#endif
                return;
            }
        }
        // If this is currently the inset the perimeter inline is the outline
        else  // if(static_cast<bool>(m_region_type & RegionType::kInset))
        {
            m_outline = perimeter_inline;

            // If enabled calculate the inline
            if (cacheValue< bool >(Constants::PrintSettings::Insets::kEnable))
            {
                bead_width = cacheValue< Distance >(
                    Constants::NozzleSettings::BeadWidth::kInset);
                rings = cacheValue< int >(
                    Constants::PrintSettings::Insets::kNumber);
                m_inline = perimeter_outline.offset(-bead_width * rings);
            }
            // If disabled the inline and outline are the same
            // Also clear everything
            else
            {
                m_inline = m_outline;
                m_paths.clear();
#ifdef DEBUG
                m_debug_rings.clear();
#endif
                return;
            }
        }

        /*
        if(region_type & RegionType::kPerimeters ?
        setting<bool>(Constants::PrintSettings::Perimeters::kVariableExtrusion)
        : setting<bool>(Constants::PrintSettings::Insets::kVariableExtrusion))
        {
            computeVariableConcentric();
        }
        else if(region_type & RegionType::kPerimeters ?
        setting<bool>(Constants::PrintSettings::Perimeters::kSkeletons) :
        setting<bool>(Constants::PrintSettings::Insets::kSkeletons))
        {
            computeSkeletonConcentric();
        }
        else
        {
        */
        computeConcentric(bead_width, rings);
        //}
    }

    void Perimeters::computeConcentric(Distance& bead_width, int& rings)
    {
        for (int ring_nr = 0; ring_nr < rings; ring_nr++)
        {
            PolygonList path_line =
                m_outline.offset(-(bead_width * ring_nr + bead_width / 2.0));
            if (!path_line.size())
            {
                return;
            }
            m_paths += Path::createPaths(this, m_region_type, path_line);
#ifdef DEBUG
            m_debug_rings += path_line;
#endif
        }
    }

    void Perimeters::orderInternal(Point& previous_location)
    {
        // TODO
    }

#ifdef DEBUG
    QStringList Perimeters::debugOptionsInternal()
    {
        QStringList rv;
        rv << "Outline";
        for (int i = 0; i < m_debug_rings.size(); i++)
        {
            rv << QString("Ring %1").arg(i + 1);
        }
        rv << "Inline";
        for (int i = 0; i < m_paths.size(); i++)
        {
            rv << QString("Path %1").arg(i + 1);
        }
        return rv;
    }

    void Perimeters::paintDebugInternal(QString option,
                                        QPainter* painter,
                                        Distance& ratio,
                                        int vertical_offset)
    {
        if (option == "Outline")
        {
            m_polygon_pen.setWidth(3);
            QPainterPath painter_path;
            for (Polygon polygon : m_outline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            // painter->setPen(m_outline_pen);
            // painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            // painter->setPen(m_centerline_pen);
            // painter->drawPath(painter_path);
        }
        else if (option == "Inline")
        {
            m_polygon_pen.setWidth(3);
            QPainterPath painter_path;
            for (Polygon polygon : m_inline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            // painter->setPen(m_outline_pen);
            // painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            // painter->setPen(m_centerline_pen);
            // painter->drawPath(painter_path);
        }
        else if (option.contains("Ring"))
        {
            m_polygon_pen.setWidth(
                cacheValue< Distance >(
                    m_region_type == RegionType::kPerimeter
                        ? Constants::NozzleSettings::BeadWidth::kPerimeter
                        : Constants::NozzleSettings::BeadWidth::kInset)() *
                ratio());

            int ring_nr = option.mid(5).toInt() - 1;
            QPainterPath painter_path;
            Q_ASSERT(ring_nr < m_debug_rings.size());
            for (Polygon polygon : m_debug_rings[ring_nr])
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            // painter->setPen(m_outline_pen);
            // painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            // painter->setPen(m_centerline_pen);
            // painter->drawPath(painter_path);
        }
        else if (option.contains("Path"))
        {
            int path_nr = option.mid(5).toInt();
            m_paths[path_nr].paintDebug(painter, ratio, vertical_offset);
        }
        else
        {
            Q_ASSERT(false);
        }
    }
#endif
}  // namespace ORNL
