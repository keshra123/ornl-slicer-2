#ifndef LINE_H
#define LINE_H

#include "point.h"

namespace ORNL
{
    class Line
    {
    public:
        Line();
        Line(Point start, Point end);

        Point start() const;
        void start(Point s);

        Point end() const;
        void end(Point e);

    private:
        Point m_start, m_end;
    };
}  // namespace ORNL
#endif  // LINE_H
