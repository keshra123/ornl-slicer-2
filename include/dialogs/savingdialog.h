#ifndef SAVINGDIALOG_H
#define SAVINGDIALOG_H

#include <QDialog>

namespace Ui
{
    class SavingDialog;
}

namespace ORNL
{
    /*!
     * \class SavingDialog
     * \brief Dialog window displayed while saving a file
     */
    class SavingDialog : public QDialog
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit SavingDialog(QString filename, QWidget* parent = 0);

        //! \brief Destructor
        ~SavingDialog();

    private:
        Ui::SavingDialog* ui;
    };  // class SavingDialog
}  // namespace ORNL
#endif  // SAVINGDIALOG_H
