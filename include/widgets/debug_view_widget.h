#include <QtGlobal>

#ifndef DEBUGVIEWWIDGET_H
#    define DEBUGVIEWWIDGET_H

#    include <QWidget>
#    ifdef DEBUG
#        include <QBrush>
#        include <QPen>

class QPaintEvent;

#        include "units/unit.h"

#    endif  // DEBUG

namespace ORNL
{
    class ProjectManager;

    /*!
     * \class DebugViewWidget
     *
     * \brief Widget for displaying debug information from the previous slice
     */
    class DebugViewWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit DebugViewWidget(QWidget* parent = nullptr);
#    ifdef DEBUG
    public slots:
        void updateMachineRatio();

    protected:
        void paintEvent(QPaintEvent* event) override;

        void resizeEvent(QResizeEvent* event) override;

    private:
        QBrush m_background;
        QPen m_machine_pen;

        int m_horizontal_offset;
        int m_vertical_offset;

        int m_width;
        int m_height;

        QSharedPointer< ProjectManager > m_project_manager;
        Distance m_ratio;  //!< ratio from real world size to screen size
#    endif                 // QT_DEBUG
    };
}  // namespace ORNL
#endif  // DEBUGVIEWWIDGET_H
