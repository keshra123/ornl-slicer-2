#ifndef ERRORDIALOG_H
#define ERRORDIALOG_H

//! \file errordialog.h

#include <QDialog>

namespace Ui
{
    class ErrorDialog;
}

namespace ORNL
{
    /*!
     * \class ErrorDialog
     * \brief Dialog Window for displaying error messages
     */
    class ErrorDialog : public QDialog
    {
        Q_OBJECT

    public:
        //! \brief Constructor for solely showing the error
        explicit ErrorDialog(QString error, QWidget* parent = 0);

        //! \brief Constructor for showing an error associated with a file
        explicit ErrorDialog(QString error,
                             QString filename,
                             QWidget* parent = 0);
        ~ErrorDialog();

    private:
        Ui::ErrorDialog* ui;
    };  // class ORNL
}  // namespace ORNL
#endif  // ERRORDIALOG_H
