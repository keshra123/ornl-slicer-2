#ifndef WOLFPARSER_H
#define WOLFPARSER_H

//! \file wolfparser.h

#include "common_parser.h"


namespace ORNL
{
    /*!
     * \class WolfParser
     * \brief This class implements the GCode parsing configuration for the Wolf
     * printer(s).
     */
    class WolfParser : public CommonParser
    {
    public:
        WolfParser();

    protected:
    private:
    };
}  // namespace ORNL
#endif  // WOLFPARSER_H
