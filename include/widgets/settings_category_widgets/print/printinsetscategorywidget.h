#ifndef PRINTINSETSCATEGORYWIDGET_H
#define PRINTINSETSCATEGORYWIDGET_H

#include <QWidget>

#include "utilities/constants.h"
#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class PrintInsetsCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class PrintInsetsCategoryWidget
     *
     * \brief Settings Category Widget for insets
     */
    class PrintInsetsCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        //! \brief default constructor
        explicit PrintInsetsCategoryWidget(QWidget* parent = 0);

        //! \brief destructor
        ~PrintInsetsCategoryWidget();

        //! \brief handles conditional
        void handleConditions();

    private:
        Ui::PrintInsetsCategoryWidget* ui;
    };  // class PrintInsetsCategoryWidget
}  // namespace ORNL

#endif  // PRINTINSETSCATEGORYWIDGET_H
