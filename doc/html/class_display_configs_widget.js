var class_display_configs_widget =
[
    [ "DisplayConfigsWidget", "class_display_configs_widget.html#a2ed9d8b2cfd4684b2dd467d65e104bd3", null ],
    [ "~DisplayConfigsWidget", "class_display_configs_widget.html#a4de963550782aa9516581418d824ee56", null ],
    [ "handleAddConfig", "class_display_configs_widget.html#a6c5fada9fdce1c5142989d43d1480590", null ],
    [ "handleCategorySelectionChanged", "class_display_configs_widget.html#a62c03108eb6398fad16e498dd62a2cd6", null ],
    [ "handleDeleteConfig", "class_display_configs_widget.html#a580b61ea168ff17dfe886a556bf7643a", null ],
    [ "handleSaveConfig", "class_display_configs_widget.html#a6198e995a0b94fc9ed0b7e1e08afb6a1", null ],
    [ "reload", "class_display_configs_widget.html#a0fc716c5425a2a68c99f5cb6faddc1eb", null ],
    [ "setConfigType", "class_display_configs_widget.html#a9c44400f92ef0080e26cf4450346e6dc", null ],
    [ "setCurrentConfig", "class_display_configs_widget.html#ab8382267366ae6ec0282c54d9eeb1e91", null ]
];