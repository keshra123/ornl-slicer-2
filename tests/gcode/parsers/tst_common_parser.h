#ifndef TST_COMMONPARSER_H
#define TST_COMMONPARSER_H

//! \file tst_commonparser.h

#include <QtTest/QtTest>

#include "auto_test.h"
#include "gcode/parsers/common_parser.h"

using namespace ORNL;
using namespace AutoTest;

#if 0 // TODO fix tests

/*!
 * \brief The TestCommonParser class, this class decalres units tests for the commonparser
 *        and partially the baseparser, since it cannot be directly instantiated.
 */
class TestCommonParser : public QObject
{
public:
    Q_OBJECT
private:
    CommonParser* test; // Needs to be a pointer because the Unit variables are unititalized once this object is
	        	         	  // initalized and thus will always be zero.
private slots:

    // TODO: Write more tests for the parser.
    //       Particularly one for each function handler
    //       and corresponding funcitonality, then combine them
    //       together.
    // TODO: Add lists of what each of the unit tests test for each function.
    //! \brief Runs once for this test class.
    void initTestCase();
    //! \brief Runs once all test cases have been completed
    void cleanupTestCase();

    //! \brief Runs before each test case to setup the correct testing environment for each individual test case.
    void init();
    //! \brief Runs before each test case to prevent spilling over of data between cases
    void cleanup();

    //! \brief Throughly tests the G0 command handler for every type fo erroneous input and correct input.
    //!        It tests:
    //!				- Normal Extraction with all parameters
    //!				- Normal Extraction with some parameters missing
    //!				- Normal Extraction with scrambled parameters
    //!				- Invlaid input with no parameters
    //!				- Invlaid input with parameters passed but no numbers coniciding with them
    //!				- Invlaid input with an extra parameter passed
    //!				- Invlaid input with multiple of the same parameter passed
    void testG0Handler();

    //! \brief Throughly tests the G1 command handler for every type of erroneous or valid input
    //!		   It tests:
    //!				- Normal Extraction with and without flowrate
    //!				- Normal Extraction with some parameters missing
    //!				- Normal Extraction with scrambled parameters
    //!				- Invalid input with no parameters
    //!				- Invalid input with parameters passed but no numbers coniciding with them
    //!				- Invalid input with an extra parameter passed
    //!				- Invalid input with multiple of the same parameter passed
    //!				- Invalid input with only the flow rate passed as a parameter
    void testG1Handler();

    //! \brief Throughly tests the G2 command handler for every type of erroneous or valid input
    //!		   It tests:
    //!				- Normal Extraction with and without flowrate
    //!				- Normal Extraction with some parameters missing
    //!				- Normal Extraction with scrambled parameters
    //!				- Invlaid input with no parameters
    //!				- Invalid input with parameters missing
    //!				- Invlaid input with parameters passed but no numbers coniciding with them
    //!				- Invalid input with an extra parameter passed
    //!				- Invalid input with multiple of the same parameter passed
    //!				- Invalid input with only the flow rate passed as a parameter
    void testG2Handler();

    //! \brief Throughly tests the G3 command handler for every type of erroneous or valid input
    //!		   It tests:
    //!				- Normal Extraction with and without flowrate
    //!				- Normal Extraction with some parameters missing
    //!				- Normal Extraction with scrambled parameters
    //!				- Invlaid input with no parameters
    //!				- Invalid input with parameters missing
    //!				- Invlaid input with parameters passed but no numbers coniciding with them
    //!				- Invalid input with an extra parameter passed
    //!				- Invalid input with multiple of the same parameter passed
    //!				- Invalid input with only the flow rate passed as a parameter
    void testG3Handler();

    //! \brief Throughly tests the G4 command handler for every type of erroneous or valid input
    //!		   It tests:
    //!				- Normal Extraction
    //!				- Invlaid input with no parameters
    //!				- Invlaid input with parameter passed but no number coniciding with it
    //!				- Invalid input with an extra parameter passed
    //!				- Invalid input with multiple of the same parameter passed
    void testG4Handler();


    void testM3Handler();
    void testM5Handler();
    void testM10Handler();
    void testM11Handler();
    void testM13Handler();
    void testM14Handler();
    void testM15Handler();
    void testM16Handler();
    void testM64Handler();
    void testM65Handler();
    void testM66Handler();
    void testM68Handler();
    void testM69Handler();

    void testLineComment();
    //void testBlockComment();

    void testInvalidCommand();
    void testInvalidParameter_InvalidFloatConversion();
    void testInvalidParameter_InvalidParameterPassed();

    void testChainnedCommands();
    void testPreviousCommands();
    void testMultipleCommands();

    void testDifferentUnits();
    void testDifferntBlockCommentDelimiters();
    void testDifferntLineCommentDelimiters();

};

DECLARE_TEST(TestCommonParser)

#endif // TST_COMMONPARSER_H

#endif
