#ifndef PROJECTEXPORTER_H
#define PROJECTEXPORTER_H

//! \brief projectexporter.h

#include <QString>
#include <QThread>
#include <QtDebug>
#include <json.hpp>

#include "managers/global_settings_manager.h"
#include "managers/project_manager.h"
#ifdef GUI
#    include "windowmanager.h"
#endif

using json = nlohmann::json;

namespace ORNL
{
    class GlobalSettingsManager;
    class WindowManager;

    /*!
     * \class ProjectExporter
     * \brief Thread that exports a project
     */
    class ProjectExporter : public QThread
    {
        Q_OBJECT
    public:
        /*!
         * \brief Constructor
         * \param filename folder to create/export to
         */
        explicit ProjectExporter(QObject* parent, QString filename = "");

        //! \brief runs when thread starts
        void run();
    signals:
        //! \brief emits error
        void errorSavingFile(QString filename, QString error);

    private:
        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        QSharedPointer< WindowManager > m_window_manager;
        QString m_folder;
    };  // class ProjectExporter
}  // namespace ORNL
#endif  // PROJECTEXPORTER_H
