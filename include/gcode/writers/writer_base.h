#ifndef WRITER_BASE_H
#define WRITER_BASE_H

//! \file writer_base.h

#include <QDate>
#include <QFile>
#include <QTextStream>

#include "geometry/arc.h"
#include "geometry/line.h"
#include "geometry/path_segment.h"
#include "utilities/constants.h"
#include "utilities/enums.h"

namespace ORNL
{
    class PathSegment;

    /*!
     * \class WriterBase
     * \brief The abstract base class for all gcode writers
     *
     * \note All the virtual functions have no body; however, all
     *       inheriting classes are required to implement them.
     */
    class WriterBase
    {
    public:
        //! \brief Default Constructor
        WriterBase() = default;

        //! \brief Writes a tag denoting that the gcode came from this slicer
        QString slicerHeader(QString filename);

        //! \brief Write the header to the file
        virtual QString header() = 0;

        //! \brief Write the line for a layer change
        virtual QString onLayerChange(uint layer_nr) = 0;

        //! \brief Write the footer to the file
        virtual QString footer() = 0;

        //! \brief Write a path segment
        virtual QString pathSegment(PathSegment& path_segment) = 0;

        //! \brief write a line path
        virtual QString line(Line& line,
                             Velocity speed,
                             Acceleration acceleration,
                             RegionType region_type,
                             PathModifiers path_modifiers) = 0;

        //! \brief write an arc path
        virtual QString arc(Arc& arc,
                            Velocity speed,
                            Acceleration acceleration,
                            RegionType region_type,
                            PathModifiers path_modifiers) = 0;

        //! \brief Write a comment
        virtual QString comment(QString text) = 0;

    protected:
        Distance m_current_x;
        Distance m_current_y;
        Distance m_current_z;
        Distance m_current_w;
        Velocity m_current_speed;
        AngularVelocity m_current_extruder_speed;
        Acceleration m_current_acceleration;
    };  // class WriterBase
}  // namespace ORNL
#endif  // WRITER_BASE_H
