#ifndef GCODECOMMAND_H
#define GCODECOMMAND_H

#include <QMap>
#include <QString>

#include "utilities/constants.h"

namespace ORNL
{
    //! \brief A plain old data structure class for a GCode command.
    class GcodeCommand
    {
    public:
        GcodeCommand();

        //! \brief Gets the line number from the Gcode command.
        //! \return The Linenumber of the Gcode command.
        const int& getLineNumber() const;

        //! \brief Gets the command character from the Gcode command.
        //! \return The command character of the Gcode command.
        const char& getCommand() const;

        //! \brief Gets the command ID number from the Gcode command.
        //! \return The command ID number of the Gcode command.
        const int& getCommandID() const;

        //! \brief Gets the parameters mapping associated with that Gcode
        //! command. \return The parameters mapping of the Gcode command.
        const QMap< char, float >& getParameters() const;

        //! \brief Gets the comment associated with that Gcode command.
        //! \return The Gcode command's comment
        const QString& getComment() const;


        // Setters
        //! \brief Sets the line number.
        //! \param line_number Line number of the command.
        void setLineNumber(const int line_number);

        //! \brief Sets the command character.
        //! \param command Command character of the Gcode command.
        void setCommand(const char command);

        //! \brief Sets the command ID number of the Gcode command.
        //! \param commandID ID number of the GCode command.
        void setCommandID(const int commandID);

        //! \brief Sets the comment of the Gcode command.
        //! \param comment Comment string assoicated with the Gcode Command.
        void setComment(const QString comment);

        //! \brief Adds a parameter to the parameter mapping.
        //! \param param_key Character of the Gcode command parameter.
        //! \param parma_value Float value that conincides with the Gcode
        //! command key.
        void addParameter(const char param_key, const float param_value);

        //! \brief Removes a parameter to the parameter mapping.
        //! \param param_key Character of the Gcode command parameter.
        //! \return true if the key was sucessfully removed. false if the key
        //! was not in the mapping.
        bool removeParameter(const char param_key);

        //! \brief Clears all parameters from the mapping.
        void clearParameters();

        //! \brief Clears the comment string.
        void clearComment();

        //! \brief Equality operator to check if two gcode commands are equal.
        //! \note Equality is determined for the mapping of both commands if the
        //! commands have the
        //!       same mappings, not the same order.
        bool operator==(const GcodeCommand& r);

        //! \brief Inequality operator to check for inequality.
        bool operator!=(const GcodeCommand& r);

    private:
        int m_line_number;
        char m_command;
        int m_command_id;
        QMap< char, float > m_parameters;
        QString m_comment;
    };
}  // namespace ORNL

#endif  // GCODECOMMAND_H
