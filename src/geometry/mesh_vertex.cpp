#include "geometry/mesh_vertex.h"

namespace ORNL
{
    MeshVertex::MeshVertex(Point location, Point normal)
        : location(location)
        , normal(normal)
    {
        connected_faces.reserve(8);
    }

    MeshVertex MeshVertex::transform(
        const Qt3DCore::QTransform& transform) const
    {
        MeshVertex rv;
        rv.normal          = normal;
        rv.connected_faces = connected_faces;
        rv.location =
            Point::fromQVector3D(location.toQVector3D() * transform.matrix());
        return rv;
    }

    bool operator<(const MeshVertex& lhs, const MeshVertex& rhs)
    {
        if (lhs.location.x() < rhs.location.x())
        {
            return true;
        }
        else if (lhs.location.x() > rhs.location.x())
        {
            return false;
        }
        // lhs.location.x() == rhs.location.x()
        else if (lhs.location.y() < rhs.location.y())
        {
            return true;
        }
        else if (lhs.location.y() > rhs.location.y())
        {
            return false;
        }
        // lhs.location.y() == rhs.location.y()
        else if (lhs.location.z() < rhs.location.z())
        {
            return true;
        }
        else if (lhs.location.z() > rhs.location.z())
        {
            return false;
        }
        // lhs.location.z() == rhs.location.z()
        else
        {
            return false;
        }
    }

}  // namespace ORNL
