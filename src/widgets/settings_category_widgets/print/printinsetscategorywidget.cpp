#include "widgets/settings_category_widgets/print/printinsetscategorywidget.h"

#include "ui_print_insets_category_widget.h"

namespace ORNL
{
    PrintInsetsCategoryWidget::PrintInsetsCategoryWidget(QWidget* parent)
        : ui(new Ui::PrintInsetsCategoryWidget)
    {
        ui->setupUi(this);

        // Setup the use insets checkbox
        ui->enable->label(Constants::PrintSettings::Insets::Labels::kEnable);
        ui->enable->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kEnable);
        m_checkbox[Constants::PrintSettings::Insets::kEnable] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->enable);

        // Setup insets extruder id
        ui->extruder_id->label(
            Constants::PrintSettings::Insets::Labels::kExtruderId);
        ui->extruder_id->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kExtruderId);
        m_count[Constants::PrintSettings::Insets::kExtruderId] =
            QSharedPointer< TemplateCountSettingItem >(ui->extruder_id);

        // Setup the number of insets rings textbox
        ui->number->label(Constants::PrintSettings::Insets::Labels::kNumber);
        ui->number->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kNumber);
        m_count[Constants::PrintSettings::Insets::kNumber] =
            QSharedPointer< TemplateCountSettingItem >(ui->number);

        // Setup enable external first
        ui->external_first->label(
            Constants::PrintSettings::Insets::Labels::kExternalFirst);
        ui->external_first->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kExternalFirst);
        m_checkbox[Constants::PrintSettings::Insets::kExternalFirst] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->external_first);

        // Setup enable skeletons
        ui->enable_skeletons->label(
            Constants::PrintSettings::Insets::Labels::kEnableSkeletons);
        ui->enable_skeletons->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kEnableSkeletons);
        m_checkbox[Constants::PrintSettings::Insets::kEnableSkeletons] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->enable_skeletons);

        // Setup enable variable width skeletons
        ui->enable_variable_width_skeletons->label(
            Constants::PrintSettings::Insets::Labels::
                kEnableVariableWidthSkeletons);
        ui->enable_variable_width_skeletons->tooltip(
            Constants::PrintSettings::Insets::Tooltips::
                kEnableVariableWidthSkeletons);
        m_checkbox
            [Constants::PrintSettings::Insets::kEnableVariableWidthSkeletons] =
                QSharedPointer< TemplateCheckboxSettingItem >(
                    ui->enable_variable_width_skeletons);

        // Setup only retract when crossing perimeters
        ui->only_retract_when_crossing->label(
            Constants::PrintSettings::Insets::Labels::kOnlyRetractWhenCrossing);
        ui->only_retract_when_crossing->tooltip(
            Constants::PrintSettings::Insets::Tooltips::
                kOnlyRetractWhenCrossing);
        m_checkbox[Constants::PrintSettings::Insets::kOnlyRetractWhenCrossing] =
            QSharedPointer< TemplateCheckboxSettingItem >(
                ui->only_retract_when_crossing);

        // Setup small length
        ui->small_length->label(
            Constants::PrintSettings::Insets::Labels::kSmallLength);
        ui->small_length->tooltip(
            Constants::PrintSettings::Insets::Tooltips::kSmallLength);
        m_distance[Constants::PrintSettings::Insets::kSmallLength] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->small_length);

        initializeSettings();
    }

    PrintInsetsCategoryWidget::~PrintInsetsCategoryWidget()
    {
        delete ui;
    }

    void PrintInsetsCategoryWidget::handleConditions()
    {
        // None of these settings are affected by other settings
    }
}  // namespace ORNL
