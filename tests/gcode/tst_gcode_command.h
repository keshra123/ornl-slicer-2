#ifndef TST_GCODECOMMAND_H
#define TST_GCODECOMMAND_H

//! \file tst_gcodecommand.h

#include <QtTest/QtTest>

#include "auto_test.h"

#if 0 // TODO fix tests

using namespace AutoTest;
    /*!
     * \brief The TestGCodeCommand tests getters, setters and operators.
     */
    class TestGcodeCommand : public QObject
    {
    public:
        Q_OBJECT
    private slots:

        void testGettersAndSetters();
        void testEqualsOperator();
        void testNotEqualsOperator();
    };

DECLARE_TEST(TestGcodeCommand)

#endif // PARSERTESTS_H

#endif
