#ifndef BLUEGANTRYPARSER_H
#define BLUEGANTRYPARSER_H

//! \file bluegantryparser.h

#include "common_parser.h"

namespace ORNL
{
    /*!
     * \class BluegantryParser
     * \brief This class implements the GCode parsing configuration for the
     * Bluegantry 3D printer(s). \note The current commands that this class
     * implements are:
     *          - M Commands:
     *              -
     *          - Tool Change Commands:
     *              - All T commands are checked for syntax errors but otherwise
     * do nothing. The commands inherited from the CommonParser superclass are:
     *          - G Commands:
     *              - G0, G1, G2, G3, G4
     *          - Line Comment Delimiter:
     *              - ;
     *
     */
    class BluegantryParser : public CommonParser
    {
    public:
        BluegantryParser();

        virtual void config();

    protected:
        // - G1: Just like normal but with an extra parameter
        //     - E: tells whether the extruder is on or off.
        //         - -1: retract
        //         - 0: Off
        //         - 1: On
        //     - If has no parameters it moves to the next layer
        virtual void G1Handler(QStringList& commands);

        // - G21: Sets units to mm
        virtual void G21Handler();

        // - G90: Use absolute coordinates
        virtual void G90Handler();

        // - M67: ????
        //      - B: #
        //      - V: #
        //      - This can have multiple of the same parameter
        virtual void M67Handler(QStringList& commands);

        // - M72: Repeats last layer
        //      - E: ?? (Prob on/off)
        //      - Z: (Height?)
        virtual void M72Handler(QStringList& commands);

        // - M83: User relative distances for extrusion
        virtual void M83Handler();

        // - M84: ????
        virtual void M84Handler();

        // - M104: Sets Temperature
        //      - Has S Parameter (Temparture value)
        virtual void M104Handler(QStringList& commands);

        // - M109: Waits for temperature to be reached
        //      - Has S Parameter (Temparture value)
        virtual void M109Handler(QStringList& commands);

        //! \brief Funciton handler for tool changes in GCode. For now its just
        //! a placeholder function.
        virtual void ToolChangeHandler(QStringList& params);

    private:
    };
}  // namespace ORNL
#endif  // BLUEGANTRYPARSER_H
