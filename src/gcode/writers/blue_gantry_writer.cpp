#include "gcode/writers/blue_gantry_writer.h"
namespace ORNL
{
    QString BlueGantryWriter::header()
    {
        return "";
    }

    QString BlueGantryWriter::onLayerChange()
    {
        return "";
    }

    QString BlueGantryWriter::footer()
    {
        return "";
    }

    QString BlueGantryWriter::pathSegment(PathSegment& path_segment)
    {
        return "";
    }

    QString BlueGantryWriter::line(Line& line)
    {
        return "";
    }

    QString BlueGantryWriter::arc(Arc& arc)
    {
        return "";
    }

    QString BlueGantryWriter::comment(QString text)
    {
        QString rv = "; ";
        rv += text;
        return rv;
    }
}  // namespace ORNL
