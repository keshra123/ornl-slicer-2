#ifndef LOCALCONFIGWIDGET_H
#define LOCALCONFIGWIDGET_H

#include <QMap>
#include <QStringList>
#include <QWidget>

#include "configs/settings_base.h"
#include "managers/preferences_manager.h"
#include "utilities/constants.h"
#include "utilities/enums.h"
#include "widgets/settings_category_widgets/settingcategorywidgets.h"

namespace Ui
{
    class LocalConfigWidget;
}

namespace ORNL
{
    /*!
     * \class LocalConfigWidget
     * \brief The config widget for model/mesh/layer specific settings
     */
    class LocalConfigWidget : public QWidget
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        explicit LocalConfigWidget(QWidget* parent = nullptr);

        //! \brief Destructor
        ~LocalConfigWidget();

        //! \brief Sets which Config Type this widget is for
        void setConfigType(ConfigTypes config_type);

        //! \brief Sets the current config
        void setCurrentConfig(SettingsBase* temp, SettingsBase* undo_temp);

    public slots:
        //! \brief Reload settings display based on category
        void handleCategorySelectionChanged();

    private:
        Ui::LocalConfigWidget* ui;

        ConfigTypes m_config_type;

        QStringList m_categories_names;
        QString m_current_category;
        QMap< QString, SettingsCategoryWidgetBase* > m_category_widgets;
        SettingsBase* m_temp_settings;
        SettingsBase* m_undo_temp_settings;

        QSharedPointer< PreferencesManager > m_preferences_manager;
    };
}  // namespace ORNL

#endif  // LOCALCONFIGWIDGET_H
