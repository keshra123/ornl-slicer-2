#include "geometry/polygon.h"
namespace ORNL
{
    Polygon::Polygon(const QVector< Point >& path)
    {
        for (Point point : path)
        {
            append(point);
        }
    }

    Polygon::Polygon(const QVector< ClipperLib::IntPoint >& path)
    {
        for (ClipperLib::IntPoint point : path)
        {
            append(Point(point));
        }
    }

    Polygon::Polygon(const ClipperLib::Path& path)
    {
        for (ClipperLib::IntPoint point : path)
        {
            append(Point(point));
        }
    }

    Polygon::Polygon(const QVector< Distance2D >& path)
    {
        for (Distance2D point : path)
        {
            append(Point(point));
        }
    }

    bool Polygon::orientation() const
    {
        return ClipperLib::Orientation((*this)());
    }

    PolygonList Polygon::offset(Distance distance,
                                ClipperLib::JoinType joinType) const
    {
        ClipperLib::Paths paths;
        ClipperLib::ClipperOffset clipper;
        clipper.AddPath(operator()(), joinType, ClipperLib::etClosedPolygon);
        clipper.Execute(paths, distance());
        PolygonList polygons;
        for (ClipperLib::Path path : paths)
        {
            Polygon polygon;
            foreach (ClipperLib::IntPoint point, path)
            {
                polygon.push_back(Point(point));
            }
            polygons.push_back(polygon);
        }
        return polygons;
    }

    int64_t Polygon::polygonLength() const
    {
        int64_t rv = 0;
        Point prev = operator[](size() - 1);
        for (int i = 0; i < size(); i++)
        {
            Point current = (*this)[i];
            rv += current.distance(prev)();
            prev = (*this)[i];
        }
        return rv;
    }

    bool Polygon::shorterThan(Distance rhs) const
    {
        return polygonLength() < rhs();
    }

    Point Polygon::center() const
    {
        Point minimum = min();
        Point maximum = max();

        return (maximum + minimum) / 2.0;
    }

    Point Polygon::min() const
    {
        Point rv(std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max());
        for (Point point : (*this))
        {
            if (point.x() < rv.x())
            {
                rv.x(point.x());
            }

            if (point.y() < rv.y())
            {
                rv.y(point.y());
            }

            if (point.z() < rv.z())
            {
                rv.z(point.z());
            }
        }
        return rv;
    }

    Point Polygon::max() const
    {
        Point rv(std::numeric_limits< double >::lowest(),
                 std::numeric_limits< double >::lowest(),
                 std::numeric_limits< double >::lowest());

        for (Point point : (*this))
        {
            if (point.x() > rv.x())
            {
                rv.x(point.x());
            }

            if (point.y() > rv.y())
            {
                rv.y(point.y());
            }

            if (point.z() > rv.z())
            {
                rv.z(point.z());
            }
        }
        return rv;
    }

    Polygon Polygon::rotate(Angle rotation_angle, QVector3D axis)
    {
        return rotateAround({0, 0, 0}, rotation_angle, axis);
    }

    Polygon Polygon::rotateAround(Point center,
                                  Angle rotation_angle,
                                  QVector3D axis)
    {
        Polygon polygon;

        for (Point point : (*this))
        {
            polygon.append(point.rotateAround(center, rotation_angle, axis));
        }
        return polygon;
    }

    bool Polygon::inside(Point point, bool border_result) const
    {
        int res = ClipperLib::PointInPolygon(point.toIntPoint(), (*this)());
        if (res == -1)
        {
            return border_result;
        }
        return res == 1;
    }


    Area Polygon::area() const
    {
        return Area(ClipperLib::Area((*this)()));
    }

    Point Polygon::centerOfMass() const
    {
        double x = 0, y = 0;
        Point p0 = (*this)[size() - 1];

        for (Point p1 : (*this))
        {
            double second_factor = (p0.x() * p1.y()) - (p1.x() * p0.y());

            x += double(p0.x() + p1.x()) * second_factor;
            y += double(p0.y() + p1.y()) * second_factor;
            p0 = p1;
        }

        Area a = area();

        x = x / 6 / a();
        y = y / 6 / a();

        return Point(x, y);
    }

    Point Polygon::closestPointTo(const Point& rhs) const
    {
        Point ret         = rhs;
        Distance bestDist = Distance(std::numeric_limits< float >::max());
        for (Point point : (*this))
        {
            Distance dist = rhs.distance(point);
            if (dist < bestDist)
            {
                ret      = point;
                bestDist = dist;
            }
        }
        return ret;
    }

    PolygonList Polygon::operator+(const PolygonList& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPaths(rhs(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator+(const Polygon& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPath(rhs(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator-(const PolygonList& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator-(const Polygon& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPath(rhs(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator|(const PolygonList& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPaths(rhs(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator|(const Polygon& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPath(rhs(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator&(const PolygonList& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPaths(rhs(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator&(const Polygon& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPath(rhs(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    QVector< Polyline > Polygon::operator&(const Polyline& rhs)
    {
        ClipperLib::PolyTree poly_tree;
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath(rhs(), ClipperLib::ptSubject, false);
        clipper.AddPath(operator()(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        poly_tree,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        ClipperLib::OpenPathsFromPolyTree(poly_tree, paths);

        QVector< Polyline > rv;
        for (ClipperLib::Path path : paths)
        {
            rv += Polyline(path);
        }
        return rv;
    }

    PolygonList Polygon::operator^(const PolygonList& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPaths(rhs(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList Polygon::operator^(const Polygon& rhs)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath((*this)(), ClipperLib::ptSubject, true);
        clipper.AddPath(rhs(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    bool Polygon::operator==(const Polygon& right)
    {
        if ((*this).size() == right.size())
        {
            int i = 0;
            for (i = 0; i < right.size(); i++)
            {
                if ((right.size() - 1) == i && ((*this)[0] != right[i]))
                {
                    return false;
                }
                if ((*this)[0] == right[i])
                {
                    break;
                }
            }
            int index = i;
            for (int j = 0; j < right.size(); j++)
            {
                if ((*this)[j] != right[index])
                {
                    return false;
                }
                index = (index + 1) % right.size();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Polygon::operator!=(const Polygon& right)
    {
        if ((*this).size() == right.size())
        {
            int i = 0;
            for (i = 0; i < right.size(); i++)
            {
                if ((right.size() - 1) == i && ((*this)[0] != right[i]))
                {
                    return true;
                }
                if ((*this)[0] == right[i])
                {
                    break;
                }
            }
            int index = i;
            for (int j = 0; j < right.size(); j++)
            {
                if ((*this)[j] != right[index])
                {
                    return true;
                }
                index = (index + 1) % right.size();
            }
            return false;
        }
        else
        {
            return true;
        }
    }

    ClipperLib::Path Polygon::operator()() const
    {
        ClipperLib::Path path;
        for (Point point : (*this))
        {
            path.push_back(point.toIntPoint());
        }
        return path;
    }
}  // namespace ORNL
