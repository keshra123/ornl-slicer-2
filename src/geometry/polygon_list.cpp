#include "geometry/polygon_list.h"
namespace ORNL
{
    PolygonList::PolygonList()
    {}

    uint PolygonList::pointCount() const
    {
        uint rv = 0;
        for (Polygon p : (*this))
        {
            rv += p.size();
        }
        return rv;
    }

    PolygonList PolygonList::offset(Distance distance,
                                    ClipperLib::JoinType joinType) const
    {
        ClipperLib::Paths paths;
        ClipperLib::ClipperOffset clipper;
        clipper.AddPaths((*this)(), joinType, ClipperLib::etClosedPolygon);
        clipper.Execute(paths, distance());
        PolygonList polygons;
        for (ClipperLib::Path path : paths)
        {
            Polygon polygon;
            foreach (ClipperLib::IntPoint point, path)
            {
                polygon.push_back(Point(point));
            }
            polygons.push_back(polygon);
        }
        return polygons;
    }

    bool PolygonList::inside(Point p, bool border_result)
    {
        int poly_count_inside = 0;
        for (const ClipperLib::Path& poly : (*this)())
        {
            const int is_inside_this_poly =
                ClipperLib::PointInPolygon(p.toIntPoint(), poly);
            if (is_inside_this_poly == -1)
            {
                return border_result;
            }
            poly_count_inside += is_inside_this_poly;
        }
        return (poly_count_inside % 2) == 1;
    }

    uint PolygonList::findInside(Point p, bool border_result)
    {
        PolygonList& thiss = *this;
        if (size() < 1)
        {
            return false;
        }

        uint size_    = size();
        double* min_x = new double[size_];
        std::fill_n(
            min_x,
            size_,
            std::numeric_limits< double >::max());  // initialize with int.max
        int* crossings = new int[size_];
        std::fill_n(crossings, size_, 0);  // initialize with zeros

        for (uint poly_idx = 0; poly_idx < size_; poly_idx++)
        {
            Polygon poly = thiss[poly_idx];
            Point p0     = poly.back();
            for (Point& p1 : poly)
            {
                short comp = LinearAlg2D::pointLiesOnTheRightOfLine(p, p0, p1);
                if (comp == 1)
                {
                    crossings[poly_idx]++;
                    double x;
                    if (p1.y() == p0.y())
                    {
                        x = p0.x();
                    }
                    else
                    {
                        x = p0.x() +
                            (p1.x() - p0.x()) * (p.y() - p0.y()) /
                                (p1.y() - p0.y());
                    }
                    if (x < min_x[poly_idx])
                    {
                        min_x[poly_idx] = x;
                    }
                }
                else if (border_result && comp == 0)
                {
                    return poly_idx;
                }
                p0 = p1;
            }
        }

        double min_x_uneven = std::numeric_limits< double >::max();
        uint ret            = NO_INDEX;
        uint n_unevens      = 0;
        for (uint array_idx = 0; array_idx < size_; array_idx++)
        {
            if (crossings[array_idx] % 2 == 1)
            {
                n_unevens++;
                if (min_x[array_idx] < min_x_uneven)
                {
                    min_x_uneven = min_x[array_idx];
                    ret          = array_idx;
                }
            }
        }
        if (n_unevens % 2 == 0)
        {
            ret = NO_INDEX;
        }
        return ret;
    }

    Polygon PolygonList::convexHull() const
    {
        uint num_points = 0;
        for (const ClipperLib::Path& path : (*this)())
        {
            num_points += path.size();
        }

        QVector< ClipperLib::IntPoint > all_points;
        for (const ClipperLib::Path& path : (*this)())
        {
            for (const ClipperLib::IntPoint& point : path)
            {
                all_points.push_back(point);
            }
        }

        struct HullSort
        {
            bool operator()(const ClipperLib::IntPoint& a,
                            const ClipperLib::IntPoint& b)
            {
                return (a.X < b.X) || (a.X == b.X && a.Y < b.Y);
            }
        };

        qSort(all_points.begin(), all_points.end(), HullSort());

        // positive for left turn, 0 for straight, negative for right turn
        auto ccw = [](const ClipperLib::IntPoint& p0,
                      const ClipperLib::IntPoint& p1,
                      const ClipperLib::IntPoint& p2) -> int64_t {
            ClipperLib::IntPoint v01(p1.X - p0.X, p1.Y - p0.Y);
            ClipperLib::IntPoint v12(p2.X - p1.X, p2.Y - p1.Y);

            return static_cast< int64_t >(v01.X) * v12.Y -
                static_cast< int64_t >(v01.Y) * v12.X;
        };

        Polygon hull_poly;
        ClipperLib::Path hull_points = hull_poly();
        hull_points.resize(num_points + 1);

        // index to insert next hull point, also number of valid hull points
        uint hull_idx = 0;

        // Build lower hull
        for (uint pt_idx = 0; pt_idx < num_points; pt_idx++)
        {
            while (hull_idx >= 2 &&
                   ccw(hull_points[hull_idx - 2],
                       hull_points[hull_idx - 1],
                       all_points[pt_idx]) <= 0)
            {
                hull_idx--;
            }
            hull_points[hull_idx] = all_points[pt_idx];
            hull_idx++;
        }

        // Build upper hull
        uint min_upper_hull_chain_end_idx = hull_idx + 1;
        for (int pt_idx = num_points - 2; pt_idx >= 0; pt_idx--)
        {
            while (hull_idx >= min_upper_hull_chain_end_idx &&
                   ccw(hull_points[hull_idx - 2],
                       hull_points[hull_idx - 1],
                       all_points[pt_idx]) <= 0)
            {
                hull_idx--;
            }
            hull_points[hull_idx] = all_points[pt_idx];
            hull_idx++;
        }


        // assert(hull_idx <= hull_points.size());

        // Last point is duplicted with first.  It is removed in the resize.
        hull_points.resize(hull_idx - 2);

        return hull_poly;
    }

    PolygonList PolygonList::smooth(Distance removeLength)
    {
        // TODO: psmooth
        return PolygonList();
    }

    PolygonList PolygonList::simplify(Distance smallest_line_segment,
                                      Distance allowed_error_distance)
    {
        // TODO
        return PolygonList();
    }

    PolygonList PolygonList::removeEmptyHoles() const
    {
        PolygonList ret;
        ClipperLib::Clipper clipper(clipper_init);
        ClipperLib::PolyTree poly_tree;
        constexpr bool paths_are_closed_polys = true;
        clipper.AddPaths(
            (*this)(), ClipperLib::ptSubject, paths_are_closed_polys);
        clipper.Execute(ClipperLib::ctUnion, poly_tree);

        bool remove_holes = true;
        removeEmptyHoles_processPolyTreeNode(poly_tree, remove_holes, ret);
        return ret;
    }

    PolygonList PolygonList::getEmptyHoles() const
    {
        PolygonList ret;
        ClipperLib::Clipper clipper(clipper_init);
        ClipperLib::PolyTree poly_tree;
        constexpr bool paths_are_closed_polys = true;
        clipper.AddPaths(
            (*this)(), ClipperLib::ptSubject, paths_are_closed_polys);
        clipper.Execute(ClipperLib::ctUnion, poly_tree);

        bool remove_holes = false;
        removeEmptyHoles_processPolyTreeNode(poly_tree, remove_holes, ret);
        return ret;
    }

    void PolygonList::removeEmptyHoles_processPolyTreeNode(
        const ClipperLib::PolyNode& node,
        const bool remove_holes,
        PolygonList& ret) const
    {
        for (int outer_poly_idx = 0; outer_poly_idx < node.ChildCount();
             outer_poly_idx++)
        {
            ClipperLib::PolyNode* child = node.Childs[outer_poly_idx];
            if (remove_holes)
            {
                ret += child->Contour;
            }
            for (int hole_node_idx = 0; hole_node_idx < child->ChildCount();
                 hole_node_idx++)
            {
                ClipperLib::PolyNode& hole_node = *child->Childs[hole_node_idx];
                if ((hole_node.ChildCount() > 0) == remove_holes)
                {
                    ret += hole_node.Contour;
                    removeEmptyHoles_processPolyTreeNode(
                        hole_node, remove_holes, ret);
                }
            }
        }
    }

    QVector< PolygonList > PolygonList::splitIntoParts(bool unionAll) const
    {
        QVector< PolygonList > ret;
        ClipperLib::Clipper clipper(clipper_init);
        ClipperLib::PolyTree resultPolyTree;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        if (unionAll)
            clipper.Execute(ClipperLib::ctUnion,
                            resultPolyTree,
                            ClipperLib::pftNonZero,
                            ClipperLib::pftNonZero);
        else
            clipper.Execute(ClipperLib::ctUnion, resultPolyTree);

        splitIntoParts_processPolyTreeNode(&resultPolyTree, ret);
        return ret;
    }

    void PolygonList::splitIntoParts_processPolyTreeNode(
        ClipperLib::PolyNode* node,
        QVector< PolygonList >& ret) const
    {
        for (int n = 0; n < node->ChildCount(); n++)
        {
            ClipperLib::PolyNode* child = node->Childs[n];
            PolygonList part;
            part += child->Contour;
            for (int i = 0; i < child->ChildCount(); i++)
            {
                part += child->Childs[i]->Contour;
                splitIntoParts_processPolyTreeNode(child->Childs[i], ret);
            }
            ret.push_back(part);
        }
    }

    PolygonList PolygonList::removeSmallAreas(Area minAreaSize)
    {
        PolygonList ret(*this);
        for (uint i = 0; i < ret.size(); i++)
        {
            Area area = Area(fabs(ret[i].area()()));
            if (area < minAreaSize)  // Only create an up/down skin if the area
                                     // is large enough. So you do not create
                                     // tiny blobs of "trying to fill"
            {
                ret.remove(i);
                i -= 1;
            }
        }
        return ret;
    }

    PolygonList PolygonList::removeDegenerateVertices()
    {
        PolygonList ret(*this);
        for (int poly_idx = 0; poly_idx < ret.size(); poly_idx++)
        {
            Polygon poly = ret[poly_idx];
            Polygon result;

            auto isDegenerate = [](Point& last, Point& now, Point& next) {
                Point last_line = now - last;
                Point next_line = next - now;
                return Point::dot(last_line, next_line) ==
                    -1 * last_line.distance() * next_line.distance();
            };
            bool isChanged = false;
            for (int idx = 0; idx < poly.size(); idx++)
            {
                Point& last =
                    (result.size() == 0) ? poly.back() : result.back();
                if (idx + 1 == poly.size() && result.size() == 0)
                {
                    break;
                }
                Point& next =
                    (idx + 1 == poly.size()) ? result[0] : poly[idx + 1];
                // lines are in the opposite direction
                if (isDegenerate(last, poly[idx], next))
                {
                    // don't add vert to the result
                    isChanged = true;
                    while (result.size() > 1 &&
                           isDegenerate(
                               result[result.size() - 2], result.back(), next))
                    {
                        result.pop_back();
                    }
                }
                else
                {
                    result += (poly[idx]);
                }
            }

            if (isChanged)
            {
                if (result.size() > 2)
                {
                    ret[poly_idx] = result;
                }
                else
                {
                    ret.remove(poly_idx);
                    poly_idx--;  // effectively the next iteration has the same
                                 // poly_idx (referring to a new poly which is
                                 // not yet processed)
                }
            }
        }
        return ret;
    }

    Point PolygonList::min()
    {
        Point rv(std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max());
        for (Polygon p : (*this))
        {
            // ignore holes as the minimum values will always come from an
            // exterior
            if (p.area()() > 0)
            {
                Point temp_min = p.min();

                if (temp_min.x() < rv.x())
                {
                    rv.x(temp_min.x());
                }

                if (temp_min.y() < rv.y())
                {
                    rv.y(temp_min.y());
                }

                if (temp_min.z() < rv.z())
                {
                    rv.z(temp_min.z());
                }
            }
        }
        return rv;
    }

    Point PolygonList::max()
    {
        Point rv(std::numeric_limits< double >::lowest(),
                 std::numeric_limits< double >::lowest(),
                 std::numeric_limits< double >::lowest());

        for (Polygon p : (*this))
        {
            // ignore holes as the maximum values will always come from an
            // exterior
            if (p.area()() > 0)
            {
                Point temp_min = p.max();

                if (temp_min.x() > rv.x())
                {
                    rv.x(temp_min.x());
                }

                if (temp_min.y() > rv.y())
                {
                    rv.y(temp_min.y());
                }

                if (temp_min.z() > rv.z())
                {
                    rv.z(temp_min.z());
                }
            }
        }
        return rv;
    }

    PolygonList PolygonList::rotate(Angle angle, QVector3D axis)
    {
        return rotateAround({0, 0, 0}, angle, axis);
    }

    PolygonList PolygonList::rotateAround(Point center,
                                          Angle angle,
                                          QVector3D axis)
    {
        PolygonList rv;
        for (Polygon polygon : (*this))
        {
            rv += polygon.rotateAround(center, angle, axis);
        }
        return rv;
    }

    PolygonList PolygonList::operator+(const PolygonList& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator+(const Polygon& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator+=(const PolygonList& rhs)
    {
        return _add_to_this(rhs);
    }

    PolygonList PolygonList::operator+=(const Polygon& rhs)
    {
        return _add_to_this(rhs);
    }

    PolygonList PolygonList::operator-(const PolygonList& rhs)
    {
        return _subtract(rhs);
    }

    PolygonList PolygonList::operator-(const Polygon& rhs)
    {
        return _subtract(rhs);
    }

    PolygonList PolygonList::operator-=(const PolygonList& rhs)
    {
        return _subtract_from_this(rhs);
    }

    PolygonList PolygonList::operator-=(const Polygon& rhs)
    {
        return _subtract_from_this(rhs);
    }

    PolygonList PolygonList::operator<<(const PolygonList& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator<<(const Polygon& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator|(const PolygonList& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator|(const Polygon& rhs)
    {
        return _add(rhs);
    }

    PolygonList PolygonList::operator|=(const PolygonList& rhs)
    {
        return _add_to_this(rhs);
    }

    PolygonList PolygonList::operator|=(const Polygon& rhs)
    {
        return _add_to_this(rhs);
    }

    PolygonList PolygonList::operator&(const PolygonList& rhs)
    {
        return _intersect(rhs);
    }

    PolygonList PolygonList::operator&(const Polygon& rhs)
    {
        return _intersect(rhs);
    }

    QVector< Polyline > PolygonList::operator&(const Polyline& rhs)
    {
        return _intersect(rhs);
    }

    PolygonList PolygonList::operator&=(const PolygonList& rhs)
    {
        return _intersect_with_this(rhs);
    }

    PolygonList PolygonList::operator&=(const Polygon& rhs)
    {
        return _intersect_with_this(rhs);
    }

    PolygonList PolygonList::operator^(const PolygonList& rhs)
    {
        return _xor(rhs);
    }

    PolygonList PolygonList::operator^(const Polygon& rhs)
    {
        return _xor(rhs);
    }

    PolygonList PolygonList::operator^=(const PolygonList& rhs)
    {
        return _xor_with_this(rhs);
    }

    PolygonList PolygonList::operator^=(const Polygon& rhs)
    {
        return _xor_with_this(rhs);
    }

    PolygonList::PolygonList(const ClipperLib::Paths& paths)
    {
        clipperLoad(paths);
    }

    void PolygonList::clipperLoad(const ClipperLib::Paths& paths)
    {
        clear();
        for (ClipperLib::Path path : paths)
        {
            Polygon polygon;
            for (ClipperLib::IntPoint point : path)
            {
                polygon += Point(point);
            }
            append(polygon);
        }
    }

    ClipperLib::Paths PolygonList::operator()() const
    {
        ClipperLib::Paths paths;
        for (Polygon polygon : (*this))
        {
            ClipperLib::Path path;
            for (Point point : polygon)
            {
                path.push_back(point.toIntPoint());
            }
            paths.push_back(path);
        }
        return paths;
    }

    PolygonList PolygonList::_add(const Polygon& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_add(const PolygonList& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_add_to_this(const Polygon& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_add_to_this(const PolygonList& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptSubject, true);
        clipper.Execute(ClipperLib::ctUnion,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_subtract(const Polygon& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_subtract(const PolygonList& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_subtract_from_this(const Polygon& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_subtract_from_this(const PolygonList& other)
    {
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctDifference,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_intersect(const Polygon& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_intersect(const PolygonList& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    QVector< Polyline > PolygonList::_intersect(const Polyline& polyline)
    {
        ClipperLib::PolyTree poly_tree;
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptClip, true);
        clipper.AddPath(polyline(), ClipperLib::ptSubject, false);
        clipper.Execute(ClipperLib::ctIntersection,
                        poly_tree,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        ClipperLib::OpenPathsFromPolyTree(poly_tree, paths);

        QVector< Polyline > rv;
        for (ClipperLib::Path path : paths)
        {
            rv += Polyline(path);
        }
        return rv;
    }

    PolygonList PolygonList::_intersect_with_this(const Polygon& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_intersect_with_this(const PolygonList& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctIntersection,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_xor(const Polygon& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_xor(const PolygonList& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        return PolygonList(paths);
    }

    PolygonList PolygonList::_xor_with_this(const Polygon& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPath(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }

    PolygonList PolygonList::_xor_with_this(const PolygonList& other)
    {
        ClipperLib::Clipper clipper;
        ClipperLib::Paths paths;
        clipper.AddPaths(operator()(), ClipperLib::ptSubject, true);
        clipper.AddPaths(other(), ClipperLib::ptClip, true);
        clipper.Execute(ClipperLib::ctXor,
                        paths,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        clipperLoad(paths);
        return (*this);
    }
}  // namespace ORNL
