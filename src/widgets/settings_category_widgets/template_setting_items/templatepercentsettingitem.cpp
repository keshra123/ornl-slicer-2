#include "widgets/settings_category_widgets/template_setting_items/templatepercentsettingitem.h"

#include "ui_template_percent_setting_item.h"

namespace ORNL
{
    TemplatePercentSettingItem::TemplatePercentSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplatePercentSettingItem)
    {
        ui->setupUi(this);

        ui->line_edit->setValidator(new QDoubleValidator(0.0, 100.0, 2));
        ui->line_edit->setText("0");

        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->line_edit,
                SIGNAL(editingFinished()),
                this,
                SLOT(handleValueChanged()));
    }

    TemplatePercentSettingItem::~TemplatePercentSettingItem()
    {
        delete ui;
    }

    void TemplatePercentSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }

    void TemplatePercentSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }

    void TemplatePercentSettingItem::value(double p)
    {
        ui->line_edit->setText(QString::number(100.0f * p, 'f', 2));
    }

    double TemplatePercentSettingItem::value()
    {
        bool ok;
        double d = ui->line_edit->text().toDouble(&ok);
        if (ok)
        {
            return d / 100.0;
        }
        throw SettingValueException("percentage");
    }

    void TemplatePercentSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->line_edit->setStyleSheet(stylesheet);
    }

    void TemplatePercentSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplatePercentSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplatePercentSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplatePercentSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplatePercentSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplatePercentSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplatePercentSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
