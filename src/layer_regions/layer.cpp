#include "layer_regions/layer.h"

#include "geometry/mesh.h"
#include "layer_regions/island.h"

namespace ORNL
{
    Layer::Layer(Mesh* mesh, uint layer_nr)
        : SettingsBase(mesh)
        , m_parent(mesh)
        , m_layer_nr(layer_nr)
    {}

    Mesh* Layer::parent()
    {
        return m_parent;
    }

    uint Layer::layerNumber()
    {
        return m_layer_nr;
    }

    void Layer::setOutlines(PolygonList& polygons)
    {
        clear();
        QVector< PolygonList > polygon_islands = polygons.splitIntoParts();

        for (PolygonList& polygon_island : polygon_islands)
        {
            append(QSharedPointer< Island >(new Island(this, polygon_island)));
        }
    }
}  // namespace ORNL
