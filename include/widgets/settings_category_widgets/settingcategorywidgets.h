#ifndef SETTINGCATEGORYWIDGETS_H
#define SETTINGCATEGORYWIDGETS_H

#include "exceptions/exceptions.h"
#include "machine/machinesettingcategorywidgets.h"
#include "material/materialsettingcategorywidgets.h"
#include "nozzle/nozzlesettingcategorywidget.h"
#include "print/printsettingcategorywidgets.h"

#endif  // SETTINGCATEGORYWIDGETS_H
