#ifndef TEMPLATECOUNTSETTINGITEM_H
#define TEMPLATECOUNTSETTINGITEM_H

#include <QIntValidator>
#include <QWidget>

#include "exceptions/exceptions.h"

namespace Ui
{
    class TemplateCountSettingItem;
}

namespace ORNL
{
    class TemplateCountSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        explicit TemplateCountSettingItem(QWidget* parent = 0);

        //! \brief Destructor
        ~TemplateCountSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        //! \brief Sets the minimum value allowed
        void minimum(int m);

        //! \brief Sets the maximum value allowed
        void maximum(int m);

        //! \brief Returns the value in the textbox
        int value();

        //! \brief Sets the stylesheet for the textbox
        void setStyleSheet(const QString& stylesheet);

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        //! \brief Signals that the value has changed
        void valueChanged(int);

    public slots:
        //! \brief Sets the value
        void value(int number);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    private slots:
        void handleUndo();

        void handleRedo();

        void handleValueChanged();

    private:
        Ui::TemplateCountSettingItem* ui;

        QIntValidator* m_validator;
    };
}  // namespace ORNL


#endif  // TEMPLATECOUNTSETTINGITEM_H
