#ifndef PERIMETERS_H
#define PERIMETERS_H

//! \file perimeters.h

#include "geometry/polygon_list.h"
#include "region.h"

namespace ORNL
{
    class Island;
    class Path;
    enum class RegionType : uint8_t;

    /*!
     * \class Perimeters
     * \brief All the information needed for creating the gcode for the
     * perimeters and insets of this particular island
     */
    class Perimeters : public Region
    {
    public:
        Perimeters(Island* parent, RegionType region_type);

        ~Perimeters() = default;

        //! \brief computes the perimeters and/or insets based on settings
        void computeInternal() override;

        //! \brief order the paths. previous location gets updated to the last
        //! point
        void orderInternal(Point& previous_location) override;

        //! \brief Returns the numbered path
        Path* operator[](uint path_nr);

#ifdef DEBUG
        QStringList debugOptionsInternal() override;

        void paintDebugInternal(QString option,
                                QPainter* painter,
                                Distance& ratio,
                                int vertical_offset) override;
#endif
    private:
        //! \brief Computes either the perimeters or insets as basic loops
        void computeConcentric(Distance& bead_width, int& rings);

        //! \brief Computes either the perimeters or insets with skeletons added
        void computeSkeletonConcentric(Distance& bead_width, int& rings);

        //! \brief Computes either the perimeters or insets with variable
        //! extrusion added
        void computeVariableConcentric(Distance& bead_width, int& rings);

        PolygonList m_inline;
        RegionType m_region_type;

#ifdef DEBUG
        QVector< PolygonList > m_debug_rings;
#endif
    };  // class Perimeters
}  // namespace ORNL
#endif  // INSET_H
