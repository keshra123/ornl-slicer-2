#include "widgets/settings_category_widgets/machine/machinedimensionscategorywidget.h"

#include "ui_machine_dimensions_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    MachineDimensionsCategoryWidget::MachineDimensionsCategoryWidget()
        : ui(new Ui::MachineDimensionsCategoryWidget)
    {
        ui->setupUi(this);

        // Setup minimum x
        ui->minimum_x->label(
            Constants::MachineSettings::Dimensions::Labels::kXMin);
        ui->minimum_x->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kXMin);
        m_distance[Constants::MachineSettings::Dimensions::kXMin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->minimum_x);

        // Setup maximum x
        ui->maximum_x->label(
            Constants::MachineSettings::Dimensions::Labels::kXMax);
        ui->maximum_x->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kXMax);
        m_distance[Constants::MachineSettings::Dimensions::kXMax] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->maximum_x);

        // Setup minimum y
        ui->minimum_y->label(
            Constants::MachineSettings::Dimensions::Labels::kYMin);
        ui->minimum_y->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kYMin);
        m_distance[Constants::MachineSettings::Dimensions::kYMin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->minimum_y);

        // Setup maximum y
        ui->maximum_y->label(
            Constants::MachineSettings::Dimensions::Labels::kYMax);
        ui->maximum_y->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kYMax);
        m_distance[Constants::MachineSettings::Dimensions::kYMax] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->maximum_y);

        // Setup minimum z
        ui->minimum_z->label(
            Constants::MachineSettings::Dimensions::Labels::kZMin);
        ui->minimum_z->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kZMin);
        m_distance[Constants::MachineSettings::Dimensions::kZMin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->minimum_z);

        // Setup maximum z
        ui->maximum_z->label(
            Constants::MachineSettings::Dimensions::Labels::kZMax);
        ui->maximum_z->tooltip(
            Constants::MachineSettings::Dimensions::Tooltips::kZMax);
        m_distance[Constants::MachineSettings::Dimensions::kZMax] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->maximum_z);


        initializeSettings();
    }

    MachineDimensionsCategoryWidget::~MachineDimensionsCategoryWidget()
    {
        delete ui;
    }

    void MachineDimensionsCategoryWidget::handleConditions()
    {}

}  // namespace ORNL
