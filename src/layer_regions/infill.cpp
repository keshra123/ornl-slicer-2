#include "layer_regions/infill.h"

#ifdef DEBUG
#    include <QPainter>

#    include "geometry/polygon_list.h"
#    include "geometry/polyline.h"
#endif

#include "geometry/mesh.h"
#include "geometry/path.h"
#include "layer_regions/island.h"
#include "layer_regions/layer.h"

namespace ORNL
{
    Infill::Infill(Island* island)
        : Region(island)
    {
#ifdef DEBUG
        m_polygon_pen.setColor(Constants::Colors::kInfill);
#endif
    }

    void Infill::computeInternal()
    {
        switch (setting< InfillPatterns >(
            Constants::PrintSettings::Infill::kPattern))
        {
            case InfillPatterns::kLines:
                break;
            case InfillPatterns::kGrid:
                computeGrid();
                break;
            case InfillPatterns::kConcentric:
                Distance offset = setting< Distance >(
                    Constants::PrintSettings::Infill::kLineSpacing);
                PolygonList first_ring = m_outline.offset(-offset);
                computeConcentric(first_ring, offset);
                break;
                /*
            case InfillPatterns::kTriangles:
                computeTriangles();
                break;
            case InfillPatterns::kHexagonsAndTriangles:
                computeHexagonsAndTriangles();
                break;
            case InfillPatterns::kHoneycomb:
                computeHoneycomb();
                break;
            case InfillPatterns::kCubic:
                computeCubic();
                break;
            case InfillPatterns::kTetrahedral:
                computeTetrahedral();
                break;
            case InfillPatterns::kConcentric3D:
                computeConcentric3D();
                break;
            case InfillPatterns::kHoneycomb3D:
                computeHoneycomb3D();
                break;
                */
        }
    }

    void Infill::orderInternal(Point& previous_location)
    {}

    void Infill::computeConcentric(PolygonList first_ring, Distance offset)
    {
        PolygonList* previous = &first_ring;

#ifdef DEBUG
        m_debug_rings += first_ring;
#endif
        m_paths += Path::createPaths(this, RegionType::kInfill, first_ring);

        while (previous->size())
        {
            PolygonList next = previous->offset(-offset);
            m_paths += Path::createPaths(this, RegionType::kInfill, next);
            previous = &next;
        }
    }

    void Infill::computeLine(Distance line_spacing, Angle rotation)
    {
        Point mesh_minimum = m_parent->parent()->parent()->min();
        Point mesh_maximum = m_parent->parent()->parent()->max();

        mesh_minimum = mesh_minimum.rotate(rotation);
        mesh_maximum = mesh_maximum.rotate(rotation);

        PolygonList rotated_polygon = m_outline.rotate(rotation);

        Point outline_minimum = rotated_polygon.min();
        Point outline_maximum = rotated_polygon.max();

        QVector< Polyline > cutlines;

        //! grid is based on the mesh, so no matter where you put it the infill
        //! pattern should be the same
        for (Distance x = mesh_minimum.toDistance3D().x + line_spacing / 2;
             x < mesh_maximum.toDistance3D().x;
             x += line_spacing)
        {
            if (x < outline_minimum.x())
            {
                continue;
            }
            if (x > outline_maximum.x())
            {
                break;
            }

            Polyline cutline;
            cutline << Point(x(), outline_minimum.y());
            cutline << Point(x(), outline_maximum.y());

            cutlines += rotated_polygon & cutline;
        }

        for (int i = 0; i < cutlines.size(); i++)
        {
            // unrotate
            cutlines[i] = cutlines[i].rotate(-rotation);
        }

        m_paths += Path::createPaths(this, RegionType::kInfill, cutlines);
    }

    void Infill::computeGrid()
    {}

    void Infill::computeTriangles()
    {}

    void Infill::computeHoneycomb()
    {}

    void Infill::computeCirclePack()
    {}


#ifdef DEBUG
    QStringList Infill::debugOptionsInternal()
    {
        QStringList rv;
        rv << "Outline";
        // TODO: Add other infill patterns
        switch (
            setting< InfillPatterns >(Constants::PrintSettings::Skin::kPattern))
        {
            case InfillPatterns::kConcentric:
                for (int i = 0; i < m_debug_rings.size(); i++)
                {
                    rv << QString("Ring %1").arg(i + 1);
                }
                break;
            case InfillPatterns::kLines:
                for (int i = 0; i < m_debug_lines.size(); i++)
                {
                    rv << QString("Line %1").arg(i + 1);
                }
                break;
            default:
                Q_ASSERT(false);
        }
        rv << "Inline";
        for (int i = 0; i < m_paths.size(); i++)
        {
            rv << QString("Path %1").arg(i + 1);
        }
        return rv;
    }

    void Infill::paintDebugInternal(QString option,
                                    QPainter* painter,
                                    Distance& ratio,
                                    int vertical_offset)
    {
        if (option == "Outline")
        {
            QPainterPath painter_path;
            for (Polygon polygon : m_outline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            painter->setPen(m_outline_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_centerline_pen);
            painter->drawPath(painter_path);
        }
        else if (option == "Inline")
        {
            QPainterPath painter_path;
            for (Polygon polygon : m_outline)
            {
                if (polygon.size())
                {
                    Point p  = polygon[0];
                    double x = p.x() * ratio();
                    double y = p.y() * ratio() + vertical_offset;
                    painter_path.moveTo(x, y);
                    for (int i = 1; i <= polygon.size(); i++)
                    {
                        p = polygon[i % polygon.size()];
                        x = p.x() * ratio();
                        y = p.y() * ratio() + vertical_offset;
                        painter_path.lineTo(x, y);
                    }
                }
            }
            painter->setPen(m_outline_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_polygon_pen);
            painter->drawPath(painter_path);
            painter->setPen(m_centerline_pen);
            painter->drawPath(painter_path);
        }
        else if (option.contains("Path"))
        {
            int path_nr = option.mid(5).toInt();
            m_paths[path_nr].paintDebug(painter, ratio, vertical_offset);
        }
        else
        {
            // TODO: Add other infill patterns
            switch (setting< InfillPatterns >(
                Constants::PrintSettings::Skin::kPattern))
            {
                case InfillPatterns::kConcentric:
                    if (option.contains("Ring"))
                    {
                        int ring_nr = option.mid(5).toInt() - 1;
                        QPainterPath painter_path;
                        Q_ASSERT(ring_nr < m_debug_rings.size());
                        for (Polygon polygon : m_debug_rings[ring_nr])
                        {
                            if (polygon.size())
                            {
                                Point p  = polygon[0];
                                double x = p.x() * ratio();
                                double y = p.y() * ratio() + vertical_offset;
                                painter_path.moveTo(x, y);
                                for (int i = 1; i <= polygon.size(); i++)
                                {
                                    p = polygon[i % polygon.size()];
                                    x = p.x() * ratio();
                                    y = p.y() * ratio() + vertical_offset;
                                    painter_path.lineTo(x, y);
                                }
                            }
                        }
                        painter->setPen(m_outline_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_polygon_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_centerline_pen);
                        painter->drawPath(painter_path);
                    }
                    else
                    {
                        Q_ASSERT(false);
                    }
                    break;
                case InfillPatterns::kLines:
                    if (option.contains("Line"))
                    {
                        int line_nr = option.mid(5).toInt() - 1;
                        QPainterPath painter_path;
                        Q_ASSERT(line_nr < m_debug_lines.size());
                        Polyline polyline = m_debug_lines[line_nr];

                        if (polyline.size())
                        {
                            Point p  = polyline[0];
                            double x = p.x() * ratio();
                            double y = p.y() * ratio() + vertical_offset;
                            painter_path.moveTo(x, y);
                            for (int i = 1; i < polyline.size(); i++)
                            {
                                p = polyline[i];
                                x = p.x() * ratio();
                                y = p.y() * ratio() + vertical_offset;
                                painter_path.lineTo(x, y);
                            }
                        }

                        painter->setPen(m_outline_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_polygon_pen);
                        painter->drawPath(painter_path);
                        painter->setPen(m_centerline_pen);
                        painter->drawPath(painter_path);
                    }
                default:
                    Q_ASSERT(false);
            }
        }
    }

#endif
}  // namespace ORNL
