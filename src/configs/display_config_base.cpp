#include "configs/display_config_base.h"

#include <QDir>
#include <QStandardPaths>

#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    DisplayConfigBase::DisplayConfigBase(QString name,
                                         QString settings_type,
                                         SettingsBase* parent,
                                         QString folder)
        : SettingsBase(parent)
        , m_name(name)
        , m_extension("." + settings_type)
    {
        if (!folder.isEmpty())
        {
            m_path = folder + QDir::separator() + name + "." + settings_type;
        }
        else if (settings_type == "nozzle")
        {
            uint colon            = name.indexOf(':');
            QString material_name = name.left(colon);
            QString nozzle_name   = name.mid(colon + 1);

            m_path = QStandardPaths::writableLocation(
                         QStandardPaths::AppDataLocation) +
                QDir::separator() + settings_type + QString("_settings") +
                QDir::separator() + material_name + QDir::separator() +
                nozzle_name + "." + settings_type;
        }
        else
        {
            m_path = QStandardPaths::writableLocation(
                         QStandardPaths::AppDataLocation) +
                QDir::separator() + settings_type + QString("_settings") +
                QDir::separator() + name + "." + settings_type;
        }

        // TODO: determine if attempt load
    }

    QString DisplayConfigBase::getName()
    {
        return m_name;
    }

    QString DisplayConfigBase::getExtension()
    {
        return m_extension;
    }

    QString DisplayConfigBase::getPath()
    {
        return m_path;
    }

    bool DisplayConfigBase::saveToFile(QString folder)
    {
        if (!folder.isEmpty())
        {
            m_path = folder + QDir::separator() + m_name + m_extension;
        }
        QFile file(m_path);

        if (!file.open(QIODevice::WriteOnly))
        {
            qWarning("Couldn't open save file.");
            return false;
        }


        file.write(m_values.dump(4).c_str());
        file.close();
        return true;
    }

    bool DisplayConfigBase::loadFromFile(QString folder)
    {
        if (!folder.isEmpty())
        {
            m_path = folder + QDir::separator() + m_name + m_extension;
        }
        QFile file(m_path);

        if (!file.exists())
        {
            return false;
        }

        if (!file.open(QIODevice::ReadOnly))
        {
            qWarning("Couldn't open save file.");
            return false;
        }

        QString save_data = file.readAll();

        m_values = json::parse(save_data.toStdString());

        return true;
    }

    bool DisplayConfigBase::deleteFile()
    {
        QFile file(m_path);

        if (!file.exists())
        {
            return false;
        }

        QFile::remove(m_path);

        return true;
    }

    QSharedPointer< DisplayConfigBase > DisplayConfigBase::copy(QString name)
    {
        QSharedPointer< DisplayConfigBase > dcb(
            new DisplayConfigBase(name, m_extension, SettingsBase::m_parent));

        // Copy SettingsBase
        dcb->m_parent                = m_parent;
        dcb->m_values                = m_values;
        dcb->m_settings_inherit_base = m_settings_inherit_base;

        return dcb;
    }

}  // namespace ORNL
