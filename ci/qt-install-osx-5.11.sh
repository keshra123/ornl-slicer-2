
set -v

git -C "$(brew --repo homebrew/core)" fetch --unshallow

set -e

brew install qt@5.11
