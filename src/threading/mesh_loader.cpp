#include "threading/mesh_loader.h"

#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <QLinkedList>
#include <QStack>
#include <QtDebug>
#include <assimp/Importer.hpp>
#include <utility>

#include "geometry/mesh.h"
#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"
#include "managers/global_settings_manager.h"

namespace ORNL
{
    MeshLoader::MeshLoader(QObject* parent, const QString& filename)
        : m_filename(filename)
        , m_zero_threshold(1.0e-9f)
    {
        // Nothing to do here
    }

    void MeshLoader::run()
    {
        Assimp::Importer importer;

        const aiScene* scene = importer.ReadFile(
            m_filename.toStdString(),
            aiProcess_GenSmoothNormals |  // GenSmoothNormals generates normals
                                          // if they are not already in the
                                          // model
                aiProcess_CalcTangentSpace |  // CalcTangentSpace calculates
                                              // tangent space, only necessary
                                              // if doing normal mapping
                aiProcess_Triangulate |  // Triangulate splits up primitives
                                         // with more than three vertices to
                                         // triangles
                aiProcess_JoinIdenticalVertices |  // JoinIdenticalVertices
                                                   // joins identical vertex
                                                   // data, improves performance
                                                   // with indexed drawing
                aiProcess_SortByPType);

        if (!scene)
        {
            qDebug() << "Error loading file: (assimp:) "
                     << importer.GetErrorString();
            emit errorLoadingFile(m_filename, importer.GetErrorString());
            return;
        }

        if (scene->HasMeshes())
        {
            QStringList filename_split = m_filename.split('/');
            QString name               = filename_split.back();
            name                       = name.left(name.lastIndexOf('.'));
            for (uint i = 0; i < scene->mNumMeshes; ++i)
            {
                splitMesh(scene->mMeshes[i], name);
            }
        }
        else
        {
            qDebug() << "Error: No meshes found";
            emit errorLoadingFile(m_filename, "Error: No meshes found");
        }

        emit finishedLoading();
    }

    QVector< MeshVertex > MeshLoader::extractVertices(aiMesh* assimp_mesh)
    {
        QVector< MeshVertex > vertices;

        /* set coordinates below the threshold to zero */
        for (uint i = 0; i < assimp_mesh->mNumVertices; ++i)
        {
            aiVector3D* assimp_vertex = &assimp_mesh->mVertices[i];
            Point location;
            location.x(qAbs(assimp_vertex->x) < m_zero_threshold
                           ? 0
                           : assimp_vertex->x);
            location.y(qAbs(assimp_vertex->y) < m_zero_threshold
                           ? 0
                           : assimp_vertex->y);
            location.z(qAbs(assimp_vertex->z) < m_zero_threshold
                           ? 0
                           : assimp_vertex->z);

            aiVector3D* assimp_normal = &assimp_mesh->mNormals[i];
            Point normal;
            normal.x(assimp_normal->x < m_zero_threshold ? 0
                                                         : assimp_normal->x);
            normal.y(assimp_normal->y < m_zero_threshold ? 0
                                                         : assimp_normal->y);
            normal.z(assimp_normal->z < m_zero_threshold ? 0
                                                         : assimp_normal->z);

            vertices.push_back(MeshVertex(location, normal));
        }

        return vertices;
    }

    QVector< uint > MeshLoader::extractIndices(aiMesh* assimp_mesh)
    {
        QVector< uint > indices;

        for (uint i = 0; i < assimp_mesh->mNumFaces; ++i)
        {
            aiFace* assimp_face = &assimp_mesh->mFaces[i];

            if (assimp_face->mNumIndices != 3)
            {
                // TODO: Throw exception
                qDebug() << "Warning! Face containing "
                         << assimp_face->mNumIndices << " found. Should be 3";
                emit errorLoadingFile(
                    m_filename,
                    "Error: one of the faces contains more than 3 vertices");
                terminate();
            }

            for (unsigned int j = 0; j < assimp_face->mNumIndices; ++j)
            {
                indices.push_back(assimp_face->mIndices[j]);
            }
        }

        return indices;
    }

    QVector< MeshVertex > MeshLoader::dereferenceIndices(
        const QVector< MeshVertex >& vertices,
        const QVector< uint >& indices)
    {
        QVector< MeshVertex > dereferenced_vertices;

        for (int i = 0; i < indices.size(); ++i)
        {
            dereferenced_vertices.push_back(vertices[indices[i]]);
        }

        return dereferenced_vertices;
    }

    /* Removes duplicates and populates vectors */
    void MeshLoader::extract(aiMesh* assimp_mesh,
                             QVector< MeshVertex >& vertices,
                             QLinkedList< MeshFace >& faces)
    {
        /* Contains data directly from Assimp */
        QVector< MeshVertex > assimp_vertices = extractVertices(assimp_mesh);

        QVector< uint > indices = extractIndices(assimp_mesh);

        /* Each index is replaced with the vertex/normal/tangent/bitangents it
         * references */
        QVector< MeshVertex > dereferenced_vertices =
            dereferenceIndices(assimp_vertices, indices);

        /* Map the dereferenced coordinates into a new set of vertices, normals,
         * and indices as faces */
        remap(dereferenced_vertices, vertices, faces);
    }

    void MeshLoader::remap(QVector< MeshVertex >& assimp_vertices,
                           QVector< MeshVertex >& vertices,
                           QLinkedList< MeshFace >& faces)
    {
        /* Data structure to track duplicate vertices */
        QMap< MeshVertex, uint > vertex_to_index;
        QVector< uint > indices;

        uint new_index = 0;

        /* populate all_vertices with unique vertices */
        for (int i = 0; i < assimp_vertices.size(); ++i)
        {
            if (!vertex_to_index.contains(assimp_vertices[i]))
            {
                vertex_to_index.insert(assimp_vertices[i], new_index);
                vertices.push_back(assimp_vertices[i]);
                indices.push_back(new_index);

                new_index++;
            }

            else
            {
                indices.push_back(vertex_to_index[assimp_vertices[i]]);
            }
        }

        groupFaces(indices, faces);
    }

    void MeshLoader::remap(QVector< MeshVertex >& assimp_vertices,
                           QVector< MeshVertex >& vertices,
                           QVector< MeshFace >& faces)
    {
        /* Data structure to track duplicate vertices */
        QMap< MeshVertex, uint > vertex_to_index;
        QVector< uint > indices;

        uint new_index = 0;

        /* populate all_vertices with unique vertices */
        for (int i = 0; i < assimp_vertices.size(); ++i)
        {
            if (!vertex_to_index.contains(assimp_vertices[i]))
            {
                vertex_to_index.insert(assimp_vertices[i], new_index);
                vertices.push_back(assimp_vertices[i]);
                indices.push_back(new_index);

                new_index++;
            }

            else
            {
                indices.push_back(vertex_to_index[assimp_vertices[i]]);
            }
        }

        groupFaces(indices, faces);
    }

    void MeshLoader::groupFaces(const QVector< uint >& indices,
                                QLinkedList< MeshFace >& faces)
    {
        /* Group each set of 3 consecutive indices into a face */
        for (int i = 0; i < indices.size(); i += 3)
        {
            MeshFace face;
            for (int j = 0; j < 3; j++)
            {
                face.vertex_index[j] = indices[i + j];
            }
            faces.append(face);
        }
    }

    void MeshLoader::groupFaces(const QVector< uint >& indices,
                                QVector< MeshFace >& faces)
    {
        /* Group each set of 3 consecutive indices into a face */
        for (int i = 0; i < indices.size(); i += 3)
        {
            MeshFace face;
            for (int j = 0; j < 3; j++)
            {
                face.vertex_index[j] = indices[i + j];
            }
            faces.append(face);
        }
    }

    QVector< uint > MeshLoader::flattenFaces(
        QVector< MeshFace >& connected_faces)
    {
        QVector< uint > flattened_faces;

        for (int i = 0; i < connected_faces.size(); ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                flattened_faces.push_back(connected_faces[i].vertex_index[j]);
            }
        }

        return flattened_faces;
    }

    void MeshLoader::splitMesh(aiMesh* assimp_mesh, QString name)
    {
        QStack< MeshFace > stack;

        /* Superset of all indexed faces in the .stl file */
        QLinkedList< MeshFace > faces;

        /* Superset of all vertices/normals/tangents/bitangents in the .stl file
         */
        QVector< MeshVertex > vertices;

        /* remove duplicate vertices and round near-zero coordinates to zero */
        extract(assimp_mesh, vertices, faces);

        /* Test code */
        qDebug() << "all_vertices before:";

        vertexData(vertices);

        qDebug() << "all_faces before:";

        faceData(faces);
        /* End test code */

        /* Find subsets of faces that outline solid objects */
        while (!faces.isEmpty())
        {
            /* Represents a single object of connected faces */
            QVector< MeshFace > connected_faces;

            /* Arbitrarily pick a face to start off the new subset */
            stack.push(faces.first());

            /* Remove the face from the superset of faces to be processed */
            faces.removeFirst();

            /* Find all faces reachable from the one you started with above */
            while (!stack.isEmpty())
            {
                connected_faces.push_back(stack.pop());

                QLinkedList< MeshFace >::iterator iter = faces.begin();
                QLinkedList< MeshFace >::iterator prev;

                while (iter != faces.end())
                {
                    if (facesConnected(connected_faces.last(), *iter))
                    {
                        prev = iter++;
                        stack.push(*prev);
                        faces.erase(prev);
                    }
                    else
                    {
                        ++iter;
                    }
                }
            }

            /* Test code */
            qDebug() << "all_faces after:";

            faceData(faces);

            qDebug() << "connected faces:";

            faceData(connected_faces);
            /* End test code */

            /* Flatten connected_faces */
            QVector< uint > flattened_faces = flattenFaces(connected_faces);

            /* Dereference the indices in flattened_faces */
            QVector< MeshVertex > dereferenced_vertices =
                dereferenceIndices(vertices, flattened_faces);

            /* Containers for coordinate data for a single object */
            QVector< MeshVertex > mesh_vertices;
            QVector< MeshFace > mesh_faces;

            /* Remap the mesh_vertices to a new set of indices*/
            remap(dereferenced_vertices, mesh_vertices, mesh_faces);

            qDebug() << "after final remapping:";
            /* Test code */
            checkData(mesh_vertices, mesh_faces);

            // For each face collect which other faces are connected to it
            for (int face_idx = 0; face_idx < mesh_faces.size(); face_idx++)
            {
                MeshFace& face = mesh_faces[face_idx];
                // faces are connected via the edges
                for (uint j = 0; j < 3; j++)
                {
                    int other_face_idx =
                        getFaceIndexWithPoints(face.vertex_index[j],
                                               face.vertex_index[(j + 1) % 3],
                                               face_idx,
                                               face.vertex_index[(j + 2) % 3],
                                               mesh_vertices,
                                               mesh_faces);
                    // qDebug() << QString("Face: %1 Vertices: %2, %3, %4 Other:
                    // %5 j:
                    // %6").arg(QString::number(face_idx),QString::number(face.vertex_index[j]),
                    // QString::number(face.vertex_index[(j + 1) % 3]),
                    // QString::number(face.vertex_index[(j + 2) % 3]),
                    // QString::number(other_face_idx), QString::number(j));
                    face.connected_face_index[j] = other_face_idx;
                    mesh_vertices[face.vertex_index[j]].connected_faces.append(
                        face_idx);
                }
            }

            qDebug() << "after connected faces processing:";
            /* Test code */
            checkData(mesh_vertices, mesh_faces);

            /* Consider centering and dropping the meshes before sending them to
             * mesh? */
            QSharedPointer< Mesh > mesh(new Mesh(
                mesh_vertices,
                mesh_faces,
                GlobalSettingsManager::getInstance()->getGlobal().data()));

            mesh->name(name);

            emit finishedMesh(mesh);
        }
    }

    /*!

    Returns the index of the 'other' face connected to the edge between vertices
    with indices idx0 and idx1. In case more than two faces are connected via
    the same edge, the next face in a counter-clockwise ordering (looking from
    idx1 to idx0) is returned.

    \cond DOXYGEN_EXCLUDE
        [NON-RENDERED COMENTS]
        For two faces abc and abd with normals n and m, we have that:
        \f{eqnarray*}{
        n &=& \frac{ab \times ac}{\|ab \times ac\|}     \\
        m &=& \frac{ab \times ad}{\|ab \times ad\|}     \\
        n \times m &=& \|n\| \cdot \|m\| \mathbf{p} \sin \alpha  \\
        && (\mathbf{p} \perp n \wedge \mathbf{p} \perp m) \\
        \sin \alpha &=& \|n \times m \|
        &=& \left\| \frac{(ab \times ac) \times (ab \times ad)}{\|ab \times ac\|
    \cdot \|ab \times ad\|}  \right\|    \\
        &=& \left\| \frac{ (ab \cdot (ac \times ad)) ab  }{\|ab \times ac\|
    \cdot \|ab \times ad\|}  \right\|    \\
        &=&  \frac{ (ab \cdot (ac \times ad)) \left\| ab   \right\| }{\|ab\|
    \|ac\| \sin bac \cdot \|ab\| \|ad\| \sin bad}    \\
        &=&  \frac{  ab \cdot (ac \times ad)  }{\|ab\| \|ac\| \|ad\|  \sin bac
    \sin bad}    \\ \f}} \endcond

    See <a
    href="http://stackoverflow.com/questions/14066933/direct-way-of-computing-clockwise-angle-between-2-vectors">Direct
    way of computing clockwise angle between 2 vectors</a>

    */
    int MeshLoader::getFaceIndexWithPoints(int index0,
                                           int index1,
                                           int not_face_index,
                                           int not_face_vertex_index,
                                           QVector< MeshVertex >& vertices,
                                           QVector< MeshFace >& faces) const
    {
        // In case more than two faces meet at an edge, multiple candidates are
        // generated
        QVector< int > candidate_faces;

        // Search through all faces connected to the first vertex and find those
        // that are also connected to the second
        for (int f : vertices[index0].connected_faces)
        {
            if (f == not_face_index)
            {
                continue;
            }

            if (faces[f].vertex_index[0] ==
                    index1  // && m_faces[f].vertex_index[1] == index0 // next
                            // face should have the right direction!
                || faces[f].vertex_index[1] ==
                    index1  // && m_faces[f].vertex_index[2] == index0
                || faces[f].vertex_index[2] ==
                    index1  // && m_faces[f].vertex_index[0] == index0
            )
            {
                candidate_faces.push_back(f);
            }
        }

        if (!candidate_faces.size())
        {
            qDebug() << QString("Couldn't find face connected to face %1")
                            .arg(not_face_index);
            qWarning() << "Mesh has disconnected faces!";
            return -1;
        }

        if (candidate_faces.size() == 1)
        {
            return candidate_faces[0];
        }

        if (candidate_faces.size() % 2 == 0)
        {
            qDebug() << QString("Warning! Edge with uneven number of faces "
                                "connecting it! (%1)")
                            .arg(candidate_faces.size() + 1);
            qWarning() << "Mesh has disconnected faces!";
        }

        Point vn = vertices[index1].location - vertices[index0].location;

        // The normal of the plane in which all normals of faces connected to
        // the edge lie => the normalized normal
        Point n  = vn / vn.distance()();
        Point v0 = vertices[index1].location - vertices[index0].location;

        // the normals below are abnormally directed! : these normals all point
        // counterclockwise (viewed from idx1 to idx0) from the face,
        // irrespective of the direction of the face.
        Point n0 = (vertices[not_face_vertex_index].location -
                    vertices[index0].location)
                       .cross(v0);

        if (n0.distance() <= 0)
        {
            qDebug() << QString("Face %1 has zero area!").arg(not_face_index);
        }

        // More than 2 Pi (impossible angle)
        double smallest_angle = 1000;
        int best_index        = -1;
        for (int candidate_face : candidate_faces)
        {
            int candidate_vertex;
            // find third vertex belonging to the face (besides index0 and
            // index1)
            for (candidate_vertex = 0; candidate_vertex < 3; candidate_vertex++)
            {
                if (faces[candidate_face].vertex_index[candidate_vertex] !=
                        index0 &&
                    faces[candidate_face].vertex_index[candidate_vertex] !=
                        index1)
                {
                    break;
                }
            }

            Point v1 =
                vertices[candidate_vertex].location - vertices[index0].location;
            Point n1 = v1.cross(v0);

            double dot   = n0.dot(n1);
            double det   = n.dot(n0.cross(n1));
            double angle = qAtan2(det, dot);


            // 0 <= angle <= 2*pi
            if (angle < 0)
            {
                angle += 2 * M_PI;
            }

            if (!angle)
            {
                qDebug() << QString("Overlapping faces: face %1 and face %2")
                                .arg(QString::number(not_face_index),
                                     QString::number(candidate_face));
                qWarning() << "Mesh has overlapping faces!";
            }

            if (angle < smallest_angle)
            {
                smallest_angle = angle;
                best_index     = candidate_face;
            }
        }

        if (best_index < 0)
        {
            qDebug() << QString("Couldn't find face conencted to face %1")
                            .arg(QString::number(not_face_index));
            qWarning() << "Mesh has disconnected faces!";
        }

        return best_index;
    }

    void MeshLoader::checkData(QVector< MeshVertex >& mesh_vertices,
                               QVector< MeshFace >& mesh_faces)
    {
        qDebug() << "new mesh:";

        qDebug() << "mesh faces";

        for (int i = 0; i < mesh_faces.size(); ++i)
        {
            int i1 = mesh_faces[i].vertex_index[0];
            int i2 = mesh_faces[i].vertex_index[1];
            int i3 = mesh_faces[i].vertex_index[2];

            qDebug() << i1 << " " << i2 << " " << i3;
        }

        qDebug() << "dereferenced faces:";

        for (int i = 0; i < mesh_faces.size(); ++i)
        {
            int i1 = mesh_faces[i].vertex_index[0];
            int i2 = mesh_faces[i].vertex_index[1];
            int i3 = mesh_faces[i].vertex_index[2];

            MeshVertex& v1 = mesh_vertices[i1];
            MeshVertex& v2 = mesh_vertices[i2];
            MeshVertex& v3 = mesh_vertices[i3];

            double x1 = v1.location.x();
            double y1 = v1.location.y();
            double z1 = v1.location.z();
            double x2 = v2.location.x();
            double y2 = v2.location.y();
            double z2 = v2.location.z();
            double x3 = v3.location.x();
            double y3 = v3.location.y();
            double z3 = v3.location.z();

            qDebug() << "(" << x1 << " " << y1 << " " << z1
                     << "), "
                        "("
                     << x2 << " " << y2 << " " << z2 << "), (" << x3 << " "
                     << y3 << " " << z3 << ")";
        }
    }

    bool MeshLoader::facesConnected(MeshFace& face, MeshFace& other_face)
    {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                if (face.vertex_index[i] == other_face.vertex_index[j])
                {
                    return true;
                }
            }
        }

        return false;
    }

    void MeshLoader::vertexData(QVector< MeshVertex >& vertices)
    {
        for (int i = 0; i < vertices.size(); ++i)
        {
            qDebug() << vertices[i].location.x() << " "
                     << vertices[i].location.y() << " "
                     << vertices[i].location.z();
        }
    }

    void MeshLoader::faceData(QVector< MeshFace >& faces)
    {
        for (int i = 0; i < faces.size(); ++i)
        {
            qDebug() << faces[i].vertex_index[0] << " "
                     << faces[i].vertex_index[1] << " "
                     << faces[i].vertex_index[2];
        }
    }

    void MeshLoader::faceData(QLinkedList< MeshFace >& faces)
    {
        QLinkedList< MeshFace >::iterator iter;

        for (iter = faces.begin(); iter != faces.end(); ++iter)
        {
            qDebug() << (*iter).vertex_index[0] << " "
                     << (*iter).vertex_index[1] << " "
                     << (*iter).vertex_index[2];
        }
    }
}  // namespace ORNL
