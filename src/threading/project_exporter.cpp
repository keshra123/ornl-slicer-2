#include "threading/project_exporter.h"

#include "managers/window_manager.h"
#include "utilities/qt_json_conversion.h"

namespace ORNL
{
    ProjectExporter::ProjectExporter(QObject* parent, QString folder)
        : QThread(parent)
        , m_folder(folder)
        , m_project_manager(ProjectManager::getInstance())
        , m_settings_manager(GlobalSettingsManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
    {
        connect(this,
                SIGNAL(errorSavingFile(QString, QString)),
                m_window_manager.data(),
                SLOT(createErrorDialog(QString, QString)));
    }


    void ProjectExporter::run()
    {
        /*
        if(!m_folder.isEmpty())
        {
            m_project_manager->filepath(m_folder);
            m_project_manager->name(m_folder.split('/').back());
        }

        QDir project_folder(m_project_manager->filepath());
        bool existed = project_folder.exists();
        QString temp_path =
        QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) +
        QDir::separator() + "temp_stl" + QDir::separator(); if(existed)
        {
            QDir temp(temp_path);
            temp.mkpath(".");

            QStringList filters;
            filters << "*.stl";
            QStringList file_list = project_folder.entryList(filters);
            for(QString filename : file_list)
            {
                QFile file(project_folder.absoluteFilePath(filename));
                file.copy(temp_path + filename);
            }

            project_folder.removeRecursively();
        }
        project_folder.mkpath(".");

        json j;

        // Record global settings names
        j["machine_settings"] = m_project_manager->currentMachineName();
        j["material_settings"] = m_project_manager->currentMaterialName();
        j["print_settings"] = m_project_manager->currentPrintName();

        // save settings to project folder
        m_settings_manager->getConfig(ConfigTypes::kMachine,
        j["machine_settings"])->saveToFile(m_project_manager->filepath());
        m_settings_manager->getConfig(ConfigTypes::kMaterial,
        j["material_settings"])->saveToFile(m_project_manager->filepath());
        m_settings_manager->getConfig(ConfigTypes::kPrint,
        j["print_settings"])->saveToFile(m_project_manager->filepath());

        // Record project name
        j["name"] = m_project_manager->name();

        // Save models to project folder
        json model_array;
        foreach(Model* model, m_project_manager->modelList())
        {
            json model_json = model->json();

            QString filename = model->filepath().split('/').back();
            QFile file(existed ? temp_path + filename : model->filepath());
            QString newPath = m_project_manager->filepath() + QDir::separator()
        + filename; file.copy(newPath);

            model_json["filename"] = filename;

            model_array.push_back(model_json);
        }

        if(existed)
        {
            QDir temp(temp_path);
            temp.removeRecursively();
        }

        j["models"] = model_array;


        QString project_filename = m_project_manager->filepath() +
        QDir::separator() + m_project_manager->name() + ".os2";

        QFile file(project_filename);

        if(!file.open(QIODevice::WriteOnly))
        {
            emit errorSavingFile(m_folder, "Error writing project file");
            return;
        }

        file.write(j.dump(4).c_str());

        m_project_manager->markRecent();
        */
    }
}  // namespace ORNL
