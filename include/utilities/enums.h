#ifndef ENUMS_H
#define ENUMS_H

//! \file enums.h

#include <json.hpp>

#include "constants.h"
#include "exceptions/exceptions.h"

using json = nlohmann::json;

namespace ORNL
{
    /*!
     * \enum ConfigTypes
     * \brief The ConfigTypes enum
     */
    enum class ConfigTypes : uint8_t
    {
        kMachine  = 0,
        kPrint    = 1,
        kMaterial = 2,
        kNozzle   = 3
    };

    /*!
     * \enum AffectedArea
     * \brief The AffectedArea enum
     */
    enum class AffectedArea : uint8_t
    {
        kNone       = 0,
        kPerimeter  = 1 << 0,
        kInset      = 1 << 1,
        kInfill     = 1 << 2,
        kTopSkin    = 1 << 3,
        kBottomSkin = 1 << 4,
        kSkin       = 1 << 5,
        kSupport    = 1 << 6
    };

    /*!
     * \enum GcodeSyntax
     * \brief The GcodeSyntax enum
     */
    enum class GcodeSyntax : uint8_t
    {
        kNone,
        kCommon,
        kCincinnati,
        kBlueGantry,
        kWolf,
        kIngersoll,
        kNorthrup
    };

    /*!
     * \enum InfillPatterns
     * \brief The InfillPatterns enum
     */
    enum class InfillPatterns : uint8_t
    {
        kLines                = 1,
        kGrid                 = 2,
        kConcentric           = 3,
        kTriangles            = 4,
        kHexagonsAndTriangles = 5,
        kHoneycomb            = 6,
        kCubic                = 7,
        kTetrahedral          = 8,
        kConcentric3D         = 9,
        kHoneycomb3D          = 10
    };

    //! \brief Function for going from json to InfillPatterns
    void to_json(json& j, const InfillPatterns& i);

    //! \brief Function for going from InfillPatterns to json
    void from_json(const json& j, InfillPatterns& i);

    /*!
     * \enum RegionType
     * \brief The PathType enum
     */
    enum class RegionType : uint8_t
    {
        kUnknown     = 0,
        kPerimeter   = 1 << 0,
        kInset       = 1 << 1,
        kInfill      = 1 << 2,
        kTopSkin     = 1 << 3,
        kBottomSkin  = 1 << 4,
        kSkin        = 1 << 5,
        kSupport     = 1 << 6,
        kSupportRoof = 1 << 7,
    };

    inline constexpr RegionType operator|(const RegionType& lhs,
                                          const RegionType& rhs)
    {
        return static_cast< RegionType >(static_cast< int >(lhs) |
                                         static_cast< int >(rhs));
    }

    inline RegionType& operator|=(RegionType& lhs, const RegionType& rhs)
    {
        return lhs = lhs | rhs;
    }

    inline constexpr RegionType operator&(const RegionType& lhs,
                                          const RegionType& rhs)
    {
        return static_cast< RegionType >(static_cast< int >(lhs) &
                                         static_cast< int >(rhs));
    }

    inline RegionType& operator&=(RegionType& lhs, const RegionType& rhs)
    {
        return lhs = lhs & rhs;
    }

    inline RegionType fromString(QString type)
    {
        if (type == Constants::RegionTypeStrings::kUnknown)
        {
            return RegionType::kUnknown;
        }
        else if (type == Constants::RegionTypeStrings::kPerimeter)
        {
            return RegionType::kPerimeter;
        }
        else if (type == Constants::RegionTypeStrings::kInset)
        {
            return RegionType::kInset;
        }
        else if (type == Constants::RegionTypeStrings::kTopSkin)
        {
            return RegionType::kTopSkin;
        }
        else if (type == Constants::RegionTypeStrings::kBottomSkin)
        {
            return RegionType::kBottomSkin;
        }
        else if (type == Constants::RegionTypeStrings::kSkin)
        {
            return RegionType::kSkin;
        }
        else if (type == Constants::RegionTypeStrings::kInfill)
        {
            return RegionType::kInfill;
        }
        else if (type == Constants::RegionTypeStrings::kSupport)
        {
            return RegionType::kSupport;
        }
        else if (type == Constants::RegionTypeStrings::kSupportRoof)
        {
            return RegionType::kSupportRoof;
        }
        throw UnknownRegionTypeException(
            "Cannot convert this string to RegionType");
    }

    inline QString toString(RegionType region_type)
    {
        switch (region_type)
        {
            case RegionType::kUnknown:
                return Constants::RegionTypeStrings::kUnknown;
            case RegionType::kPerimeter:
                return Constants::RegionTypeStrings::kPerimeter;
            case RegionType::kInset:
                return Constants::RegionTypeStrings::kInset;
            case RegionType::kTopSkin:
                return Constants::RegionTypeStrings::kTopSkin;
            case RegionType::kBottomSkin:
                return Constants::RegionTypeStrings::kBottomSkin;
            case RegionType::kSkin:
                return Constants::RegionTypeStrings::kSkin;
            case RegionType::kInfill:
                return Constants::RegionTypeStrings::kInfill;
            case RegionType::kSupport:
                return Constants::RegionTypeStrings::kSupport;
            case RegionType::kSupportRoof:
                return Constants::RegionTypeStrings::kSupportRoof;
        }
    }


    /*!
     * \enum PathModifiers
     * \brief The PathModifiers enum
     */
    enum class PathModifiers : uint8_t
    {
        kNone             = 0,
        kReverseTipWipe   = 1 << 0,
        kForwardTipWipe   = 1 << 1,
        kPerimeterTipWipe = 1 << 2,  // Rename to not include perimeter
        kInitialStartup   = 1 << 3,
        kSlowDown         = 1 << 4,
        kCoasting         = 1 << 5,
        kPrestart         = 1 << 6
    };

    inline constexpr PathModifiers operator|(const PathModifiers& lhs,
                                             const PathModifiers& rhs)
    {
        return static_cast< PathModifiers >(static_cast< int >(lhs) |
                                            static_cast< int >(rhs));
    }

    inline PathModifiers& operator|=(PathModifiers& lhs,
                                     const PathModifiers& rhs)
    {
        return lhs = lhs | rhs;
    }

    inline constexpr PathModifiers operator&(const PathModifiers& lhs,
                                             const PathModifiers& rhs)
    {
        return static_cast< PathModifiers >(static_cast< int >(lhs) &
                                            static_cast< int >(rhs));
    }

    inline PathModifiers& operator&=(PathModifiers& lhs,
                                     const PathModifiers& rhs)
    {
        return lhs = lhs & rhs;
    }

    /*!
     * \enum PathOrderOptimization
     * \brief The PathOrderOptimization enum
     */
    enum class OrderOptimization : uint8_t
    {
        kShortestTime,
        kShortestDistance,
        kLargestDistance,
        kLeastRecentlyVisited  //,
        // kBestStrength,
        // kBestThermalProperties
    };

}  // namespace ORNL
#endif  // ENUMS_H
