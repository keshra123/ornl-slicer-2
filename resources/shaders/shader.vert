#version 440
layout(location = 0) in vec3 position;
out vec4 vColor;

uniform vec3 color;
uniform mat4 meshToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;

void main()
{
  gl_Position = cameraToView * worldToCamera * meshToWorld * vec4(position, 1.0);
  vColor = vec4(color, 1.0f);
}
