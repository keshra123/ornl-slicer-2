#include "gcode/parsers/ingersoll_parser.h"

#include <QString>
#include <QStringList>
#include <QVector>

#include "exceptions/exceptions.h"
#include "units/unit.h"

namespace ORNL
{
    IngersollParser::IngersollParser()
    {}

    void IngersollParser::config()
    {}

    void IngersollParser::NHandler(QStringList& LineNumber)
    {}

    void IngersollParser::HSPHandler()
    {}

    void IngersollParser::HALOHandler(QStringList& commands)
    {}

    void IngersollParser::LAY_BEADHandler(QStringList& commands)
    {}

    void IngersollParser::EXTRUDERHandler(QStringList& commands)
    {}


    void IngersollParser::MSGHandler(QStringList& commands)
    {}

    void IngersollParser::transHandler()
    {}

    void IngersollParser::rotHandler()
    {}

    void IngersollParser::scaleHandler()
    {}

    void IngersollParser::mirrorHandler()
    {}

    void IngersollParser::atransHandler(QStringList& commands)
    {}

    void IngersollParser::G17Handler()
    {}

    void IngersollParser::G40Handler()
    {}

    void IngersollParser::G54Handler()
    {}

    void IngersollParser::G90Handler()
    {}

    void IngersollParser::G94Handler()
    {}

    void IngersollParser::G700Handler()
    {}

    void IngersollParser::M100Handler()
    {}

    void IngersollParser::M101Handler()
    {}

    void IngersollParser::ToolChangeHandler(QStringList& params)
    {}
}  // namespace ORNL
