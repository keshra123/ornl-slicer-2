#include "geometry/mesh_face.h"

namespace ORNL
{
    MeshFace::MeshFace()
    {
        for (int i = 0; i < 3; i++)
        {
            vertex_index[i]         = -1;
            connected_face_index[i] = -1;
        }
    }
}  // namespace ORNL
