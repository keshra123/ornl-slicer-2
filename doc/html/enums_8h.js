var enums_8h =
[
    [ "json", "enums_8h.html#ab701e3ac61a85b337ec5c1abaad6742d", null ],
    [ "AffectedArea", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106", [
      [ "kNone", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a35c3ace1970663a16e5c65baa5941b13", null ],
      [ "kPerimeter", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a3b20a82de71a61e0abd9b1cd57f85bee", null ],
      [ "kInset", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a006e863fe4b818b7b33f99d61633418b", null ],
      [ "kInfill", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a09785bbb87e834339a6e39cc94422005", null ],
      [ "kTopSkin", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a6c9392ef2036b56d28fd60b5d16a0754", null ],
      [ "kBottomSkin", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106aee4b5d4177db31f10ff1aa9a47795784", null ],
      [ "kSkin", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106a33ed58103a5be3ba0dd62600de5d1571", null ],
      [ "kSupport", "enums_8h.html#a77b27c8805b20c187cb7ffb1fd4f6106aaab0226a1be7a88fdffaa2442c55aa60", null ]
    ] ],
    [ "ConfigTypes", "enums_8h.html#adb40fc18a002edc6ae59fcd3ed1d6743", [
      [ "kMachine", "enums_8h.html#adb40fc18a002edc6ae59fcd3ed1d6743a3a016828bd447a6d33f9e45c238f3b69", null ],
      [ "kPrint", "enums_8h.html#adb40fc18a002edc6ae59fcd3ed1d6743ad4d82e1088c137abf05863d20f04c4df", null ],
      [ "kMaterial", "enums_8h.html#adb40fc18a002edc6ae59fcd3ed1d6743a5e2e033777318b88b46ad1e974ccb1eb", null ],
      [ "kNozzle", "enums_8h.html#adb40fc18a002edc6ae59fcd3ed1d6743ae05b11c80511dc717df0a25899b46390", null ]
    ] ],
    [ "GcodeSyntax", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0", [
      [ "kCincinnati", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0a4e4f93c592281c64e53c805b46ef7863", null ],
      [ "kBlueGantry", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0a814ef3375dd336ae0c3d3a4e4c8819f9", null ],
      [ "kWolf", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0ae6312e538babda854915ba3381902485", null ],
      [ "kIngersoll", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0a09ed4623902c941fbcd6f643d83602ac", null ],
      [ "kNorthrup", "enums_8h.html#ab2b7f768205b296ee097430e13f0f5e0a956170282ee64d783c59868236c16d2e", null ]
    ] ],
    [ "InfillPatterns", "enums_8h.html#a455ed90160cb88d4761e46d72522a378", [
      [ "kLines", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a3a16032bb8b05d840b0f80a14181a5df", null ],
      [ "kGrid", "enums_8h.html#a455ed90160cb88d4761e46d72522a378aef22950f87352c1654b4b527212a3be7", null ],
      [ "kConcentric", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a04e1e2f52a1e1bc4a84b992a0fb98059", null ],
      [ "kTriangles", "enums_8h.html#a455ed90160cb88d4761e46d72522a378aa4d46c1e7f196c89922524221297378c", null ],
      [ "kHexagonsAndTriangles", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a26f6ffe585ce9550041f056bfeed0d01", null ],
      [ "kHoneycomb", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a76c9a86875f0206c8d7f7680c5eea5fd", null ],
      [ "kCubic", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a619f9f4e8a88cbb914ba5abbcc356d79", null ],
      [ "kTetrahedral", "enums_8h.html#a455ed90160cb88d4761e46d72522a378a0561aa747d71526621f8052b2ee03b0f", null ],
      [ "kConcentric3D", "enums_8h.html#a455ed90160cb88d4761e46d72522a378af787ea2b4b0797d233cc2d3c0897be64", null ],
      [ "kHoneycomb3D", "enums_8h.html#a455ed90160cb88d4761e46d72522a378ac05fcc5255579e9c1d929737920f29cc", null ]
    ] ],
    [ "OrderOptimization", "enums_8h.html#aa08d4776c610cfdd6c95b3ef5b15a399", [
      [ "kShortestTime", "enums_8h.html#aa08d4776c610cfdd6c95b3ef5b15a399af4686898a0a04968412fcd1be62522b3", null ],
      [ "kShortestDistance", "enums_8h.html#aa08d4776c610cfdd6c95b3ef5b15a399ac1d55b039f05034d771e005d7487a1af", null ],
      [ "kLargestDistance", "enums_8h.html#aa08d4776c610cfdd6c95b3ef5b15a399a7cd8fcb2c2f9840f598e68d5fe1e2326", null ],
      [ "kLeastRecentlyVisited", "enums_8h.html#aa08d4776c610cfdd6c95b3ef5b15a399a432eb62b8f8031ef1113000b87c24bd8", null ]
    ] ],
    [ "PathModifiers", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734e", [
      [ "kNone", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea35c3ace1970663a16e5c65baa5941b13", null ],
      [ "kReverseTipWipe", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea85218f2f5079c3858c82ca50d2cad87b", null ],
      [ "kForwardTipWipe", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea1b12b1a795845b7283d06fad2686bfcf", null ],
      [ "kPerimeterTipWipe", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea7f554fa6728390d6f40263e7835d6d92", null ],
      [ "kInitialStartup", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea7b9da801dccaa8aec310827f48bfacf3", null ],
      [ "kSlowDown", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734ea02392f889dd9dcbe75cf6342c04021d7", null ],
      [ "kCoasting", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734eab11751fa3242e10c92fec8e81186d962", null ],
      [ "kPrestart", "enums_8h.html#af41e1454b3c6c5fa5f00879c5794734eae5284a2f5136083386d68ed321af60fa", null ]
    ] ],
    [ "RegionType", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860", [
      [ "kUnknown", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a25c2dc47991b3df171ed5192bcf70390", null ],
      [ "kPerimeter", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a3b20a82de71a61e0abd9b1cd57f85bee", null ],
      [ "kInset", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a006e863fe4b818b7b33f99d61633418b", null ],
      [ "kInfill", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a09785bbb87e834339a6e39cc94422005", null ],
      [ "kTopSkin", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a6c9392ef2036b56d28fd60b5d16a0754", null ],
      [ "kBottomSkin", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860aee4b5d4177db31f10ff1aa9a47795784", null ],
      [ "kSkin", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860a33ed58103a5be3ba0dd62600de5d1571", null ],
      [ "kSupport", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860aaab0226a1be7a88fdffaa2442c55aa60", null ],
      [ "kSupportRoof", "enums_8h.html#a4a7ca9b7124be5ff95d83232857b8860af9bfd41bfb67a50326f79a23183ab753", null ]
    ] ],
    [ "from_json", "enums_8h.html#a12ee3c5902d5f3d84b96f51ffbbbff4e", null ],
    [ "fromString", "enums_8h.html#ad6d474bcb0985e5e01fdb4f0013501a7", null ],
    [ "operator &", "enums_8h.html#abcdb5ac285183074498ad095aa5ca6a5", null ],
    [ "operator &=", "enums_8h.html#ac628cd43bd1fd67eecd2d5e249ccbd56", null ],
    [ "operator&", "enums_8h.html#a5d9f73864302a8a63dd1e233e74eff18", null ],
    [ "operator&=", "enums_8h.html#a2a7e7aff8cb74ccef5850e680898dcc9", null ],
    [ "operator|", "enums_8h.html#ab0bb0cd6e5f5788ec06cb5a12d0bd9bb", null ],
    [ "operator|", "enums_8h.html#a8ba2db6da93122ec491f637b7caeaef3", null ],
    [ "operator|=", "enums_8h.html#afc8c05b1fbaacffcafa4d3d7f4d5f996", null ],
    [ "operator|=", "enums_8h.html#a2fc63154f83826b848fec304e33d295f", null ],
    [ "to_json", "enums_8h.html#a3eda545c0ebd7e60656fb6228514ff1c", null ],
    [ "toString", "enums_8h.html#aa712727a59edeffcb5774ad9b146df97", null ]
];