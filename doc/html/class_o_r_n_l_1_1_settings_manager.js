var class_o_r_n_l_1_1_settings_manager =
[
    [ "addConfig", "class_o_r_n_l_1_1_settings_manager.html#a76b45c2aaa0bf3de2c6639a9c2b198ba", null ],
    [ "error", "class_o_r_n_l_1_1_settings_manager.html#a5ef459a84641a530c4546a7e45e16ac2", null ],
    [ "getConfig", "class_o_r_n_l_1_1_settings_manager.html#aa4e9f86e577573843591c20098e66255", null ],
    [ "getConfigsNames", "class_o_r_n_l_1_1_settings_manager.html#a162adc7ff92407e1248b4cddfcef27f0", null ],
    [ "removeConfig", "class_o_r_n_l_1_1_settings_manager.html#a0532603cc3e35be0309f3727485bccd8", null ],
    [ "saveConfig", "class_o_r_n_l_1_1_settings_manager.html#af80271cebbf4ed77a34aedb92bc0e73b", null ]
];