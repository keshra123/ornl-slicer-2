#ifndef PRINTPERIMETERSCATEGORYWIDGET_H
#define PRINTPERIMETERSCATEGORYWIDGET_H

#include <QWidget>

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class PrintPerimeterCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class PrintPerimetersCategoryWidget
     * \brief Widget for the Inset Settings
     */
    class PrintPerimeterCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        PrintPerimeterCategoryWidget();

        //! \brief Destructor
        ~PrintPerimeterCategoryWidget();

        //! \brief handles conditions for enabling settings and runs parent
        //! update
        void handleConditions();

    private:
        Ui::PrintPerimeterCategoryWidget* ui;
    };  // class PrintPerimetersCategoryWidget
}  // namespace ORNL

#endif  // PRINTPERIMETERSCATEGORYWIDGET_H
