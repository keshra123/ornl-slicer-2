#ifndef MATERIALPROPERTIESCATEGORYWIDGET_H
#define MATERIALPROPERTIESCATEGORYWIDGET_H

#include <QWidget>

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class MaterialPropertiesCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class MaterialPropertiesCategoryWidget
     * \brief Widget for the Material Properties Category
     */
    class MaterialPropertiesCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        //! \brief Constructor
        MaterialPropertiesCategoryWidget();

        //! \brief Destructor
        ~MaterialPropertiesCategoryWidget();

        //! \brief handles conditions for enabling settings
        void handleConditions();

    private:
        Ui::MaterialPropertiesCategoryWidget* ui;
    };  // class MaterialPropertiesCategoryWidget
}  // namespace ORNL
#endif  // MATERIALPROPERTIESCATEGORYWIDGET_H
