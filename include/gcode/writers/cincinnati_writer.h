#ifndef CINCINNATI_H
#define CINCINNATI_H

//! \file cincinnati.h

#include "gcode/writers/writer_base.h"
namespace ORNL
{
    /*!
     * \class CincinnatiWriter
     * \brief The gcode writer for the Cincinnati Inc. syntax
     */
    class CincinnatiWriter : public WriterBase
    {
    public:
        //! \brief Constructor
        CincinnatiWriter() = default;

        //! \brief Write the header to the file
        QString header();

        //! \brief Write the line for a layer change
        QString onLayerChange();

        //! \brief Write the footer to the file
        QString footer();

        //! \brief Write a path segment
        QString pathSegment(PathSegment& path_segment);

        //! \brief write a line path
        QString line(Line& line);

        //! \brief write an arc path
        QString arc(Arc& arc);

        //! \brief Write a comment
        QString comment(QString text);
    };  // class CincinnatiWriter
}  // namespace ORNL
#endif  // CINCINNATI_H
