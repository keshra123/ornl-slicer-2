#ifndef GCODETREEWIDGET_H
#define GCODETREEWIDGET_H

#include <QTreeWidget>

namespace ORNL
{
    class ProjectManager;
    class WindowManager;

    /*!
     * \class GcodeTreeWidget
     *
     * \brief Tree Widget that displays the current sliced results.
     */
    class GcodeTreeWidget : public QTreeWidget
    {
        Q_OBJECT
    public:
        GcodeTreeWidget(QWidget* parent = nullptr);

    public slots:
        //! \brief Display the context menu for the gcode heirachy tree item
        //! that is clicked
        void showContextMenu(const QPoint& point);

        //! \brief mark what is selected
        void selected(QTreeWidgetItem* twi);

        //! \brief delect any selected items
        void deselected();

    private:
        void mousePressEvent(QMouseEvent* event);

        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< WindowManager > m_window_manager;
    };
}  // namespace ORNL

#endif  // GCODETREEWIDGET_H
