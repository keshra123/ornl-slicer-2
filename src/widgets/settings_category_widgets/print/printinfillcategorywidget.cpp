#include "widgets/settings_category_widgets/print/printinfillcategorywidget.h"

#include "ui_print_infill_category_widget.h"

namespace ORNL
{
    PrintInfillCategoryWidget::PrintInfillCategoryWidget(QWidget* parent)
        : ui(new Ui::PrintInfillCategoryWidget)
    {
        ui->setupUi(this);
    }

    PrintInfillCategoryWidget::~PrintInfillCategoryWidget()
    {
        delete ui;
    }

    void PrintInfillCategoryWidget::handleConditions()
    {
        // None of these settings are affected by other settings
    }
}  // namespace ORNL
