FROM ubuntu:trusty

RUN apt-get update
RUN apt-get --yes upgrade

RUN apt-get --yes install \
 cmake vim \
 software-properties-common

RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test
RUN add-apt-repository -y ppa:beineri/opt-qt591-trusty

RUN apt-get update
RUN apt-get --yes --force-yes --no-install-recommends --no-install-suggests install qt59-meta-full

RUN apt-get --yes --force-yes install cmake gcc-4.9 g++-4.9 vim libassimp-dev mesa-common-dev

COPY . /tmp/ORNL-Slicer-2

