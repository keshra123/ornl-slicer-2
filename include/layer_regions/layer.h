#ifndef LAYER_H
#define LAYER_H

//! \file layer.h

#include <QVector>

#include "configs/settings_base.h"
#include "geometry/polygon_list.h"

namespace ORNL
{
    class Island;
    class Mesh;

    /*!
     * \class Layer
     * \brief An individual layer of an individual mesh
     */
    class Layer
        : public SettingsBase
        , public QVector< QSharedPointer< Island > >
    {
    public:
        //! \brief Constructor
        Layer(Mesh* parent, uint layer_nr);

        //! \brief Returns the parent Mesh
        Mesh* parent();

        //! \brief Returns the layer number
        uint layerNumber();

        //! \brief creates islands based on outlines
        void setOutlines(PolygonList& polygons);

    private:
        uint m_layer_nr;
        Mesh* m_parent;
    };  // class Layer
}  // namespace ORNL
#endif  // LAYER_H
