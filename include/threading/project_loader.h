#ifndef PROJECTLOADER_H
#define PROJECTLOADER_H

//! \file projectloader.h

#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <QString>
#include <QThread>
#include <assimp/Importer.hpp>
#include <json.hpp>

#include "managers/global_settings_manager.h"
#include "managers/project_manager.h"

using json = nlohmann::json;

namespace ORNL
{
    class Model;

    /*!
     * \class ProjectLoader
     * \brief Thread for loading a project
     */
    class ProjectLoader : public QThread
    {
        Q_OBJECT
    public:
        /*!
         * \brief Constructor
         * \param filename project file to used to load project
         */
        ProjectLoader(QObject* parent, const QString& filename);

        //! \brief runs upon starting the thread
        void run();
    signals:
        //! \brief emits error signal
        void errorLoadingFile(QString filename, QString error);

    private:
        //! \brief converts aiMesh from assimp to mesh
        Mesh* process_mesh(Model* model, aiMesh* m);

        //! \brief opens the model, applies the tranform, and sets the settings
        Model* import_model(QString filepath, json model_json);

        QString m_filename;
        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< GlobalSettingsManager > m_settings_manager;
    };  // class ProjectLoader
}  // namespace ORNL
#endif  // PROJECTLOADER_H
