#ifndef NOZZLEBEADWIDTHCATEGORYWIDGET_H
#define NOZZLEBEADWIDTHCATEGORYWIDGET_H

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class NozzleBeadWidthCategoryWidget;
}

namespace ORNL
{
    class NozzleBeadWidthCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        NozzleBeadWidthCategoryWidget();

        ~NozzleBeadWidthCategoryWidget();

        void handleConditions();

    private:
        Ui::NozzleBeadWidthCategoryWidget* ui;
    };
}  // namespace ORNL


#endif  // NOZZLEBEADWIDTHCATEGORYWIDGET_H
