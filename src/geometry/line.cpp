#include "geometry/line.h"
namespace ORNL
{
    Line::Line()
    {}

    Line::Line(Point start, Point end)
        : m_start(start)
        , m_end(end)
    {}

    Point Line::start() const
    {
        return m_start;
    }

    void Line::start(Point s)
    {
        m_start = s;
    }

    Point Line::end() const
    {
        return m_end;
    }

    void Line::end(Point e)
    {
        m_end = e;
    }
}  // namespace ORNL
