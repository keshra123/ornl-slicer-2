var dir_9870864803860a03def48f6710b1f137 =
[
    [ "errordialog.cpp", "errordialog_8cpp.html", null ],
    [ "errordialog.h", "errordialog_8h.html", [
      [ "ErrorDialog", "class_o_r_n_l_1_1_error_dialog.html", "class_o_r_n_l_1_1_error_dialog" ]
    ] ],
    [ "loadingdialog.cpp", "loadingdialog_8cpp.html", null ],
    [ "loadingdialog.h", "loadingdialog_8h.html", [
      [ "LoadingDialog", "class_o_r_n_l_1_1_loading_dialog.html", "class_o_r_n_l_1_1_loading_dialog" ]
    ] ],
    [ "nameconfigdialog.cpp", "nameconfigdialog_8cpp.html", null ],
    [ "nameconfigdialog.h", "nameconfigdialog_8h.html", [
      [ "NameConfigDialog", "class_o_r_n_l_1_1_name_config_dialog.html", "class_o_r_n_l_1_1_name_config_dialog" ]
    ] ],
    [ "savingdialog.cpp", "savingdialog_8cpp.html", null ],
    [ "savingdialog.h", "savingdialog_8h.html", [
      [ "SavingDialog", "class_o_r_n_l_1_1_saving_dialog.html", "class_o_r_n_l_1_1_saving_dialog" ]
    ] ]
];