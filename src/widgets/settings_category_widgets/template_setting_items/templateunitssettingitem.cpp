#include "widgets/settings_category_widgets/template_setting_items/templateunitssettingitem.h"

#include "ui_template_units_setting_item.h"

namespace ORNL
{
    TemplateUnitsSettingItem::TemplateUnitsSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplateUnitsSettingItem)
    {
        ui->setupUi(this);

        ui->line_edit->setValidator(new QDoubleValidator());
        ui->line_edit->setText("0");

        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->line_edit,
                SIGNAL(editingFinished()),
                this,
                SLOT(handleValueChanged()));
    }

    TemplateUnitsSettingItem::~TemplateUnitsSettingItem()
    {
        delete ui;
    }

    void TemplateUnitsSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }

    void TemplateUnitsSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }

    void TemplateUnitsSettingItem::unit(QString text)
    {
        ui->unit->setText(text);
    }

    void TemplateUnitsSettingItem::value(double v)
    {
        ui->line_edit->setText(QString::number(v, 'f', 3));
    }

    double TemplateUnitsSettingItem::value()
    {
        bool ok;
        double d = ui->line_edit->text().toDouble(&ok);
        if (ok)
        {
            return d;
        }
        throw SettingValueException("unit");
    }

    void TemplateUnitsSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->line_edit->setStyleSheet(stylesheet);
    }

    void TemplateUnitsSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplateUnitsSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplateUnitsSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplateUnitsSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplateUnitsSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplateUnitsSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplateUnitsSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
