#ifndef TST_CINCINNATIPARSER_H
#define TST_CINCINNATIPARSER_H

//! \file tst_cincinnatiparser.h

#include <QtTest/QtTest>

#include "auto_test.h"
#include "gcode/parsers/cincinnati_parser.h"

using namespace ORNL;
using namespace AutoTest;

#if 0 // TODO fix tests

/*!
 * \breif The TestCincinattiParser class, this class declares unit tests for the CincinattiParser.
 */
class TestCincinnatiParser : public QObject
{
public:
    Q_OBJECT
private:
    CincinnatiParser* test; // Needs to be a pointer because the Unit variables are unititalized once this object is
	         		    		  // initalized and thus will always be zero.
private slots:

    //! \brief Runs once for this test class.
    void initTestCase();
    //! \brief Runs once all test cases have been completed
    void cleanupTestCase();

    //! \brief Runs before each test case to setup the correct testing environment for each individual test case.
    void init();
    //! \brief Runs before each test case to prevent spilling over of data between cases
    void cleanup();

    void testWholeRead();
};

DECLARE_TEST(TestCincinnatiParser)

#endif // TST_CINCINNATIPARSER_H

#endif
