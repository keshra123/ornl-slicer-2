#include "layer_regions/region.h"

#include <QReadWriteLock>
#include <utility>

#include "layer_regions/island.h"
#ifdef DEBUG
#    include <QPainter>
#endif
#include "geometry/path.h"

namespace ORNL
{
    Region::Region(Island* parent)
        : SettingsBase(parent)
        , m_parent(parent)
        , m_lock(new QReadWriteLock)
    {
#ifdef DEBUG

        // outline pen width should the polygon pen width + 2 * the outline pen
        // width drawing should always be done in the order outline, polygon,
        // center line

        m_centerline_pen.setStyle(Qt::DashLine);
        m_centerline_pen.setCapStyle(Qt::RoundCap);
        m_centerline_pen.setJoinStyle(Qt::RoundJoin);
        m_centerline_pen.setWidth(1);

        m_outline_pen.setCapStyle(Qt::RoundCap);
        m_outline_pen.setJoinStyle(Qt::RoundJoin);
        m_outline_pen.setWidth(9);

        m_polygon_pen.setCapStyle(Qt::RoundCap);
        m_polygon_pen.setJoinStyle(Qt::RoundJoin);
        m_polygon_pen.setWidth(5);
#endif
    }

    PolygonList Region::outline()
    {
        QReadLocker locker(m_lock.data());
        return m_outline;
    }

    QVector< Path > Region::paths()
    {
        QReadLocker locker(m_lock.data());
        return m_paths;
    }

    void Region::compute()
    {
        QWriteLocker locker(m_lock.data());
        bool settings_changed = false;
        for (json::iterator it = m_cache_settings.begin();
             it != m_cache_settings.end();
             it++)
        {
            auto key   = it.key();
            auto value = it.value();
            settings_changed |=
                tryUpdateCache< decltype(key), decltype(value) >(key, value);
        }

        if (settings_changed)
        {
#ifdef DEBUG
            m_debug_dirty = true;
#endif
            computeInternal();
        }
    }

    void Region::order(Point& previous_location)
    {
        QWriteLocker locker(m_lock.data());
        orderInternal(previous_location);
    }

    QString Region::gcode(WriterBase* syntax)
    {
        QReadLocker locker(m_lock.data());
        QString gcode;
        for (Path path : m_paths)
        {
            gcode += path.gcode(syntax);
        }
        return gcode;
    }

    uint Region::numberPaths()
    {
        return static_cast< uint >(m_paths.size());
    }

    Path& Region::operator[](int path_nr)
    {
        QReadLocker locker(m_lock.data());
        return m_paths[path_nr];
    }

    void Region::orderTime(Point& previous_location, QVector< Path >& paths)
    {
        // TODO
    }

    void Region::orderDistance(Point& previous_location, QVector< Path >& paths)
    {
        // TODO
    }

    void Region::orderGenetic(Point& previous_location, QVector< Path >& paths)
    {
        // TODO
    }

#ifdef DEBUG
    bool Region::isDebugOption(QString option)
    {
        return debugOptions().contains(option);
    }

    QStringList Region::debugOptions()
    {
        QReadLocker locker(m_lock.data());
        if (m_debug_dirty)
        {
            m_debug_options = debugOptionsInternal();
            m_debug_dirty   = false;
        }
        return m_debug_options;
    }

    void Region::paintDebug(QString option,
                            QPainter* painter,
                            Distance& ratio,
                            int vertical_offset)
    {
        QReadLocker locker(m_lock.data());
        paintDebugInternal(option, painter, ratio, vertical_offset);
    }
#endif
}  // namespace ORNL
