#include "utilities/richtextitemdelegate.h"

namespace ORNL
{
    void RichTextItemDelegate::paint(QPainter* painter,
                                     const QStyleOptionViewItem& option,
                                     const QModelIndex& index) const
    {
        QStyleOptionViewItemV4 options = option;
        initStyleOption(&options, index);

        QStyle* style = QApplication::style();

        QTextDocument doc;
        doc.setHtml(options.text);

        options.text = "";
        style->drawControl(QStyle::CE_ItemViewItem, &options, painter);

        QAbstractTextDocumentLayout::PaintContext ctx;

        // Highlighting text if item is selected
        if (options.state & QStyle::State_Selected)
        {
            ctx.palette.setColor(
                QPalette::Text,
                options.palette.color(QPalette::Active,
                                      QPalette::HighlightedText));
        }

        QRect text_rect =
            style->subElementRect(QStyle::SE_ItemViewItemText, &option);
        painter->save();
        painter->translate(text_rect.topLeft());
        painter->setClipRect(text_rect.translated(-text_rect.topLeft()));
        doc.documentLayout()->draw(painter, ctx);
        painter->restore();
    }

    QSize RichTextItemDelegate::sizeHint(const QStyleOptionViewItem& option,
                                         const QModelIndex& index) const
    {
        QStyleOptionViewItemV4 options = option;
        initStyleOption(&options, index);

        QTextDocument doc;
        doc.setHtml(options.text);
        doc.setTextWidth(options.rect.width());
        return QSize(doc.idealWidth(), doc.size().height());
    }
}  // namespace ORNL
