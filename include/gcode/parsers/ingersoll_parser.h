#ifndef INGERSOLLPARSER_H
#define INGERSOLLPARSER_H

//! \file ingersollparser.h

#include "gcode/parsers/common_parser.h"

namespace ORNL
{
    /*!
     * \class IngersollParser
     * \brief This class implements the GCode parsing configuration for the
     * Ingersoll 3D printer(s). \note The current commands that this class
     * implements are:
     *          - M Commands:
     *              -
     *          - Tool Change Commands:
     *              - All T commands are checked for syntax errors but otherwise
     * do nothing. The commands inherited from the CommonParser superclass are:
     *          - G Commands:
     *              - G0, G1, G2, G3, G4
     *          - Line Comment Delimiter:
     *              - ;
     *
     */
    // TODO: Remove Line numbers.
    //
    class IngersollParser : public CommonParser
    {
    public:
        IngersollParser();

        virtual void config();

    protected:
        // Line Numbers
        virtual void NHandler(QStringList& LineNumber);

        // High Speed Print
        virtual void HSPHandler();

        // No clue (Probably heats the extruder)
        virtual void HALOHandler(QStringList& commands);

        // Tells the extruder which layer and dead is being used.
        virtual void LAY_BEADHandler(QStringList& commands);

        // Needs 4 different modes, First turns pump on, Second changes pump
        // percentage, Third changes the pump speed, If a 0 is encountered turn
        // off pump.
        virtual void EXTRUDERHandler(QStringList& commands);

        // Just sends a message to the console or w/e, is double quote delimted.
        virtual void MSGHandler(QStringList& commands);


        // No clue
        virtual void transHandler();

        // No clue
        virtual void rotHandler();

        // No clue
        virtual void scaleHandler();

        // No clue
        virtual void mirrorHandler();

        // Sets print offset (inches)
        virtual void atransHandler(QStringList& commands);


        // XY Plane selection
        virtual void G17Handler();

        // Tool Radius Compensation Off
        virtual void G40Handler();

        // No clue
        virtual void G54Handler();

        // Absolute value distance mode
        virtual void G90Handler();

        // inch/min for feedrate
        virtual void G94Handler();

        // Sets units to inches
        virtual void G700Handler();


        // Enable extruder
        virtual void M100Handler();

        // Disable extruder
        virtual void M101Handler();

        //! \brief Funciton handler for tool changes in GCode. For now its just
        //! a placeholder function.
        virtual void ToolChangeHandler(QStringList& params);


    private:
    };
}  // namespace ORNL
#endif  // INGERSOLPARSER_H
