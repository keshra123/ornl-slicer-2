#ifndef MESH_H
#define MESH_H

//! \file mesh.h
#include <QMatrix4x4>
#include <json.hpp>

#include "configs/settings_base.h"
#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"
#include "geometry/point.h"

struct aiMesh;

class QQuaternion;
class QVector3D;

namespace ORNL
{
    class PreferencesManager;
    class MeshGraphics;
    class MeshVertex;
    class MeshFace;
    class Layer;

    /*!
     * \class Mesh
     * \brief An individual mesh contained within a model
     */
    class Mesh
        : public QObject
        , public SettingsBase
        , public QVector< QSharedPointer< Layer > >
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        Mesh(const QVector< MeshVertex >& vertices,
             const QVector< MeshFace >& faces,
             SettingsBase* parent = nullptr);

        //! \brief Copy Constructor
        Mesh(const Mesh& m);

        //! \brief Returns the name of the mesh
        QString name();

    public slots:
        //! \brief Sets the name for the mesh
        void name(QString n);

        //! \brief Set the translation for the mesh
        void setTranslation(float x, float y, float z);

        //! \brief Set the translation for the mesh
        void setTranslation(QVector3D t);

        //! \brief Set the translation for the mesh in the x direction
        void setTranslationX(float x);

        //! \brief Set the translation for the mesh in the y direction
        void setTranslationY(float y);

        //! \brief Set the translation for the mesh in the z direction
        void setTranslationZ(float z);

        //! \brief Translate the mesh
        void translate(float x, float y, float z);

        //! \brief Translate the mesh
        void translate(QVector3D t);

        //! \brief Translate the mesh in the x direction
        void translateX(float x);

        //! \brief Translate the mesh in the y direction
        void translateY(float y);

        //! \brief Translate the mesh in the z direction
        void translateZ(float z);

        //! \brief Returns the translation of the mesh
        QVector3D translation();

        //! \brief Sets the rotation for the mesh (euler version)
        void setRotation(float x, float y, float z);

        //! \brief Sets the rotation around the x axis
        void setRotationX(float x);

        //! \brief Sets the rotation around the y axis
        void setRotationY(float y);

        //! \brief Sets the rotation around the z axis
        void setRotationZ(float z);

        //! \brief Sets the rotation for the mesh (quaternion version)
        void setRotation(float w, float x, float y, float z);

        //! \brief Sets the rotation for the mesh
        void setRotation(QQuaternion q);

        //! \brief Rotate the mesh (euler version)
        void rotate(float x, float y, float z);

        //! \brief Rotate the mesh around the x axis
        void rotateX(float x);

        //! \brief Rotate the mesh around the y axis
        void rotateY(float y);

        //! \brief Rotate the mesh around the z axis
        void rotateZ(float z);

        //! \brief Rotate the mesh (quaternion version)
        void rotate(float w, float x, float y, float z);

        //! \brief Rotate the mesh
        void rotate(QQuaternion q);

        //! \brief Returns the rotation of the mesh
        QQuaternion rotation();

        //! \brief Sets the scale for the mesh (uniform)
        void setScale(float s);

        //! \brief Sets the scale for the mesh
        void setScale(float x, float y, float z);

        //! \brief Sets the scale for the mesh in the x direction
        void setScaleX(float x);

        //! \brief Sets the scale for the mesh in the y direction
        void setScaleY(float y);

        //! \brief Sets the scale for the mesh in the z direction
        void setScaleZ(float z);

        //! \brief Sets the scale for the mesh
        void setScale(QVector3D s);

        //! \brief Scale the mesh (uniform)
        void scale(float s);

        //! \brief Scale the mesh
        void scale(float x, float y, float z);

        //! \brief Scale the mesh in the x direction
        void scaleX(float x);

        //! \brief Scale the mesh in the y direction
        void scaleY(float y);

        //! \brief Scale the mesh in the z direction
        void scaleZ(float z);

        //! \brief Scale the mesh
        void scale(QVector3D v);

        //! \brief Returns the scale for the mesh
        QVector3D scale();

        //! \brief Sets the unit for the mesh
        void unit(Distance unit);

        //! \brief Returns the unit for the mesh
        Distance unit();

        //! \brief Set the default display color
        void color(QVector3D c);

        //! \brief Returns the default display color
        QVector3D color();

        //! \brief Sets whether the mesh is selected
        void selected(bool selected);

        //! \brief Returns whether the mesh is selected
        bool selected();

        //! \brief Returns vector with the minimum values in each of x, y, and z
        Point min();

        //! \brief Returns vector with the maximum values in each of x, y, and z
        Point max();

        //! \brief Returns vector with the average values in each of x, y, and z
        Point center();

        //! \brief Translates the mesh to be centered and drops it to the table
        void centerMesh();

        //! \brief Drops the mesh to the table
        void dropMesh();

        //! \brief Creates more layers if needed or removes them if too many
        void updateLayers();

        //! \brief Gathers all the metadata and returns json containing it
        nlohmann::json json();

        //! \brief Loads the models with the metadate from json
        void json(nlohmann::json json_object);

        //! \brief Returns a shared pointer to the MeshGraphics object
        QSharedPointer< MeshGraphics > graphics();

        //! \brief creates the MeshGraphic for the mesh view
        void createGraphics();

        QVector< MeshVertex > vertices();

        QVector< MeshFace >& faces();

    private:
        QVector< MeshVertex > m_vertices;
        QVector< MeshFace > m_faces;

        QSharedPointer< MeshGraphics > m_graphics;

        QSharedPointer< PreferencesManager > m_preferences_manager;

        QString m_name;
        Distance m_unit;
        bool m_selected;
        QVector3D m_display_color;

        friend class MeshGraphics;
    };  // class Mesh
}  // namespace ORNL

Q_DECLARE_METATYPE(QSharedPointer< ORNL::Mesh >)

#endif  // MESH_H
