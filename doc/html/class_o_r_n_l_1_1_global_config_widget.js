var class_o_r_n_l_1_1_global_config_widget =
[
    [ "GlobalConfigWidget", "class_o_r_n_l_1_1_global_config_widget.html#a85b7e2eefdc8d5dd8d7c422231881164", null ],
    [ "~GlobalConfigWidget", "class_o_r_n_l_1_1_global_config_widget.html#a6cd07960e356a3d37b3403fa107342c6", null ],
    [ "handleAddConfig", "class_o_r_n_l_1_1_global_config_widget.html#a559c5a759a120cacf6753135f799cf3c", null ],
    [ "handleCategorySelectionChanged", "class_o_r_n_l_1_1_global_config_widget.html#ad5210dbe266cb3744409403917839e0c", null ],
    [ "handleConfigSelectionChanged", "class_o_r_n_l_1_1_global_config_widget.html#aae590f2718c05917026063f66ec2ef8d", null ],
    [ "handleDeleteConfig", "class_o_r_n_l_1_1_global_config_widget.html#a8c4e68292cb610e73072ee7edd62aaf5", null ],
    [ "handleModified", "class_o_r_n_l_1_1_global_config_widget.html#aa7afaf29a77051723c4437382372e734", null ],
    [ "handleNozzleSelectionChanged", "class_o_r_n_l_1_1_global_config_widget.html#a562269696ec72f06222534aec730f176", null ],
    [ "handleSaveConfig", "class_o_r_n_l_1_1_global_config_widget.html#a7e4f418f912cb85d8c0b63ab93624e9f", null ],
    [ "loadFirstOption", "class_o_r_n_l_1_1_global_config_widget.html#a74c7f3f397eb79b311fc2b8ff64751b3", null ],
    [ "reload", "class_o_r_n_l_1_1_global_config_widget.html#a1512c4634cacb89692a1b6c10a7e1d8b", null ],
    [ "setConfigType", "class_o_r_n_l_1_1_global_config_widget.html#abb2bbd2ce9762aa6c30ecd62e4245342", null ],
    [ "setCurrentConfig", "class_o_r_n_l_1_1_global_config_widget.html#a6205a8a66a352e1502d1afbc9597eb72", null ],
    [ "update", "class_o_r_n_l_1_1_global_config_widget.html#aa6fad85941b6717ec4cd5c0fb7f2ff31", null ],
    [ "updateCategoryList", "class_o_r_n_l_1_1_global_config_widget.html#a0f7bb96b8dd5d3b150cf97c7f78fc353", null ]
];