#ifndef TEMPLATEDROPDOWNSETTINGITEM_H
#define TEMPLATEDROPDOWNSETTINGITEM_H

#include <QWidget>

namespace Ui
{
    class TemplateDropdownSettingItem;
}
namespace ORNL
{
    class TemplateDropdownSettingItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit TemplateDropdownSettingItem(QWidget* parent = 0);
        ~TemplateDropdownSettingItem();

        //! \brief Sets the value for the label
        void label(QString text);

        //! \brief Sets the value for the tooltip
        void tooltip(QString text);

        //! \brief Sets the list of items in the dropdown
        void list(QStringList list);

        //! \brief Returns the value in the dropdown
        QString value();

        //! \brief Sets the stylesheet for the checkbox
        void setStyleSheet(const QString& stylesheet);

    signals:
        //! \brief Signals that the undo button has been pressed
        void undo();

        //! \brief Signals that the redo button has been pressed
        void redo();

        //! \brief Signals that the value has changed
        void valueChanged(QString);

    public slots:
        void value(QString c);

        //! \brief Show the undo button
        void showUndo();

        //! \brief Hide the undo button
        void hideUndo();

        //! \brief Show the redo button
        void showRedo();

        //! \brief Hide the redo button
        void hideRedo();

    private slots:
        void handleUndo();
        void handleRedo();
        void handleValueChanged();

    private:
        Ui::TemplateDropdownSettingItem* ui;
    };

}  // namespace ORNL

#endif  // TEMPLATEDROPDOWNSETTINGITEM_H
