#include "widgets/settings_category_widgets/template_setting_items/templatestringsettingitem.h"

#include "ui_template_string_setting_item.h"

namespace ORNL
{
    TemplateStringSettingItem::TemplateStringSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplateStringSettingItem)
    {
        ui->setupUi(this);

        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->text_edit,
                SIGNAL(editingFinished()),
                this,
                SLOT(handleValueChanged()));
    }

    TemplateStringSettingItem::~TemplateStringSettingItem()
    {
        delete ui;
    }

    void TemplateStringSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }

    void TemplateStringSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }

    void TemplateStringSettingItem::value(QString t)
    {
        ui->text_edit->setText(t);
    }

    void TemplateStringSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->text_edit->setStyleSheet(stylesheet);
    }

    QString TemplateStringSettingItem::value()
    {
        return ui->text_edit->toPlainText();
    }

    void TemplateStringSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplateStringSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplateStringSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplateStringSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplateStringSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplateStringSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplateStringSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
