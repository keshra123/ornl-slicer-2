#include "geometry/polyline.h"

namespace ORNL
{
    Polyline::Polyline(const QVector< Point >& path)
    {
        QVector< Point >::operator+=(path);
    }

    Polyline::Polyline(const QVector< ClipperLib::IntPoint >& path)
    {
        for (ClipperLib::IntPoint p : path)
        {
            append(Point(p));
        }
    }

    Polyline::Polyline(const ClipperLib::Path& path)
    {
        for (ClipperLib::IntPoint p : path)
        {
            append(Point(p));
        }
    }

    bool Polyline::shorterThan(Distance check_length) const
    {
        Distance length;
        Point p0 = operator[](0);
        for (int i = 1; i < size(); i++)
        {
            Point p1 = operator[](i);
            length += p1.distance(p0);
            p0 = p1;
        }

        return length < check_length;
    }

    Point Polyline::closestPointTo(const Point& rhs) const
    {
        Point ret         = rhs;
        Distance bestDist = Distance(std::numeric_limits< float >::max());
        for (Point point : (*this))
        {
            Distance dist = rhs.distance(point);
            if (dist < bestDist)
            {
                ret      = point;
                bestDist = dist;
            }
        }
        return ret;
    }

    Polyline Polyline::smooth(Distance removeLength)
    {
        // TODO
        return Polyline();
    }

    Polyline Polyline::simplify(Area smallest_line_segment_squared,
                                Area allowed_error_distance_squared)
    {
        // TODO
        return Polyline();
    }

    Polygon Polyline::close()
    {
        Polygon ret(operator()());
        if (ret.back() == ret.front())
        {
            ret.pop_back();
        }
        return ret;
    }

    Polyline Polyline::rotate(Angle rotation_angle, QVector3D axis)
    {
        return rotateAround({0, 0, 0}, rotation_angle, axis);
    }

    Polyline Polyline::rotateAround(Point center,
                                    Angle rotation_angle,
                                    QVector3D axis)
    {
        Polyline polyline;
        QVector3D c = center.toQVector3D();
        QMatrix4x4 m;
        m.rotate(rotation_angle.to(deg), axis);
        for (int i = 0; i < size(); i++)
        {
            QVector3D p = operator[](i).toQVector3D();
            p -= c;
            p = m * p;
            p += c;
            polyline.append(Point::fromQVector3D(p));
        }
        return polyline;
    }

    Polyline Polyline::operator+(Polyline rhs)
    {
        if (last() == rhs.first())
        {
            rhs.removeFirst();
        }
        Polyline polyline(*this);
        for (int i = 0; i < rhs.size(); i++)
        {
            polyline.push_back(rhs[i]);
        }
        return polyline;
    }

    Polyline Polyline::operator+(const Point& rhs)
    {
        Polyline rv(*this);
        rv += rhs;
        return rv;
    }

    Polyline& Polyline::operator+=(Polyline rhs)
    {
        if (last() == rhs.first())
        {
            rhs.removeFirst();
        }
        for (int i = 0; i < rhs.size(); i++)
        {
            push_back(rhs[i]);
        }
        return *this;
    }

    Polyline& Polyline::operator+=(const Point& rhs)
    {
        push_back(rhs);
        return *this;
    }

    QVector< Polyline > Polyline::operator-(const Polygon& rhs)
    {
        ClipperLib::PolyTree poly_tree;
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPath(rhs(), ClipperLib::ptClip, true);
        clipper.AddPath(operator()(), ClipperLib::ptSubject, false);
        clipper.Execute(ClipperLib::ctDifference,
                        poly_tree,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        ClipperLib::OpenPathsFromPolyTree(poly_tree, paths);

        QVector< Polyline > rv;
        for (ClipperLib::Path path : paths)
        {
            rv += Polyline(path);
        }
        return rv;
    }

    QVector< Polyline > Polyline::operator-(const PolygonList& rhs)
    {
        ClipperLib::PolyTree poly_tree;
        ClipperLib::Paths paths;
        ClipperLib::Clipper clipper;
        clipper.AddPaths(rhs(), ClipperLib::ptClip, true);
        clipper.AddPath(operator()(), ClipperLib::ptSubject, false);
        clipper.Execute(ClipperLib::ctDifference,
                        poly_tree,
                        ClipperLib::pftNonZero,
                        ClipperLib::pftNonZero);
        ClipperLib::OpenPathsFromPolyTree(poly_tree, paths);

        QVector< Polyline > rv;
        for (ClipperLib::Path path : paths)
        {
            rv += Polyline(path);
        }
        return rv;
    }

    QVector< Point > Polyline::operator&(const Polyline& rhs)
    {
        // TODO
        return QVector< Point >();
    }

    ClipperLib::Path Polyline::operator()() const
    {
        ClipperLib::Path path;
        for (Point p : (*this))
        {
            path.push_back(p.toIntPoint());
        }
        return path;
    }
}  // namespace ORNL
