#ifndef LINEARALG2D_H
#define LINEARALG2D_H

#include "point.h"
#include "units/unit.h"

namespace ORNL
{
    class LinearAlg2D
    {
    public:
        static short pointLiesOnTheRightOfLine(Point p, Point p0, Point p1);
    };
}  // namespace ORNL
#endif  // LINEARALG2D_H
