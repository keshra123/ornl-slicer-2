#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H
/*
namespace Qt3DCore {
    class QEntity;
}
namespace Qt3DRender {
    class QCamera;
}

namespace Qt3DInput {
    class QAction;
    class QAxis;
    class QActionInput;
    class QAnalogAxisInput;
    class QButtonAxisInput;
    class QKeyboardDevice;
    class QMouseDevice;
    class QLogicalDevice;
}

namespace Qt3DLogic {
    class QFrameAction;
}

namespace ORNL {
    class CameraController : public Qt3DCore::QEntity
    {
        Q_OBJECT
        Q_PROPERTY(float m_linear_speed READ linearSpeed WRITE setLinearSpeed
NOTIFY linearSpeedChanged) Q_PROPERTY(float m_look_speed READ lookSpeed WRITE
setLookSpeed NOTIFY lookSpeedChanged) Q_PROPERTY(float m_zoom_in_limit READ
zoomInLimit WRITE setZoomInLimitSpeed NOTIFY zoomInLimitChanged) public:
        CameraController(Qt3DCore::QEntity* entity, Qt3DRender::QCamera*
camera);

        Qt3DRender::QCamera* camera() const;
        float linearSpeed() const;
        float lookSpeed() const;
        float zoomInLimit() const;

        void setLinearSpeed(float linear_speed);
        void setLookSpeed(float look_speed);
        void setZoomInLimit(float zoom_in_limit);

    signals:
        void linearSpeedChanged();
        void lookSpeedChanged();
        void zoomInLimitChanged();

    private:
        Qt3DRender::QCamera* m_camera;
        Qt3DCore::QTransform* m_transform;

        Qt3DInput::QAction* m_left_mouse_button_action;
        Qt3DInput::QAction* m_right_mouse_button_action;
        Qt3DInput::QAction* m_alt_button_action;
        Qt3DInput::QAction* m_shift_button_action;

        Qt3DInput::QAxis* m_rx_axis;
        Qt3DInput::QAxis* m_ry_acis;
        Qt3DInput::QAxis* m_tx_axis;
        Qt3DInput::QAxis* m_ty_axis;
        Qt3DInput::QAxis* m_tz_axis;


        Qt3DInput::QActionInput* m_left_mouse_button_input;
        Qt3DInput::QActionInput* m_right_mouse_button_input;
        Qt3DInput::QActionInput* m_alt_button_input;
        Qt3DInput::QActionInput* m_shift_button_input;

        Qt3DInput::QAnalogAxisInput* m_mouse_rx_input;
        Qt3DInput::QAnalogAxisInput* m_mouse_ry_input;
        Qt3DInput::QAnalogAxisInput* m_mouse_tzx_input;
        Qt3DInput::QAnalogAxisInput* m_mouse_tzy_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_tx_pos_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_ty_pos_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_tz_pos_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_tx_neg_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_ty_neg_input;
        Qt3DInput::QButtonAxisInput* m_keyboard_tz_neg_input;

        Qt3DInput::QKeyboardDevice* m_keyboard;
        Qt3DInput::QMouseDevice* m_mouse;

        Qt3DInput::QLogicalDevice* m_logical_device;

        Qt3DLogic::QFrameAction* m_frame_action;

        float m_linear_speed;
        float m_look_speed;
        float m_zoom_in_limit;

        QVector3D m_camera_up;
    };
}

*/
#endif  // CAMERACONTROLLER_H
