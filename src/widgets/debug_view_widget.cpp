#include "widgets/debug_view_widget.h"
#ifdef DEBUG
#    include <QPaintEvent>
#    include <QPainter>

#    include "configs/settings_base.h"
#    include "managers/project_manager.h"
#    include "utilities/constants.h"
#    include "utilities/mathutils.h"
#endif  // NDEBUG

namespace ORNL
{
    DebugViewWidget::DebugViewWidget(QWidget* parent)
        : QWidget(parent)
#ifndef DEBUG
    {}
#else
        , m_project_manager(ProjectManager::getInstance())
        , m_background(QColor(255, 255, 255))
        , m_machine_pen()
        , m_horizontal_offset(10)
    {
        m_machine_pen.setWidth(5);
        connect(m_project_manager.data(),
                SIGNAL(currentMachineChanged(QString)),
                this,
                SLOT(updateMachineRatio()));
    }

    void DebugViewWidget::updateMachineRatio()
    {
        QSharedPointer< SettingsBase > machine =
            m_project_manager->currentMachine();

        if (machine &&
            machine->contains(Constants::MachineSettings::Dimensions::kXMin))
        {
            m_width  = width() - (m_horizontal_offset * 2);
            m_height = height();

            Distance machine_x =
                machine->setting< Distance >(
                    Constants::MachineSettings::Dimensions::kXMax) -
                machine->setting< Distance >(
                    Constants::MachineSettings::Dimensions::kXMin);
            Distance machine_y =
                machine->setting< Distance >(
                    Constants::MachineSettings::Dimensions::kYMax) -
                machine->setting< Distance >(
                    Constants::MachineSettings::Dimensions::kYMin);
            Distance width_ratio;
            Distance height_ratio;
            /*
            if(machine_x > machine_y)
            {
                width_ratio = static_cast<double>(m_width) / machine_x();
                length_ratio = static_cast<double>(m_height) / machine_y();
            }
            else
            {
            */
            width_ratio  = static_cast< double >(m_width) / machine_x();
            height_ratio = static_cast< double >(m_height) / machine_y();
            //}

            m_ratio = qMin(width_ratio, height_ratio);

            m_height *= machine_y() / machine_x();
            m_vertical_offset = height() / 2 - m_height / 2;
        }
    }

    void DebugViewWidget::paintEvent(QPaintEvent* event)
    {
        QPainter painter;
        painter.begin(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.save();
        painter.fillRect(event->rect(), m_background);

        painter.setPen(m_machine_pen);
        painter.drawRect(
            m_horizontal_offset, m_vertical_offset, m_width, m_height);

        m_project_manager->paintSelectedDebug(
            &painter, m_ratio, m_vertical_offset);
        painter.restore();
        painter.end();
    }

    void DebugViewWidget::resizeEvent(QResizeEvent* event)
    {
        updateMachineRatio();
        QWidget::resizeEvent(event);
    }

#endif  // QT_DEBUG

}  // namespace ORNL
