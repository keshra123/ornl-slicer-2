#ifndef ISLANDTHREAD_H
#define ISLANDTHREAD_H

#include <QSharedPointer>
#include <QThread>
namespace ORNL
{
    class Island;

    /*!
     * \class IslandThread
     *
     * \brief Thread for running slicing calculations and path planning for an
     * individual island on an individual layer on an individual mesh.
     */
    class IslandThread : public QThread
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        IslandThread(QObject* parent, QSharedPointer< Island > island);

    signals:
        //! \brief Signal that the computation for this island has finished
        void slicingFinished(QSharedPointer< Island > island);

        //! \brief Signal that the computation for this island has been
        //! interrupted
        void slicingInterrupted(QSharedPointer< Island > island);

    protected:
        //! \brief Main computation function
        void run() override;

    private:
        QSharedPointer< Island > m_island;
    };
}  // namespace ORNL
#endif  // ISLANDTHREAD_H
