#include "widgets/settings_category_widgets/machine/machinesyntaxcategorywidget.h"

#include "ui_machine_syntax_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    MachineSyntaxCategoryWidget::MachineSyntaxCategoryWidget()
        : ui(new Ui::MachineSyntaxCategoryWidget)
    {
        ui->setupUi(this);

        // Setup syntax
        ui->syntax->label(
            Constants::MachineSettings::Syntax::Labels::kMachineSyntax);
        ui->syntax->tooltip(
            Constants::MachineSettings::Syntax::Tooltips::kMachineSyntax);
        ui->syntax->list(Constants::MachineSettings::Syntax::kSyntaxOptions);
        m_enum[Constants::MachineSettings::Syntax::kMachineSyntax] =
            QSharedPointer< TemplateDropdownSettingItem >(ui->syntax);

        initializeSettings();
    }

    MachineSyntaxCategoryWidget::~MachineSyntaxCategoryWidget()
    {
        delete ui;
    }

    void MachineSyntaxCategoryWidget::handleConditions()
    {
        // None of these settings are affected by other settings
    }
}  // namespace ORNL
