#ifndef NOZZLESETTINGCATEGORYWIDGETS_H
#define NOZZLESETTINGCATEGORYWIDGETS_H

#include "widgets/settings_category_widgets/nozzle/nozzleaccelerationcategorywidget.h"
#include "widgets/settings_category_widgets/nozzle/nozzlebeadwidthcategorywidget.h"
#include "widgets/settings_category_widgets/nozzle/nozzlelayercategorywidget.h"
#include "widgets/settings_category_widgets/nozzle/nozzlespeedcategorywidget.h"

#endif  // NOZZLESETTINGCATEGORYWIDGETS_H
