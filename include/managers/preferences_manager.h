#ifndef PREFERENCESMANAGER_H
#define PREFERENCESMANAGER_H

#include <QObject>

#include "units/unit.h"

namespace ORNL
{
    /*!
     * \class PreferencesManager
     * \brief Manager for preferences
     */
    class PreferencesManager : public QObject
    {
        Q_OBJECT

    public:
        //! \brief Returns the singleton
        static QSharedPointer< PreferencesManager > getInstance();

        //! \brief import preferences from file
        void importPreferences(QString filepath = "");

        //! \brief export preferences to file
        void exportPreferences(QString filepath = "");

        //! \brief Returns the unit used for distance/length
        Distance getDistanceUnit();

        //! \brief Returns the unit used for velocity
        Velocity getVelocityUnit();

        //! \brief Returns the unit used for acceleration
        Acceleration getAccelerationUnit();

        //! \brief Returns the unit used for angle
        Angle getAngleUnit();

        //! \brief Returns the unit used for time
        Time getTimeUnit();

        //! \brief Returns the text for the unit used for distance/length
        QString getDistanceUnitText();

        //! \brief Returns the text for the unit used for velocity
        QString getVelocityUnitText();

        //! \brief Returns the text for the unit used for acceleration
        QString getAccelerationUnitText();

        //! \brief Returns the text for the unit used for angle
        QString getAngleUnitText();

        //! \brief Returns the text for the unit used for time
        QString getTimeUnitText();

    signals:
        //! \brief Signal emitted when the distance unit is changed
        void distanceUnitChanged(Distance new_value, Distance old_value);

        //! \brief Signal emitted when the velocity unit is changed
        void velocityUnitChanged(Velocity new_value, Velocity old_value);

        //! \brief Signal emitted when the acceleration unit is changed
        void accelerationUnitChanged(Acceleration new_value,
                                     Acceleration old_value);

        //! \brief Signal emitted when the angle unit is changed
        void angleUnitChanged(Angle new_value, Angle old_value);

        //! \brief Signal emitted when the time unit is changed
        void timeUnitChanged(Time new_value, Time old_value);


    public slots:
        //! \brief Sets the unit used for distance/length
        void setDistanceUnit(QString du);

        //! \brief Sets the unit used for distance/length
        void setDistanceUnit(Distance d);

        //! \brief Sets the unit used for velocity
        void setVelocityUnit(QString vu);

        //! \brief Sets the unit used for velocity
        void setVelocityUnit(Velocity v);

        //! \brief Sets the unit used for acceleration
        void setAccelerationUnit(QString au);

        //! \brief Sets the unit used for acceleration
        void setAccelerationUnit(Acceleration a);

        //! \brief Sets the unit used for angle
        void setAngleUnit(QString au);

        //! \brief Sets the unit used for angle
        void setAngleUnit(Angle a);

        //! \brief Sets the unit used for time
        void setTimeUnit(QString t);

        //! \brief Sets the unit used for time
        void setTimeUnit(Time t);

    private:
        //! \brief Constructor
        PreferencesManager();

        static QSharedPointer< PreferencesManager > m_singleton;

        Distance m_distance_unit;
        Velocity m_velocity_unit;
        Acceleration m_acceleration_unit;
        Angle m_angle_unit;
        Time m_time_unit;
    };  // class PreferencesManager
}  // namespace ORNL
#endif  // PREFERENCESMANAGER_H
