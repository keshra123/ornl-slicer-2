#include "graphics/meshgraphics.h"

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QGeometry>
#include <Qt3DRender/QGeometryRenderer>
#include <Qt3DRender/QMaterial>
#include <Qt3DRender/QPickingSettings>

#include "geometry/mesh.h"
#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"

namespace ORNL
{
    QByteArray* MeshGraphics::createVertexByteArray()
    {
        QByteArray* byte_array = new QByteArray();

        // Vertices and Normals
        QVector< MeshVertex >& vertices = m_mesh->m_vertices;

        byte_array->resize(vertices.size() * (2 * 3) * sizeof(float));

        float* raw_vertex_array =
            reinterpret_cast< float* >(byte_array->data());

        for (int i = 0; i < vertices.size(); i++)
        {
            int idx = i * 6;

            raw_vertex_array[idx] = vertices[i].location.x();

            raw_vertex_array[idx + 1] = vertices[i].location.y();

            raw_vertex_array[idx + 2] = vertices[i].location.z();

            raw_vertex_array[idx + 3] = vertices[i].normal.x();

            raw_vertex_array[idx + 4] = vertices[i].normal.y();

            raw_vertex_array[idx + 5] = vertices[i].normal.z();
        }

        return byte_array;
    }

    QByteArray* MeshGraphics::createIndexByteArray()
    {
        QByteArray* byte_array = new QByteArray();

        // Indices
        QVector< MeshFace >& faces = m_mesh->m_faces;

        if (m_mesh->m_vertices.size() <
            65535)  // If there are few enough vertices then an unsigned short
                    // is all that is needed
        {
            byte_array->resize(faces.size() * 3 * sizeof(ushort));

            ushort* raw_index_array =
                reinterpret_cast< ushort* >(byte_array->data());

            for (int i = 0; i < faces.size(); i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    raw_index_array[i * 3 + j] =
                        static_cast< ushort >(faces[i].vertex_index[j]);
                }
            }
        }

        else
        {
            byte_array->resize(faces.size() * 3 * sizeof(uint));

            uint* raw_index_array =
                reinterpret_cast< uint* >(byte_array->data());

            for (int i = 0; i < faces.size(); i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    raw_index_array[i * 3 + j] =
                        static_cast< uint >(faces[i].vertex_index[j]);
                }
            }
        }

        return byte_array;
    }

    Qt3DRender::QBuffer* MeshGraphics::createQBuffer(
        QByteArray* data,
        Qt3DRender::QBuffer::BufferType buffer_type,
        Qt3DRender::QGeometry* parent_geometry)
    {
        Qt3DRender::QBuffer* buffer = new Qt3DRender::QBuffer();
        buffer->setData(*data);
        buffer->setType(buffer_type);
        buffer->setParent(parent_geometry);

        return buffer;
    }

    Qt3DRender::QAttribute* MeshGraphics::createQAttribute(
        Qt3DRender::QAttribute::AttributeType attribute_type,
        Qt3DRender::QBuffer* buffer,
        uint byte_offset,
        uint byte_stride,
        uint count,
        Qt3DRender::QAttribute::VertexBaseType vertex_base_type,
        uint vertex_size,
        Qt3DRender::QGeometry* parent_geometry)
    {
        Qt3DRender::QAttribute* attribute = new Qt3DRender::QAttribute();
        attribute->setAttributeType(attribute_type);
        attribute->setBuffer(buffer);
        attribute->setVertexBaseType(vertex_base_type);
        attribute->setVertexSize(vertex_size);
        attribute->setByteOffset(byte_offset);
        attribute->setByteStride(byte_stride);
        attribute->setCount(count);
        attribute->setParent(parent_geometry);

        return attribute;
    }

    Qt3DRender::QGeometryRenderer* MeshGraphics::createQGeometryRenderer(
        int first_instance,
        int first_vertex,
        Qt3DRender::QGeometry* geometry,
        int index_offset,
        int instance_count,
        bool primitive_restart_enabled,
        Qt3DRender::QGeometryRenderer::PrimitiveType primitive_type,
        int vertex_count,
        QString objectName,
        Qt3DCore::QEntity* parent_entity)
    {
        Qt3DRender::QGeometryRenderer* geometryRenderer =
            new Qt3DRender::QGeometryRenderer();
        geometry->setParent(geometryRenderer);
        geometryRenderer->setFirstVertex(first_vertex);
        geometryRenderer->setInstanceCount(instance_count);
        geometryRenderer->setFirstInstance(first_instance);
        geometryRenderer->setVertexCount(vertex_count);
        geometryRenderer->setPrimitiveType(primitive_type);
        geometryRenderer->setIndexOffset(index_offset);
        geometryRenderer->setObjectName(objectName);
        geometryRenderer->setPrimitiveRestartEnabled(primitive_restart_enabled);
        geometryRenderer->setParent(parent_entity);
        geometryRenderer->setGeometry(geometry);

        return geometryRenderer;
    }

    Qt3DRender::QObjectPicker* MeshGraphics::createObjectPicker()
    {
        Qt3DRender::QObjectPicker* picker = new Qt3DRender::QObjectPicker();

        if (QObject::connect(picker,
                             SIGNAL(pressed(Qt3DRender::QPickEvent*)),
                             this,
                             SLOT(pickMesh(Qt3DRender::QPickEvent*))))
        {
            qDebug() << "signal successfully connected";

            return picker;
        }

        else
        {
            qDebug() << "connect() failed";

            return nullptr;
        }
    }

    void MeshGraphics::pickMesh(Qt3DRender::QPickEvent* pickEvent)
    {
        QVector3D local_pos = pickEvent->localIntersection();
        QVector3D world_pos = pickEvent->worldIntersection();

        qDebug() << "Intersected entity at: " << local_pos.x() << " "
                 << local_pos.y() << " " << local_pos.z();
        // qDebug() << "Intersected entity at: " << world_pos.x() << " " <<
        // world_pos.y() << " " << world_pos.z();
        // m_project_manager->selectMesh(m_mesh->name());
    }

    Qt3DCore::QEntity* MeshGraphics::createQEntity()
    {
        uint vertices_count = m_mesh->m_vertices.size();
        uint indices_count  = m_mesh->m_faces.size() * 3;

        QByteArray* vertex_buffer_data = createVertexByteArray();
        QByteArray* index_buffer_data  = createIndexByteArray();

        Qt3DRender::QGeometry* meshGeometry = new Qt3DRender::QGeometry();

        Qt3DRender::QBuffer* vertex_buffer =
            createQBuffer(vertex_buffer_data,
                          Qt3DRender::QBuffer::VertexBuffer,
                          meshGeometry);
        Qt3DRender::QBuffer* index_buffer = createQBuffer(
            index_buffer_data, Qt3DRender::QBuffer::IndexBuffer, meshGeometry);
        Qt3DRender::QAttribute* vertexAttribute =
            createQAttribute(Qt3DRender::QAttribute::VertexAttribute,
                             vertex_buffer,
                             0,
                             3 * 2 * sizeof(float),
                             vertices_count,
                             Qt3DRender::QAttribute::Float,
                             3,
                             meshGeometry);

        /* This should only be set on the vertex position attribute. */
        vertexAttribute->setName(
            Qt3DRender::QAttribute::defaultPositionAttributeName());


        Qt3DRender::QAttribute* normalAttribute =
            createQAttribute(Qt3DRender::QAttribute::VertexAttribute,
                             vertex_buffer,
                             3 * sizeof(float),
                             3 * 2 * sizeof(float),
                             vertices_count,
                             Qt3DRender::QAttribute::Float,
                             3,
                             meshGeometry);
        normalAttribute->setName(
            Qt3DRender::QAttribute::defaultNormalAttributeName());

        /* Determine the data type and data size of the index data */
        Qt3DRender::QAttribute::VertexBaseType index_base_type;
        uint index_data_size;
        if (vertices_count < 65535)
        {
            index_data_size = sizeof(ushort);
            index_base_type =
                Qt3DRender::QAttribute::VertexBaseType::UnsignedShort;
        }

        else
        {
            index_data_size = sizeof(uint);
            index_base_type =
                Qt3DRender::QAttribute::VertexBaseType::UnsignedInt;
        }

        Qt3DRender::QAttribute* indexAttribute =
            createQAttribute(Qt3DRender::QAttribute::IndexAttribute,
                             index_buffer,
                             0,
                             index_data_size,
                             indices_count,
                             index_base_type,
                             1,
                             meshGeometry);

        meshGeometry->addAttribute(vertexAttribute);
        meshGeometry->addAttribute(indexAttribute);

        Qt3DCore::QEntity* entity = new Qt3DCore::QEntity();
        Qt3DRender::QGeometryRenderer* geometryRenderer =
            createQGeometryRenderer(0,
                                    0,
                                    meshGeometry,
                                    0,
                                    1,
                                    true,
                                    Qt3DRender::QGeometryRenderer::Triangles,
                                    indexAttribute->count(),
                                    m_mesh->name() + "component",
                                    entity);

        entity->addComponent(createObjectPicker());
        meshGeometry->setParent(geometryRenderer);
        entity->addComponent(geometryRenderer);
        entity->addComponent(m_material);
        // m_transform->setScale(m_mesh->m_unit());
        entity->addComponent(m_transform);

        return entity;
    }

    MeshGraphics::MeshGraphics(Mesh* mesh)
        : m_mesh(mesh)
        , m_material(new Qt3DExtras::QPhongMaterial)
        , m_transform(new Qt3DCore::QTransform)
        , m_project_manager(ProjectManager::getInstance())
    {
        m_entity = createQEntity();
        m_entity->setObjectName(m_mesh->name());
        m_material->setAmbient(QColor(Qt::darkRed));
    }

    void MeshGraphics::setParentEntity(Qt3DCore::QEntity* parent)
    {
        m_entity->setParent(parent);
    }

}  // namespace ORNL
