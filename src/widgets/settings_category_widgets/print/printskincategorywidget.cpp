#include "widgets/settings_category_widgets/print/printskincategorywidget.h"

#include "ui_print_skin_category_widget.h"

namespace ORNL
{
    PrintSkinCategoryWidget::PrintSkinCategoryWidget(QWidget* parent)
        : ui(new Ui::PrintSkinCategoryWidget)
    {
        ui->setupUi(this);

        // Setup use
        ui->use->label(Constants::PrintSettings::Skin::Labels::kUse);
        ui->use->tooltip(Constants::PrintSettings::Skin::Tooltips::kUse);
        m_checkbox[Constants::PrintSettings::Skin::kUse] =
            QSharedPointer< TemplateCheckboxSettingItem >(ui->use);

        // Setup line overlap
        ui->line_overlap->label(
            Constants::PrintSettings::Skin::Labels::kLineOverlap);
        ui->line_overlap->tooltip(
            Constants::PrintSettings::Skin::Tooltips::kLineOverlap);
        m_percent[Constants::PrintSettings::Skin::kLineOverlap] =
            QSharedPointer< TemplatePercentSettingItem >(ui->line_overlap);

        // Setup pattern
        ui->pattern->label(Constants::PrintSettings::Skin::Labels::kPattern);
        ui->pattern->tooltip(
            Constants::PrintSettings::Skin::Tooltips::kPattern);
        m_enum[Constants::PrintSettings::Skin::kPattern] =
            QSharedPointer< TemplateDropdownSettingItem >(ui->pattern);

        // Setup perimeter overlap
        ui->overlap->label(Constants::PrintSettings::Skin::Labels::kOverlap);
        ui->overlap->tooltip(
            Constants::PrintSettings::Skin::Tooltips::kOverlap);
        m_distance[Constants::PrintSettings::Skin::kOverlap] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->overlap);
    }

    PrintSkinCategoryWidget::~PrintSkinCategoryWidget()
    {
        delete ui;
    }

    void PrintSkinCategoryWidget::handleConditions()
    {
        // None of these settings are affected by other settings
    }
}  // namespace ORNL
