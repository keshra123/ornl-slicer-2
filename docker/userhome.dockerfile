FROM robobenklein/home:latest

# running as uid/901

RUN sudo add-apt-repository -y ppa:beineri/opt-qt-5.10.1-xenial
RUN sudo apt-get update
RUN sudo apt-get -yq --no-install-recommends --no-install-suggests --force-yes install qt510-meta-full

RUN sudo install_clean \
 zlib1g-dev libassimp-dev mesa-common-dev

COPY --chown=901:901 . /home/${LUSER}/code/ORNL-Slicer-2

WORKDIR /home/${LUSER}/code/ORNL-Slicer-2

