#ifndef COMMONPARSER_H
#define COMMONPARSER_H

//! \file commonparser.h

#include <QScopedPointer>
#include <QVector>

#include "gcode/gcode_command.h"
#include "geometry/arc.h"
#include "geometry/line.h"
#include "geometry/path_segment.h"
#include "parser_base.h"


// NOTE: May need to change this to be named parserbase and parserbase
//       to parsercore for clarity.
namespace ORNL
{
    // TODO: Need to calculate the acceleration based on movement/feedrate (if
    // feedrate changes)
    /*!
     * \class CommonParser
     * \brief This class implements a common parsing configuration for 3D
     * printers. This configuration can be changed and overwritten by its
     * subclasses by overriding the mapping between. command string and
     * associated funciton handler. \note The current commands that this class
     * implements are:
     *          - GCommands:
     *              - G0, G1, G2, G3, G4
     *          - MCommands:
     *              - None
     *          - Line Comment Delimiter:
     *              - ;
     *          - Block Comment Delimiter:
     *              - None
     *
     */
    class CommonParser : public ParserBase
    {
    public:
        //! \brief Default Constructor that uses predefined units.
        //! \note The predefined units are:
        //!         - Distance: mm
        //!         - Time: s
        //!         - Mass : g
        //!         - Angle: rad
        //!         - Velocity: mm/s
        //!         - Acceleration mm/s^2
        CommonParser();

        //! \breif Constructor that allows for unit type selection
        //! \param distance_unit Unit assumed when no unit is provided for
        //! distance calculations. \param time_unit Unit assumed when no unit is
        //! provided for time calculations. \param mass_unit Unit assumed when
        //! no unit is provided for mass calculations. \param angle_unit Unit
        //! assumed when no unit is provided for angle calculations. \param
        //! velocity_unit Unit assumed when no unit is provided for velocity
        //! calculations. \param acceleration_unit Unit assumed when no unit
        //! provided for acceleration calculations.
        CommonParser(Distance distance_unit,
                     Time time_unit,
                     Mass mass_unit,
                     Angle angle_unit,
                     Velocity velocity_unit,
                     Acceleration acceleration_unit);


        //! \brief Gets a copy of the current GcodeCommand.
        //! \return Copy of the current GcodeCommand.
        GcodeCommand getCurrentCommand();

        // ---- Regular Commands ----

        //! \brief Base configuration that contains all common configuration
        //! commands
        //!        for each machine.
        //!        The current commands that this function implements are:
        //!             - GCommands:
        //!                     - G0, G1, G2, G3, G4
        //!             - MCommands:
        //!                     - None
        //!             - Line Comment Delimiter:
        //!                     - ;
        //!             - Block Comment Delimiter:
        //!                     - None
        //! \note This command clears all previous commands that were
        //! configured. \note This function should be overridden and called from
        //! a subclass to allow
        //!       a single command to enable all common commands and the
        //!       specific implementations that each machine needs.
        virtual void config();


        //! \brief Returns the current X position of the extruder.
        //! \return The number type converted to the internal distance unit.
        NT getXPos() const;

        //! \brief Returns the current X position of the extruder.
        //! \param distance_unit Unit of distance to convert the dextruder X
        //! position to. \return The number type converted to the passed
        //! distance unit.
        NT getXPos(Distance distance_unit) const;

        //! \brief Returns the current Y position of the extruder.
        //! \return The number type converted to the internal distance unit.
        NT getYPos() const;

        //! \brief Returns the current Y position of the extruder.
        //! \param distance_unit Unit of distance to convert the extruder Y
        //! position to. \return The number type converted to the passed
        //! distance unit.
        NT getYPos(Distance distance_unit) const;

        //! \brief Returns the current Z position of the extruder.
        //! \return The number type converted to the internal distance unit.
        NT getZPos() const;

        //! \brief Returns the current Z position of the extruder.
        //! \param distance_unit Unit of distance to convert the extuder Z
        //! position to. \return The number type converted to the passed
        //! distance unit.
        NT getZPos(Distance distance_unit) const;

        //! \brief Returns the current W position of the table..
        //! \return The number type converted to the internal distance unit.
        NT getWPos() const;

        //! \brief Returns the current W position of the table.
        //! \param distance_unit Unit of distance to convert the table position
        //! to. \return The number type converted to the passed distance unit.
        NT getWPos(Distance distance_unit) const;

        //! \brief Returns the current X position for the center of the arc.
        //! \return The number type converted to the internal distance unit.
        NT getArcXPos() const;

        //! \brief Returns the current X position for the center of the arc.
        //! \param distance_unit Unit of distance to convert the X-axis arc
        //! center position to. \return The number type converted to the passed
        //! distance unit.
        NT getArcXPos(Distance distance_unit) const;

        //! \brief Returns the current y position for the center of the arc.
        //! \return The number type converted to the internal distance unit.
        NT getArcYPos() const;

        //! \brief Returns the current y position for the center of the arc.
        //! \param distance_unit Unit of distance to convert the Y-axis arc
        //! center position to. \return The number type converted to the passed
        //! distance unit.
        NT getArcYPos(Distance distance_unit) const;

        //! \brief Returns the current speed of the extruder.
        //! \return The number type converted to the internal distance/time
        //! unit.
        NT getSpeed() const;

        //! \brief Returns the current speed of the extruder.
        //! \param velocity_unit Unit of velocity to convert the extruder speed
        //! to. \return The number type converted to the passed velocity unit.
        NT getSpeed(Velocity velocity_unit) const;

        //! \brief Returns the current angular velocity of the spindle.
        //! \return The number type converted to rev/min.
        NT getSpindleSpeed() const;

        //! \brief Returns the current angular velocity of the spindle.
        //! \param angular_velocity_unit Unit of angular velocity to convert the
        //! spindle speed to. \return The number type converted to the passed
        //! angular velocity unit.
        NT getSpindleSpeed(AngularVelocity angular_velocity_unit) const;

        //! \brief Returns the current acceleration of the extruder.
        //! \return The number type converted to the internal
        //! distance_unit/time_unit/time unit.
        NT getAcceleration() const;

        //! \brief Returns the current acceleration of the extruder.
        //! \param acceleration_unit Unit of acceleration to convert the
        //! acceleration to. \return The number type converted to the passed
        //! acceleration unit.
        NT getAcceleration(Acceleration acceleration_unit) const;

        //! \brief Returns the current sleep time of the extruder.
        //! \return The number type converted to the internal time unit.
        NT getSleepTime() const;

        //! \brief Returns the current sleep time of the extruder.
        //! \param time_unit Unit of time to convert the sleep time to.
        //! \return The number type converted to the defined time unit.
        NT getSleepTime(Time time_unit) const;

        //! \brief Completely restes the internal state of this parser. This
        //! includes all command mappings,
        //!        and position values. This does NOT include resetting units.
        void reset();

        //! \brief Returns the parser identifier string.
        //! \return The Parser Identifier String.
        const QString& getParserID() const;

    protected:
        //! \brief Sets the X position of the extruder.
        //! \param value Value to set the X position to.
        //! \note This value will be set using the defined unit type.
        void setXPos(NT value);

        //! \brief Sets the Y position of the extruder.
        //! \param value Value to set the Y position to.
        //! \note This value will be set using the defined unit type.
        void setYPos(NT value);

        //! \brief Sets the Z position of the extruder.
        //! \param value Value to set the Z position to.
        //! \note This value will be set using the defined unit type.
        void setZPos(NT value);

        //! \brief Sets the W position of the extruder.
        //! \param value Value to set the W position to.
        //! \note This value will be set using the defined unit type.
        void setWPos(NT value);

        //! \brief Sets the center arc position on the X axis.
        //! \param value Value to set the X arc center position to.
        //! \note This value will be set using the defined unit type.
        void setArcXPos(NT value);

        //! \brief Sets the center arc position on the Y axis.
        //! \param value Value to set the Y arc center position to.
        //! \note This value will be set using the defined unit type.
        void setArcYPos(NT value);

        //! \brief Sets the center arc position on the Z axis.
        //! \param value Value to set the Z arc center position to.
        //! \note This value will be set using the defined unit type.
        void setArcZPos(NT value);

        //! \brief Sets the speed of the extruder.
        //! \param value Value to set the speed to.
        //! \note This value will be set using the defined unit type.
        void setSpeed(NT value);

        //! \brief Sets the spindle speed of the extruder.
        //! \param value Value to set the spindle speed to.
        //! \note This value will be set using the defined unit type.
        void setSpindleSpeed(NT value);

        //! \brief Sets the acceleration of the extruder.
        //! \param value Value to set the accleration to.
        //! \note This value will be set using the defined unit types.
        void setAcceleration(NT value);

        //! \brief Sets the sleep time of the extruder.
        //! \param value Value to set the sleep time to.
        //! \note This value will be set using the defined unit type.
        void setSleepTime(NT value);

        //! \brief Function handler for the 'G0' Gcode command for rapid linear
        //! movement. This function handler
        //!        accepts the following parameters:
        //!             - X: Changes the X position of the extruder.
        //!             - Y: Changes the Y position of the extruder.
        //!             - Z: Changes the Z position of the extruder.
        //!        There must exist one of the X, Y, or Z command.
        //! \param params List of parameters that have been split up to be
        //! parsed. \throws IllegalParameterException This occurs during either
        //! a conversion error, a parameter missing a value,
        //!                                   a duplicate parameter, if a
        //!                                   required parameter is not passed,
        //!                                   or an illegal parameter passed.
        virtual void G0Handler(QStringList& params);

        //! \brief Function handler for the 'G1' Gcode command for linear
        //! movement. This function handler
        //!        accepts the following parameters:
        //!             - X: Changes the X position of the extruder.
        //!             - Y: Changes the Y position of the extruder.
        //!             - Z: Changes the Z position of the extruder.
        //!             - F: Changes the flow rate of the extruder.
        //!        There must exist one of the X, Y, or Z command. The F command
        //!        is optional.
        //! \param params List of parameters that have been split up to be
        //! parsed. \throws IllegalParameterException This occurs during either
        //! a conversion error, a parameter missing a value,
        //!                                   a duplicate parameter, if a
        //!                                   required parameter is not passed,
        //!                                   or an illegal parameter passed.
        virtual void G1Handler(QStringList& params);

        //! \brief Function handler for the 'G2' Gcode command for clockwise arc
        //! movement. This function handler
        //!        accepts the following parameters:
        //!             - X: Changes the X position of the extruder.
        //!             - Y: Changes the Y position of the extruder.
        //!             - I: Changes the X arc center position based off the
        //!             previous X position.
        //!             - I: Changes the Y arc center position based off the
        //!             previous Y position.
        //!             - F: Changes the flow rate of the extruder.
        //!        All X, Y, I, and J commands must exist or an exception is
        //!        thrown. The F command is optional.
        //! \param params List of parameters that have been split up to be
        //! parsed. \throws IllegalParameterException This occurs during either
        //! a conversion error, a parameter missing a value,
        //!                                   a duplicate parameter, if a
        //!                                   required parameter is not passed,
        //!                                   or an illegal parameter passed.
        virtual void G2Handler(QStringList& params);

        //! \brief Function handler for the 'G3' Gcode command for counter
        //! clockwise arc movement. This function handler
        //!        accepts the following parameters:
        //!             - X: Changes the X position of the extruder.
        //!             - Y: Changes the Y position of the extruder.
        //!             - I: Changes the X arc center position based off the
        //!             previous X position.
        //!             - I: Changes the Y arc center position based off the
        //!             previous Y position.
        //!             - F: Changes the flow rate of the extruder.
        //!        All X, Y, I, and J commands must exist or an exception is
        //!        thrown. The F command is optional.
        //! \param params List of parameters that have been split up to be
        //! parsed. \throws IllegalParameterException This occurs during either
        //! a conversion error, a parameter missing a value,
        //!                                   a duplicate parameter, if a
        //!                                   required parameter is not passed,
        //!                                   or an illegal parameter passed.
        virtual void G3Handler(QStringList& params);

        //! \brief Function handler for the 'G4' command for setting the sleep
        //! time of the extruder. This function handler
        //!        accepts the following parameters:
        //!             - L: Changes the amount of time to sleep.
        //!        The L command must exist or an exception is thrown.
        //! \param params List of parameters that have been split up to be
        //! parsed. \throws IllegalParameterException This occurs during either
        //! a conversion error, a parameter missing a value,
        //!                                   a duplicate parameter, if a
        //!                                   required parameter is not passed,
        //!                                   or an illegal parameter passed.
        virtual void G4Handler(QStringList& params);

        //! \brief Function handler to use for parsing a GCode command that this
        //! parser can ignore.
        void IGNORE();

        //! \breif Function handler to use when parsing a valid Gcode command
        //! the provides parameters.
        void IGNORE_WITH_PARAMS(QStringList& params);

        //! \brief Helper function that throws an IllegalParameterException when
        //! multiple of the same parameter are encountered. \param parameter
        //! Duplicate Parameter id. \throws IllegalParameterException
        void throwMultipleParameterException(char parameter);

        //! \brief Helper function that throws an IllegalParameterException when
        //! a float conversion occurs. \throws IllegalParameterException
        void throwFloatConversionErrorException();

        //! \brief Helper function that throws an IllegalParameterException when
        //! an integer conversion occurs. \throws IllegalParameterException
        void throwIntegerConversionErrorException();

        //! \brief String representation of the parser being used Used by
        //! subclasses to determine which parser is being used.
        QString m_parser_id;

        bool m_pump_ON;                  // true if on, false if off.
        bool m_dynamic_spindle_control;  // true if on, false if off.
        bool m_park;                     // true if parking, false if not.

        // Purging variables
        bool m_return_to_prev_location;
        Time m_purge_time, m_wait_to_wipe_time, m_wait_time_to_start_purge;

    private:
        Distance m_current_x;
        Distance m_current_y;
        Distance m_current_z;
        Distance m_current_w;

        Distance m_current_arc_center_x;
        Distance m_current_arc_center_y;
        Distance m_current_arc_center_z;

        Velocity m_current_speed;

        AngularVelocity m_current_spindle_speed;

        Acceleration m_current_acceleration;

        Time m_sleep_time;

        Distance m_distance_unit;
        Time m_time_unit;
        Angle m_angle_unit;
        Mass m_mass_unit;
        Velocity m_velocity_unit;
        Acceleration m_acceleration_unit;


    };  // class CommonParser

}  // namespace ORNL
#endif  // COMMONPARSER_H
