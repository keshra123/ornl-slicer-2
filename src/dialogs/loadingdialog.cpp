#include "dialogs/loadingdialog.h"

#include "ui_loading_dialog.h"
namespace ORNL
{
    LoadingDialog::LoadingDialog(QString filename, QWidget* parent)
        : QDialog(parent)
        , ui(new Ui::LoadingDialog)
    {
        ui->setupUi(this);
        setWindowTitle(filename);
    }

    LoadingDialog::~LoadingDialog()
    {
        delete ui;
    }
}  // namespace ORNL
