#ifndef PRINTSKINCATEGORYWIDGET_H
#define PRINTSKINCATEGORYWIDGET_H

#include <QWidget>

#include "utilities/constants.h"
#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class PrintSkinCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class PrintSkinCategoryWidget
     *
     * \brief The category widget for the top skin path type
     *
     * A skin is a type of infill that can be seen from the top or bottom of the
     * part. This region is solid for a few layers.
     */
    class PrintSkinCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        //! \brief default constructor
        explicit PrintSkinCategoryWidget(QWidget* parent = 0);

        //! \brief destructor
        ~PrintSkinCategoryWidget();

        //! \brief handles conditionals
        void handleConditions();

    private:
        Ui::PrintSkinCategoryWidget* ui;
    };  // class PrintSkinCategoryWidget
}  // namespace ORNL
#endif  // PRINTSKINCATEGORYWIDGET_H
