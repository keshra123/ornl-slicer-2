#include "geometry/path.h"

#include "utilities/constants.h"
#ifdef QT_DEBUG
#    include <QPainter>
#endif
namespace ORNL
{
    Path::Path(Region* region)
        : SettingsBase(region)
        , m_parent(region)
    {
#ifdef QT_DEBUG

        // outline pen width should the polygon pen width + 2 * the outline pen
        // width drawing should always be done in the order outline, polygon,
        // center line

        m_centerline_pen.setStyle(Qt::DashLine);
        m_centerline_pen.setCapStyle(Qt::RoundCap);
        m_centerline_pen.setJoinStyle(Qt::RoundJoin);
        m_centerline_pen.setWidth(1);

        m_outline_pen.setCapStyle(Qt::RoundCap);
        m_outline_pen.setJoinStyle(Qt::RoundJoin);
        m_outline_pen.setWidth(7);

        m_path_pen.setCapStyle(Qt::RoundCap);
        m_path_pen.setJoinStyle(Qt::RoundJoin);
        m_path_pen.setWidth(5);

        switch (m_type)
        {
            case RegionType::kPerimeter:
                m_path_pen.setColor(Constants::Colors::kPerimeter);
                break;
            case RegionType::kInset:
                m_path_pen.setColor(Constants::Colors::kInset);
                break;
            case RegionType::kInfill:
                m_path_pen.setColor(Constants::Colors::kInfill);
                break;
            case RegionType::kSkin:
                m_path_pen.setColor(Constants::Colors::kSkin);
                break;
        }

        for (PathSegment ps : (*this))
        {
            if (m_modifier_pens.contains(ps.modifier()))
            {
                continue;
            }
            m_modifier_pens[ps.modifier()] = QPen();
            m_modifier_pens[ps.modifier()].setCapStyle(Qt::RoundCap);
            m_modifier_pens[ps.modifier()].setJoinStyle(Qt::RoundJoin);
            m_modifier_pens[ps.modifier()].setWidth(5);

            switch (ps.modifier())
            {
                case PathModifiers::kReverseTipWipe:
                case PathModifiers::kForwardTipWipe:
                case PathModifiers::kPerimeterTipWipe:
                    m_modifier_pens[ps.modifier()].setColor(
                        Constants::Colors::kTipWipe);
                    break;
                case PathModifiers::kInitialStartup:
                    m_modifier_pens[PathModifiers::kInitialStartup].setColor(
                        Constants::Colors::kInitialStartup);
                    break;
                case PathModifiers::kSlowDown:
                    m_modifier_pens[PathModifiers::kSlowDown].setColor(
                        Constants::Colors::kSlowDown);
                    break;
                case PathModifiers::kCoasting:
                    m_modifier_pens[PathModifiers::kCoasting].setColor(
                        Constants::Colors::kCoasting);
                    break;
                case PathModifiers::kPrestart:
                    m_modifier_pens[PathModifiers::kPrestart].setColor(
                        Constants::Colors::kPrestart);
                    break;
            }
        }

#endif
    }

    Path::Path(const Path& path)
        : m_parent(path.m_parent)
        , SettingsBase(path.m_parent)
    {
        for (PathSegment ps : path)
        {
            append(ps);
        }
#ifdef QT_DEBUG
        m_outline_pen    = path.m_outline_pen;
        m_centerline_pen = path.m_centerline_pen;
        m_path_pen       = path.m_path_pen;
        m_modifier_pens  = path.m_modifier_pens;
#endif
    }

    void Path::add(PathSegment ps)
    {
        append(ps);
    }

    Path Path::operator+(const PathSegment& ps)
    {
        Path p(*this);
        return p += ps;
    }

    Path Path::operator+(const PathSegment& ps) const
    {
        Path p(*this);
        return p += ps;
    }

    Path Path::operator+=(const PathSegment& ps)
    {
        append(ps);
        return (*this);
    }

    RegionType Path::type()
    {
        return m_type;
    }

    bool Path::closed()
    {
        if (last().isLine())
        {
            if (first().isLine())
            {
                return last().line().end() == first().line().start();
            }
            else
            {
                return last().line().end() == first().arc().start();
            }
        }
        else
        {
            if (first().isLine())
            {
                return last().arc().end() == first().line().start();
            }
            else
            {
                return last().arc().end() == first().arc().start();
            }
        }
    }

    QString Path::gcode(WriterBase* syntax)
    {
        QString gcode;
        for (PathSegment ps : (*this))
        {
            gcode += ps.gcode(syntax);
        }
        return gcode;
    }

    Path Path::createPath(Region* region,
                          RegionType path_type,
                          Polygon& polygon)
    {
        // TODO
        return Path(region);
    }

    Path Path::createPath(Region* region,
                          RegionType path_type,
                          Polyline& polyline)
    {
        return Path(region);
    }

    QVector< Path > Path::createPaths(Region* region,
                                      RegionType path_type,
                                      PolygonList& polygons)
    {
        // TODO
        return QVector< Path >();
    }

    QVector< Path > Path::createPaths(Region* region,
                                      RegionType path_type,
                                      QVector< PolygonList >& polygons)
    {
        return QVector< Path >();
    }

    QVector< Path > Path::createPaths(Region* region,
                                      RegionType path_type,
                                      QVector< Polyline >& polylines)
    {
        return QVector< Path >();
    }

#ifdef QT_DEBUG
    void Path::paintDebug(QPainter* painter,
                          Distance& ratio,
                          int vertical_offset)
    {
        QPainterPath painter_path;
        QMap< PathModifiers, QPainterPath > m_modifier_paths;
        PathModifiers previous = PathModifiers::kNone;
        Point p;
        double x, y;
        for (int i = 0; i < size(); i++)
        {
            PathSegment path_segment = operator[](i);
            if (!i)
            {
                p = path_segment.isLine() ? path_segment.line().start()
                                          : path_segment.arc().start();
                x = p.x() * ratio();
                y = p.y() * ratio() + vertical_offset;
                painter_path.moveTo(x, y);

                previous = path_segment.modifier();

                if (m_modifier_pens.contains(previous))
                {
                    m_modifier_paths[previous] = QPainterPath();
                    m_modifier_paths[previous].moveTo(x, y);
                }
            }


            p = path_segment.isLine() ? path_segment.line().start()
                                      : path_segment.arc().end();
            x = p.x() * ratio();
            y = p.y() * ratio() + vertical_offset;
            if (path_segment.isLine())
            {
                painter_path.lineTo(x, y);
            }
            else
            {
                // TODO: arcTo - http://doc.qt.io/qt-5/qpainterpath.html#arcTo
            }

            // If new modifier switch
            if (previous != path_segment.modifier())
            {
                previous = path_segment.modifier();
                if (m_modifier_pens.contains(previous))
                {
                    m_modifier_paths[previous] = QPainterPath();
                    m_modifier_paths[previous].moveTo(x, y);
                }
            }

            if (m_modifier_pens.contains(previous))
            {
                if (path_segment.isLine())
                {
                    m_modifier_paths[previous].lineTo(x, y);
                }
                else
                {
                    // TODO: arcTo -
                    // http://doc.qt.io/qt-5/qpainterpath.html#arcTo
                }
            }
        }
        painter->setPen(m_outline_pen);
        painter->drawPath(painter_path);
        painter->setPen(m_path_pen);
        painter->drawPath(painter_path);
        for (PathModifiers key : m_modifier_pens.keys())
        {
            painter->setPen(m_modifier_pens[key]);
            painter->drawPath(m_modifier_paths[key]);
        }
        painter->setPen(m_centerline_pen);
        painter->drawPath(painter_path);
    }
#endif
}  // namespace ORNL
