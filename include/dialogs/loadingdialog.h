#ifndef LOADINGSTLDIALOG_H
#define LOADINGSTLDIALOG_H

//! \file loadingdialog.h

#include <QDialog>

namespace Ui
{
    class LoadingDialog;
}


namespace ORNL
{
    /*!
     * \class LoadingDialog
     * \brief Dialog Window when load
     */
    class LoadingDialog : public QDialog
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit LoadingDialog(QString filename, QWidget* parent = 0);

        //! \brief Destructor
        ~LoadingDialog();

    private:
        Ui::LoadingDialog* ui;
    };  // class LoadingDialog
}  // namespace ORNL
#endif  // LOADINGSTLDIALOG_H
