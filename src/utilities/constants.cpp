#include "utilities/constants.h"

#include "units/unit.h"
namespace ORNL
{
    QString Constants::VERSION = "1.0.0";

    //================================================================================
    // Units
    //================================================================================
    const QString Constants::Units::kInch        = "Inch";
    const QString Constants::Units::kInchPerSec  = "Inch/Sec";
    const QString Constants::Units::kInchPerSec2 = "Inch/Sec<sup>2</sup>";
    const QString Constants::Units::kInchPerSec3 = "Inch/Sec<sup>3</sup>";

    const QString Constants::Units::kFeet        = "Feet";
    const QString Constants::Units::kFeetPerSec  = "Feet/Sec";
    const QString Constants::Units::kFeetPerSec2 = "Feet/Sec<sup>2</sup>";
    const QString Constants::Units::kFeetPerSec3 = "Feet/Sec<sup>3</sup>";

    const QString Constants::Units::kMm        = "MM";
    const QString Constants::Units::kMmPerSec  = "MM/Sec";
    const QString Constants::Units::kMmPerSec2 = "MM/Sec<sup>2</sup>";
    const QString Constants::Units::kMmPerSec3 = "MM/Sec<sup>3</sup>";

    const QString Constants::Units::kCm        = "CM";
    const QString Constants::Units::kCmPerSec  = "CM/Sec";
    const QString Constants::Units::kCmPerSec2 = "CM/Sec<sup>2</sup>";
    const QString Constants::Units::kCmPerSec3 = "CM/Sec<sup>3</sup>";

    const QString Constants::Units::kMicron        = "Micron";
    const QString Constants::Units::kMicronPerSec  = "Micron/Sec";
    const QString Constants::Units::kMicronPerSec2 = "Micron/Sec<sup>2</sup>";
    const QString Constants::Units::kMicronPerSec3 = "Micron/Sec<sup>3</sup>";

    const QString Constants::Units::kDegree     = "Degree";
    const QString Constants::Units::kRadian     = "Radian";
    const QString Constants::Units::kRevolution = "Rev";

    const QString Constants::Units::kSecond      = "Second";
    const QString Constants::Units::kMillisecond = "Millisecond";
    const QString Constants::Units::kMinute      = "Minute";

    const QString Constants::Units::kKg = "Kilogram";
    const QString Constants::Units::kG  = "Gram";
    const QString Constants::Units::kMg = "Milligram";

    const QStringList Constants::Units::kDistanceUnits = {
        Constants::Units::kInch,
        Constants::Units::kFeet,
        Constants::Units::kMm,
        Constants::Units::kCm,
        Constants::Units::kMicron};

    const QStringList Constants::Units::kVelocityUnits = {
        Constants::Units::kInchPerSec,
        Constants::Units::kFeetPerSec,
        Constants::Units::kMmPerSec,
        Constants::Units::kCmPerSec,
        Constants::Units::kMicronPerSec};

    const QStringList Constants::Units::kAccelerationUnits = {
        Constants::Units::kInchPerSec2,
        Constants::Units::kFeetPerSec2,
        Constants::Units::kMmPerSec2,
        Constants::Units::kCmPerSec2,
        Constants::Units::kMicronPerSec2};

    const QStringList Constants::Units::kJerkUnits = {
        Constants::Units::kInchPerSec3,
        Constants::Units::kFeetPerSec3,
        Constants::Units::kMmPerSec3,
        Constants::Units::kCmPerSec3,
        Constants::Units::kMicronPerSec3};

    const QStringList Constants::Units::kAngleUnits = {
        Constants::Units::kDegree,
        Constants::Units::kRadian,
        Constants::Units::kRevolution};

    const QStringList Constants::Units::kTimeUnits = {
        Constants::Units::kSecond,
        Constants::Units::kMillisecond,
        Constants::Units::kMinute};

    //================================================================================
    // Region Type Strings
    //================================================================================
    const QString Constants::RegionTypeStrings::kUnknown     = "unknown";
    const QString Constants::RegionTypeStrings::kPerimeter   = "perimeter";
    const QString Constants::RegionTypeStrings::kInset       = "inset";
    const QString Constants::RegionTypeStrings::kInfill      = "infill";
    const QString Constants::RegionTypeStrings::kTopSkin     = "top_skin";
    const QString Constants::RegionTypeStrings::kBottomSkin  = "bottom_skin";
    const QString Constants::RegionTypeStrings::kSkin        = "skin";
    const QString Constants::RegionTypeStrings::kSupport     = "support";
    const QString Constants::RegionTypeStrings::kSupportRoof = "support_roof";
    const QString Constants::RegionTypeStrings::kTravel      = "travel";

    //================================================================================
    // Legacy Region Type Strings
    //================================================================================
    const QString Constants::LegacyRegionTypeStrings::kThing = "";


    //================================================================================
    // Path Modifer Strings
    //================================================================================
    const QString Constants::PathModifierStrings::kPrestart = "prestart";
    const QString Constants::PathModifierStrings::kInitialStartup =
        "inital-startup";
    const QString Constants::PathModifierStrings::kSlowDown   = "SLOW-DOWN";
    const QString Constants::PathModifierStrings::kTipWipe    = "Tip Wipe";
    const QString Constants::PathModifierStrings::kCoasting   = "coasting";
    const QString Constants::PathModifierStrings::kSpiralLift = "Spiral Lift";

    //================================================================================
    // Machine Settings
    //================================================================================

    // Categories
    const QString Constants::MachineSettings::kDimensionsCategory =
        "Dimensions";
    const QString Constants::MachineSettings::kSyntaxCategory = "Syntax";
    const QStringList Constants::MachineSettings::kCategories = {
        Constants::MachineSettings::kDimensionsCategory,
        Constants::MachineSettings::kSyntaxCategory};

    // Dimensions
    // Keys
    const QString Constants::MachineSettings::Dimensions::kXMin =
        "dimensions_x_min";
    const QString Constants::MachineSettings::Dimensions::kXMax =
        "dimensions_x_max";
    const QString Constants::MachineSettings::Dimensions::kYMin =
        "dimensions_y_min";
    const QString Constants::MachineSettings::Dimensions::kYMax =
        "dimensions_y_max";
    const QString Constants::MachineSettings::Dimensions::kZMin =
        "dimensions_z_min";
    const QString Constants::MachineSettings::Dimensions::kZMax =
        "dimensions_z_max";
    const QString Constants::MachineSettings::Dimensions::kEnableW =
        "dimensions_enable_w";
    const QString Constants::MachineSettings::Dimensions::kWMin =
        "dimensions_w_min";
    const QString Constants::MachineSettings::Dimensions::kWMax =
        "dimensions_w_max";
    const QString Constants::MachineSettings::Dimensions::kLayerChangeAxis =
        "dimensions_layer_change_axis";
    const QString Constants::MachineSettings::Dimensions::kZOffset =
        "dimensions_z_offset";
    const QString Constants::MachineSettings::Dimensions::kEnableDoffing =
        "dimensions_enable_doffing";
    const QString Constants::MachineSettings::Dimensions::kDoffingHeight =
        "dimensions_doffing_location";


    // Dimensions Labels
    const QString Constants::MachineSettings::Dimensions::Labels::kXMin =
        "Minimum X:";
    const QString Constants::MachineSettings::Dimensions::Labels::kXMax =
        "Maximum X:";
    const QString Constants::MachineSettings::Dimensions::Labels::kYMin =
        "Minimum Y:";
    const QString Constants::MachineSettings::Dimensions::Labels::kYMax =
        "Maximum Y:";
    const QString Constants::MachineSettings::Dimensions::Labels::kZMin =
        "Minimum Z:";
    const QString Constants::MachineSettings::Dimensions::Labels::kZMax =
        "Maximum Z:";
    const QString Constants::MachineSettings::Dimensions::Labels::kEnableW =
        "Enable W Axis:";
    const QString Constants::MachineSettings::Dimensions::Labels::kWMin =
        "Minimum W:";
    const QString Constants::MachineSettings::Dimensions::Labels::kWMax =
        "Maximum W:";
    const QString
        Constants::MachineSettings::Dimensions::Labels::kLayerChangeAxis =
            "Layer Change Axis:";
    const QString Constants::MachineSettings::Dimensions::Labels::kZOffset =
        "Z Offset:";
    const QString
        Constants::MachineSettings::Dimensions::Labels::kEnableDoffing =
            "Enable Doffing Station:";
    const QString
        Constants::MachineSettings::Dimensions::Labels::kDoffingHeight =
            "Doffing Station Height:";


    // Dimensions Tooltips
    const QString Constants::MachineSettings::Dimensions::Tooltips::kXMin =
        "Minimum X axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kXMax =
        "Maximum X axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kYMin =
        "Minimum Y axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kYMax =
        "Maximum Y axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kZMin =
        "Minimum Z axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kZMax =
        "Maximum Z axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kEnableW =
        "If selected, W axis will be enabled for Z motion";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kWMin =
        "Minimum W axis value";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kWMax =
        "Maximum W axis value";
    const QString
        Constants::MachineSettings::Dimensions::Tooltips::kLayerChangeAxis =
            "What type of Z axis movement is used to change layers";
    const QString Constants::MachineSettings::Dimensions::Tooltips::kZOffset =
        "Height of the Z axis where the nozzle touches the build surface "
        "(table at maximum value)";
    const QString
        Constants::MachineSettings::Dimensions::Tooltips::kEnableDoffing =
            "If selected, W Table will lower to a specific height at the end "
            "of print";
    const QString
        Constants::MachineSettings::Dimensions::Tooltips::kDoffingHeight =
            "Height for the W Table when doffing";


    // Syntax Keys
    const QString Constants::MachineSettings::Syntax::kMachineSyntax =
        "machine_syntax";

    // Gcode Syntax Options
    QString Constants::MachineSettings::Syntax::kCincinnati = "Cincinnati";
    QString Constants::MachineSettings::Syntax::kCincinnatiLegacy =
        "Cincinnati-BERTHA";
    QString Constants::MachineSettings::Syntax::kBlueGantry = "Blue Gantry";
    QString Constants::MachineSettings::Syntax::kWolf       = "Wolf";
    QString Constants::MachineSettings::Syntax::kMach3      = "Mach3";
    QString Constants::MachineSettings::Syntax::kIngersoll  = "Ingersoll";
    QStringList Constants::MachineSettings::Syntax::kSyntaxOptions = {
        Constants::MachineSettings::Syntax::kCincinnati,
        Constants::MachineSettings::Syntax::kBlueGantry,
        Constants::MachineSettings::Syntax::kWolf,
        Constants::MachineSettings::Syntax::kMach3,
        Constants::MachineSettings::Syntax::kIngersoll};

    // Syntax Labels
    const QString Constants::MachineSettings::Syntax::Labels::kMachineSyntax =
        "Syntax";

    // Syntax Tooltips
    const QString Constants::MachineSettings::Syntax::Tooltips::kMachineSyntax =
        "The type of gcode used by the machine (usually specified by machine "
        "vendor)";


    const Distance
        Constants::MachineSettings::BlueGantry::Units::kDistanceUnit      = mm;
    const Time Constants::MachineSettings::BlueGantry::Units::kTimeUnit   = s;
    const Angle Constants::MachineSettings::BlueGantry::Units::kAngleUnit = rad;
    const Mass Constants::MachineSettings::BlueGantry::Units::kMassUnit   = g;
    const Velocity
        Constants::MachineSettings::BlueGantry::Units::kVelocityUnit = m / s;
    const Acceleration
        Constants::MachineSettings::BlueGantry::Units::kAccelerationUnit =
            m / s / s;

    const Distance
        Constants::MachineSettings::Cincinnati::Units::kDistanceUnit      = mm;
    const Time Constants::MachineSettings::Cincinnati::Units::kTimeUnit   = s;
    const Angle Constants::MachineSettings::Cincinnati::Units::kAngleUnit = rad;
    const Mass Constants::MachineSettings::Cincinnati::Units::kMassUnit   = g;
    const Velocity
        Constants::MachineSettings::Cincinnati::Units::kVelocityUnit = m / s;
    const Acceleration
        Constants::MachineSettings::Cincinnati::Units::kAccelerationUnit =
            m / s / s;

    const Distance Constants::MachineSettings::Wolf::Units::kDistanceUnit = mm;
    const Time Constants::MachineSettings::Wolf::Units::kTimeUnit         = s;
    const Angle Constants::MachineSettings::Wolf::Units::kAngleUnit       = rad;
    const Mass Constants::MachineSettings::Wolf::Units::kMassUnit         = g;
    const Velocity Constants::MachineSettings::Wolf::Units::kVelocityUnit =
        m / s;
    const Acceleration
        Constants::MachineSettings::Wolf::Units::kAccelerationUnit = m / s / s;

    const Distance Constants::MachineSettings::Mach3::Units::kDistanceUnit = mm;
    const Time Constants::MachineSettings::Mach3::Units::kTimeUnit         = s;
    const Angle Constants::MachineSettings::Mach3::Units::kAngleUnit = rad;
    const Mass Constants::MachineSettings::Mach3::Units::kMassUnit   = g;
    const Velocity Constants::MachineSettings::Mach3::Units::kVelocityUnit =
        m / s;
    const Acceleration
        Constants::MachineSettings::Mach3::Units::kAccelerationUnit = m / s / s;

    const Distance Constants::MachineSettings::Ingersoll::Units::kDistanceUnit =
        mm;
    const Time Constants::MachineSettings::Ingersoll::Units::kTimeUnit   = s;
    const Angle Constants::MachineSettings::Ingersoll::Units::kAngleUnit = rad;
    const Mass Constants::MachineSettings::Ingersoll::Units::kMassUnit   = g;
    const Velocity Constants::MachineSettings::Ingersoll::Units::kVelocityUnit =
        m / s;
    const Acceleration
        Constants::MachineSettings::Ingersoll::Units::kAccelerationUnit =
            m / s / s;


    //================================================================================
    // Material Settings
    //================================================================================

    // Material Categories
    const QString Constants::MaterialSettings::kPropertiesCategory =
        "Properties";
    const QStringList Constants::MaterialSettings::kCategories = {
        Constants::MaterialSettings::kPropertiesCategory};

    //================================================================================
    // Nozzle Settings
    //================================================================================

    // Nozzle Categories
    const QString Constants::NozzleSettings::kLayerCategory     = "Layer";
    const QString Constants::NozzleSettings::kBeadWidthCategory = "Bead Width";
    const QString Constants::NozzleSettings::kSpeedCategory     = "Speed";
    const QString Constants::NozzleSettings::kAccelerationCategory =
        "Acceleration";
    const QStringList Constants::NozzleSettings::kCategories = {
        Constants::NozzleSettings::kLayerCategory,
        Constants::NozzleSettings::kBeadWidthCategory,
        Constants::NozzleSettings::kSpeedCategory,
        Constants::NozzleSettings::kAccelerationCategory};

    // Layer Keys
    const QString Constants::NozzleSettings::Layer::kLayerHeight =
        "layer_height";

    // Layer Labels
    const QString Constants::NozzleSettings::Layer::Labels::kLayerHeight =
        "Layer Height:";

    // Layer Tooltips
    const QString Constants::NozzleSettings::Layer::Tooltips::kLayerHeight = "";

    // Bead Width Keys
    const QString Constants::NozzleSettings::BeadWidth::kDefault =
        "bead_width_default";
    const QString Constants::NozzleSettings::BeadWidth::kPerimeter =
        "bead_width_perimeter";
    const QString Constants::NozzleSettings::BeadWidth::kInset =
        "bead_width_inset";
    const QString Constants::NozzleSettings::BeadWidth::kSkin =
        "bead_width_skin";
    const QString Constants::NozzleSettings::BeadWidth::kInfill =
        "bead_width_infill";

    // Bead Width Labels
    const QString Constants::NozzleSettings::BeadWidth::Labels::kDefault =
        "Default:";
    const QString Constants::NozzleSettings::BeadWidth::Labels::kPerimeter =
        "Perimeter:";
    const QString Constants::NozzleSettings::BeadWidth::Labels::kInset =
        "Inset:";
    const QString Constants::NozzleSettings::BeadWidth::Labels::kSkin = "Skin:";
    const QString Constants::NozzleSettings::BeadWidth::Labels::kInfill =
        "Infill:";

    // Bead Width Tooltips
    const QString Constants::NozzleSettings::BeadWidth::Tooltips::kDefault = "";
    const QString Constants::NozzleSettings::BeadWidth::Tooltips::kPerimeter =
        "";
    const QString Constants::NozzleSettings::BeadWidth::Tooltips::kInset  = "";
    const QString Constants::NozzleSettings::BeadWidth::Tooltips::kSkin   = "";
    const QString Constants::NozzleSettings::BeadWidth::Tooltips::kInfill = "";

    // Speed Keys
    const QString Constants::NozzleSettings::Speed::kDefault = "speed_default";
    const QString Constants::NozzleSettings::Speed::kPerimeter =
        "speed_perimeter";
    const QString Constants::NozzleSettings::Speed::kInset  = "speed_inset";
    const QString Constants::NozzleSettings::Speed::kSkin   = "speed_skin";
    const QString Constants::NozzleSettings::Speed::kInfill = "speed_infill";

    // Speed Labels
    const QString Constants::NozzleSettings::Speed::Labels::kDefault =
        "Default:";
    const QString Constants::NozzleSettings::Speed::Labels::kPerimeter =
        "Perimeter:";
    const QString Constants::NozzleSettings::Speed::Labels::kInset  = "Inset:";
    const QString Constants::NozzleSettings::Speed::Labels::kSkin   = "Skin:";
    const QString Constants::NozzleSettings::Speed::Labels::kInfill = "Infill:";

    // Speed Tooltips
    const QString Constants::NozzleSettings::Speed::Tooltips::kDefault   = "";
    const QString Constants::NozzleSettings::Speed::Tooltips::kPerimeter = "";
    const QString Constants::NozzleSettings::Speed::Tooltips::kInset     = "";
    const QString Constants::NozzleSettings::Speed::Tooltips::kSkin      = "";
    const QString Constants::NozzleSettings::Speed::Tooltips::kInfill    = "";

    // Acceleration Keys
    const QString Constants::NozzleSettings::Acceleration::kEnable =
        "acceleration_enable";
    const QString Constants::NozzleSettings::Acceleration::kDefault =
        "acceleration_default";
    const QString Constants::NozzleSettings::Acceleration::kPerimeter =
        "acceleration_perimeter";
    const QString Constants::NozzleSettings::Acceleration::kInset =
        "acceleration_inset";
    const QString Constants::NozzleSettings::Acceleration::kSkin =
        "acceleration_skin";
    const QString Constants::NozzleSettings::Acceleration::kInfill =
        "acceleration_infill";

    // Acceleration Labels
    const QString Constants::NozzleSettings::Acceleration::Labels::kEnable =
        "Enable:";
    const QString Constants::NozzleSettings::Acceleration::Labels::kDefault =
        "Default:";
    const QString Constants::NozzleSettings::Acceleration::Labels::kPerimeter =
        "Perimeter:";
    const QString Constants::NozzleSettings::Acceleration::Labels::kInset =
        "Inset:";
    const QString Constants::NozzleSettings::Acceleration::Labels::kSkin =
        "Skin:";
    const QString Constants::NozzleSettings::Acceleration::Labels::kInfill =
        "Infill:";

    // Acceleration Tooltips
    const QString Constants::NozzleSettings::Acceleration::Tooltips::kEnable =
        "";
    const QString Constants::NozzleSettings::Acceleration::Tooltips::kDefault =
        "";
    const QString
        Constants::NozzleSettings::Acceleration::Tooltips::kPerimeter = "";
    const QString Constants::NozzleSettings::Acceleration::Tooltips::kInset =
        "";
    const QString Constants::NozzleSettings::Acceleration::Tooltips::kSkin = "";
    const QString Constants::NozzleSettings::Acceleration::Tooltips::kInfill =
        "";

    //================================================================================
    // Print Settings
    //================================================================================

    // Print Categories
    const QString Constants::PrintSettings::kPerimetersCategory = "Perimeter";
    const QString Constants::PrintSettings::kInsetsCategory     = "Inset";
    const QString Constants::PrintSettings::kInfillCategory     = "Infill";
    const QString Constants::PrintSettings::kSkinsCategory      = "Skin";
    const QString Constants::PrintSettings::kSupportCategory    = "Support";
    const QString Constants::PrintSettings::kFixModelCategory   = "Fix Model";
    const QStringList Constants::PrintSettings::kCategories     = {
        Constants::PrintSettings::kPerimetersCategory,
        Constants::PrintSettings::kInsetsCategory,
        Constants::PrintSettings::kInfillCategory,
        Constants::PrintSettings::kSkinsCategory,
        Constants::PrintSettings::kSupportCategory,
        Constants::PrintSettings::kFixModelCategory};

    // Perimeter Keys
    const QString Constants::PrintSettings::Perimeters::kEnable =
        "perimeter_enable";
    const QString Constants::PrintSettings::Perimeters::kExtruderId =
        "perimeter_extruder_id";
    const QString Constants::PrintSettings::Perimeters::kNumber =
        "perimeter_number";
    const QString Constants::PrintSettings::Perimeters::kExternalFirst =
        "perimeter_external_first";
    const QString Constants::PrintSettings::Perimeters::kEnableSkeletons =
        "perimeter_perimeter_bead_width";
    const QString
        Constants::PrintSettings::Perimeters::kEnableVariableWidthSkeletons =
            "perimeter_perimeter_extruder_id";
    const QString
        Constants::PrintSettings::Perimeters::kOnlyRetractWhenCrossing =
            "perimeter_only_retract_when_crossing";
    const QString Constants::PrintSettings::Perimeters::kSmallLength =
        "perimeter_small_length";

    // Perimeter Labels
    const QString Constants::PrintSettings::Perimeters::Labels::kEnable =
        "Enable :";
    const QString Constants::PrintSettings::Perimeters::Labels::kExternalFirst =
        "External First:";
    const QString
        Constants::PrintSettings::Perimeters::Labels::kEnableSkeletons =
            "Enable Skeletons:";
    const QString Constants::PrintSettings::Perimeters::Labels::
        kEnableVariableWidthSkeletons = "Enable Variable Width Skeletons:";
    const QString Constants::PrintSettings::Perimeters::Labels::kNumber =
        "Number:";
    const QString
        Constants::PrintSettings::Perimeters::Labels::kOnlyRetractWhenCrossing =
            "Only Retract When Crossing:";
    const QString Constants::PrintSettings::Perimeters::Labels::kExtruderId =
        "Extruder ID:";
    const QString Constants::PrintSettings::Perimeters::Labels::kSmallLength =
        "Small Perimeter Length:";

    // Perimeter Tooltips
    const QString Constants::PrintSettings::Perimeters::Tooltips::kEnable =
        "If selected, perimeters will be generated";
    const QString
        Constants::PrintSettings::Perimeters::Tooltips::kExternalFirst =
            "If selected, external perimeters will be printed first";
    const QString
        Constants::PrintSettings::Perimeters::Tooltips::kEnableSkeletons =
            "If selected, skeletons will allow for an open loop path to fill a "
            "space that is too thin to use concentric paths";
    const QString Constants::PrintSettings::Perimeters::Tooltips::
        kEnableVariableWidthSkeletons =
            "If selected, the extruder speed is altered allowing for different "
            "bead widths with the same layer height for skeletons";
    const QString Constants::PrintSettings::Perimeters::Tooltips::kNumber =
        "Total number of perimeters per layer";
    const QString Constants::PrintSettings::Perimeters::Tooltips::
        kOnlyRetractWhenCrossing = "If selected, extruder will not retract "
                                   "unless crossing a perimeter path";
    const QString Constants::PrintSettings::Perimeters::Tooltips::kExtruderId =
        "ID number of the extruder used for perimeters";
    const QString Constants::PrintSettings::Perimeters::Tooltips::kSmallLength =
        "Maximum length to qualify as small perimeter";

    // Inset Keys
    const QString Constants::PrintSettings::Insets::kEnable = "inset_enable";
    const QString Constants::PrintSettings::Insets::kExtruderId =
        "inset_extruder_id";
    const QString Constants::PrintSettings::Insets::kNumber = "inset_number";
    const QString Constants::PrintSettings::Insets::kExternalFirst =
        "inset_external_first";
    const QString Constants::PrintSettings::Insets::kEnableSkeletons =
        "inset_perimeter_bead_width";
    const QString
        Constants::PrintSettings::Insets::kEnableVariableWidthSkeletons =
            "inset_perimeter_extruder_id";
    const QString Constants::PrintSettings::Insets::kOnlyRetractWhenCrossing =
        "inset_only_retract_when_crossing";
    const QString Constants::PrintSettings::Insets::kSmallLength =
        "inset_small_length";

    // Inset Labels
    const QString Constants::PrintSettings::Insets::Labels::kEnable =
        "Enable :";
    const QString Constants::PrintSettings::Insets::Labels::kExternalFirst =
        "External First:";
    const QString Constants::PrintSettings::Insets::Labels::kEnableSkeletons =
        "Enable Skeletons:";
    const QString Constants::PrintSettings::Insets::Labels::
        kEnableVariableWidthSkeletons = "Enable Variable Width Skeletons:";
    const QString Constants::PrintSettings::Insets::Labels::kNumber = "Number:";
    const QString
        Constants::PrintSettings::Insets::Labels::kOnlyRetractWhenCrossing =
            "Only Retract When Crossing:";
    const QString Constants::PrintSettings::Insets::Labels::kExtruderId =
        "Extruder ID:";
    const QString Constants::PrintSettings::Insets::Labels::kSmallLength =
        "Small Perimeter Length:";

    // Inset Tooltips
    const QString Constants::PrintSettings::Insets::Tooltips::kEnable =
        "If selected, insets will be generated";
    const QString Constants::PrintSettings::Insets::Tooltips::kExternalFirst =
        "If selected, external insets will be printed first";
    const QString Constants::PrintSettings::Insets::Tooltips::kEnableSkeletons =
        "If selected, skeletons will allow for an open loop path to fill a "
        "space that is too thin to use concentric paths";
    const QString Constants::PrintSettings::Insets::Tooltips::
        kEnableVariableWidthSkeletons =
            "If selected, the extruder speed is altered allowing for different "
            "bead widths with the same layer height for skeletons";
    const QString Constants::PrintSettings::Insets::Tooltips::kNumber =
        "Total number of insets per layer";
    const QString
        Constants::PrintSettings::Insets::Tooltips::kOnlyRetractWhenCrossing =
            "If selected, extruder will not retract unless crossing a inset "
            "path";
    const QString Constants::PrintSettings::Insets::Tooltips::kExtruderId =
        "ID number of the extruder used for insets";
    const QString Constants::PrintSettings::Insets::Tooltips::kSmallLength =
        "Maximum length to qualify as small inset";


    // Infill Keys
    const QString Constants::PrintSettings::Infill::kUse     = "infill_use";
    const QString Constants::PrintSettings::Infill::kPattern = "infill_pattern";
    const QString Constants::PrintSettings::Infill::kLineSpacing =
        "infill_line_spacing";
    const QString Constants::PrintSettings::Infill::kDensity = "infill_density";
    const QString Constants::PrintSettings::Infill::kOverlap = "infill_overlap";
    const QString Constants::PrintSettings::Infill::kInitialAngle =
        "infill_initial_angle";
    const QString Constants::PrintSettings::Infill::kRotationAngle =
        "infill_rotation_angle";

    // Infill Labels
    const QString Constants::PrintSettings::Infill::Labels::kUse = "Use:";
    const QString Constants::PrintSettings::Infill::Labels::kPattern =
        "Pattern:";
    const QString Constants::PrintSettings::Infill::Labels::kLineSpacing =
        "Line Spacing:";
    const QString Constants::PrintSettings::Infill::Labels::kDensity =
        "Density:";
    const QString Constants::PrintSettings::Infill::Labels::kOverlap =
        "Overlap with Inset/Perimeter:";
    const QString Constants::PrintSettings::Infill::Labels::kInitialAngle =
        "Initial Angle";
    const QString Constants::PrintSettings::Infill::Labels::kRotationAngle =
        "Rotation Angle";

    // Infill Tooltips
    const QString Constants::PrintSettings::Infill::Tooltips::kUse         = "";
    const QString Constants::PrintSettings::Infill::Tooltips::kPattern     = "";
    const QString Constants::PrintSettings::Infill::Tooltips::kLineSpacing = "";
    const QString Constants::PrintSettings::Infill::Tooltips::kDensity     = "";
    const QString Constants::PrintSettings::Infill::Tooltips::kOverlap     = "";
    const QString Constants::PrintSettings::Infill::Tooltips::kInitialAngle =
        "";
    const QString Constants::PrintSettings::Infill::Tooltips::kRotationAngle =
        "";

    // Skin Keys
    const QString Constants::PrintSettings::Skin::kUse = "skin_use";
    const QString Constants::PrintSettings::Skin::kLayersAbove =
        "skin_layers_above";
    const QString Constants::PrintSettings::Skin::kLayersBelow =
        "skin_layers_below";
    const QString Constants::PrintSettings::Skin::kPattern = "skin_pattern";
    const QString Constants::PrintSettings::Skin::kLineOverlap =
        "skin_line_overlap";
    const QString Constants::PrintSettings::Skin::kOverlap = "skin_overlap";

    // Skin Labels
    const QString Constants::PrintSettings::Skin::Labels::kUse = "Use:";
    const QString Constants::PrintSettings::Skin::Labels::kLayersAbove =
        "Layers Above:";
    const QString Constants::PrintSettings::Skin::Labels::kLayersBelow =
        "Layers Below:";
    const QString Constants::PrintSettings::Skin::Labels::kPattern = "Pattern:";
    const QString Constants::PrintSettings::Skin::Labels::kLineOverlap =
        "Overlap with self:";
    const QString Constants::PrintSettings::Skin::Labels::kOverlap =
        "Overlap with Infill/Insets/Perimeter:";

    // Skin Tooltips
    const QString Constants::PrintSettings::Skin::Tooltips::kUse         = "";
    const QString Constants::PrintSettings::Skin::Tooltips::kLayersAbove = "";
    const QString Constants::PrintSettings::Skin::Tooltips::kLayersBelow = "";
    const QString Constants::PrintSettings::Skin::Tooltips::kPattern     = "";
    const QString Constants::PrintSettings::Skin::Tooltips::kLineOverlap = "";
    const QString Constants::PrintSettings::Skin::Tooltips::kOverlap     = "";


    // Fix Model Keys
    const QString Constants::PrintSettings::FixModel::kStitch = "stitch";
    const QString Constants::PrintSettings::FixModel::kExtensiveStitch =
        "extensive_stitch";

    //================================================================================
    // Colors
    //================================================================================
    const QVector3D Constants::Colors::kYellow = QVector3D(1.0f, 1.0f, 0.0f);
    const QVector3D Constants::Colors::kRed    = QVector3D(1.0f, 0.0f, 0.0f);
    const QVector3D Constants::Colors::kBlue   = QVector3D(0.0f, 0.0f, 1.0f);
    const QVector3D Constants::Colors::kGreen  = QVector3D(0.0f, 1.0f, 0.0f);
    const QVector3D Constants::Colors::kPurple = QVector3D(1.0f, 0.0f, 1.0f);
    const QVector3D Constants::Colors::kOrange = QVector3D(1.0f, 0.5f, 0.5f);
    const QVector3D Constants::Colors::kWhite  = QVector3D(1.0f, 1.0f, 1.0f);
    const QVector3D Constants::Colors::kBlack  = QVector3D(0.0f, 0.0f, 0.0f);
    const QVector< QVector3D > Constants::Colors::kModelColors = {kBlue,
                                                                  kPurple,
                                                                  kRed,
                                                                  kOrange,
                                                                  kGreen};

    const QColor Constants::Colors::kPerimeter   = QColor(0, 0, 255);
    const QColor Constants::Colors::kInset       = QColor(0, 204, 255);
    const QColor Constants::Colors::kSkin        = QColor(0, 255, 0);
    const QColor Constants::Colors::kInfill      = QColor(0, 128, 0);
    const QColor Constants::Colors::kTravel      = QColor(233, 175, 198);
    const QColor Constants::Colors::kSupport     = QColor(255, 102, 0);
    const QColor Constants::Colors::kSupportRoof = QColor(255, 179, 128);
    const QColor Constants::Colors::kPrestart =
        QColor(0, 0, 0);  // TODO: update to color
    const QColor Constants::Colors::kInitialStartup = QColor(135, 222, 205);
    const QColor Constants::Colors::kSlowDown       = QColor(44, 160, 137);
    const QColor Constants::Colors::kTipWipe        = QColor(179, 128, 255);
    const QColor Constants::Colors::kCoasting       = QColor(211, 95, 141);
    const QColor Constants::Colors::kSpiralLift     = QColor(113, 55, 200);
    const QColor Constants::Colors::kUnknown        = QColor(255, 0, 0);


    //================================================================================
    // OpenGL
    //================================================================================
    const QVector3D Constants::OpenGL::kUp         = QVector3D(0.0, 0.0, 1.0);
    const double Constants::OpenGL::kRotationSpeed = 0.5;
    const double Constants::OpenGL::kRotationIncrement    = 0.5;
    const double Constants::OpenGL::kTranslationSpeed     = 300000;
    const double Constants::OpenGL::kTranslationIncrement = 5000000;
    const double Constants::OpenGL::kZoomSpeed            = 1000000;
    const double Constants::OpenGL::kZoomIncrement        = 1000000;
    const double Constants::OpenGL::kFov                  = 60.0;

    // TODO: Figure out real values
    const double Constants::OpenGL::kNearPlane = 0.0;
    const double Constants::OpenGL::kFarPlane  = 10000000000.0;

    const char* Constants::OpenGL::kCameraToView  = "cameraToView";
    const char* Constants::OpenGL::kWorldToCamera = "worldToCamera";
    const char* Constants::OpenGL::kMeshToWorld   = "meshToWorld";
    const char* Constants::OpenGL::kColor         = "color";
}  // namespace ORNL
