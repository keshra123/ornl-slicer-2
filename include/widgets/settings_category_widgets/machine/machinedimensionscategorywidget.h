#ifndef MACHINEDIMENSIONSCATEGORYWIDGET_H
#define MACHINEDIMENSIONSCATEGORYWIDGET_H

#include <QWidget>

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class MachineDimensionsCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class MachineDimensionsCategoryWidget
     * \brief Widget that handles the Machine Dimensions category
     */
    class MachineDimensionsCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        MachineDimensionsCategoryWidget();

        //! \brief Destructor
        ~MachineDimensionsCategoryWidget();

        //! \brief handles conditions for enabling settings
        void handleConditions();

    private:
        Ui::MachineDimensionsCategoryWidget* ui;
    };  // class MachineDimensionsCategoryWidget
}  // namespace ORNL
#endif  // MACHINEDIMENSIONSCATEGORYWIDGET_H
