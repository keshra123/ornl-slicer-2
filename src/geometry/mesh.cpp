#include "geometry/mesh.h"

#include <QVector3D>
#include <Qt3DCore/QTransform>

//#include "

#include "exceptions/exceptions.h"
#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"
#include "graphics/meshgraphics.h"
#include "layer_regions/layer.h"
#include "managers/preferences_manager.h"
#include "utilities/constants.h"
#include "utilities/mathutils.h"
#include "utilities/qt_json_conversion.h"

bool operator<(const QVector3D& v1, const QVector3D& v2)
{
    if (v1.x() != v2.x())
    {
        return v1.x() < v2.x();
    }
    else if (v1.y() != v2.y())
    {
        return v1.y() < v2.y();
    }
    else
    {
        return v1.z() < v2.z();
    }
}

namespace ORNL
{
    Mesh::Mesh(const QVector< MeshVertex >& vertices,
               const QVector< MeshFace >& faces,
               SettingsBase* parent)
        : SettingsBase(parent)
        , m_selected(false)
        , m_vertices(vertices)
        , m_faces(faces)
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_display_color(Constants::Colors::kWhite)
    {
        // Center and drop
        /* Move the center and drop code into Meshloader */
        Point min_v    = min();
        Point center_v = center();
        for (int v = 0; v < m_vertices.size(); v++)
        {
            m_vertices[v].location.x(m_vertices[v].location.x() - center_v.x());
            m_vertices[v].location.y(m_vertices[v].location.y() - center_v.y());
            m_vertices[v].location.z(m_vertices[v].location.z() - min_v.z());
        }

        float mesh_height = (max() - min()).z();

        float layer_height = setting< Distance >(
            Constants::NozzleSettings::Layer::kLayerHeight)();

        if (MathUtils::notEquals(layer_height, 0))
        {
            uint number_of_layers = mesh_height / layer_height;

            for (uint layer_nr = 0; layer_nr < number_of_layers; layer_nr++)
            {
                append(QSharedPointer< Layer >(new Layer(this, size())));
            }
        }

        /* Set the units */
        m_unit = Distance::fromString(Constants::Units::kDistanceUnits[0]);
    }


    Mesh::Mesh(const Mesh& m)
        : SettingsBase(m.m_parent)
        , m_selected(false)
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_display_color(Constants::Colors::kWhite)
    {
        m_vertices = m.m_vertices;
        m_faces    = m.m_faces;
        m_unit     = m.m_unit;
        m_name     = m.m_name;
    }

    QSharedPointer< MeshGraphics > Mesh::graphics()
    {
        return m_graphics;
    }

    void Mesh::createGraphics()
    {
        m_graphics = QSharedPointer< MeshGraphics >(new MeshGraphics(this));

        Distance center_x =
            (setting< Distance >(
                 Constants::MachineSettings::Dimensions::kXMax) +
             setting< Distance >(
                 Constants::MachineSettings::Dimensions::kXMin)) /
            2.0;
        Distance center_y =
            (setting< Distance >(
                 Constants::MachineSettings::Dimensions::kYMax) +
             setting< Distance >(
                 Constants::MachineSettings::Dimensions::kYMin)) /
            2.0;

        m_graphics->m_transform->setTranslation(
            QVector3D(center_x(), center_y(), 0.0));
    }

    QVector< MeshVertex > Mesh::vertices()
    {
        QVector< MeshVertex > rv;
        for (const MeshVertex& vertex : m_vertices)
        {
            rv.append(vertex.transform(*m_graphics->m_transform));
        }
        return rv;
    }

    QVector< MeshFace >& Mesh::faces()
    {
        return m_faces;
    }

    QString Mesh::name()
    {
        return m_name;
    }

    void Mesh::name(QString n)
    {
        m_name = n;
    }

    void Mesh::setTranslation(float x, float y, float z)
    {
        setTranslation(QVector3D(x, y, z));
    }

    void Mesh::setTranslation(QVector3D t)
    {
        m_graphics->m_transform->setTranslation(t);
    }

    void Mesh::setTranslationX(float x)
    {
        QVector3D t = translation();
        t.setX(x);
        m_graphics->m_transform->setTranslation(t);
    }

    void Mesh::setTranslationY(float y)
    {
        QVector3D t = translation();
        t.setY(y);
        m_graphics->m_transform->setTranslation(t);
    }

    void Mesh::setTranslationZ(float z)
    {
        QVector3D t = translation();
        t.setZ(z);
        m_graphics->m_transform->setTranslation(t);
    }

    void Mesh::translate(float x, float y, float z)
    {
        translate(QVector3D(x, y, z));
    }

    void Mesh::translate(QVector3D t)
    {
        QVector3D tran = translation();
        tran += t;
        m_graphics->m_transform->setTranslation(tran);
    }

    void Mesh::translateX(float x)
    {
        translate(x, 0, 0);
    }

    void Mesh::translateY(float y)
    {
        translate(0, y, 0);
    }

    void Mesh::translateZ(float z)
    {
        translate(0, 0, z);
    }

    QVector3D Mesh::translation()
    {
        return m_graphics->m_transform->translation();
    }

    void Mesh::setRotation(float x, float y, float z)
    {
        setRotation(QQuaternion::fromEulerAngles(QVector3D(x, y, z)));
    }

    void Mesh::setRotationX(float x)
    {
        m_graphics->m_transform->setRotationX(x);
    }

    void Mesh::setRotationY(float y)
    {
        m_graphics->m_transform->setRotationY(y);
    }

    void Mesh::setRotationZ(float z)
    {
        m_graphics->m_transform->setRotationZ(z);
    }

    void Mesh::setRotation(float w, float x, float y, float z)
    {
        setRotation(QQuaternion(w, x, y, z));
    }

    void Mesh::setRotation(QQuaternion q)
    {
        m_graphics->m_transform->setRotation(q);
    }

    void Mesh::rotate(float x, float y, float z)
    {
        rotate(QQuaternion::fromEulerAngles(QVector3D(x, y, z)));
    }

    void Mesh::rotateX(float x)
    {
        rotate(x, 0, 0);
    }

    void Mesh::rotateY(float y)
    {
        rotate(0, y, 0);
    }

    void Mesh::rotateZ(float z)
    {
        rotate(0, 0, z);
    }

    void Mesh::rotate(float w, float x, float y, float z)
    {
        rotate(QQuaternion(w, x, y, z));
    }

    void Mesh::rotate(QQuaternion q)
    {
        QQuaternion r = rotation();
        r *= q;
        setRotation(r);
    }

    QQuaternion Mesh::rotation()
    {
        return m_graphics->m_transform->rotation();
    }

    void Mesh::setScale(float s)
    {
        m_graphics->m_transform->setScale(s * m_unit());
    }

    void Mesh::setScale(float x, float y, float z)
    {
        setScale(QVector3D(x, y, z));
    }

    void Mesh::setScaleX(float x)
    {
        QVector3D s = scale();
        s.setX(x);
        setScale(s);
    }

    void Mesh::setScaleY(float y)
    {
        QVector3D s = scale();
        s.setY(y);
        setScale(s);
    }

    void Mesh::setScaleZ(float z)
    {
        QVector3D s = scale();
        s.setZ(z);
        setScale(s);
    }

    void Mesh::setScale(QVector3D s)
    {
        m_graphics->m_transform->setScale3D(s * m_unit());
    }

    void Mesh::scale(float s)
    {
        QVector3D sc = s * scale();
        setScale(sc);
    }

    void Mesh::scale(float x, float y, float z)
    {
        scale(QVector3D(x, y, z));
    }

    void Mesh::scaleX(float x)
    {
        scale(x, 0, 0);
    }

    void Mesh::scaleY(float y)
    {
        scale(0, y, 0);
    }

    void Mesh::scaleZ(float z)
    {
        scale(0, 0, z);
    }

    void Mesh::scale(QVector3D v)
    {
        QVector3D s = scale();
        s += v;
        setScale(s);
    }

    QVector3D Mesh::scale()
    {
        return m_graphics->m_transform->scale3D() / m_unit();
    }

    void Mesh::unit(Distance new_unit)
    {
        QVector3D new_scale = scale() * new_unit() / m_unit();
        setScale(new_scale);
        m_unit = new_unit;
    }

    Distance Mesh::unit()
    {
        return m_unit;
    }

    void Mesh::color(QVector3D c)
    {
        m_display_color = c;
    }

    QVector3D Mesh::color()
    {
        return m_display_color;
    }


    void Mesh::selected(bool selected)
    {
        m_selected = selected;
    }

    bool Mesh::selected()
    {
        return m_selected;
    }

    Point Mesh::min()
    {
        Point rv(std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max(),
                 std::numeric_limits< double >::max());
        for (const MeshVertex& vertex : m_vertices)
        {
            if (vertex.location.x() < rv.x())
            {
                rv.x(vertex.location.x());
            }

            if (vertex.location.y() < rv.y())
            {
                rv.y(vertex.location.y());
            }

            if (vertex.location.z() < rv.z())
            {
                rv.z(vertex.location.z());
            }
        }

        return rv;
    }

    Point Mesh::max()
    {
        Point rv(std::numeric_limits< double >::min(),
                 std::numeric_limits< double >::min(),
                 std::numeric_limits< double >::min());
        for (const MeshVertex& vertex : m_vertices)
        {
            if (vertex.location.x() > rv.x())
            {
                rv.x(vertex.location.x());
            }

            if (vertex.location.y() > rv.y())
            {
                rv.y(vertex.location.y());
            }

            if (vertex.location.z() > rv.z())
            {
                rv.z(vertex.location.z());
            }
        }

        return rv;
    }

    Point Mesh::center()
    {
        Point max_v = max();
        Point min_v = min();
        Point rv    = ((max_v - min_v) / 2.0) + min_v;
        return rv;
    }

    void Mesh::centerMesh()
    {
        Point min_v    = min();
        Point center_v = center();
        translate(-center_v.x(), -center_v.y(), -min_v.z());
    }

    void Mesh::dropMesh()
    {
        Point min_v           = min();
        QVector3D translation = m_graphics->m_transform->translation();
        translate(translation.x(), translation.y(), -min_v.z());
    }

    void Mesh::updateLayers()
    {
        // min().z() should be 0
        Distance mesh_height = (max().z() - min().z()) * unit();

        Distance current_height = 0;
        for (uint layer_nr = 0; layer_nr < size(); layer_nr++)
        {
            current_height += operator[](layer_nr)->setting< Distance >(
                Constants::NozzleSettings::Layer::kLayerHeight);
        }

        Distance mesh_layer_height =
            setting< Distance >(Constants::NozzleSettings::Layer::kLayerHeight);

        if (MathUtils::equals(mesh_layer_height(), 0))
        {
            throw ZeroLayerHeightException(
                "Mesh layer height is 0 in updateLayers");
        }

        while (current_height < mesh_height)
        {
            append(QSharedPointer< Layer >(new Layer(this, size())));
            current_height += mesh_layer_height;
        }

        while (mesh_height < current_height)
        {
            current_height -=
            operator[](size() - 1)
                ->setting< Distance >(
                    Constants::NozzleSettings::Layer::kLayerHeight);
            removeLast();
        }
    }

    nlohmann::json Mesh::json()
    {
        nlohmann::json rv;

        rv["translation"] = translation();
        rv["rotation"]    = rotation();
        rv["scale"]       = scale();
        rv["unit"]        = m_unit;
        rv["settings"]    = m_values;

        return rv;
    }

    void Mesh::json(nlohmann::json j)
    {
        translate(j["translation"]);
        rotate(j["rotation"]);
        unit(m_unit);
        scale(
            j["scale"]
                .get< QVector3D >());  // Removes any ambiguity for the compiler
        m_values = j["settings"];
    }
}  // namespace ORNL
