#ifndef PATHSEGMENT_H
#define PATHSEGMENT_H

#include "arc.h"
#include "exceptions/exceptions.h"
#include "gcode/writers/writer_base.h"
#include "line.h"
#include "path.h"
#include "units/unit.h"
#include "utilities/enums.h"

namespace ORNL
{
    class WriterBase;
    class Path;

    /*!
     * \class PathSegment
     *
     * \brief A single path segment with all of its parameters
     */
    class PathSegment
    {
    public:
        PathSegment() = default;
        PathSegment(Path* parent,
                    Line line,
                    Distance width                 = 0,
                    Distance height                = 0,
                    Velocity speed                 = 0,
                    Acceleration acceleration      = 0,
                    AngularVelocity extruder_speed = 0,
                    PathModifiers modifiers        = PathModifiers::kNone);
        PathSegment(Path* parent,
                    Arc arc,
                    Distance width                 = 0,
                    Distance height                = 0,
                    Velocity speed                 = 0,
                    Acceleration acceleration      = 0,
                    AngularVelocity extruder_speed = 0,
                    PathModifiers modifiers        = PathModifiers::kNone);

        Distance beadWidth();
        void beadWidth(Distance width);

        Distance beadHeight();
        void beadHeight(Distance height);

        Velocity speed();
        void speed(Velocity speed);

        Acceleration acceleration();
        void acceleration(Acceleration acceleration);

        AngularVelocity extruderSpeed();
        void extruderSpeed(AngularVelocity extruder_speed);

        void modifier(PathModifiers modifier);
        PathModifiers modifier();

        bool isLine();
        Line line();

        bool isArc();
        Arc arc();

        Distance distance();

        Volume volume();

        QString gcode(WriterBase* syntax);

    private:
        Distance m_height;
        Distance m_width;
        Velocity m_speed;
        Acceleration m_acceleration;
        AngularVelocity m_extruder_speed;

        PathModifiers m_path_modifiers;  //!< Types are ored together

        bool m_is_line;
        Arc m_arc;
        Line m_line;

        Path* m_parent;
    };
}  // namespace ORNL


#endif  // PATHSEGMENT_H
