#ifndef SKIN_H
#define SKIN_H

//! \file skin.h

#include "geometry/polygon_list.h"
#include "region.h"

namespace ORNL
{
    class Island;
    class Region;

    /*!
     * \class Skin
     * \brief All the information needed for creating the gcode for the skins of
     * this particular island
     */
    class Skin : public Region
    {
    public:
        //! \brief Constructor
        Skin(Island* parent);

        //! \brief compute the skin polygons and paths
        void computeInternal();

        //! \brief order the paths. previous location gets updated to the last
        //! point
        void orderInternal(Point& previous_location);

#ifdef DEBUG
        //! \brief Returns a list of debug options
        QStringList debugOptionsInternal() override;

        //! \brief Paints the selected debug option to the screen
        void paintDebugInternal(QString option,
                                QPainter* painter,
                                Distance& ratio,
                                int vertical_offset) override;
#endif
    private:
        //! \brief Compute the outline for the top skin
        void computeTopSkin();

        //! \brief Computer the outline for the bottom skin
        void computeBottomSkin();

        //! \brief Computes either the perimeters or insets as basic loops
        void computeConcentric();

        //! \brief Computes either the perimeters or insets with skeletons added
        void computeSkeletonConcentric();

        //! \brief Computes either the perimeters or insets with variable
        //! extrusion added
        void computeVariableConcentric();

        PolygonList m_top_outline;
        PolygonList m_bottom_outline;

#ifdef DEBUG
        QVector< PolygonList > m_debug_rings;
        QVector< Polyline > m_debug_lines;
#endif
    };  // class Skin
}  // namespace ORNL
#endif  // SKIN_H
