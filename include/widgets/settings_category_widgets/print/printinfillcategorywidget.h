#ifndef PRINTINFILLCATEGORYWIDGET_H
#define PRINTINFILLCATEGORYWIDGET_H

#include <QWidget>

#include "utilities/constants.h"
#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class PrintInfillCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class PrintInfillCategoryWidget
     *
     * \brief Settings Category widget for infill
     */
    class PrintInfillCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        //! \brief default constructor
        explicit PrintInfillCategoryWidget(QWidget* parent = 0);

        //! \brief destructor
        ~PrintInfillCategoryWidget();

        //! \brief handles conditional
        void handleConditions();

    private:
        Ui::PrintInfillCategoryWidget* ui;
    };  // class PrintInfillCategoryWidget
}  // namespace ORNL

#endif  // PRINTINFILLCATEGORYWIDGET_H
