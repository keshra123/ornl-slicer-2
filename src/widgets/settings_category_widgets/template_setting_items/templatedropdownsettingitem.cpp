#include "widgets/settings_category_widgets/template_setting_items/templatedropdownsettingitem.h"

#include "ui_template_dropdown_setting_item.h"

namespace ORNL
{
    TemplateDropdownSettingItem::TemplateDropdownSettingItem(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::TemplateDropdownSettingItem)
    {
        ui->setupUi(this);

        connect(ui->undo, SIGNAL(clicked(bool)), this, SLOT(handleUndo()));
        connect(ui->redo, SIGNAL(clicked(bool)), this, SLOT(handleRedo()));
        connect(ui->combo_box,
                SIGNAL(currentIndexChanged(int)),
                this,
                SLOT(handleValueChanged()));
    }

    TemplateDropdownSettingItem::~TemplateDropdownSettingItem()
    {
        delete ui;
    }

    void TemplateDropdownSettingItem::label(QString text)
    {
        ui->label->setText(text);
    }

    void TemplateDropdownSettingItem::tooltip(QString text)
    {
        ui->tooltip->setToolTip(text);
    }

    void TemplateDropdownSettingItem::list(QStringList list)
    {
        ui->combo_box->addItems(list);
    }

    void TemplateDropdownSettingItem::value(QString c)
    {
        ui->combo_box->setCurrentText(c);
    }

    QString TemplateDropdownSettingItem::value()
    {
        return ui->combo_box->currentText();
    }

    void TemplateDropdownSettingItem::setStyleSheet(const QString& stylesheet)
    {
        ui->combo_box->setStyleSheet(stylesheet);
    }

    void TemplateDropdownSettingItem::handleUndo()
    {
        emit undo();
    }

    void TemplateDropdownSettingItem::handleRedo()
    {
        emit redo();
    }

    void TemplateDropdownSettingItem::handleValueChanged()
    {
        emit valueChanged(value());
    }

    void TemplateDropdownSettingItem::showUndo()
    {
        ui->undo->show();
    }

    void TemplateDropdownSettingItem::hideUndo()
    {
        ui->undo->hide();
    }

    void TemplateDropdownSettingItem::showRedo()
    {
        ui->redo->show();
    }

    void TemplateDropdownSettingItem::hideRedo()
    {
        ui->redo->hide();
    }
}  // namespace ORNL
