#include "managers/window_manager.h"

#include <QAction>
#include <QMenu>
#include <QSignalMapper>
#include <QtDebug>

#include "dialogs/errordialog.h"
#include "dialogs/loadingdialog.h"
#include "dialogs/nameconfigdialog.h"
#include "dialogs/savingdialog.h"
#include "managers/global_settings_manager.h"
#include "managers/preferences_manager.h"
#include "windows/global_settings_window.h"
#include "windows/local_settings_window.h"
#include "windows/main_window.h"
#include "windows/preferences_window.h"

namespace ORNL
{
    QSharedPointer< WindowManager > WindowManager::m_singleton =
        QSharedPointer< WindowManager >();

    QSharedPointer< WindowManager > WindowManager::getInstance()
    {
        if (m_singleton.isNull())
        {
            m_singleton.reset(new WindowManager());
        }
        return m_singleton;
    }

    WindowManager::WindowManager()
        : m_global_settings_manager(GlobalSettingsManager::getInstance())
        , m_preferences_manager(PreferencesManager::getInstance())
        , m_main_window(nullptr)
        , m_global_settings_window(nullptr)
        , m_local_settings_window(nullptr)
        , m_name_config_dialog(nullptr)
        , m_preferences_window(nullptr)
        , m_loading_dialog(nullptr)
        , m_saving_dialog(nullptr)
    {}

    void WindowManager::createMainWindow()
    {
        if (!m_main_window)
        {
            m_main_window = new MainWindow();
            m_main_window->showMaximized();
        }
    }

    void WindowManager::createGlobalSettingsWindow(ConfigTypes configType,
                                                   QString config)
    {
        // Only one settings window at a time
        if (!m_global_settings_window && !m_local_settings_window)
        {
            m_global_settings_window =
                new GlobalSettingsWindow(configType, config);
            m_global_settings_window->show();

            // Both are set based on the window as this and
            // m_global_settings_manager are singletons that will last until the
            // entire program closes
            m_connections.insert(static_cast< void* >(m_global_settings_window),
                                 connect(m_global_settings_window,
                                         SLOT(error(QString)),
                                         this,
                                         SLOT(createErrorDialog(QString))));
            m_connections.insert(static_cast< void* >(m_global_settings_window),
                                 connect(m_global_settings_manager.data(),
                                         SIGNAL(updateConfig(ConfigTypes)),
                                         m_global_settings_window,
                                         SLOT(updateConfig(ConfigTypes))));
        }
    }

    void WindowManager::createGlobalSettingsWindow(QString config)
    {
        // Only one settings window at a time
        if (!m_global_settings_window && !m_local_settings_window)
        {
            if (config.contains("MACHINE"))
            {
                createGlobalSettingsWindow(ConfigTypes::kMachine,
                                           config.mid(7));
            }
            else if (config.contains("MATERIAL"))
            {
                createGlobalSettingsWindow(ConfigTypes::kMaterial,
                                           config.mid(8));
            }
            else if (config.contains("NOZZLE"))
            {
                createGlobalSettingsWindow(ConfigTypes::kNozzle, config.mid(6));
            }
            else if (config.contains("PRINT"))
            {
                createGlobalSettingsWindow(ConfigTypes::kPrint, config.mid(5));
            }
            else
            {
                qDebug() << "Unknown config type";
                return;
            }
        }
    }

    void WindowManager::createLocalSettingsWindow(QString mesh_name)
    {
        // Only one settings window at a time
        if (!m_local_settings_window && !m_global_settings_window)
        {
            m_local_settings_window = new LocalSettingsWindow(mesh_name);
            m_local_settings_window->show();
        }
    }


    void WindowManager::createLocalSettingsWindow(QString mesh_name,
                                                  int layer_number)
    {
        // Only one settings window at a time
        if (!m_local_settings_window && !m_global_settings_window)
        {
            m_local_settings_window =
                new LocalSettingsWindow(mesh_name, layer_number);
            m_local_settings_window->show();
        }
    }

    void WindowManager::createNameConfigDialog(QString material)
    {
        if (!m_name_config_dialog)
        {
            m_name_config_dialog = new NameConfigDialog();
            m_name_config_dialog->setConfigType(ConfigTypes::kNozzle);
            m_name_config_dialog->setMaterial(material);
            m_name_config_dialog->show();
        }
    }

    void WindowManager::createNameConfigDialog(ConfigTypes ct)
    {
        if (!m_name_config_dialog)
        {
            m_name_config_dialog = new NameConfigDialog();
            m_name_config_dialog->setConfigType(ct);
            if (ct == ConfigTypes::kNozzle)
            {
                QSignalMapper* sm = static_cast< QSignalMapper* >(sender());
                QAction* a        = static_cast< QAction* >(
                    sm->mapping(static_cast< uint8_t >(ct)));
                QMenu* m      = static_cast< QMenu* >(a->parent());
                QString title = m->title();
                m_name_config_dialog->setMaterial(title);
            }
            m_name_config_dialog->show();
        }
    }

    void WindowManager::createNameConfigDialog(int ct)
    {
        createNameConfigDialog(static_cast< ConfigTypes >(ct));
    }

    void WindowManager::createPreferencesWindow()
    {
        if (!m_preferences_window)
        {
            m_preferences_window = new PreferencesWindow();
            m_preferences_window->show();
        }
    }

    void WindowManager::createLoadingDialog(QString filename)
    {
        if (!m_loading_dialog)
        {
            m_loading_dialog = new LoadingDialog(filename);
            m_loading_dialog->show();
        }
    }

    void WindowManager::createSavingDialog(QString filename)
    {
        if (!m_saving_dialog)
        {
            m_saving_dialog = new SavingDialog(filename);
            m_saving_dialog->show();
        }
    }

    void WindowManager::createErrorDialog(QString error)
    {
        // Multiple error dialogs can be created
        ErrorDialog* error_dialog = new ErrorDialog(error);
        connect(error_dialog,
                SIGNAL(finished(int)),
                this,
                SLOT(deleteLater()));  // handles the clean up
        error_dialog->show();
        error_dialog->setFocus();
    }

    void WindowManager::createErrorDialog(QString filename, QString error)
    {
        // Multiple error dialogs can be created
        ErrorDialog* error_dialog = new ErrorDialog(error, filename);
        connect(error_dialog,
                SIGNAL(finished(int)),
                this,
                SLOT(deleteLater()));  // handles the clean up
        error_dialog->show();
        error_dialog->setFocus();
    }

    void WindowManager::removeGlobalSettingsWindow()
    {
        if (m_global_settings_window)
        {
            for (QMetaObject::Connection connection : m_connections.values(
                     static_cast< void* >(m_global_settings_window)))
            {
                disconnect(connection);
            }
            delete m_global_settings_window;
            m_global_settings_window = nullptr;
            m_main_window->reloadSettings();
        }
    }

    void WindowManager::removeLocalSettingsWindow()
    {
        if (m_local_settings_window)
        {
            for (QMetaObject::Connection connection : m_connections.values(
                     static_cast< void* >(m_local_settings_window)))
            {
                disconnect(connection);
            }
            delete m_local_settings_window;
            m_local_settings_window = nullptr;
        }
    }

    void WindowManager::removeNameConfigDialog()
    {
        if (m_name_config_dialog)
        {
            delete m_name_config_dialog;
            m_name_config_dialog = nullptr;
        }
    }

    void WindowManager::removePreferencesWindow()
    {
        if (m_preferences_window)
        {
            m_preferences_manager->exportPreferences();
            delete m_preferences_window;
            m_preferences_window = nullptr;
        }
    }

    void WindowManager::removeLoadingDialog()
    {
        if (m_loading_dialog)
        {
            delete m_loading_dialog;
            m_loading_dialog = nullptr;
        }
    }

    void WindowManager::removeSavingDialog()
    {
        if (m_saving_dialog)
        {
            delete m_saving_dialog;
            m_saving_dialog = nullptr;
        }
    }
}  // namespace ORNL
