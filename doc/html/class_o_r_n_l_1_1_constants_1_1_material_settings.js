var class_o_r_n_l_1_1_constants_1_1_material_settings =
[
    [ "Acceleration", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_acceleration.html", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_acceleration" ],
    [ "Properties", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_properties.html", [
      [ "Labels", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_properties_1_1_labels.html", null ],
      [ "Tooltips", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_properties_1_1_tooltips.html", null ]
    ] ],
    [ "Speed", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_speed.html", "class_o_r_n_l_1_1_constants_1_1_material_settings_1_1_speed" ]
];