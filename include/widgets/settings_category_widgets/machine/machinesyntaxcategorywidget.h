#ifndef MACHINESYNTAXCATEGORYWIDGET_H
#define MACHINESYNTAXCATEGORYWIDGET_H

#include <QWidget>

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class MachineSyntaxCategoryWidget;
}

namespace ORNL
{
    /*!
     * \class MachineSyntaxCategoryWidget
     * \brief Widget that handles the Machine Syntax Category
     */
    class MachineSyntaxCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        MachineSyntaxCategoryWidget();

        //! \brief Destructor
        ~MachineSyntaxCategoryWidget();

        //! \brief handles conditions for enabling settings
        void handleConditions();

    private:
        Ui::MachineSyntaxCategoryWidget* ui;
    };  // class MachineSyntaxCategoryWidget
}  // namespace ORNL
#endif  // MACHINESYNTAXCATEGORYWIDGET_H
