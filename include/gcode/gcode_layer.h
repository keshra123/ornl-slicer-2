#ifndef GCODELAYER_H
#define GCODELAYER_H

#include <QMap>
#include <QSharedPointer>
#include <QVector>

namespace ORNL
{
    class Island;
    class Mesh;
    class ProjectManager;
    class WriterBase;

    /*!
     * \class GcodeLayer
     *
     * \brief Contains a specific layer from all meshes
     */
    class GcodeLayer : public QVector< QSharedPointer< Island > >
    {
    public:
        //! \brief Constructor
        GcodeLayer(uint layer_nr);

        //! \brief Add the islands from this specific layer from a mesh
        void updateMesh(QSharedPointer< Mesh > mesh);

        //! \brief Remove the mesh from the layer
        void removeMesh(QString mesh_name);

        //! \brief Remove the mesh from the layer
        void removeMesh(QSharedPointer< Mesh > mesh);

        //! \brief Returns the layer number
        uint layerNumber();

        //! \brief order the islands
        void order();

        //! \brief Returns the gcode as a string for a specifc layer
        QString gcode(WriterBase* syntax);

    private:
        uint m_layer_nr;
        QSharedPointer< ProjectManager > m_project_manager;
    };  // class GcodeLayer
}  // namespace ORNL
#endif  // GCODELAYER_H
