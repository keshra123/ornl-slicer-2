#include "windows/global_settings_window.h"

#include "ui_global_settings_window.h"
#include "utilities/enums.h"

namespace ORNL
{
    GlobalSettingsWindow::GlobalSettingsWindow(ConfigTypes config_type,
                                               QString start)
        : ui(new Ui::GlobalSettingsWindow)
        , m_settings_manager(GlobalSettingsManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
        , m_project_manager(ProjectManager::getInstance())
    {
        ui->setupUi(this);

        ui->machine->setConfigType(ConfigTypes::kMachine);
        ui->print->setConfigType(ConfigTypes::kPrint);
        ui->material->setConfigType(ConfigTypes::kMaterial);
        ui->nozzle->setConfigType(ConfigTypes::kNozzle);

        switch (config_type)
        {
            case ConfigTypes::kMachine:
                ui->tabWidget->setCurrentIndex(
                    static_cast< int >(ConfigTypes::kMachine));
                ui->machine->setCurrentConfig(start);
                // Load the first config
                ui->print->loadFirstOption();
                ui->material->loadFirstOption();
                ui->nozzle->loadFirstOption();
                break;
            case ConfigTypes::kMaterial:
                ui->tabWidget->setCurrentIndex(
                    static_cast< int >(ConfigTypes::kMaterial));
                ui->material->setCurrentConfig(start);
                // Load the first config
                ui->machine->loadFirstOption();
                ui->print->loadFirstOption();
                ui->nozzle->loadFirstOption();
                break;
            case ConfigTypes::kNozzle:
                ui->tabWidget->setCurrentIndex(
                    static_cast< int >(ConfigTypes::kNozzle));
                ui->nozzle->setCurrentConfig(start);
                // Load the first config
                ui->machine->loadFirstOption();
                ui->print->loadFirstOption();
                ui->material->loadFirstOption();
                break;
            case ConfigTypes::kPrint:
                ui->tabWidget->setCurrentIndex(
                    static_cast< int >(ConfigTypes::kPrint));
                ui->print->setCurrentConfig(start);
                // Load the first config
                ui->machine->loadFirstOption();
                ui->material->loadFirstOption();
                ui->nozzle->loadFirstOption();
                break;
        }

        connect(ui->actionExit, SIGNAL(triggered(bool)), this, SLOT(close()));
        connect(ui->actionImport,
                SIGNAL(triggered(bool)),
                this,
                SLOT(importSetting()));
        connect(ui->actionExport,
                SIGNAL(triggered(bool)),
                this,
                SLOT(exportSetting()));
    }

    GlobalSettingsWindow::~GlobalSettingsWindow()
    {
        delete ui;
    }

    void GlobalSettingsWindow::closeEvent(QCloseEvent* event)
    {
        m_project_manager->updateGlobalSettings();
        m_window_manager->removeGlobalSettingsWindow();
        event->accept();
    }

    void GlobalSettingsWindow::importSetting()
    {}

    void GlobalSettingsWindow::exportSetting()
    {}

    void GlobalSettingsWindow::updateConfig(ConfigTypes ct)
    {
        switch (ct)
        {
            case ConfigTypes::kMachine:
                ui->machine->update();
                break;
            case ConfigTypes::kMaterial:
                ui->material->update();
                break;
            case ConfigTypes::kNozzle:
                ui->nozzle->update();
                break;
            case ConfigTypes::kPrint:
                ui->print->update();
                break;
        }
    }
}  // namespace ORNL
