#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <algorithm>
#include <cmath>

/*!
 * \class MathUtils
 *
 * \brief A math utilities class
 */
class MathUtils
{
public:
    /*!
     * \brief a function that compares two doubles
     *
     * \note Much better than a == b which can result
     *      in them being deemed unequal, even when they are logically
     * equivalent
     */
    inline static bool equals(double a, double b)
    {
        return (a == b) ||
            (std::abs(a - b) < std::abs(std::min(a, b)) *
                 std::numeric_limits< double >::epsilon());
    }

    /*!
     * \brief a function that compares two doubles
     *
     * \note Much better than a != b which can result
     *      in them being deemed unequal, even when they are logically
     * equivalent
     */
    inline static bool notEquals(double a, double b)
    {
        return !equals(a, b);
    }
};

#endif  // MATHUTILS_H
