#include "dialogs/savingdialog.h"

#include "ui_saving_dialog.h"
namespace ORNL
{
    SavingDialog::SavingDialog(QString filename, QWidget* parent)
        : QDialog(parent)
        , ui(new Ui::SavingDialog)
    {
        ui->setupUi(this);
        setWindowTitle(filename);
    }

    SavingDialog::~SavingDialog()
    {
        delete ui;
    }
}  // namespace ORNL
