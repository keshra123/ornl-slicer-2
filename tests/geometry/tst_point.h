#ifndef TESTPOINT_H
#define TESTPOINT_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

#if 0 // TODO fix tests

/*!
 * \class TestPoint
 *
 * \brief Unit test class for Point
 */
class TestPoint : public QObject
{
    Q_OBJECT
private slots:
    //! \brief Generates data for the testConversion function
    void testConversion_data();

    //! \brief Tests conversion from and to other point classes
    void testConversion();

    //! \brief Generates data for the testDistance function
    void testDistance_data();

    //! \brief Tests distance from origin and to another point
    void testDistance();

    //! \brief Generates data for the testDotProduct function
    void testDotProduct_data();

    //! \brief Tests the dot product of two points (as vectors)
    void testDotProduct();

    //! \brief Generates data for the testCrossProduct function
    void testCrossProduct_data();

    //! \brief Tests the cross product of two points (as vectors)
    void testCrossProduct();
};


DECLARE_TEST(TestPoint);
#endif // TESTPOINT_H

#endif
