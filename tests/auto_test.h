#ifndef AUTO_TEST_H
#define AUTO_TEST_H

#include <QtTest>
#include <QList>
#include <QString>

namespace AutoTest
{

    inline QList<QObject*>& testList()
    {
        static QList<QObject*> list;
        return list;
    }

    inline bool findObject(QObject* object)
    {
        QList<QObject*>& list = testList();
        if(list.contains(object))
        {
            return true;
        }

        for(QObject* test: list)
        {
            if(test->objectName() == object->objectName())
            {
                return true;
            }
        }
        return false;
    }

    inline void addTest(QObject* object)
    {
        QList<QObject*>& list = testList();
        if(!findObject(object))
        {
            list.append(object);
        }
    }

    inline int run(int argc, char** argv)
    {
        int ret = 0;

        for(QObject* test: testList())
        {
            ret += QTest::qExec(test, argc, argv);
        }

        return ret;
    }

    template <class T>
    class Test
    {
    public:
        QSharedPointer<T> child;

        Test(const QString& name) : child(new T)
        {
            child->setObjectName(name);
            AutoTest::addTest(child.data());
        }
    };

#define DECLARE_TEST(className) static Test<className> t(#className);
}
#endif // AUTO_TEST_H
