#ifndef ARC_H
#define ARC_H

#include "point.h"

namespace ORNL
{
    class Arc
    {
    public:
        //! \brief Default Constructor (For when pathsegment is a line)
        Arc() = default;

        //! \brief Constructor
        Arc(Point start, Point end, Point center, Angle angle, bool clockwise);

        //! \brief Returns the start point
        Point start() const;

        //! \brief Returns the center point
        Point center() const;

        //! \brief Returns the end point
        Point end() const;

        //! \brief Returns the angle along the path
        Angle angle() const;

        //! \brief Return whether the arc is clockwise
        bool clockwise() const;

    private:
        Point m_start;
        Point m_end;
        Point m_center;
        Angle m_angle;
        bool m_clockwise;
    };
}  // namespace ORNL
#endif  // ARC_H
