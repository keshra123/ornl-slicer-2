#ifndef GLOBALSETTINGSMANAGER_H
#define GLOBALSETTINGSMANAGER_H

//! \file globalsettingsmanager.h

#include <QMap>
#include <QObject>
#include <QSharedPointer>
#include <QVector>

namespace ORNL
{
    class SettingsWindow;
    class SettingsBase;
    class DisplayConfigBase;
    class NameConfigDialog;
    enum class ConfigTypes : uint8_t;

#define NUM_SETTING_TYPES 4

    /*!
     * \class GlobalSettingsManager
     * \brief Manager for holding configs
     */
    class GlobalSettingsManager : public QObject
    {
        Q_OBJECT
    public:
        //! \brief Returns the singleton
        static QSharedPointer< GlobalSettingsManager > getInstance();

        //! \brief Adds a new config
        //! base is not the parent, but a config to copy as a starting point
        void addConfig(ConfigTypes ct,
                       QString config,
                       QString base,
                       bool project   = false,
                       QString folder = "");

        //! \brief Saves the config to file
        void saveConfig(ConfigTypes ct,
                        QString config,
                        bool project   = false,
                        QString folder = "");

        //! \brief Removes the config (and its file)
        void removeConfig(ConfigTypes ct, QString config, bool project = false);

        //! \brief Returns the base for the config
        QSharedPointer< DisplayConfigBase > getConfig(ConfigTypes ct,
                                                      QString config,
                                                      bool project = false);

        //! \brief Returns a list of names for the configs of the specified type
        QStringList getConfigsNames(ConfigTypes ct);

        //! \brief Returns a multimap with names for the nozzles for various
        //! materials
        QMultiMap< QString, QString > getNozzleNames();

        //! \brief Updates the global config
        void updateGlobal(QString machine,
                          QString material,
                          QString print,
                          QString nozzle);

        //! \brief Returns the global SettingsBases
        QSharedPointer< SettingsBase > getGlobal();

    signals:
        //! \brief signal that there has been an error
        void error(QString error_message);

        //! \brief signal that a config has been added or removed
        void updateConfig(ConfigTypes ct);

    private:
        //! \brief Constructor
        GlobalSettingsManager();

        static QSharedPointer< GlobalSettingsManager > m_singleton;

        QSharedPointer< SettingsBase > m_global;

        QVector< QMap< QString, QSharedPointer< DisplayConfigBase > > >
            m_configs;
        QVector< QMap< QString, QSharedPointer< DisplayConfigBase > > >
            m_project_configs;
    };  // class GlobakSettingsManager
}  // namespace ORNL
#endif  // GLOBALSETTINGSMANAGER_H
