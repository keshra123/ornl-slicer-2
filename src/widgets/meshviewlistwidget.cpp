#include "widgets/meshviewlistwidget.h"

#include <QMenu>
#include <QMouseEvent>
#include <QSignalMapper>

namespace ORNL
{
    MeshViewListWidget::MeshViewListWidget(QWidget* parent)
        : QListWidget(parent)
        , m_project_manager(ProjectManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
    {
        connect(m_project_manager.data(),
                SIGNAL(updateList(QStringList)),
                this,
                SLOT(updateList(QStringList)));
        setContextMenuPolicy(Qt::CustomContextMenu);
    }

    void MeshViewListWidget::updateList(QStringList mesh_name_list)
    {
        clear();
        addItems(mesh_name_list);
    }

    // happens on right click
    void MeshViewListWidget::showContextMenu(const QPoint& point)
    {
        QMenu context_menu(tr("Context menu"), this);

        QListWidgetItem* lwi = itemAt(point);

        QAction* action;

        // CENTER XY
        action = new QAction("Center on Table", this);
        action->setIcon(QIcon(":/Icons/center.png"));
        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            // TODO: add function
        }
        context_menu.addAction(action);

        // CENTER XY TO BUILD SHEET
        action = new QAction("Center on Build Sheet", this);
        action->setIcon(QIcon(":/Icons/center.png"));
        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            // TODO: add function
        }

        // DROP TO TABLE
        action = new QAction("Drop to Table", this);
        action->setIcon(QIcon(":/Icons/drop.png"));
        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            // TODO: add function
        }
        context_menu.addAction(action);

        // SEPARATOR
        action = new QAction(this);
        action->setSeparator(true);
        context_menu.addAction(action);

        // COPY
        action = new QAction("Copy", this);
        action->setIcon(QIcon(":/Icons/copy.png"));

        // disable copy when right on nothing
        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            QSignalMapper* copy_redirect = new QSignalMapper(this);
            connect(
                action, SIGNAL(triggered(bool)), copy_redirect, SLOT(map()));
            copy_redirect->setMapping(action, lwi->text());
            connect(copy_redirect,
                    SIGNAL(mapped(QString)),
                    m_project_manager.data(),
                    SLOT(copyMesh(QString)));
        }
        context_menu.addAction(action);

        // PASTE
        action = new QAction("Paste", this);
        action->setIcon(QIcon(":/Icons/paste.png"));

        // disable paste is there is nothing copied
        QString copy = m_project_manager->copiedMeshName();
        if (copy.isEmpty())
        {
            action->setEnabled(false);
        }
        else
        {
            connect(action,
                    SIGNAL(triggered(bool)),
                    m_project_manager.data(),
                    SLOT(paste()));
        }

        context_menu.addAction(action);

        // SEPARATOR
        action = new QAction(this);
        action->setSeparator(true);
        context_menu.addAction(action);

        // DELETE
        action = new QAction("Delete", this);
        action->setIcon(QIcon(":/Icons/delete.png"));
        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            QSignalMapper* delete_redirect = new QSignalMapper(this);
            connect(
                action, SIGNAL(triggered(bool)), delete_redirect, SLOT(map()));
            delete_redirect->setMapping(action, lwi->text());
            connect(delete_redirect,
                    SIGNAL(mapped(QString)),
                    m_project_manager.data(),
                    SLOT(removeMesh(QString)));
        }
        context_menu.addAction(action);

        // SEPARATOR
        action = new QAction(this);
        action->setSeparator(true);
        context_menu.addAction(action);

        // SETTINGS
        action = new QAction("Settings", this);
        action->setIcon(QIcon(":/Icons/edit_property.png"));

        if (lwi == NULL)
        {
            action->setEnabled(false);
        }
        else
        {
            QSignalMapper* settings_redirect = new QSignalMapper(this);
            connect(action,
                    SIGNAL(triggered(bool)),
                    settings_redirect,
                    SLOT(map()));
            settings_redirect->setMapping(action, lwi->text());
            connect(settings_redirect,
                    SIGNAL(mapped(QString)),
                    m_window_manager.data(),
                    SLOT(createLocalSettingsWindow(QString)));
        }

        context_menu.addAction(action);

        context_menu.exec(mapToGlobal(point));
    }

    // happens on left click
    void MeshViewListWidget::select(QListWidgetItem* lwi)
    {
        m_project_manager->selectMesh(lwi->text());
    }

    void MeshViewListWidget::deselect()
    {
        m_project_manager->deselectMesh();
        clearSelection();
    }


    void MeshViewListWidget::mousePressEvent(QMouseEvent* event)
    {
        // Right clicks bring up the context menu, but don't affect selection
        if (event->button() == Qt::RightButton)
        {
            showContextMenu(event->pos());
            return;
        }

        QListWidgetItem* item = itemAt(event->pos());

        // If a new item is selected then mark it as selected
        if (item)
        {
            if (!item->isSelected())
            {
                select(item);
            }
        }
        // If anywhere that isn't an item is selected clear the selection
        else
        {
            deselect();
        }

        QListWidget::mousePressEvent(event);
    }
}  // namespace ORNL
