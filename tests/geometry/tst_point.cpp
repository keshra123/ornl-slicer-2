#include "geometry/tst_point.h"
#include "geometry/point.h"
#include "utilities/mathutils.h"

using namespace ORNL;

#if 0 // TODO fix tests

void TestPoint::testConversion_data()
{

    QTime time = QTime::currentTime();
    qsrand(static_cast<uint>(time.msec()));

    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");
    QTest::addColumn<double>("z");

    for(int i = 0; i < 1000; i++)
    {
        double x = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double y = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double z = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        QTest::newRow(QString::number(i+1).toStdString().c_str()) << x << y << z ;
    }
}

void TestPoint::testConversion()
{
    QFETCH(double, x);
    QFETCH(double, y);
    QFETCH(double, z);

    // Distance2D
    Distance2D d2(x, y);
    Point p_d2(d2);
    Distance2D other_d2 = p_d2.toDistance2D();
    QVERIFY2(d2 == other_d2, QString::asprintf("Distance2D - X: %lf, Y: %lf", x, y).toStdString().c_str());

    // Distance3D
    Distance3D d3(x, y, z);
    Point p_d3(d3);
    Distance3D other_d3 = p_d3.toDistance3D();
    QVERIFY2(d3 == other_d3, QString::asprintf("Distance3D - X: %lf, Y: %lf, Z: %lf", x, y, z).toStdString().c_str());

    // IntPoint
    ClipperLib::IntPoint i(x, y);
    Point p_i(i);
    ClipperLib::IntPoint other_i = p_i.toIntPoint();
    QVERIFY2(i == other_i, QString::asprintf("IntPoint - X: %lf, Y: %lf", x, y).toStdString().c_str());

    // QPoint
    QPoint q(x, y);
    Point p_q(q);
    QPoint other_q = p_q.toQPoint();
    QVERIFY2(q == other_q, QString::asprintf("QPoint - X: %lf, Y: %lf", x, y).toStdString().c_str());

    // QVector2D
    QVector2D qv2(x, y);
    Point p_q2 = Point::fromQVector2D(qv2);
    QVector2D other_qv2 = p_q2.toQVector2D();
    QVERIFY2(qv2 == other_qv2, QString::asprintf("QVector2D - X: %lf, Y: %lf", x, y).toStdString().c_str());

    // QVector3D
    QVector3D qv3(x, y, z);
    Point p_q3 = Point::fromQVector3D(qv3);
    QVector3D other_qv3 = p_q3.toQVector3D();
    QVERIFY2(qv3 == other_qv3, QString::asprintf("QVector3D - X: %lf, Y: %lf, Z: %lf", x, y, z).toStdString().c_str());
}


void TestPoint::testDistance_data()
{
    QTime time = QTime::currentTime();
    qsrand(static_cast<uint>(time.msec()));

    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");
    QTest::addColumn<double>("z");
    QTest::addColumn<double>("other_x");
    QTest::addColumn<double>("other_y");
    QTest::addColumn<double>("other_z");
    double other_x = 0;
    double other_y = 0;
    double other_z = 0;
    for(int i = 0; i < 1000; i++)
    {
        double x = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double y = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double z = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        QTest::newRow(QString::number(i+1).toStdString().c_str()) << x << y << z << other_x << other_y << other_z;
        other_x = x;
        other_y = y;
        other_z = z;
    }
}

void TestPoint::testDistance()
{
    QFETCH(double, x);
    QFETCH(double, y);
    QFETCH(double, z);

    Point p(x, y, z);
    QVERIFY2(MathUtils::equals(p.distance()(), qSqrt(qPow(x, 2) + qPow(y, 2) + qPow(z, 2))),
             QString::asprintf("X: %lf, Y: %lf, Z: %lf", x, y, z).toStdString().c_str());

    QFETCH(double, other_x);
    QFETCH(double, other_y);
    QFETCH(double, other_z);

    Point p2(other_x, other_y, other_z);
    QVERIFY2(MathUtils::equals(p.distance(p2)(),  qSqrt(qPow(other_x - x, 2) + qPow(other_y - y, 2) + qPow(other_z - z, 2))),
             QString::asprintf("X1: %lf, Y1: %lf, Z1: %lf, X2: %lf, Y2: %lf, Z2: %lf", x, y, z, other_x, other_y, other_z).toStdString().c_str());
}

void TestPoint::testDotProduct_data()
{
    QTime time = QTime::currentTime();
    qsrand(static_cast<uint>(time.msec()));

    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");
    QTest::addColumn<double>("z");
    QTest::addColumn<double>("other_x");
    QTest::addColumn<double>("other_y");
    QTest::addColumn<double>("other_z");
    double other_x = 0;
    double other_y = 0;
    double other_z = 0;
    for(int i = 0; i < 1000; i++)
    {
        double x = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double y = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double z = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        QTest::newRow(QString::number(i+1).toStdString().c_str()) << x << y << z << other_x << other_y << other_z;
        other_x = x;
        other_y = y;
        other_z = z;
    }
}

void TestPoint::testDotProduct()
{
    QFETCH(double, x);
    QFETCH(double, y);
    QFETCH(double, z);
    Point p(x, y, z);

    QFETCH(double, other_x);
    QFETCH(double, other_y);
    QFETCH(double, other_z);
    Point p2(other_x, other_y, other_z);


    QVERIFY2(MathUtils::equals(p.dot(p2), (p.x() * p2.x() + p.y() * p2.y() + p.z() * p2.z())), QString::asprintf("X1: %lf, Y1: %lf, Z1: %lf, X2: %lf, Y2: %lf, Z2: %lf", x, y, z, other_x, other_y, other_z).toStdString().c_str());
}

void TestPoint::testCrossProduct_data()
{
    QTime time = QTime::currentTime();
    qsrand(static_cast<uint>(time.msec()));

    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");
    QTest::addColumn<double>("z");
    QTest::addColumn<double>("other_x");
    QTest::addColumn<double>("other_y");
    QTest::addColumn<double>("other_z");
    double other_x = 0;
    double other_y = 0;
    double other_z = 0;
    for(int i = 0; i < 1000; i++)
    {
        double x = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double y = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        double z = static_cast<double>(qrand()) * (static_cast<double>(qrand()) / RAND_MAX);
        QTest::newRow(QString::number(i+1).toStdString().c_str()) << x << y << z << other_x << other_y << other_z;
        other_x = x;
        other_y = y;
        other_z = z;
    }
}

void TestPoint::testCrossProduct()
{
    QFETCH(double, x);
    QFETCH(double, y);
    QFETCH(double, z);
    Point p(x, y, z);

    QFETCH(double, other_x);
    QFETCH(double, other_y);
    QFETCH(double, other_z);
    Point p2(other_x, other_y, other_z);

    Point crossed(y * other_z - z * other_y,
                  z * other_x - x * other_z,
                  x * other_y - y * other_x);
    QVERIFY2(p.cross(p2) == crossed,
             QString::asprintf("X1: %lf, Y1: %lf, Z1: %lf, X2: %lf, Y2: %lf, Z2: %lf", x, y, z, other_x, other_y, other_z).toStdString().c_str());
}

#endif
