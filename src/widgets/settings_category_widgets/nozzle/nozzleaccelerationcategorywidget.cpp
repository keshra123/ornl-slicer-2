#include "widgets/settings_category_widgets/nozzle/nozzleaccelerationcategorywidget.h"

#include "ui_nozzle_acceleration_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    NozzleAccelerationCategoryWidget::NozzleAccelerationCategoryWidget()
        : ui(new Ui::NozzleAccelerationCategoryWidget)
    {
        ui->setupUi(this);

        ui->default_->label(
            Constants::NozzleSettings::Acceleration::Labels::kDefault);
        ui->default_->tooltip(
            Constants::NozzleSettings::Acceleration::Tooltips::kDefault);
        m_acceleration[Constants::NozzleSettings::Acceleration::kDefault] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->default_);

        ui->perimeter->label(
            Constants::NozzleSettings::Acceleration::Labels::kPerimeter);
        ui->perimeter->tooltip(
            Constants::NozzleSettings::Acceleration::Tooltips::kPerimeter);
        m_acceleration[Constants::NozzleSettings::Acceleration::kPerimeter] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->perimeter);

        ui->inset->label(
            Constants::NozzleSettings::Acceleration::Labels::kInset);
        ui->inset->tooltip(
            Constants::NozzleSettings::Acceleration::Tooltips::kInset);
        m_acceleration[Constants::NozzleSettings::Acceleration::kInset] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->inset);

        ui->skin->label(Constants::NozzleSettings::Acceleration::Labels::kSkin);
        ui->skin->tooltip(
            Constants::NozzleSettings::Acceleration::Tooltips::kSkin);
        m_acceleration[Constants::NozzleSettings::Acceleration::kSkin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->skin);

        ui->infill->label(
            Constants::NozzleSettings::Acceleration::Labels::kInfill);
        ui->infill->tooltip(
            Constants::NozzleSettings::Acceleration::Tooltips::kInfill);
        m_acceleration[Constants::NozzleSettings::Acceleration::kInfill] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->infill);

        initializeSettings();
    }

    NozzleAccelerationCategoryWidget::~NozzleAccelerationCategoryWidget()
    {
        delete ui;
    }

    void NozzleAccelerationCategoryWidget::handleConditions()
    {
        // TODO
    }
}  // namespace ORNL
