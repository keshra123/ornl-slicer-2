#ifndef NOZZLESPEEDCATEGORYWIDGET_H
#define NOZZLESPEEDCATEGORYWIDGET_H

#include "widgets/settings_category_widgets/settingscategorywidgetbase.h"

namespace Ui
{
    class NozzleSpeedCategoryWidget;
}

namespace ORNL
{
    class NozzleSpeedCategoryWidget : public SettingsCategoryWidgetBase
    {
        Q_OBJECT

    public:
        NozzleSpeedCategoryWidget();

        ~NozzleSpeedCategoryWidget();

        void handleConditions();

    private:
        Ui::NozzleSpeedCategoryWidget* ui;
    };
}  // namespace ORNL


#endif  // NOZZLESPEEDCATEGORYWIDGET_H
