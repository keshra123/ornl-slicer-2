#include "layer_regions/island.h"

#include "gcode/writers/writer_base.h"
#include "layer_regions/infill.h"
#include "layer_regions/layer.h"
#include "layer_regions/perimeters.h"
#include "layer_regions/skin.h"

namespace ORNL
{
    Island::Island(Layer* layer)
        : SettingsBase(layer)
        , m_parent(layer)
        , m_perimeters(new Perimeters(this, RegionType::kPerimeter))
        , m_insets(new Insets(this, RegionType::kInset))
        //, m_infill(new Infill(this))
        , m_skin(new Skin(this))
    {}

    Island::Island(Layer* layer, PolygonList polygons)
        : SettingsBase(layer)
        , m_parent(layer)
        , m_outlines(polygons)
        , m_perimeters(new Perimeters(this, RegionType::kPerimeter))
        , m_insets(new Insets(this, RegionType::kInset))
        //, m_infill(new Infill(this))
        , m_skin(new Skin(this))
    {}

    Layer* Island::parent()
    {
        return m_parent;
    }

    void Island::outlines(PolygonList polygons)
    {
        m_outlines = polygons;
    }

    PolygonList Island::outlines()
    {
        return m_outlines;
    }

    QSharedPointer< Perimeters > Island::perimeters()
    {
        return m_perimeters;
    }

    QSharedPointer< Insets > Island::insets()
    {
        return m_insets;
    }

    QSharedPointer< Skin > Island::skin()
    {
        return m_skin;
    }

    QSharedPointer< Infill > Island::infill()
    {
        return m_infill;
    }

    void Island::order(Point& previous_location, RegionType region_type)
    {
        bool dirty = false;  //!< If an earlier stage is reordered then the
                             //!< following stages must be as well

        if (static_cast< bool >(region_type & RegionType::kPerimeter))
        {
            m_perimeters->order(previous_location);
            dirty = true;
        }

        if (dirty || static_cast< bool >(region_type & RegionType::kInset))
        {
            m_insets->order(previous_location);
            dirty = true;
        }

        if (dirty || static_cast< bool >(region_type & RegionType::kTopSkin) ||
            static_cast< bool >(region_type & RegionType::kBottomSkin) ||
            static_cast< bool >(region_type & RegionType::kSkin))
        {
            m_skin->order(previous_location);
            dirty = true;
        }

        if (dirty || static_cast< bool >(region_type & RegionType::kInfill))
        {
            m_infill->order(previous_location);
        }
    }

    QString Island::gcode(WriterBase* syntax)
    {
        QString gcode;
        // TODO: use settings for order
        gcode += m_perimeters->gcode(syntax);
        gcode += m_insets->gcode(syntax);
        gcode += m_skin->gcode(syntax);
        gcode += m_infill->gcode(syntax);
        return gcode;
    }

    QSharedPointer< Region > Island::operator[](RegionType region_type)
    {
        switch (region_type)
        {
            case RegionType::kPerimeter:
                return m_perimeters;
            case RegionType::kInset:
                return m_insets;
            case RegionType::kInfill:
                return m_infill;
            case RegionType::kSkin:
                return m_skin;
        }
        return QSharedPointer< Region >(nullptr);
    }
}  // namespace ORNL
