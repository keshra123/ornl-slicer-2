#include "widgets/settings_category_widgets/nozzle/nozzlebeadwidthcategorywidget.h"

#include "ui_nozzle_beadwidth_category_widget.h"
#include "utilities/constants.h"

namespace ORNL
{
    NozzleBeadWidthCategoryWidget::NozzleBeadWidthCategoryWidget()
        : ui(new Ui::NozzleBeadWidthCategoryWidget)
    {
        ui->setupUi(this);

        ui->default_->label(
            Constants::NozzleSettings::BeadWidth::Labels::kDefault);
        ui->default_->tooltip(
            Constants::NozzleSettings::BeadWidth::Tooltips::kDefault);
        m_distance[Constants::NozzleSettings::BeadWidth::kDefault] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->default_);

        ui->perimeter->label(
            Constants::NozzleSettings::BeadWidth::Labels::kPerimeter);
        ui->perimeter->tooltip(
            Constants::NozzleSettings::BeadWidth::Tooltips::kPerimeter);
        m_distance[Constants::NozzleSettings::BeadWidth::kPerimeter] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->perimeter);

        ui->inset->label(Constants::NozzleSettings::BeadWidth::Labels::kInset);
        ui->inset->tooltip(
            Constants::NozzleSettings::BeadWidth::Tooltips::kInset);
        m_distance[Constants::NozzleSettings::BeadWidth::kInset] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->inset);

        ui->skin->label(Constants::NozzleSettings::BeadWidth::Labels::kSkin);
        ui->skin->tooltip(
            Constants::NozzleSettings::BeadWidth::Tooltips::kSkin);
        m_distance[Constants::NozzleSettings::BeadWidth::kSkin] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->skin);

        ui->infill->label(
            Constants::NozzleSettings::BeadWidth::Labels::kInfill);
        ui->infill->tooltip(
            Constants::NozzleSettings::BeadWidth::Tooltips::kInfill);
        m_distance[Constants::NozzleSettings::BeadWidth::kInfill] =
            QSharedPointer< TemplateUnitsSettingItem >(ui->infill);

        initializeSettings();
    }

    NozzleBeadWidthCategoryWidget::~NozzleBeadWidthCategoryWidget()
    {
        delete ui;
    }

    void NozzleBeadWidthCategoryWidget::handleConditions()
    {
        // TODO
    }
}  // namespace ORNL
