var dir_d71c2fe00ed3b147a8f961a1bf80b6a4 =
[
    [ "cameramanager.cpp", "cameramanager_8cpp.html", "cameramanager_8cpp" ],
    [ "cameramanager.h", "cameramanager_8h.html", "cameramanager_8h" ],
    [ "enginemanager.cpp", "enginemanager_8cpp.html", null ],
    [ "enginemanager.h", "enginemanager_8h.html", [
      [ "EngineManager", "class_o_r_n_l_1_1_engine_manager.html", "class_o_r_n_l_1_1_engine_manager" ]
    ] ],
    [ "globalsettingsmanager.cpp", "globalsettingsmanager_8cpp.html", null ],
    [ "globalsettingsmanager.h", "globalsettingsmanager_8h.html", [
      [ "GlobalSettingsManager", "class_o_r_n_l_1_1_global_settings_manager.html", "class_o_r_n_l_1_1_global_settings_manager" ]
    ] ],
    [ "inputmanager.cpp", "inputmanager_8cpp.html", "inputmanager_8cpp" ],
    [ "inputmanager.h", "inputmanager_8h.html", [
      [ "InputManager", "class_o_r_n_l_1_1_input_manager.html", "class_o_r_n_l_1_1_input_manager" ]
    ] ],
    [ "preferencesmanager.cpp", "preferencesmanager_8cpp.html", null ],
    [ "preferencesmanager.h", "preferencesmanager_8h.html", [
      [ "PreferencesManager", "class_o_r_n_l_1_1_preferences_manager.html", "class_o_r_n_l_1_1_preferences_manager" ]
    ] ],
    [ "projectmanager.cpp", "projectmanager_8cpp.html", null ],
    [ "projectmanager.h", "projectmanager_8h.html", "projectmanager_8h" ],
    [ "windowmanager.cpp", "windowmanager_8cpp.html", null ],
    [ "windowmanager.h", "windowmanager_8h.html", [
      [ "WindowManager", "class_o_r_n_l_1_1_window_manager.html", "class_o_r_n_l_1_1_window_manager" ]
    ] ]
];