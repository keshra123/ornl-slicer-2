#ifndef SLICINGTHREAD_H
#define SLICINGTHREAD_H

#include <QMap>
#include <QSet>
#include <QSharedPointer>
#include <QThread>

class QMutex;
class QWaitCondition;
class QReadWriteLock;

namespace ORNL
{
    class ProjectManager;
    class EngineManager;
    class WindowManager;
    class Mesh;
    class Layer;
    class Island;
    class IslandThread;

    /*!
     * \brief A singleton thread for creating and removing the IslandThread
     * objects that calculate the path planning.
     */
    class SlicingThread : public QThread
    {
        Q_OBJECT
    public:
        //! \brief Returns the singleton of the SlicingThread
        static QSharedPointer< SlicingThread > getInstance();

        //! \brief Destructor
        ~SlicingThread();

    signals:
        //! \brief Signal that slicing is starting
        void sliceStarting();

        //! \brief Signal containting the new total for the progress bar
        void total(int total);

        //! \brief Signal containing the progress of the current slicing
        void progress(int complete, int total);

        //! \brief Signal the slicing has finished
        void sliceFinished();

        //! \brief Signal that there has been an error
        void error(QString error_message);

    public slots:
        //! \brief Handles if the global settings were modified, so reslice all
        //! islands
        void settingsUpdated();

        //! \brief Handles if mesh settings were modified, so reslice all
        //! islands for that mesh
        void settingsUpdated(QSharedPointer< Mesh > mesh);

        //! \brief Handles if layer settings were modified, so reslice all
        //! islands for that layer
        void settingsUpdated(QSharedPointer< Layer > layer);

        //! \brief Handles creating the IslandThread objects for when a new mesh
        //! is added
        void meshAdded(QSharedPointer< Mesh > mesh);

        //! \brief Handles interrupting and removing the IslandThread objects
        //! from a mesh that is removed
        void meshRemoved(QSharedPointer< Mesh > mesh);

        //! \brief Handles updating the progress for when an IslandThread object
        //! finishes running
        void islandThreadFinished(QSharedPointer< Island > mesh);

        /*! \brief Handles updating the progress for when an IslandThread object
         * is interrupted. Also, if run is waiting for interruptions, wakes it
         * if this was the last IslandThread object to be interrupted.
         */
        void islandThreadInterrupted(QSharedPointer< Island > island);

    protected:
        //! \brief Main function which starts more island threads
        void run() override;

    private:
        //! \brief Constructor
        SlicingThread(QObject* parent = nullptr);

        static QSharedPointer< SlicingThread > m_singleton;

        QSharedPointer< ProjectManager > m_project_manager;
        QSharedPointer< WindowManager > m_window_manager;

        QSharedPointer< QMutex > m_mutex;
        QSharedPointer< QWaitCondition > m_condition;
        bool m_restart;
        bool m_abort;

        // locks
        QSharedPointer< QReadWriteLock > m_mesh_lock;

        // queues
        QSet< QSharedPointer< Island > > m_slice_islands_queue;
        QSet< QSharedPointer< Island > > m_remove_islands_queue;
        QSet< QSharedPointer< Island > > m_request_interrupt_queue;

        // progress
        QMap< QSharedPointer< Island >, QSharedPointer< IslandThread > >
            m_island_threads;
        QMap< QSharedPointer< Island >, bool > m_island_threads_running;
        int m_complete;
        int m_total;
    };
}  // namespace ORNL
#endif  // SLICINGTHREAD_H
