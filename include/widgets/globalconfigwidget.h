#ifndef GLOBALCONFIGSWIDGET_H
#define GLOBALCONFIGSWIDGET_H

//! \file globalconfigswidget.h

#include <QMultiMap>
#include <QWidget>
#include <json.hpp>

namespace Ui
{
    class GlobalConfigWidget;
}

namespace ORNL
{
    class SettingsBase;
    class PreferencesManager;
    class WindowManager;
    class GlobalSettingsManager;
    class SettingsCategoryWidgetBase;
    enum class ConfigTypes : uint8_t;

    /*!
     * \class GlobalConfigWidget
     * \brief Widget for modfying configs
     */
    class GlobalConfigWidget : public QWidget
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        explicit GlobalConfigWidget(QWidget* parent = nullptr);

        //! \brief Destructor
        ~GlobalConfigWidget();

        //! \brief Sets which Config Type this widget is for
        void setConfigType(ConfigTypes ct);

        //! \brief Reload configs list
        void reload();

        //! \brief Set the name of the current config
        void setCurrentConfig(QString config);

        //! \brief Load the first config
        void loadFirstOption();

    public slots:
        //! \brief Reload settings display based on category
        void handleCategorySelectionChanged();

        //! \brief Reload settings display based on config
        void handleConfigSelectionChanged();

        //! \brief Reload settings display based on nozzle config
        void handleNozzleSelectionChanged();

        //! \brief Sets the category list based on the config type
        void updateCategoryList();

        //! \brief Add new config
        void handleAddConfig();

        //! \brief Save config to file
        void handleSaveConfig();

        //! \brief Delete config
        void handleDeleteConfig();

        //! \brief config has been modified (and so it gets marked)
        void handleModified();

        //! \brief updates the dropdowns with any new configs
        void update();

    private:
        Ui::GlobalConfigWidget* ui;

        ConfigTypes m_config_type;
        QStringList m_configs_names;
        QString m_current_config;

        QStringList m_categories_names;
        QString m_current_category;

        QMultiMap< QString, QString >
            m_nozzle_names;        //!< Only used in the material tab
        QString m_current_nozzle;  //!< Only used in the material tab

        QMap< QString, QSharedPointer< SettingsCategoryWidgetBase > >
            m_category_widgets;
        QMap< QString, QSharedPointer< SettingsBase > > m_temp_settings;
        QMap< QString, QSharedPointer< nlohmann::json > > m_undo_histories;
        QMap< QString, QSharedPointer< nlohmann::json > > m_redo_histories;
        QMap< QString, bool > m_modified;

        QSharedPointer< GlobalSettingsManager > m_settings_manager;
        QSharedPointer< WindowManager > m_window_manager;
        QSharedPointer< PreferencesManager > m_preferences_manager;
    };  // class GlobalConfigsWidget
}  // namespace ORNL
#endif  // GLOBALCONFIGSWIDGET_H
