#ifndef GCODECONTROLLER_H
#define GCODECONTROLLER_H

#include <QFile>
#include <QObject>

#include "gcode/gcode_command.h"
#include "gcode/gcode_parser.h"
#include "loaders/gcode_loader.h"

namespace ORNL
{
    // TODO: Selection command.
    // TODO: Need a selection signal/slot
    // TODO:

    class GcodeTextBoxWidget;

    //! \class GcodeController
    //! \brief This class is the controller/model for the MVC design paradigm
    //! for use with the
    //!        Gcode textbox and 2D and 3D model viewers.
    class GcodeController : public QObject
    {
        Q_OBJECT
    public:
        // TODO: Add the 2d tab and the 3d tab to this later.
        //! \brief Default constructor for the GcodeController.
        //! \param parent Parent of the Gcodecontroller object.
        //! \param textbox Textbox that will be send/recieve editing data.
        GcodeController(QObject* parent             = nullptr,
                        GcodeTextBoxWidget* textbox = nullptr);

        //! \brief Destructor for the GcodeController.
        ~GcodeController();

        //! \brief Method to send out to other views that a selection of the
        //! Gcode has been made. \param line_number Line number in which the
        //! selection has been made.
        void selectionMade(int line_number);

    public slots:

        //! \brief Sets the textbox to represent the Gcodestrings provided in
        //! the list,
        //!        and parses the strings based on the specified syntax.
        //! \param gcode_strings List of strings to set the textbox to and to be
        //! parsed. \param gcode_syntax Gcode syntax to parse the strings with.
        void setGcode(QStringList gcode_strings, GcodeSyntax gcode_syntax);

        //! \brief Slot that gets called everytime the text within the textbox
        //! is modified.
        //!        This function simply updates the line in which the text was
        //!        changed based on the location of the cursor.
        void textboxTextChanged();

        //! \brief Slot that is called everytime the number of lines within the
        //! textbox is
        //!        changed. This funciton updates the internal string model
        //!        while also reparsing and strings that were changed or
        //!        replaced.
        //! \param newBlockCount The number of lines within the textbox.
        void textboxBlockCountChanged(int newBlockCount);


    signals:
        //! \brief Signal emitted whenever the text has been changed and the
        //! List of
        //!        strings corresponding to it.
        //! \param QStringList the list of strings that have been modified.
        void textChanged(QStringList);
        //        void selectionMade(int);
    private:
        int m_textbox_blockcount;
        QFile m_file;

        QObject* parent;
        GcodeTextBoxWidget* m_textbox;
        GcodeParser m_parser;

        QStringList m_gcode_strings;
        QVector< GcodeCommand > m_gcode_commands;
        // TODO: Make GcodeVisual data class.
        //    QVector<GcodeVisual> m_gcode_visual_commands;
    };

}  // namespace ORNL

#endif  // GCODECONTROLLER_H
