#ifndef TST_SANITY_EXAMPLE_H
#define TST_SANITY_EXAMPLE_H

#include <QtTest>
#include "auto_test.h"

using namespace AutoTest;

/*!
 * \class TestSanityExample
 *
 * \brief An example test class for copy-pasting to get started.
 */
class TestSanityExample : public QObject
{
    Q_OBJECT

private slots:

    void initTestCase()
    {
        qDebug("Checking sanity...");
    }

    //! \brief Tests that 2+2 = fish.
    void testFish();

    //! \brief Test that the seal knows how to throw.
    void testSealError();

    void cleanupTestCase()
    {
        qDebug("Sanity has been checked.");
    }

// public:
//     TestSanityExample() {};
//     ~TestSanityExample() {};
};

DECLARE_TEST(TestSanityExample)

#endif // TST_SANITY_EXAMPLE_H
