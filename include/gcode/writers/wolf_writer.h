#ifndef WOLF_H
#define WOLF_H

//! \file wolf.h

#include "gcode/writers/writer_base.h"

namespace ORNL
{
    /*!
     * \class WolfWriter
     * \brief The gcode writer for the Wolf Syntax
     */
    class WolfWriter : public WriterBase
    {
    public:
        //! \brief Constructor
        WolfWriter() = default;

        //! \brief Write the header to the file
        QString header();

        //! \brief Write the line for a layer change
        QString onLayerChange();

        //! \brief Write the footer to the file
        QString footer();

        //! \brief Write a path segment
        QString pathSegment(PathSegment& path_segment);

        //! \brief write a line path
        QString line(Line& line);

        //! \brief write an arc path
        QString arc(Arc& arc);

        //! \brief Write a comment
        QString comment(QString text);
    };  // class WolfWriter
}  // namespace ORNL
#endif  // WOLF_H
