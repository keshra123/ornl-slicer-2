#include "widgets/globalconfigwidget.h"

#include "configs/display_config_base.h"
#include "managers/global_settings_manager.h"
#include "managers/preferences_manager.h"
#include "managers/window_manager.h"
#include "ui_global_config_widget.h"
#include "utilities/enums.h"
#include "widgets/settings_category_widgets/settingcategorywidgets.h"

namespace ORNL
{
    GlobalConfigWidget::GlobalConfigWidget(QWidget* parent)
        : QWidget(parent)
        , ui(new Ui::GlobalConfigWidget)
        , m_settings_manager(GlobalSettingsManager::getInstance())
        , m_window_manager(WindowManager::getInstance())
        , m_preferences_manager(PreferencesManager::getInstance())
    {
        ui->setupUi(this);

        connect(
            ui->add_button, SIGNAL(released()), this, SLOT(handleAddConfig()));
        connect(ui->save_button,
                SIGNAL(released()),
                this,
                SLOT(handleSaveConfig()));
        connect(ui->delete_button,
                SIGNAL(released()),
                this,
                SLOT(handleDeleteConfig()));
    }

    GlobalConfigWidget::~GlobalConfigWidget()
    {
        delete ui;
    }

    void GlobalConfigWidget::setConfigType(ConfigTypes config_type)
    {
        m_config_type = config_type;

        if (config_type == ConfigTypes::kNozzle)
        {
            // Fills config dropdown
            m_configs_names =
                m_settings_manager->getConfigsNames(ConfigTypes::kMaterial);
            ui->config_names->clear();
            ui->config_names->addItems(m_configs_names);
            m_current_config = ui->config_names->currentText();

            // Fills nozzle dropdown
            m_nozzle_names = m_settings_manager->getNozzleNames();
            ui->nozzle_names->clear();
            QStringList nozzle_names = m_nozzle_names.values(m_current_config);
            ui->nozzle_names->addItems(nozzle_names);
            m_current_nozzle = ui->nozzle_names->currentText();
        }
        else
        {
            ui->nozzle_names->hide();

            // Fills config dropdown
            m_configs_names = m_settings_manager->getConfigsNames(config_type);
            ui->config_names->clear();
            ui->config_names->addItems(m_configs_names);
        }

        updateCategoryList();
        if (config_type == ConfigTypes::kNozzle)
        {
            connect(ui->nozzle_names,
                    SIGNAL(currentIndexChanged(QString)),
                    this,
                    SLOT(handleNozzleSelectionChanged()));
        }
        connect(ui->config_names,
                SIGNAL(currentIndexChanged(QString)),
                this,
                SLOT(handleConfigSelectionChanged()));
        connect(ui->categories_list,
                SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
                this,
                SLOT(handleCategorySelectionChanged()));
    }

    void GlobalConfigWidget::handleCategorySelectionChanged()
    {
        // Hide previous category
        if (!m_current_category.isEmpty() &&
            m_category_widgets.contains(m_current_category))
        {
            m_category_widgets[m_current_category]->hide();
        }

        QListWidgetItem* lwi = ui->categories_list->currentItem();

        if (!lwi)
        {
            return;
        }

        m_current_category = lwi->text();

        if (m_current_category.isEmpty() ||
            (m_config_type == ConfigTypes::kNozzle &&
             m_current_nozzle.isEmpty()))
        {
            return;
        }

        // update settings
        if (!m_current_config.isEmpty())
        {
            if (m_config_type == ConfigTypes::kNozzle)
            {
                if (!m_current_nozzle.isEmpty())
                {
                    QString current_key =
                        m_current_config + ":" + m_current_nozzle;

                    m_category_widgets[m_current_category]->setSettings(
                        m_temp_settings[current_key],
                        m_undo_histories[current_key],
                        m_redo_histories[current_key]);
                    m_category_widgets[m_current_category]->load();
                }
            }
            else
            {
                m_category_widgets[m_current_category]->setSettings(
                    m_temp_settings[m_current_config],
                    m_undo_histories[m_current_config],
                    m_redo_histories[m_current_config]);
                m_category_widgets[m_current_category]->load();
            }
        }

        m_category_widgets[m_current_category]->show();
    }

    void GlobalConfigWidget::handleConfigSelectionChanged()
    {
        m_current_config = ui->config_names->currentText();

        if (m_current_config.isEmpty())
        {
            return;
        }

        if (m_config_type == ConfigTypes::kNozzle)
        {
            QStringList nozzles = m_nozzle_names.values(m_current_config);
            ui->nozzle_names->clear();
            ui->nozzle_names->addItems(nozzles);
            handleNozzleSelectionChanged();
            handleCategorySelectionChanged();
            return;
        }

        // Add to maps if the current config hasn't been used before with the
        // current widget
        if (!m_temp_settings.contains(m_current_config))
        {
            m_undo_histories[m_current_config] =
                QSharedPointer< json >(new json());
            m_redo_histories[m_current_config] =
                QSharedPointer< json >(new json());
            m_temp_settings[m_current_config] =
                QSharedPointer< SettingsBase >(new SettingsBase(
                    m_settings_manager
                        ->getConfig(m_config_type, m_current_config)
                        .data()));
        }

        // Update settings display
        if (!m_current_category.isEmpty())
        {
            m_category_widgets[m_current_category]->setSettings(
                m_temp_settings[m_current_config],
                m_undo_histories[m_current_config],
                m_redo_histories[m_current_config]);
            m_category_widgets[m_current_category]->load();
        }

        if (!m_modified.contains(m_current_config))
        {
            m_modified[m_current_config] = false;
        }
        handleModified();
    }

    void GlobalConfigWidget::handleNozzleSelectionChanged()
    {
        m_current_nozzle = ui->nozzle_names->currentText();
        if (m_current_nozzle.isEmpty())
        {
            return;
        }

        QString current_key = m_current_config + ":" + m_current_nozzle;

        // Add to maps if the current config hasn't been used before with the
        // current widget
        if (!m_temp_settings.contains(current_key))
        {
            m_undo_histories[current_key] = QSharedPointer< json >(new json());
            m_redo_histories[current_key] = QSharedPointer< json >(new json());
            m_temp_settings[current_key] =
                QSharedPointer< SettingsBase >(new SettingsBase(
                    m_settings_manager
                        ->getConfig(ConfigTypes::kNozzle, current_key)
                        .data()));
        }

        // Update settings display
        if (!m_current_category.isEmpty())
        {
            m_category_widgets[m_current_category]->setSettings(
                m_temp_settings[current_key],
                m_undo_histories[current_key],
                m_redo_histories[current_key]);
            m_category_widgets[m_current_category]->load();
        }

        if (!m_modified.contains(current_key))
        {
            m_modified[current_key] = false;
        }
        ui->save_button->setEnabled(m_modified[current_key]);

        handleModified();
    }

    void GlobalConfigWidget::updateCategoryList()
    {
        // Hide previous category
        if (!m_current_category.isEmpty() &&
            m_category_widgets.contains(m_current_category))
        {
            m_category_widgets[m_current_category]->hide();
        }

        // Fill category list and widget map
        ui->categories_list->clear();
        switch (m_config_type)
        {
            case ConfigTypes::kMachine:
                ui->categories_list->addItems(
                    Constants::MachineSettings::kCategories);
                m_categories_names = Constants::MachineSettings::kCategories;
                m_category_widgets
                    [Constants::MachineSettings::kDimensionsCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new MachineDimensionsCategoryWidget());
                m_category_widgets
                    [Constants::MachineSettings::kSyntaxCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new MachineSyntaxCategoryWidget());
                break;
            case ConfigTypes::kMaterial:
                ui->categories_list->addItems(
                    Constants::MaterialSettings::kCategories);
                m_categories_names = Constants::MaterialSettings::kCategories;
                m_category_widgets
                    [Constants::MaterialSettings::kPropertiesCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new MaterialPropertiesCategoryWidget());
                break;
            case ConfigTypes::kNozzle:
                ui->categories_list->addItems(
                    Constants::NozzleSettings::kCategories);
                m_categories_names = Constants::NozzleSettings::kCategories;
                m_category_widgets[Constants::NozzleSettings::kLayerCategory] =
                    QSharedPointer< SettingsCategoryWidgetBase >(
                        new NozzleLayerCategoryWidget());
                m_category_widgets
                    [Constants::NozzleSettings::kBeadWidthCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new NozzleBeadWidthCategoryWidget());
                m_category_widgets[Constants::NozzleSettings::kSpeedCategory] =
                    QSharedPointer< SettingsCategoryWidgetBase >(
                        new NozzleSpeedCategoryWidget());
                m_category_widgets
                    [Constants::NozzleSettings::kAccelerationCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new NozzleAccelerationCategoryWidget());
                break;
            case ConfigTypes::kPrint:
                ui->categories_list->addItems(
                    Constants::PrintSettings::kCategories);
                m_categories_names = Constants::PrintSettings::kCategories;
                m_category_widgets
                    [Constants::PrintSettings::kPerimetersCategory] =
                        QSharedPointer< SettingsCategoryWidgetBase >(
                            new PrintPerimeterCategoryWidget());
                m_category_widgets[Constants::PrintSettings::kInsetsCategory] =
                    QSharedPointer< SettingsCategoryWidgetBase >(
                        new PrintInsetsCategoryWidget());
                m_category_widgets[Constants::PrintSettings::kInfillCategory] =
                    QSharedPointer< SettingsCategoryWidgetBase >(
                        new PrintInfillCategoryWidget());
                m_category_widgets[Constants::PrintSettings::kSkinsCategory] =
                    QSharedPointer< SettingsCategoryWidgetBase >(
                        new PrintSkinCategoryWidget());
                break;
        }

        // Add all the widgets and hide them
        for (QSharedPointer< SettingsCategoryWidgetBase > scwb :
             m_category_widgets.values())
        {
            ui->rightVerticalLayout->addWidget(scwb.data());
            scwb->hide();
            scwb->setGlobal(true);
            connect(
                scwb.data(), SIGNAL(modified()), this, SLOT(handleModified()));
        }

        // Set the current category
        ui->categories_list->setCurrentRow(0);
        QListWidgetItem* lwi = ui->categories_list->currentItem();
        if (!lwi)
        {
            return;
        }
        m_current_category = lwi->text();
    }

    void GlobalConfigWidget::handleAddConfig()
    {
        if (m_config_type == ConfigTypes::kNozzle)
        {
            m_window_manager->createNameConfigDialog(m_current_config);
        }
        else
        {
            m_window_manager->createNameConfigDialog(m_config_type);
        }
    }

    void GlobalConfigWidget::handleSaveConfig()
    {
        QString current_config;
        if (m_config_type == ConfigTypes::kNozzle)
        {
            current_config = m_current_config + ":" + m_current_nozzle;
        }
        else
        {
            current_config = m_current_config;
        }

        QSharedPointer< SettingsBase > sb =
            m_settings_manager->getConfig(m_config_type, current_config);
        sb->update(m_temp_settings[current_config]);

        m_temp_settings.remove(current_config);
        m_temp_settings[current_config] =
            QSharedPointer< SettingsBase >(new SettingsBase(sb.data()));

        // Should undo and redo be reset when saved?

        m_settings_manager->saveConfig(m_config_type, current_config);

        m_modified[current_config] = false;
        ui->save_button->setEnabled(false);
    }

    void GlobalConfigWidget::handleDeleteConfig()
    {
        if (m_config_type == ConfigTypes::kNozzle)
        {
            QString current_config = m_current_config + ":" + m_current_nozzle;
            m_temp_settings.remove(current_config);
            m_settings_manager->removeConfig(m_config_type, current_config);
            handleNozzleSelectionChanged();
        }
        else
        {
            m_temp_settings.remove(m_current_config);
            m_settings_manager->removeConfig(m_config_type, m_current_config);
            handleConfigSelectionChanged();
        }
    }

    void GlobalConfigWidget::handleModified()
    {
        QString current_config;
        if (m_config_type == ConfigTypes::kNozzle)
        {
            current_config = m_current_config + ":" + m_current_nozzle;
        }
        else
        {
            current_config = m_current_config;
        }

        // Check if any values have been modified
        bool modified = false;
        json json_    = m_temp_settings[current_config]->json();
        if (!json_.empty())
        {
            for (json::iterator iter = json_.begin(); iter != json_.end();
                 iter++)
            {
                QString key = QString(iter.key().c_str());
                modified |=
                    m_temp_settings[current_config]->contains(key, false);
                if (modified)
                {
                    break;
                }
            }
        }

        m_modified[current_config] = modified;
        ui->save_button->setEnabled(modified);
    }

    void GlobalConfigWidget::setCurrentConfig(QString config)
    {
        if (m_config_type == ConfigTypes::kNozzle)
        {
            int colon        = config.indexOf(':');
            QString material = config.left(colon);
            disconnect(ui->config_names,
                       SIGNAL(currentIndexChanged(QString)),
                       this,
                       SLOT(handleConfigSelectionChanged()));
            ui->config_names->setCurrentText(material);
            connect(ui->config_names,
                    SIGNAL(currentIndexChanged(QString)),
                    this,
                    SLOT(handleConfigSelectionChanged()));
            m_current_config = material;

            QString nozzle      = config.mid(colon + 1);
            QStringList nozzles = m_nozzle_names.values(material);
            ui->nozzle_names->addItems(nozzles);
            ui->nozzle_names->setCurrentText(nozzle);
            m_current_nozzle = nozzle;
            handleNozzleSelectionChanged();
        }
        else
        {
            ui->config_names->setCurrentText(config);
            handleConfigSelectionChanged();
        }
        handleCategorySelectionChanged();
    }

    void GlobalConfigWidget::loadFirstOption()
    {
        if (m_config_type == ConfigTypes::kNozzle)
        {
            m_current_config = ui->config_names->currentText();

            QStringList nozzles = m_nozzle_names.values(m_current_config);
            ui->nozzle_names->addItems(nozzles);
            m_current_nozzle = ui->config_names->currentText();
            handleNozzleSelectionChanged();
        }
        else
        {
            handleConfigSelectionChanged();
        }
        handleCategorySelectionChanged();
    }

    void GlobalConfigWidget::update()
    {
        QStringList options;
        QString current;
        switch (m_config_type)
        {
            case ConfigTypes::kMachine:
            case ConfigTypes::kPrint:
            case ConfigTypes::kMaterial:
                m_configs_names =
                    m_settings_manager->getConfigsNames(m_config_type);
                current = ui->config_names->currentText();
                ui->config_names->clear();
                ui->config_names->addItems(m_configs_names);
                if (m_configs_names.contains(current))
                {
                    ui->config_names->setCurrentText(current);
                }
                handleConfigSelectionChanged();
                break;
            case ConfigTypes::kNozzle:
                m_configs_names =
                    m_settings_manager->getConfigsNames(ConfigTypes::kMaterial);
                current = ui->config_names->currentText();

                m_nozzle_names         = m_settings_manager->getNozzleNames();
                QString current_nozzle = ui->nozzle_names->currentText();

                ui->config_names->clear();
                ui->config_names->addItems(m_configs_names);
                if (m_configs_names.contains(current))
                {
                    ui->config_names->setCurrentText(current);
                }
                else
                {
                    current = ui->config_names->currentText();
                }

                QStringList nozzles = m_nozzle_names.values(current);
                ui->nozzle_names->clear();
                ui->nozzle_names->addItems(nozzles);
                if (nozzles.contains(current_nozzle))
                {
                    ui->nozzle_names->setCurrentText(current_nozzle);
                }

                handleNozzleSelectionChanged();
                break;
        }
    }
}  // namespace ORNL
