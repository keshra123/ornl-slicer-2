#ifndef UNIT_H
#define UNIT_H

//! \file unit.h

#include <QMetaType>
#include <QString>
#include <QtMath>
#include <json.hpp>
#include <ostream>
#include <sstream>
#include <string>
using json = nlohmann::json;

namespace ORNL
{
    using NT = float;  //!< \typedef Number Type
    // typedef float NT; //!< \typedef Number Type

    /*!
     * \class Unit
     * \brief Unit aware variable
     */
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    class Unit
    {
    public:
        Unit(NT value_ = NT(0))
            : m_value(value_)
        {}

        /*!
         * This turns the class into a function object that allows
         * the user to easily get at the value.
         */
        NT operator()() const
        {
#if UNITS_FUNC
#    warning("This function returns the internal value for the units class. Only use if this is exactly what you want.")
#endif
            return m_value;
        }
#define UNITS_FUNC true
        /*!
         * Helper function to get a text representation of the
         * object's dimensions.  It is static because the
         * representation is known at compile time.
         */
        static std::string dim(void)
        {
            std::stringstream s;
            s << "<" << U1 << "," << U2 << "," << U3 << "," << U4 << "," << U5
              << "," << U6 << ">";
            return s.str();
        }

        //! Helper function for unit conversions.
        NT to(const Unit& u) const
        {
            return m_value / u.m_value;
        }

        Unit& operator=(const Unit& rhs)
        {
            m_value = rhs.m_value;
            return *this;
        }

        // Arithmetic operators
        Unit& operator+=(const Unit& rhs)
        {
            m_value += rhs.m_value;
            return *this;
        }

        Unit& operator-=(const Unit& rhs)
        {
            m_value -= rhs.m_value;
            return *this;
        }

        Unit& operator*=(const NT& rhs)
        {
            m_value *= rhs;
            return *this;
        }

        Unit& operator/=(const NT& rhs)
        {
            m_value /= rhs;
            return *this;
        }

    protected:
        NT m_value;
    };  // class Unit


    // Addition
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator+(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return Unit< U1, U2, U3, U4, U5, U6 >(lhs() + rhs());
    }


    // Subtraction
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator-(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return Unit< U1, U2, U3, U4, U5, U6 >(lhs() - rhs());
    }

    // Negation
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator-(
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return Unit< U1, U2, U3, U4, U5, U6 >(-rhs());
    }


    // Multiplication
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator*(
        const NT& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return Unit< U1, U2, U3, U4, U5, U6 >(lhs * rhs());
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator*(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const NT& rhs)
    {
        return rhs * lhs;
    }

    template < int U1a,
               int U2a,
               int U3a,
               int U4a,
               int U5a,
               int U6a,
               int U1b,
               int U2b,
               int U3b,
               int U4b,
               int U5b,
               int U6b >
    const Unit< U1a + U1b,
                U2a + U2b,
                U3a + U3b,
                U4a + U4b,
                U5a + U5b,
                U6a + U6b >
    operator*(const Unit< U1a, U2a, U3a, U4a, U5a, U6a >& lhs,
              const Unit< U1b, U2b, U3b, U4b, U5b, U6b >& rhs)
    {
        return Unit< U1a + U1b,
                     U2a + U2b,
                     U3a + U3b,
                     U4a + U4b,
                     U5a + U5b,
                     U6a + U6b >(lhs() * rhs());
    }


    // Division
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< U1, U2, U3, U4, U5, U6 > operator/(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const NT& rhs)
    {
        return Unit< U1, U2, U3, U4, U5, U6 >(lhs() / rhs);
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    const Unit< -U1, -U2, -U3, -U4, -U5, -U6 > operator/(
        const NT& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return Unit< -U1, -U2, -U3, -U4, -U5, -U6 >(lhs / rhs());
    }

    template < int U1a,
               int U2a,
               int U3a,
               int U4a,
               int U5a,
               int U6a,
               int U1b,
               int U2b,
               int U3b,
               int U4b,
               int U5b,
               int U6b >
    const Unit< U1a - U1b,
                U2a - U2b,
                U3a - U3b,
                U4a - U4b,
                U5a - U5b,
                U6a - U6b >
    operator/(const Unit< U1a, U2a, U3a, U4a, U5a, U6a >& lhs,
              const Unit< U1b, U2b, U3b, U4b, U5b, U6b >& rhs)
    {
        return Unit< U1a - U1b,
                     U2a - U2b,
                     U3a - U3b,
                     U4a - U4b,
                     U5a - U5b,
                     U6a - U6b >(lhs() / rhs());
    }


    // Comparisons
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator==(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                    const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return fabs(lhs() - rhs()) < std::numeric_limits< NT >::epsilon();
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator==(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return fabs(lhs() - rhs) < std::numeric_limits< NT >::epsilon();
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator==(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return fabs(lhs - rhs()) < std::numeric_limits< NT >::epsilon();
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator!=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                    const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return not(lhs() == rhs());
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator!=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return not(lhs() == rhs);
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator!=(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return not(lhs == rhs());
    }


    // Ordering
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                    const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return lhs() <= rhs() || lhs == rhs;
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return (lhs() <= rhs) || lhs == rhs;
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<=(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return (lhs <= rhs()) || lhs == rhs;
    }


    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                    const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return lhs() >= rhs() || lhs == rhs;
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>=(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return (lhs() >= rhs) || lhs == rhs;
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>=(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return (lhs >= rhs()) || lhs == rhs;
    }


    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                   const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return lhs() < rhs();
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return (lhs() < rhs);
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator<(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return (lhs < rhs());
    }


    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>(const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
                   const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return lhs() > rhs();
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, const NT& rhs)
    {
        return (lhs() > rhs);
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    bool operator>(const NT& lhs, const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return (lhs > rhs());
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    std::ostream& operator<<(std::ostream& s,
                             const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return s << rhs();
    }

    // math operations
    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    Unit< U1, U2, U3, U4, U5, U6 > max(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return std::max(lhs(), rhs());
    }

    template < int U1,
               int U2,
               int U3,
               int U4,
               int U5,
               int U6,
               typename... Args >
    typename std::enable_if< 1 < sizeof...(Args),
                             Unit< U1, U2, U3, U4, U5, U6 > >::type
    max(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, Args... rhs)
    {
        return ORNL::max(lhs, ORNL::max(rhs...));
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    Unit< U1, U2, U3, U4, U5, U6 > min(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs,
        const Unit< U1, U2, U3, U4, U5, U6 >& rhs)
    {
        return std::min(lhs(), rhs());
    }

    template < int U1,
               int U2,
               int U3,
               int U4,
               int U5,
               int U6,
               typename... Args >
    typename std::enable_if< 1 < sizeof...(Args),
                             Unit< U1, U2, U3, U4, U5, U6 > >::type
    min(const Unit< U1, U2, U3, U4, U5, U6 >& lhs, Args... rhs)
    {
        return ORNL::min(lhs, ORNL::min(rhs...));
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    Unit< U1 / 2, U2 / 2, U3 / 2, U4 / 2, U5 / 2, U6 / 2 > sqrt(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs)
    {
        return std::sqrt(lhs());
    }

    template < int exponent, int U1, int U2, int U3, int U4, int U5, int U6 >
    auto pow(const Unit< U1, U2, U3, U4, U5, U6 >& base)
        -> Unit< U1 * exponent,
                 U2 * exponent,
                 U3 * exponent,
                 U4 * exponent,
                 U5 * exponent,
                 U6 * exponent >
    {
        return std::pow(base(), exponent);
    }

    template < int U1, int U2, int U3, int U4, int U5, int U6 >
    Unit< U1, U2, U3, U4, U5, U6 > abs(
        const Unit< U1, U2, U3, U4, U5, U6 >& lhs)
    {
        return std::fabs(lhs());
    }

    // operator>> is not provided because the unit type can not be
    // created at runtime in any reasonable fashion.  This means there is
    // no easy way to serialize unit objects.
    //
    // If you need to read in an object from a stream, read it into an NT
    // variable and put it into an appropriate-type variable.  Example:
    //
    //      NT x;
    //      cin >> x;
    //      Length y = x*m;
    //
    // where the base unit m has already been defined.  This requires you
    // to i) know the unit type at compile time and ii) assume its value
    // is in terms of the base type.

    /*!
     * \class Distance
     *
     * \brief Unit class for distance
     */
    class Distance : public Unit< 1, 0, 0, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Distance() = default;

        //! \brief Constructor
        Distance(NT value);

        //! \brief Conversion Constructor
        Distance(const Unit< 1, 0, 0, 0, 0, 0 >& u);

        //! \brief Returns the string representation of this unit (e.g. Inch)
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Distance fromString(QString str);
    };
    void to_json(json& j, const Distance& d);
    void from_json(const json& j, Distance& d);

    /*!
     * \class Time
     *
     * \brief Unit class for time
     */
    class Time : public Unit< 0, 1, 0, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Time() = default;

        //! \brief Constructor
        Time(NT value);

        Time(const Unit< 0, 1, 0, 0, 0, 0 >& u);

        //! \brief Returns the string representation of this unit
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Time fromString(QString str);
    };
    void to_json(json& j, const Time& t);

    void from_json(const json& j, Time& t);

    /*!
     * \class Mass
     *
     * \brief Unit class for mass
     */
    class Mass : public Unit< 0, 0, 1, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Mass() = default;

        //! \brief Constructor
        Mass(NT value);

        //! \brief Conversion Constructor
        Mass(const Unit< 0, 0, 1, 0, 0, 0 >& u);

        //! \brief Returns the string representation of this unit
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Mass fromString(QString str);
    };
    void to_json(json& j, const Mass& m);
    void from_json(const json& j, Mass& m);

    // Typedefs for derived units
    /*!
     * \class Velocity
     *
     * \brief Unit class for velocity
     */
    class Velocity : public Unit< 1, -1, 0, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Velocity() = default;

        //! \brief Constructor
        Velocity(NT value);

        //! \brief Conversion Constructor
        Velocity(const Unit< 1, -1, 0, 0, 0, 0 >& u);

        //! \brief Returns the string representation of this unit
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Velocity fromString(QString str);
    };
    void to_json(json& j, const Velocity& v);
    void from_json(const json& j, Velocity& v);

    /*!
     * \class Acceleration
     *
     * \brief Unit class for acceleration
     */
    class Acceleration : public Unit< 1, -2, 0, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Acceleration() = default;

        //! \brief Constructor
        Acceleration(NT value);

        //! \brief Conversion Constructor
        Acceleration(const Unit< 1, -2, 0, 0, 0, 0 >& u);

        //! \brief Returns the string representation of this unit
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Acceleration fromString(QString str);
    };
    void to_json(json& j, const Acceleration& a);
    void from_json(const json& j, Acceleration& a);

    /*!
     * \class Angle
     *
     * \brief Angle is a special unit that implements a wrap around feature
     * keeping it between 0 and 2 pi
     */
    class Angle : public Unit< 0, 0, 0, 0, 0, 1 >
    {
    public:
        //! \brief Default Constructor
        Angle() = default;

        //! \brief Constructor
        Angle(NT value);

        //! \brief Conversion Constructor
        Angle(const Unit< 0, 0, 0, 0, 0, 1 >& u);

        //! \brief Returns the string representation of this unit (e.g. Degree)
        QString toString();

        //! \brief Returns the Distance object for the given string
        static Angle fromString(QString str);
    };

    void to_json(json& j, const Angle& a);
    void from_json(const json& j, Angle& a);

    //! \typedef AngularVelocity
    typedef Unit< 0, -1, 0, 0, 0, 1 > AngularVelocity;
    void to_json(json& j, const AngularVelocity& v);
    void from_json(const json& j, AngularVelocity& v);

    //! \typedef AngularAcceleration
    typedef Unit< 0, -2, 0, 0, 0, 1 > AngularAcceleration;
    void to_json(json& j, const AngularAcceleration& a);
    void from_json(const json& j, AngularAcceleration& a);

    //! \typedef Area
    class Area : public Unit< 2, 0, 0, 0, 0, 0 >
    {
    public:
        //! \brief Default Constructor
        Area() = default;

        //! \brief Constructor
        Area(NT value);

        //! \brief Conversion Constructor
        Area(Unit< 2, 0, 0, 0, 0, 0 > u);
    };
    void to_json(json& j, const Area& a);
    void from_json(const json& j, Area& a);

    //! \typedef Volume
    typedef Unit< 3, 0, 0, 0, 0, 0 > Volume;
    void to_json(json& j, const Volume& v);
    void from_json(const json& j, Volume& v);

    typedef Unit< 0, 0, 0, 1, 0, 0 > Current;
    typedef Unit< 0, 0, 0, 0, 1, 0 > Temperature;

    typedef Unit< -3, 0, 1, 0, 0, 0 > Density;
    typedef Unit< 1, -3, 0, 0, 0, 0 > Jerk;
    typedef Unit< 1, -2, 1, 0, 0, 0 > Force;
    typedef Unit< 0, -1, 0, 0, 0, 0 > Frequency;
    typedef Unit< -1, -2, 1, 0, 0, 0 > Pressure;
    typedef Unit< 2, -2, 1, 0, 0, 0 > Torque;
    typedef Unit< 1, -2, 1, 0, 0, 0 > Weight;
    typedef Unit< 2, -2, 1, 0, 0, 0 > Energy;
    typedef Unit< 2, -2, 1, 0, 0, 0 > Work;
    typedef Unit< 0, 1, 0, 1, 0, 0 > Charge;
    typedef Unit< 2, -3, 1, 0, 0, 0 > Power;
    typedef Unit< 2, -3, 1, -1, 0, 0 > Voltage;
    typedef Unit< 2, -3, 1, -2, 0, 0 > Resistance;
    typedef Unit< -2, 3, -1, 2, 0, 0 > Conductance;
    typedef Unit< -2, 4, -1, 2, 0, 0 > Capacitance;

    //! \typedef Unitless
    typedef Unit< 0, 0, 0, 0, 0, 0 > Unitless;

    // Unit constants
    // Unit constants
    const NT tera                            = 1e12;
    const NT giga                            = 1e9;
    const NT mega                            = 1e6;
    const NT kilo                            = 1e3;
    const NT deci                            = 1e-1;
    const NT centi                           = 1e-2;
    const NT milli                           = 1e-3;
    const NT micro                           = 1e-6;
    const NT nano                            = 1e-9;
    const NT pico                            = 1e-12;
    const NT femto                           = 1e-15;
    const NT atto                            = 1e-18;
    const Distance micron                    = 1;
    const Distance m                         = mega * micron;
    const Distance km                        = kilo * m;
    const Distance cm                        = centi * m;
    const Distance mm                        = milli * m;
    const Distance in                        = 2.54 * cm;
    const Distance inch                      = in;
    const Distance inches                    = in;
    const Distance ft                        = 12 * in;
    const Distance foot                      = ft;
    const Distance feet                      = ft;
    const Area m2                            = 1 * m * m;
    const Area mm2                           = 1 * mm * mm;
    const Area cm2                           = 1 * cm * cm;
    const Area in2                           = 1 * in * in;
    const Area ft2                           = 1 * ft * ft;
    const Angle radian                       = 1;
    const Angle rad                          = 1;
    const Angle pi                           = M_PI * rad;
    const Angle deg                          = 1.74532925e-2 * rad;
    const Angle degree                       = deg;
    const Angle degrees                      = deg;
    const Angle rev                          = 2 * pi;
    const Mass kg                            = 1;
    const Mass g                             = milli * kg;
    const Mass mg                            = milli * g;
    const Mass lbm                           = 0.45359237 * kg;
    const Time s                             = 1;
    const Time ms                            = milli * s;
    const Time hr                            = 3600 * s;
    const Time hour                          = hr;
    const Time minute                        = 60 * s;
    const Force N                            = 1 * kg * m / (s * s);
    const Force lbf                          = 4.4482216 * N;
    const Force oz                           = lbf / 16;
    const Force ounce                        = oz;
    const Energy J                           = 1 * N * m;
    const Energy cal                         = 4.1868 * J;
    const Energy kcal                        = kilo * cal;
    const Energy eV                          = 1.6021765e-19 * J;
    const Energy keV                         = kilo * eV;
    const Energy MeV                         = mega * eV;
    const Energy GeV                         = giga * eV;
    const Energy btu                         = 1055.0559 * J;
    const Power W                            = 1 * J / s;
    const Current A                          = 1;
    const Current MA                         = mega * A;
    const Current kA                         = kilo * A;
    const Current mA                         = milli * A;
    const Current uA                         = micro * A;
    const Current nA                         = nano * A;
    const Current pA                         = pico * A;
    const Charge C                           = 1 * A * s;
    const Voltage V                          = 1 * J / C;
    const Resistance ohm                     = 1 * V / A;
    const Conductance S                      = 1 / ohm;
    const Capacitance F                      = 1 * C / V;
    const Capacitance pF                     = pico * F;
    const Capacitance nF                     = nano * F;
    const Capacitance uF                     = micro * F;
    const Capacitance mF                     = milli * F;
    const Pressure Pa                        = 1;
    const Pressure kPa                       = 1e3 * Pa;
    const Pressure bar                       = Pa * 100000;
    const Pressure millibar                  = 1e-3 * bar;
    const Pressure psi                       = lbf / (inch * inch);
    const Pressure atm                       = 101325 * Pa;
    const Temperature K                      = 1;
    const Temperature degC                   = 1 * K;
    const Temperature degF                   = 5 / 9 * K;
    const Acceleration AccelerationOfGravity = 9.80665 * m / (s * s);
    // const Density DensityOfWater = 1*g/cc;
    const Velocity SpeedOfLight = 2.9979246e8 * m / s;
    const Velocity SpeedOfSound = 331.46 * m / s;

    const Unitless none = 1;

    double cos(const Angle& lhs);
    double sin(const Angle& lhs);
#undef UNITS_FUNC
#define UNITS_FUNC false

}  // namespace ORNL

#endif  // UNIT_H
