
#include <QPair>
#include <QException>
#include <QString>
#include <QTemporaryFile>
#include <iostream>

#include "gcode/gcode_command.h"
#include "gcode/gcode_parser.h"
#include "gcode/tst_gcode_parser.h"
#include "exceptions/exceptions.h"
#include "utilities/enums.h"

#if 0 // this needs to get fixed before enabling

void TestGCodeParser::initTestCase()
{

    test = new GcodeParser();

}

void TestGCodeParser::cleanupTestCase()
{
    delete test;
}

void TestGCodeParser::init()
{

}

void TestGCodeParser::cleanup()
{


}


void TestGCodeParser::testParseLine()
{
    try
    {
        test->selectParser(GcodeSyntax::kCommon);
    }
    catch(ParserNotSetException& e)
    {
        qDebug() << e.what();
    }
    catch(IOException& e)
    {
        qDebug() << e.what();
    }
    const QString teststr = "G1 X1.0";
    GcodeCommand cmd;
    try
    {
        cmd = test->parseLine(teststr);
    }
    catch(ParserNotSetException& e)
    {
        qDebug() << e.what();
    }
    catch(IOException& e)
    {
        qDebug() << e.what();
    }
    const QPair<char, float> x_val = qMakePair('X', 1.0);
    QCOMPARE(cmd.getCommand(), 'G');
    QCOMPARE(cmd.getCommandID(), 1);
    QCOMPARE(cmd.getParameters().size(), 1);
    QVERIFY(*(cmd.getParameters().find(x_val.first)) == x_val.second);


}


void TestGCodeParser::testParseFile()
{
    const QString lines[4] = { "; GCODE Syntax: Blue Gantry\n"
                             , "G1 F6120.0000 Z12.1920 ; TRAVEL-Move Extruder off table\n"
                             , "G1 X12.0100 Y1.0000\n"
                             , "G1 X0.0000 Y605.2670 ; VOLUME-WALL_OUTER\n"
                             };

    QFile a("gcodeparser_file_reader_test.txt");
    a.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream b(&a);

    b << lines[0] << lines[1] << lines[2] << lines[3];
    b.flush(); a.close();

    QVector<GcodeCommand> correct_output;
    GcodeCommand cmd;
    cmd.setComment("GCODE Syntax: Blue Gantry");
    cmd.setLineNumber(1);
    correct_output.append(cmd);

    cmd.setComment("TRAVEL-Move Extruder off table");
    cmd.setLineNumber(2);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('F', 6120.0000);
    cmd.addParameter('Z', 12.1920);
    correct_output.append(cmd);

    cmd.clearComment();
    cmd.clearParameters();
    cmd.setLineNumber(3);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('X', 12.0100);
    cmd.addParameter('Y', 1.0000);
    correct_output.append(cmd);

    cmd.clearComment();
    cmd.clearParameters();
    cmd.setComment("VOLUME-WALL_OUTER");
    cmd.setLineNumber(4);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('X', 0.0000);
    cmd.addParameter('Y', 605.2670);
    correct_output.append(cmd);

    GcodeParser FileTest(a.fileName(), false);
    FileTest.selectParser(GcodeSyntax::kCommon);

    QVector<GcodeCommand> test_output;
    try
    {
        test_output = FileTest.parseFile();
    }
    catch(ParserNotSetException& e)
    {
        qDebug() << e.what();
    }
    catch(IOException& e)
    {
        qDebug() << e.what();
    }

    //FileTest.close();

    std::cerr << correct_output.size() << " " << test_output.size() << std::endl;
    QVERIFY2(correct_output.size() == test_output.size(), "Sizes differ on parsed file");
    for(unsigned int i = 0; i < correct_output.size(); i++)
    {
        std::cerr << correct_output[i].getLineNumber() << " "
                  << correct_output[i].getCommand()
                  << correct_output[i].getCommandID();
        for(auto j = correct_output[i].getParameters().begin(); j != correct_output[i].getParameters().end(); j++)
        {
            std::cerr << " " << j.key() << j.value();
        }
        std::cerr << " " << correct_output[i].getComment().toStdString() << std::endl;

        std::cerr << test_output[i].getLineNumber() << " "
                  << test_output[i].getCommand()
                  << test_output[i].getCommandID();

        for(auto j = correct_output[i].getParameters().begin(); j != correct_output[i].getParameters().end(); j++)
        {
            std::cerr << " " << j.key() << j.value();
        }
        std::cerr << " " << test_output[i].getComment().toStdString() << std::endl;
        std::cerr << " " << correct_output[i].getComment().toStdString() << std::endl;
        std::cerr << " " << test_output[i].getComment().toStdString() << std::endl;
        QVERIFY2(correct_output[i] == test_output[i], "GCodeCommands differ");
    }
    QFile c("gcodeparser_file_reader_test.txt");
    c.remove(); c.close();
}

void TestGCodeParser::testAutoSyntaxSelection()
{
    const QString lines[4] = { "; GCODE Syntax: Blue Gantry\n"
                             , "G1 F6120.0000 Z12.1920 ; TRAVEL-Move Extruder off table\n"
                             , "G1 X12.0100 Y1.0000\n"
                             , "G1 X0.0000 Y605.2670 ; VOLUME-WALL_OUTER\n"
                             };

    QFile a("gcodeparser_autosyntax_test_bluegantry.txt");
    a.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream b(&a);

    b << lines[0] << lines[1] << lines[2] << lines[3];
    b.flush(); a.close();

    QVector<GcodeCommand> correct_output;
    GcodeCommand cmd;
    cmd.setComment("GCODE Syntax: Blue Gantry");
    cmd.setLineNumber(1);
    correct_output.append(cmd);

    cmd.setComment("TRAVEL-Move Extruder off table");
    cmd.setLineNumber(2);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('F', 6120.0000);
    cmd.addParameter('Z', 12.1920);
    correct_output.append(cmd);

    cmd.clearComment();
    cmd.clearParameters();
    cmd.setLineNumber(3);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('X', 12.0100);
    cmd.addParameter('Y', 1.0000);
    correct_output.append(cmd);

    cmd.clearComment();
    cmd.clearParameters();
    cmd.setComment("VOLUME-WALL_OUTER");
    cmd.setLineNumber(4);
    cmd.setCommand('G');
    cmd.setCommandID(1.0);
    cmd.addParameter('X', 0.0000);
    cmd.addParameter('Y', 605.2670);
    correct_output.append(cmd);

//    GcodeParser FileTest(a.fileName(), true);

//  QVector<GcodeCommand> test_output = FileTest.parseFile();

    //FileTest.close();

//    QVERIFY2(correct_output.size() == test_output.size(), "Sizes differ on parsed file");
    for(unsigned int i = 0; i < correct_output.size(); i++)
    {
        /*std::cerr << correct_output[i].getLineNumber() << " "
                  << correct_output[i].getCommand()
                  << correct_output[i].getCommandID();
        for(auto j = correct_output[i].getParameters().begin(); j != correct_output[i].getParameters().end(); j++)
        {
            std::cerr << " " << j->first << j->second;
        }
        std::cerr << correct_output[i].getComment().toStdString() << std::endl;

        std::cerr << test_output[i].getLineNumber() << " "
                  << test_output[i].getCommand()
                  << test_output[i].getCommandID();
        for(auto j = test_output[i].getParameters().begin(); j != test_output[i].getParameters().end(); j++)
        {
            std::cerr << " " << j->first << j->second;
        }
        std::cerr << test_output[i].getComment().toStdString() << std::endl;
*/
//        QVERIFY2(correct_output[i] == test_output[i], "GCodeCommands differ");
    }
    QFile c("gcodeparser_autosyntax_test_bluegantry.txt");
    c.remove(); c.close();

}

void TestGCodeParser::testParserSelection()
{

    QVERIFY(true);
}

#endif
