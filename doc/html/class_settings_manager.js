var class_settings_manager =
[
    [ "addConfig", "class_settings_manager.html#ad83fb569727295f50de07da3c7031838", null ],
    [ "getCategoryNames", "class_settings_manager.html#ad04c1c11aa25382249066ead72cffbd5", null ],
    [ "getCategorySettings", "class_settings_manager.html#a7fcf6a3e70bc1c14df0ae51cc9b00f07", null ],
    [ "getConfig", "class_settings_manager.html#a76894782ca07d983e9847158d8e0dca7", null ],
    [ "getConfigsNames", "class_settings_manager.html#a6460f5bae149946d2850e9a49a320ed1", null ],
    [ "removeConfig", "class_settings_manager.html#abc8205cf29ff30a094be4c9a274dd351", null ],
    [ "saveConfig", "class_settings_manager.html#ac0feac7241a2a5c7a3336c60f731e654", null ]
];