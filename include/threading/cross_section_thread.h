#ifndef SLICER_H
#define SLICER_H

#include <QSharedPointer>
#include <QThread>
#include <QVector>

#include "geometry/mesh_face.h"
#include "geometry/mesh_vertex.h"
#include "geometry/point.h"
#include "threading/cross_section/cross_section.h"
#include "threading/cross_section/cross_section_segment.h"

namespace ORNL
{
    class CrossSection;
    class CrossSectionSegment;
    class WindowManager;
    class Mesh;
    class MeshVertex;
    class MeshFace;

    /*!
     * \class CrossSectionThread
     * \brief Thread for computing the layer cross sections of a mesh
     */
    class CrossSectionThread : public QThread
    {
        Q_OBJECT
    public:
        //! \brief Constructor
        CrossSectionThread(QSharedPointer< Mesh > mesh);

    protected:
        /*!
         * \brief Main function that computes the cross sections
         */
        void run() override;

        /*!
         * \brief Get the index of the face connected to the face with index \p
         * notFaceIdx, via vertices \p idx0 and \p idx1.
         *
         * In case multiple faces connect with the same edge, return the next
         * counter-clockwise face when viewing from \p idx1 to \p idx0.
         *
         * \param index0 The index of one of two vertices that creates one of
         * the edges of the desired face \param index1 The index of the other of
         * the two vertices that creates one of the edges of the desired face
         * \param notFaceIndex The other face that contains the edge created by
         * vertices corresponded to by indices \p index0 and \p index1 \param
         * notFaceVertexIndex \returns The index of the desired face
         */
        int getFaceIndexWithPoints(int index0,
                                   int index1,
                                   int notFaceIndex,
                                   int notFaceVertexIndex) const;

        /*!
         * \brief Normal horizontal cross sectioning
         */
        void horizontalCrossSection();

    signals:
        /*!
         * \brief Signal that an error has occurred
         *
         * \param message The error message to send
         */
        void error(QString message);

    private:
        QSharedPointer< Mesh > m_mesh;
        QVector< CrossSection > m_layers;
        QVector< MeshVertex > m_vertices;
        QVector< uint > m_vertex_indices;
        QVector< MeshFace > m_faces;

        QSharedPointer< WindowManager > m_window_manager;

        /*!
         * \brief Linear interpolation
         *
         * Get the Y of a point with X \p x in the line through (\p x0, \p y0)
         * and (\p x1, \p y1)
         *
         * \param x The x component of the point to be interpolated from the two
         * specified points \param x0 The x component of the first point \param
         * x1 The x component of the second point \param y0 The y component of
         * the first point \param y0 The y component of the second point
         * \returns The y component of the interpolated point
         */
        double interpolate(double x,
                           double x0,
                           double x1,
                           double y0,
                           double y1);

        /*!
         * \brief Computes the intersection of a triangle represented by points
         * \p p0, \p p1, \p p2 and a horizontal plane at height \p z \param p0
         * The first point of the triangle to intersect with the horizontal
         * plane \param p1 The second point of the triangle to intersect with
         * the horizontal plane \param p2 The third point of the triangle to
         * intersect with the horizontal plane \param z The height of the
         * horizontal plane for intersection \returns The intersection segment
         */
        CrossSectionSegment project2D(const Point& p0,
                                      const Point& p1,
                                      const Point& p2,
                                      Distance z);
    };  // class Slicer
}  // namespace ORNL
#endif  // SLICER_H
